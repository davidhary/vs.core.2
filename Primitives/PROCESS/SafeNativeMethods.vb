﻿Imports System.Runtime.InteropServices
Friend Class SafeNativeMethods

    Private Sub New()
        MyBase.New()
    End Sub

#If False Then
    <DllImport("kernel32", SetLastError:=True, CharSet:=CharSet.Ansi)> _
    Public Shared Function LoadLibrary(<MarshalAs(UnmanagedType.LPStr)> ByVal lpFileName As String) As IntPtr
    End Function
#End If

#If False Then
    <DllImport("user32")> _
    Public Shared Function RegisterWindowMessage(ByVal message As String) As Integer
    End Function
    Public Shared Function RegisterWindowMessage(ByVal format As String, ByVal ParamArray args() As Object) As Integer
        Dim message As String = String.Format(format, args)
        Return RegisterWindowMessage(message)
    End Function
#End If

    <DllImport("user32.dll", CharSet:=CharSet.Auto, ThrowOnUnmappableChar:=True)>
    Private Shared Function RegisterWindowMessage(<MarshalAs(UnmanagedType.LPWStr)> ByVal lpString As String) As UInteger
    End Function

    Public Shared Function RegisterWindowMessage(ByVal format As String, ByVal ParamArray args() As Object) As UInteger
        Dim message As String = String.Format(format, args)
        Return RegisterWindowMessage(message)
    End Function

    Public Const HWND_BROADCAST As Integer = &HFFFF
    Public Const SW_SHOWNORMAL As Integer = 1

    <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
    Public Shared Function PostMessage(ByVal hWnd As IntPtr, ByVal msg As UInteger,
                                       ByVal wParam As IntPtr, ByVal lParam As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

#If False Then
    <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto)> _
    Public Shared Function PostMessage(ByVal hWnd As IntPtr, ByVal msg As UInteger, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As Boolean
    End Function

    <DllImport("user32")> _
    Public Shared Function PostMessage(ByVal hwnd As IntPtr, ByVal msg As Integer, ByVal wparam As IntPtr, ByVal lparam As IntPtr) As Boolean
    End Function

    <DllImportAttribute("user32.dll")> _
    Public Shared Function SetForegroundWindow(ByVal hWnd As IntPtr) As Boolean
    End Function
#End If

    <DllImport("user32.dll")>
    Public Shared Function SetForegroundWindow(ByVal hWnd As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

End Class

