﻿Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Windows.Forms
''' <summary> Single process instance. Forces an application to start only once. </summary>
''' <license> (c) 2015 SX2008.<para>
''' Licensed under The Code Project Open License http://www.codeproject.com/info/cpol10.aspx. </para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="4/20/2015" by="SC2008"> 
''' http://www.codeproject.com/Tips/884429/A-Csharp-single-application-instance-class. </history>
''' <example> To use this class, the main function of the application are changed as follows:
'''          <code>
''' static void Main(string[] args)
''' {
'''     SingleProcessInstance.RunMain(args, (_args) =&gt;
'''     {
'''         Application.EnableVisualStyles();
'''         Application.SetCompatibleTextRenderingDefault(false);
'''         Application.Run(new Form1());
'''     });
''' }
''' </code><code>
''' Shared Sub Main(ByVal args() As String)
'''     SingleProcessInstance.RunMain(args, Sub(_args)
'''         Application.EnableVisualStyles()
'''         Application.SetCompatibleTextRenderingDefault(False)
'''         Application.Run(New Form1())
'''     End Sub)
''' End Sub</code></example>
Public NotInheritable Class SingleProcessInstance

    Private Sub New()
    End Sub


    Friend Shared WM_SHOWFIRSTINSTANCE As UInteger

    ''' <summary> Application unique identifier. </summary>
    ''' <returns> A GUID. </returns>
    Public Shared Function ApplicationGuid() As Guid
        Dim [assembly] As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly()
        Dim attribute As GuidAttribute = CType([assembly].GetCustomAttributes(GetType(GuidAttribute), True)(0), GuidAttribute)
        Dim id As String = attribute.Value
        Return New Guid(id)
    End Function


    ''' <summary> Shows the first instance. </summary>
    Private Shared Sub ShowFirstInstance()
        SafeNativeMethods.PostMessage(New IntPtr(SafeNativeMethods.HWND_BROADCAST), WM_SHOWFIRSTINSTANCE, IntPtr.Zero, IntPtr.Zero)
    End Sub

    ''' <summary> Bring main form to front. </summary>
    Public Shared Sub BringMainFormToFront()
        ' get Main Form
        Dim mainform As System.Windows.Forms.Form = Application.OpenForms(0)

        If mainform.WindowState = FormWindowState.Minimized Then
            mainform.WindowState = FormWindowState.Normal
        End If

        SafeNativeMethods.SetForegroundWindow(mainform.Handle)
    End Sub

    ''' <summary> Executes the main operation. </summary>
    ''' <param name="args"> The arguments. </param>
    ''' <param name="main"> The main. </param>
    ''' <example> To use this class, the main function of the application are changed as follows:
    ''' <code>
    ''' static void Main(string[] args)
    ''' {
    '''     SingleProcessInstance.RunMain(args, (_args) =&gt;
    '''     {
    '''         Application.EnableVisualStyles();
    '''         Application.SetCompatibleTextRenderingDefault(false);
    '''         Application.Run(new Form1());
    '''     });
    ''' }
    ''' </code><code>
    ''' Shared Sub Main(ByVal args() As String)
    '''     SingleProcessInstance.RunMain(args, Sub(_args)
    '''         Application.EnableVisualStyles()
    '''         Application.SetCompatibleTextRenderingDefault(False)
    '''         Application.Run(New Form1())
    '''     End Sub)
    ''' End Sub</code></example>
    Public Shared Sub RunMain(ByVal args() As String, ByVal main As Action(Of String()))
        SingleProcessInstance.RunMain(args, main, AddressOf SingleProcessInstance.BringMainFormToFront)
    End Sub

    ''' <summary> Executes the main operation. </summary>
    ''' <param name="args"> The arguments. </param>
    ''' <param name="main"> The main. </param>
    ''' <example>
    ''' Optional you can define what should happen if the application was started a second time:
    ''' <code>
    ''' static void Main(string[] args)
    ''' {
    '''    SingleProcessInstance.RunMain(args, (_args) =>
    '''    {
    '''        Application.EnableVisualStyles();
    '''        Application.SetCompatibleTextRenderingDefault(false);
    '''        Application.Run(new Form1());
    '''    }, () =>
    '''    {
    '''        SingleProcessInstance.BringMainFormToFront();
    '''        MessageBox.Show("this application can only be runned in a single instance");
    '''    });
    ''' }
    '''       </code>
    ''' <code>
    ''' Shared Sub Main(ByVal args() As String)
    '''	    SingleProcessInstance.RunMain(args, Sub(_args)
    '''		        Application.EnableVisualStyles()
    '''	            Application.SetCompatibleTextRenderingDefault(False)
    '''		        Application.Run(New Form1())
    '''	        End Sub, Sub()
    '''		        SingleProcessInstance.BringMainFormToFront()
    '''		        MessageBox.Show("this application can only run in a single instance")
    '''         End Sub)
    ''' End Sub</code>
    '''          </example>
    Public Shared Sub RunMain(ByVal args() As String, ByVal main As Action(Of String()), ByVal onOtherInstance As Action)

        Dim appguid As String = SingleProcessInstance.ApplicationGuid().ToString().Replace("-", "")

        Using mutex As New Mutex(False, "Global\" & appguid)
            WM_SHOWFIRSTINSTANCE = SafeNativeMethods.RegisterWindowMessage("WM_SHOWFIRSTINSTANCE|{0}", appguid)

            If Not mutex.WaitOne(0, False) Then
                SingleProcessInstance.ShowFirstInstance()
                Return
            End If

            If onOtherInstance Is Nothing Then
                onOtherInstance = AddressOf SingleProcessInstance.BringMainFormToFront
            End If
            Application.AddMessageFilter(New MFilter(onOtherInstance))

            GC.Collect()
            If main IsNot Nothing Then
                main(args)
            End If
            GC.KeepAlive(mutex)
        End Using
    End Sub

End Class

Friend Class MFilter
    Implements IMessageFilter

    Private _action As Action

    Friend Sub New(ByVal action As Action)
        _action = action
    End Sub

    Public Function PreFilterMessage(ByRef m As Message) As Boolean Implements IMessageFilter.PreFilterMessage
        If m.Msg = SingleProcessInstance.WM_SHOWFIRSTINSTANCE AndAlso _action IsNot Nothing Then
            _action()
        End If
        Return False
    End Function
End Class
