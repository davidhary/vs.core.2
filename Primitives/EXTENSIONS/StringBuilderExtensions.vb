﻿Imports System.Runtime.CompilerServices
Namespace StringBuilderExtensions
    ''' <summary> Includes extensions for <see cref="T:System.Text.StringBuilder">string builder</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created </history>
    Public Module Methods

        ''' <summary> Appends the text. A new line is added before the appended text if the
        ''' <see cref="T:System.Text.StringBuilder">string builder</see> is not empty. </summary>
        ''' <remarks> When using <see cref="T:System.Text.StringBuilder.AppendLine">Append Line</see>, the
        ''' <see cref="T:System.Text.StringBuilder">String Builder</see> the text followed by a new line.
        ''' This procedure appends a new line before appending the text. </remarks>
        ''' <param name="value">    The value. </param>
        ''' <param name="lineData"> New line text. </param>
        <Extension()>
        Public Sub AppendLine(ByVal value As System.Text.StringBuilder, ByVal lineData As String)
            If value Is Nothing Then Return
            If Not String.IsNullOrWhiteSpace(lineData) Then
                If value.Length > 0 Then
                    value.AppendLine()
                End If
                value.Append(lineData)
            End If
        End Sub

        ''' <summary> Appends the formatted text. A new line is added before the appended text if the
        ''' <see cref="T:System.Text.StringBuilder">string builder</see> is not empty. </summary>
        ''' <remarks> When using <see cref="T:System.Text.StringBuilder.AppendLine">Append Line</see>, the
        ''' <see cref="T:System.Text.StringBuilder">String Builder</see> the text followed by a new line.
        ''' This procedure appends a new line before appending the text. </remarks>
        ''' <param name="value">  The value. </param>
        ''' <param name="format"> New line format. </param>
        ''' <param name="args">   Arguments for the line format. </param>
        <Extension()>
        Public Sub AppendLine(ByVal value As System.Text.StringBuilder, ByVal format As String, ByVal ParamArray args() As Object)
            If value Is Nothing Then Return
            If Not String.IsNullOrWhiteSpace(format) Then
                If value.Length > 0 Then
                    value.AppendLine()
                End If
                value.AppendFormat(Globalization.CultureInfo.CurrentCulture, format, args)
            End If
        End Sub

        ''' <summary> Returns an array of strings split by the new line characters. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> An array that represents the data in this object. </returns>
        <Extension()>
        Public Function ToArray(ByVal value As System.Text.StringBuilder) As IEnumerable(Of String)
            If value Is Nothing Then Return New String() {}
            Return value.ToString.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
        End Function

        ''' <summary> Adds text to string builder starting with a new line. </summary>
        ''' <param name="builder"> The builder. </param>
        ''' <param name="value">   The value. </param>
        <Extension()>
        Public Sub AddWord(ByVal builder As System.Text.StringBuilder, ByVal value As String)
            StringBuilderExtensions.AddWord(builder, value, ",")
        End Sub

        ''' <summary> Adds text to string builder adding a delimiter if the string builder is not empty. </summary>
        ''' <param name="builder">   The builder. </param>
        ''' <param name="value">     The value. </param>
        ''' <param name="delimiter"> The delimiter. </param>
        <Extension()>
        Public Sub AddWord(ByVal builder As System.Text.StringBuilder, ByVal value As String, ByVal delimiter As String)
            If Not String.IsNullOrWhiteSpace(value) Then
                If builder Is Nothing Then
                    builder = New System.Text.StringBuilder
                End If
                If builder.Length > 0 Then
                    builder.Append(delimiter)
                End If
                builder.Append(value)
            End If
        End Sub

    End Module
End Namespace
