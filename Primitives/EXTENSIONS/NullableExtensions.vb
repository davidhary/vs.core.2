﻿Imports System.Runtime.CompilerServices
Namespace NullableExtensions

    ''' <summary> Includes extensions for Nullable structures. </summary>
    ''' <license> (c) 2014 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/22/2014" by="David" revision="2.1.5166.x"> Created </history>
    Public Module Methods

#Region " NULLABLE OF T "

        ''' <summary> Returns true if the values are both nothing or have equal values. </summary>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Equates(Of T As Structure)(ByVal value As Nullable(Of T), ByVal other As Nullable(Of T)) As Boolean
            If value Is Nothing Then
                Return other Is Nothing
            ElseIf other Is Nothing Then
                Return False
            Else
                Return Not value.Equals(other)
            End If
        End Function

        ''' <summary> Returns true the values are not equal or one but not the other is nothing. </summary>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Differs(Of T As Structure)(ByVal value As Nullable(Of T), ByVal other As Nullable(Of T)) As Boolean
            If value Is Nothing Then
                Return other IsNot Nothing
            ElseIf other Is Nothing Then
                Return True
            Else
                Return Not value.Equals(other)
            End If
        End Function

#End Region

#Region " NULLABLE "

        ''' <summary> Returns true if the values are both nothing or have equal values. </summary>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Equates(ByVal value As Nullable, ByVal other As Nullable) As Boolean
            If value Is Nothing Then
                Return other Is Nothing
            ElseIf other Is Nothing Then
                Return False
            Else
                Return Not value.Equals(other)
            End If
        End Function

        ''' <summary> Returns true the values are not equal or one but not the other is nothing. </summary>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Differs(ByVal value As Nullable, ByVal other As Nullable) As Boolean
            If value Is Nothing Then
                Return other IsNot Nothing
            ElseIf other Is Nothing Then
                Return True
            Else
                Return Not value.Equals(other)
            End If
        End Function

#End Region

    End Module

End Namespace
