﻿Imports System.Runtime.CompilerServices
Namespace FileVersionInfoExtensions
    ''' <summary> Includes extensions for <see cref="Diagnostics.FileVersionInfo">File Version Information</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created </history>
    Public Module Methods

        ''' <summary> The reference date. </summary>
        Private Const referenceDate As String = "1/1/2000"

        ''' <summary> The build number of today. </summary>
        ''' <returns> The build number for today. </returns>
        Public Function BuildNumber() As Integer
            Return CInt(Math.Floor(Date.Now.Subtract(Methods.BuildNumberReferenceDate).TotalDays))
        End Function

        ''' <summary> Returns the reference date for the build number. </summary>
        ''' <returns> A Date. </returns>
        Public Function BuildNumberReferenceDate() As Date
            Return Date.Parse(referenceDate, System.Globalization.CultureInfo.CurrentCulture)
        End Function

        ''' <summary> Returns the date corresponding to the
        ''' <see cref="FileVersionInfo.ProductBuildPart">build number</see>
        ''' which is the day since the <see cref="referenceDate">build number reference date</see> of
        ''' January 1, 2000. </summary>
        ''' <param name="value"> Specifies the version information. </param>
        ''' <returns> A Date. </returns>
        <Extension()>
        Public Function BuildDate(ByVal value As FileVersionInfo) As Date
            If value Is Nothing Then Return BuildNumberReferenceDate()
            Return BuildNumberReferenceDate.Add(TimeSpan.FromDays(value.ProductBuildPart))
        End Function

        ''' <summary> Returns a standard caption for forms. </summary>
        ''' <remarks> Use this function to set the captions for forms. </remarks>
        ''' <param name="value"> Specifies the version information. </param>
        ''' <returns> A <see cref="T:System.String">String</see> data type in the form ProductName
        ''' WW.XX.YYY.ZZZZß or ProductName WW.XX.YYY.ZZZZa. </returns>
        <Extension()>
        Public Function Caption(ByVal value As FileVersionInfo) As String

            If value Is Nothing Then Return ""
            ' set the caption using the product name
            Dim builder As New System.Text.StringBuilder
            builder.Append(value.FileName)
            builder.Append(" ")
            builder.Append(value.ProductVersion)
            If value.ProductMajorPart < 1 Then
                builder.Append(".")
                Select Case value.ProductMinorPart
                    Case 0
                        builder.Append(Convert.ToChar(&H3B1)) ' alpha Ascii 224
                    Case 1
                        builder.Append(Convert.ToChar(&H3B2)) ' beta Ascii 225
                    Case 2 To 8
                        builder.Append(String.Format(System.Globalization.CultureInfo.CurrentCulture, "RC{0}", value.ProductMajorPart - 1))
                    Case Else
                        builder.Append("Gold")
                End Select
            End If
            Return builder.ToString

        End Function

        ''' <summary> Compares the two versions returning 0 if they are equal, 1 if left version is greater
        ''' (newer)
        ''' than the right version or -1 if the left version is smaller (older) than the newer one. </summary>
        ''' <param name="leftVersion">  Left hand side version. </param>
        ''' <param name="rightVersion"> The right version. </param>
        ''' <returns> 0 if equals, 1 if <paramref name=" leftVersion">left version</paramref> 
        '''           is greater than <paramref name="rightVersion">right version</paramref>;
        '''           -1 otherwise. </returns>
        <Extension()>
        Public Function CompareVersions(ByVal leftVersion As FileVersionInfo, ByVal rightVersion As FileVersionInfo) As Integer
            Return CompareVersions(leftVersion, rightVersion, 4)
        End Function

        ''' <summary> Compares the two versions returning 0 if they are equal, 1 if left version is greater
        ''' (newer)
        ''' than the right version or -1 if the left version is smaller (older) than the newer one. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="leftVersion">          Left hand side version. </param>
        ''' <param name="rightVersion">         The right version. </param>
        ''' <param name="comparedElementCount"> The compared element count. Use 1 to compare major, 2 to
        ''' include minor, 3 to include build and 4 to include revision. 1 is the minimum allowed. </param>
        ''' <returns> 0 if equals, 1 if <paramref name=" leftVersion">left version</paramref> 
        '''           is greater than <paramref name="rightVersion">right version</paramref>;
        '''           -1 otherwise. </returns>
        <Extension()>
        Public Function CompareVersions(ByVal leftVersion As FileVersionInfo, ByVal rightVersion As FileVersionInfo, ByVal comparedElementCount As Integer) As Integer
            If leftVersion Is Nothing Then
                Throw New ArgumentNullException(NameOf(leftVersion))
            End If
            If rightVersion Is Nothing Then
                Throw New ArgumentNullException(NameOf(rightVersion))
            End If
            Dim result As Integer = leftVersion.ProductMajorPart.CompareTo(rightVersion.ProductMajorPart)
            If result = 0 AndAlso comparedElementCount > 1 Then
                result = leftVersion.ProductMinorPart.CompareTo(rightVersion.ProductMinorPart)
                If result = 0 AndAlso comparedElementCount > 2 Then
                    result = leftVersion.ProductBuildPart.CompareTo(rightVersion.ProductBuildPart)
                    If result = 0 AndAlso comparedElementCount > 3 Then
                        result = leftVersion.ProductPrivatePart.CompareTo(rightVersion.ProductPrivatePart)
                    End If
                End If
            End If
            Return result
        End Function

        ''' <summary> Compares the two versions returning 0 if they are equal, 1 if left version is greater
        ''' (newer)
        ''' than the right version or -1 if the left version is smaller (older) than the newer one. </summary>
        ''' <param name="leftVersion">  Left hand side version. </param>
        ''' <param name="rightVersion"> The right version. </param>
        ''' <returns> 0 if equals, 1 if <paramref name=" leftVersion">left version</paramref> 
        '''           is greater than <paramref name="rightVersion">right version</paramref>;
        '''           -1 otherwise. </returns>
        <Extension()>
        Public Function CompareVersions(ByVal leftVersion As FileVersionInfo, ByVal rightVersion As String) As Integer
            Return CompareVersions(leftVersion, rightVersion, 4)
        End Function

        ''' <summary> Compares the two versions returning 0 if they are equal, 1 if left version is greater
        ''' (newer)
        ''' than the right version or -1 if the left version is smaller (older) than the newer one. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        ''' illegal values. </exception>
        ''' <param name="leftVersion">          Left hand side version. </param>
        ''' <param name="rightVersion">         The right version. </param>
        ''' <param name="comparedElementCount"> The compared element count. Use 1 to compare major, 2 to
        ''' include minor, 3 to include build and 4 to include revision. 1 is the minimum allowed. </param>
        ''' <returns> 0 if equals, 1 if <paramref name=" leftVersion">left version</paramref> 
        '''           is greater than <paramref name="rightVersion">right version</paramref>;
        '''           -1 otherwise. </returns>
        <Extension()>
        Public Function CompareVersions(ByVal leftVersion As FileVersionInfo, ByVal rightVersion As String, ByVal comparedElementCount As Integer) As Integer
            If leftVersion Is Nothing Then
                Throw New ArgumentNullException(NameOf(leftVersion))
            End If
            If String.IsNullOrWhiteSpace(rightVersion) Then
                Throw New ArgumentNullException(NameOf(rightVersion))
            End If
            If Not rightVersion.HasValidVersionInfo Then
                Throw New ArgumentException("Invalid version information", "rightVersion")
            End If
            Dim elements() As String = rightVersion.Split("."c)
            Dim result As Integer = leftVersion.ProductMajorPart.CompareTo(CInt(elements(0)))
            If result = 0 AndAlso comparedElementCount > 1 Then
                result = leftVersion.ProductMinorPart.CompareTo(CInt(elements(1)))
                If result = 0 AndAlso comparedElementCount > 2 Then
                    result = leftVersion.ProductBuildPart.CompareTo(CInt(elements(2)))
                    If result = 0 AndAlso comparedElementCount > 3 Then
                        result = leftVersion.ProductPrivatePart.CompareTo(CInt(elements(3)))
                    End If
                End If
            End If
            Return result
        End Function

        ''' <summary> Determines if the given value has valid version info. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> <c>True</c> if [has valid version info] [the specified value]; otherwise,
        ''' <c>False</c>. </returns>
        <Extension()>
        Public Function HasValidVersionInfo(ByVal value As String) As Boolean
            Dim affirmative As Boolean = True
            If String.IsNullOrWhiteSpace(value) Then
                affirmative = False
            Else
                Dim elements() As String = value.Split("."c)
                If elements.Length < 4 Then
                    affirmative = False
                Else
                    For Each e As String In elements
                        Dim versionValue As Integer = 0
                        If Not Integer.TryParse(e, versionValue) Then
                            affirmative = False
                            Exit For
                        End If
                    Next
                End If
            End If
            Return affirmative
        End Function

    End Module
End Namespace
