﻿Imports System.Configuration
Imports System.Runtime.CompilerServices
Namespace ConfigurationExtensions
    ''' <summary> Configuration Extensions </summary>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="03/30/2013" by="David" revision="1.2.4838.x"> created. </history>
    <Extension()>
    Public Module Methods

        ''' <summary> Reads the application setting. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="configuration"> The configuration. </param>
        ''' <param name="key">           The key. </param>
        ''' <returns> Windows.Forms.FormWindowState. </returns>
        <Extension()>
        Public Function ReadApplicationSetting(ByVal configuration As System.Configuration.Configuration, ByVal key As String) As String
            If configuration Is Nothing Then
                Throw New ArgumentNullException(NameOf(configuration))
            End If
            If String.IsNullOrWhiteSpace(key) Then
                Throw New ArgumentNullException(NameOf(key))
            End If
            If configuration.AppSettings.Settings Is Nothing OrElse configuration.AppSettings.Settings(key) Is Nothing Then
                Return ""
            Else
                Return configuration.AppSettings.Settings(key).Value
            End If
        End Function

        ''' <summary> Reads the application setting. </summary>
        ''' <param name="configuration"> The configuration. </param>
        ''' <param name="key">           The key. </param>
        ''' <param name="defaultValue">  The default value. </param>
        ''' <returns> Windows.Forms.FormWindowState. </returns>
        <Extension()>
        Public Function ReadApplicationSetting(ByVal configuration As System.Configuration.Configuration, ByVal key As String,
                                           ByVal defaultValue As System.Windows.Forms.FormWindowState) As System.Windows.Forms.FormWindowState
            Dim loadedWindowsState As System.Windows.Forms.FormWindowState = defaultValue
            If [Enum].TryParse(Of System.Windows.Forms.FormWindowState)(ConfigurationExtensions.ReadApplicationSetting(configuration, key), loadedWindowsState) Then
            End If
            Return loadedWindowsState
        End Function

        ''' <summary> Reads the application setting. </summary>
        ''' <param name="configuration"> The configuration. </param>
        ''' <param name="key">           The key. </param>
        ''' <param name="defaultValue">  The default value. </param>
        ''' <returns> System.Drawing.Point. </returns>
        <Extension()>
        Public Function ReadApplicationSetting(ByVal configuration As System.Configuration.Configuration, ByVal key As String,
                                           ByVal defaultValue As System.Drawing.Point) As System.Drawing.Point
            Dim p As System.Windows.Point = ConfigurationExtensions.ReadApplicationSetting(configuration, key, New System.Windows.Point(defaultValue.X, defaultValue.Y))
            Return New System.Drawing.Point(CInt(p.X), CInt(p.Y))
        End Function

        ''' <summary> Reads the application setting. </summary>
        ''' <param name="configuration"> The configuration. </param>
        ''' <param name="key">           The key. </param>
        ''' <param name="defaultValue">  The default value. </param>
        ''' <returns> System.Drawing.Point. </returns>
        <Extension()>
        Public Function ReadApplicationSetting(ByVal configuration As System.Configuration.Configuration, ByVal key As String,
                                           ByVal defaultValue As System.Windows.Point) As System.Windows.Point
            Dim loadedValue As String = ConfigurationExtensions.ReadApplicationSetting(configuration, key)
            If String.IsNullOrWhiteSpace(loadedValue) Then
                Return defaultValue
            Else
                Return System.Windows.Point.Parse(loadedValue)
            End If
        End Function

        ''' <summary> Reads the application setting. </summary>
        ''' <param name="configuration"> The configuration. </param>
        ''' <param name="key">           The key. </param>
        ''' <param name="defaultValue">  The default value. </param>
        ''' <returns> System.Drawing.Point. </returns>
        <Extension()>
        Public Function ReadApplicationSetting(ByVal configuration As System.Configuration.Configuration, ByVal key As String,
                                           ByVal defaultValue As System.Windows.Size) As System.Windows.Size
            Dim loadedValue As String = ConfigurationExtensions.ReadApplicationSetting(configuration, key)
            If String.IsNullOrWhiteSpace(loadedValue) Then
                Return defaultValue
            Else
                Return System.Windows.Size.Parse(loadedValue)
            End If
        End Function

        ''' <summary> Reads the application setting. </summary>
        ''' <param name="configuration"> The configuration. </param>
        ''' <param name="key">           The key. </param>
        ''' <param name="defaultValue">  The default value. </param>
        ''' <returns> System.Drawing.Point. </returns>
        <Extension()>
        Public Function ReadApplicationSetting(ByVal configuration As System.Configuration.Configuration, ByVal key As String,
                                           ByVal defaultValue As System.Drawing.Size) As System.Drawing.Size
            Dim s As System.Windows.Size = ConfigurationExtensions.ReadApplicationSetting(configuration, key, New System.Windows.Size(defaultValue.Width, defaultValue.Height))
            Return New System.Drawing.Size(CInt(s.Width), CInt(s.Height))
        End Function

        ''' <summary> Writes the application setting. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="configuration"> The configuration. </param>
        ''' <param name="key">           The key. </param>
        ''' <param name="value">         The value. </param>
        <Extension()>
        Public Sub WriteApplicationSetting(ByVal configuration As System.Configuration.Configuration, ByVal key As String, ByVal value As String)
            If configuration Is Nothing Then
                Throw New ArgumentNullException(NameOf(configuration))
            ElseIf String.IsNullOrWhiteSpace(key) Then
                Throw New ArgumentNullException(NameOf(key))
            ElseIf String.IsNullOrWhiteSpace(value) Then
                Throw New ArgumentNullException(NameOf(value))
            End If
            If configuration.AppSettings.Settings Is Nothing OrElse configuration.AppSettings.Settings(key) Is Nothing Then
                configuration.AppSettings.Settings.Add(key, value)
            Else
                configuration.AppSettings.Settings(key).Value = value.ToString
            End If
        End Sub

        ''' <summary> Writes the application setting. </summary>
        ''' <param name="configuration"> The configuration. </param>
        ''' <param name="key">           The key. </param>
        ''' <param name="value">         The value. </param>
        <Extension()>
        Public Sub WriteApplicationSetting(ByVal configuration As System.Configuration.Configuration, ByVal key As String,
                                       ByVal value As System.Windows.Forms.FormWindowState)
            ConfigurationExtensions.WriteApplicationSetting(configuration, key, value.ToString)
        End Sub

        ''' <summary> Writes the application setting. </summary>
        ''' <param name="configuration"> The configuration. </param>
        ''' <param name="key">           The key. </param>
        ''' <param name="value">         The value. </param>
        <Extension()>
        Public Sub WriteApplicationSetting(ByVal configuration As System.Configuration.Configuration, ByVal key As String,
                                       ByVal value As System.Drawing.Point)
            ConfigurationExtensions.WriteApplicationSetting(configuration, key, New System.Windows.Point(value.X, value.Y))
        End Sub

        ''' <summary> Writes the application setting. </summary>
        ''' <param name="configuration"> The configuration. </param>
        ''' <param name="key">           The key. </param>
        ''' <param name="value">         The value. </param>
        <Extension()>
        Public Sub WriteApplicationSetting(ByVal configuration As System.Configuration.Configuration, ByVal key As String,
                                       ByVal value As System.Windows.Point)
            ConfigurationExtensions.WriteApplicationSetting(configuration, key, value.ToString)
        End Sub

        ''' <summary> Writes the application setting. </summary>
        ''' <param name="configuration"> The configuration. </param>
        ''' <param name="key">           The key. </param>
        ''' <param name="value">         The value. </param>
        <Extension()>
        Public Sub WriteApplicationSetting(ByVal configuration As System.Configuration.Configuration, ByVal key As String,
                                       ByVal value As System.Windows.Size)
            ConfigurationExtensions.WriteApplicationSetting(configuration, key, value.ToString)
        End Sub

        ''' <summary> Writes the application setting. </summary>
        ''' <param name="configuration"> The configuration. </param>
        ''' <param name="key">           The key. </param>
        ''' <param name="value">         The value. </param>
        <Extension()>
        Public Sub WriteApplicationSetting(ByVal configuration As System.Configuration.Configuration, ByVal key As String,
                                       ByVal value As System.Drawing.Size)
            ConfigurationExtensions.WriteApplicationSetting(configuration, key, New System.Windows.Size(value.Width, value.Height).ToString)
        End Sub

    End Module

End Namespace
