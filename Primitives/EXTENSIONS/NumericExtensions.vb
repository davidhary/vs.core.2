﻿Imports System.Runtime.CompilerServices
Namespace NumericExtensions

    ''' <summary> Includes extensions for numeric objects. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created </history>
    Public Module Methods

#Region " BYTE "

        ''' <summary> Returns the maximum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> the other value. </param>
        ''' <returns> The maximum value. </returns>
        <Extension()>
        Public Function Max(ByVal value As Byte, ByVal other As Byte) As Byte
            If value >= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns the minimum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The minimum value. </returns>
        <Extension()>
        Public Function Min(ByVal value As Byte, ByVal other As Byte) As Byte
            If value <= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns an HEX string. </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        ''' <returns> An HEX string. </returns>
        <Extension()>
        Public Function ToHex(ByVal value As Byte, ByVal nibbleCount As Byte) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:X" & CStr(nibbleCount) & "}", value)
        End Function

        ''' <summary> Returns an HEX string caption with preceding "0x". </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        ''' <returns> An HEX string caption with preceding "0x". </returns>
        <Extension()>
        Public Function ToHexCaption(ByVal value As Byte, ByVal nibbleCount As Byte) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}" & CStr(nibbleCount), value)
        End Function

#End Region

#Region " DECIMAL "

        ''' <summary> Returns true if the two values are within the specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the two values are within the specified absolute precision. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) <= precision
        End Function

        ''' <summary> Returns true if the two values are within the specified relative precision. </summary>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        ''' the
        ''' <see cref="Hypotenuse"></see> </param>
        ''' <returns> <c>True</c> if the two values are within the specified relative precision. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Decimal, ByVal other As Decimal, ByVal significantDigits As Integer) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) < Epsilon(value, other, significantDigits)
        End Function

        ''' <summary> Returns the Hypotenuse of the values scaled by the specified significant digits. </summary>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        ''' the
        ''' <see cref="Hypotenuse"></see> </param>
        ''' <returns> The Hypotenuse of the values scaled by the specified significant digits. </returns>
        <Extension()>
        Public Function Epsilon(ByVal value As Decimal, ByVal other As Decimal, ByVal significantDigits As Integer) As Decimal
            Return CDec(Hypotenuse(value, other) / 10 ^ (significantDigits - 1))
        End Function

        ''' <summary> Returns true if the first value is less than or equal the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the first value is less than or equal the other within the specified absolute
        ''' precision. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function SmallerEquals(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value < other)
        End Function

        ''' <summary> Returns True if the first value is greater than or equal the other within the
        ''' specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the first value is greater than or equal the other within the specified
        ''' absolute precision. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterEquals(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value > other)
        End Function

        ''' <summary> Returns True if the values are unequal within the specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the values are unequal within the specified absolute precision. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Differs(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return Not NumericExtensions.Approximates(value, other, precision)
        End Function

        ''' <summary> Returns True if the first value is less than the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the first value is less than the other within the specified
        ''' absolute precision. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function SmallerDiffers(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return Not value.GreaterEquals(other, precision)
        End Function

        ''' <summary> Returns True if the first value is greater than the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the first value is greater than the other within the specified
        ''' absolute precision. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterDiffers(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return Not value.SmallerEquals(other, precision)
        End Function

#End Region

#Region " DECIMAL? "

        ''' <summary> Returns true if the absolute difference is less than or equal than
        '''           <paramref name="delta">delta</paramref>. </summary>
        ''' <param name="value">     The value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="delta">     The delta. </param>
        ''' <returns> <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Decimal?, ByVal other As Decimal?, ByVal delta As Decimal) As Boolean
            If value Is Nothing Then
                Return other Is Nothing
            ElseIf other Is Nothing Then
                Return False
            Else
                Return NumericExtensions.Approximates(value.Value, other.Value, delta)
            End If
        End Function


        ''' <summary> Returns true if the absolute difference is greater than
        ''' <paramref name="delta">delta</paramref>. </summary>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <param name="delta"> The delta. </param>
        ''' <returns> <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Differs(ByVal value As Decimal?, ByVal other As Decimal?, ByVal delta As Decimal) As Boolean
            If value Is Nothing Then
                Return other IsNot Nothing
            ElseIf other Is Nothing Then
                Return True
            Else
                Return value.Value.Differs(other.Value, delta)
            End If
        End Function

#End Region

#Region " DOUBLE "

        ''' <summary> Returns True if the two values are within the specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the two values are within the specified absolute precision. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) <= precision
        End Function

        ''' <summary> Returns true if the two values are within the specified relative precision. </summary>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        ''' the
        ''' <see cref="Hypotenuse"></see> </param>
        ''' <returns> Returns true if the two values are within the specified relative precision. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Double, ByVal other As Double, ByVal significantDigits As Integer) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) < Epsilon(value, other, significantDigits)
        End Function

        ''' <summary> Returns the Hypotenuse of the values scaled by the specified significant digits. </summary>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        ''' the
        ''' <see cref="Hypotenuse"></see> </param>
        ''' <returns> the Hypotenuse of the values scaled by the specified significant digits. </returns>
        <Extension()>
        Public Function Epsilon(ByVal value As Double, ByVal other As Double, ByVal significantDigits As Integer) As Double
            ' Return Hypotenuse(value, other) / If(significantDigits >= 15, 1.0E+16, 10 ^ (significantDigits - 1))

            Return Hypotenuse(value, other) / 10 ^ (significantDigits - 1)
        End Function

        ''' <summary> Returns true if the first value is less than or equal the other within the
        ''' specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        <Extension()>
        Public Function SmallerEquals(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value < other)
        End Function

        ''' <summary> Returns true if the first value is greater than or equal the other within the
        ''' specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterEquals(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value > other)
        End Function

        ''' <summary> Returns true if the values are unequal within the specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Differs(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return Not NumericExtensions.Approximates(value, other, precision)
        End Function

        ''' <summary> Returns true if the first value is less than the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        <System.Runtime.CompilerServices.Extension()>
        Public Function SmallerDiffers(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return Not value.GreaterEquals(other, precision)
        End Function

        ''' <summary> Returns true if the first value is greater than the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterDiffers(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return Not value.SmallerEquals(other, precision)
        End Function

        ''' <summary> Returns a value that is rounded up to the specified significant digit. </summary>
        ''' <param name="value">           value which to use in the calculation. </param>
        ''' <param name="relativeDecimal"> The decimal digit relative to the most significant digit to
        ''' use. </param>
        ''' <returns> A rounded up value to the specified relative digit. </returns>
        ''' <example> Unary.Ceiling(1.123456,2) returns 1.13.<p>
        ''' Unary.Ceiling(-1.123456,2) returns -1.12.</p></example>
        <Extension()>
        Public Function Ceiling(ByVal value As Double, ByVal relativeDecimal As Integer) As Double

            Dim decimalDigitValue As Double

            ' check if the value is zero
            If value = 0 Then

                ' if so, return This value
                Return value

            Else

                ' get the representation of the significant digit
                decimalDigitValue = DecimalValue(value, -relativeDecimal)

                ' get the new value
                Return decimalDigitValue * System.Math.Ceiling(value / decimalDigitValue)

            End If

        End Function

        ''' <summary> Returns the number of decimal places for displaying the auto value. </summary>
        ''' <param name="value"> A <see cref="T:System.Double">Double</see> value. </param>
        <Extension()>
        Public Function DecimalPlaces(ByVal value As Double) As Integer
            Dim candidate As Integer = 0
            value = Math.IEEERemainder(Math.Abs(value), 1)
            Dim minimum As Double = Math.Max(value / 1000.0, 0.0000001)
            Do While value > minimum
                candidate += 1
                value = Math.IEEERemainder(10 * value, 1)
            Loop
            Return candidate
        End Function

        ''' <summary> Returns the decimal value of a number. </summary>
        ''' <param name="value">            Value which to use in the calculation. </param>
        ''' <param name="decimalLeftShift"> Decimal power by which to shift the digit. </param>
        ''' <returns> The decimal value of the value shifted by the specified decimalPlaces. </returns>
        ''' <example> Unary.DecimalValue(1.123,1) returns 10.<p>
        ''' Unary.DecimalValue(11.23,1) returns 100.</p><p>
        ''' Unary.DecimalValue(-1.123,1) returns 10.</p><p>
        ''' Unary.DecimalValue(0.1234,1) returns 1.</p><p>
        ''' Unary.DecimalValue(11.12,0) returns 10.</p></example>
        <Extension()>
        Public Function DecimalValue(ByVal value As Double, ByVal decimalLeftShift As Integer) As Double

            ' check if the value is zero
            If value = 0 Then

                ' if so, return This value
                Return 0

            Else

                ' get the representation of the most significant digit
                Return Math.Pow(10, value.Exponent() + decimalLeftShift)

            End If

        End Function

        ''' <summary> Computes e<sup>x</sup>-1. </summary>
        ''' <remarks> If x is close to 0, then e<sup>x</sup> is close to 1, and computing e<sup>x</sup>-1
        ''' by subtracting one from e<sup>x</sup> as computed by the <see cref="Math.Exp" /> function
        ''' will be subject to severe loss of significance due to cancellation. This method maintains
        ''' full precision for all values of x by switching to a series expansion for values of x near
        ''' zero. </remarks>
        ''' <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
        ''' <param name="value">             The argument. </param>
        ''' <param name="maximumIterations"> The maximum number of iterations of a series for convergence. </param>
        ''' <returns> The value of e<sup>x</sup>-1. </returns>
        <Extension()>
        Public Function ExpMinusOne(ByVal value As Double, ByVal maximumIterations As Integer) As Double
            If Math.Abs(value) < 0.125 Then
                Dim dr As Double = value
                Dim r As Double = dr
                For k As Integer = 2 To maximumIterations - 1
                    Dim r_old As Double = r
                    dr *= value / k
                    r += dr
                    If r = r_old Then
                        Return (r)
                    End If
                Next k
                Throw New OperationFailedException()
            Else
                Return (Math.Exp(value) - 1.0)
            End If
        End Function

        ''' <summary> Returns the exponent. </summary>
        ''' <param name="value"> A <see cref="T:System.Double">Double</see> value. </param>
        ''' <returns> The most significant decade of the value. </returns>
        ''' <example> NumericExtensions.Exponent(1.12) returns 0.<p>
        ''' Unary.Decade(11.23) returns 1.</p><p>
        ''' NumericExtensions.Exponent(-1.123) returns 0.</p><p>
        ''' NumericExtensions.Exponent(0.1234) returns -1</p></example>
        ''' <seealso cref="Decimal"/>
        <Extension()>
        Public Function Exponent(ByVal value As Double) As Integer

            ' check if the value is zero
            If value = 0 Then

                ' if so, return This value
                Return 0

            Else

                ' get the exponent based on the most significant digit.
                '     Return Convert.ToInt32(System.Math.Floor(System.Math.Log(System.Math.Abs(value))) / System.Math.Log(10.0#))
                Return Convert.ToInt32(System.Math.Floor(System.Math.Abs(value).Log10))
            End If

        End Function

        ''' <summary> Returns the exponent. </summary>
        ''' <param name="value">               A <see cref="T:System.Double">Double</see> value. </param>
        ''' <param name="useEngineeringScale"> true to use engineering scale. </param>
        ''' <returns> The most significant decade of the value. </returns>
        ''' <example> NumericExtensions.Exponent(11.12) returns 0.<p>
        ''' Unary.Decade(111.23) returns 1.</p><p>
        ''' NumericExtensions.Exponent(1111.123) returns 3.</p><p>
        ''' NumericExtensions.Exponent(-11.12) returns 0.</p><p>
        ''' Unary.Decade(-111.23) returns 1.</p><p>
        ''' NumericExtensions.Exponent(-1111.123) returns 3.</p><p>
        ''' NumericExtensions.Exponent(0.1234) returns 0</p><p>
        ''' NumericExtensions.Exponent(0.01234) returns 0</p><p>
        ''' NumericExtensions.Exponent(0.0001234) returns -3</p><p>
        ''' NumericExtensions.Exponent(-0.1234) returns 0</p><p>
        ''' NumericExtensions.Exponent(-0.01234) returns 0</p><p>
        ''' NumericExtensions.Exponent(-0.001234) returns -3</p><p>
        ''' NumericExtensions.Exponent(-0.0001234) returns -3</p></example>
        ''' <seealso cref="DecimalValue"/>
        <Extension()>
        Public Function Exponent(ByVal value As Double, ByVal useEngineeringScale As Boolean) As Integer

            ' check if the value is zero
            If value = 0 Then

                ' if so, return This value
                Return 0

            Else

                If useEngineeringScale Then
                    ' Use a power of 10 that is a multiple of 3 (engineering scale)
                    Return 3 * (Exponent(value) \ 3)
                Else
                    Return Exponent(value)
                End If

            End If

        End Function

        ''' <summary> Returns a suggested fixed format based requested most significant decimalPlaces. </summary>
        ''' <param name="value">         Number to format. </param>
        ''' <param name="decimalPlaces"> The number of decimal places beyond the most significant digit. </param>
        ''' <returns> A fix format string. </returns>
        ''' <example> Unary.FixedFormat(1.123,1) returns 0.0.<p>
        ''' Unary.FixedFormat(11.23,1) returns 0.</p><p>
        ''' Unary.FixedFormat(-1.123,1) returns 0.0.</p><p>
        ''' Unary.FixedFormat(0.1234,1) returns 0.0.</p></example>
        <Extension()>
        Public Function FixedFormat(ByVal value As Double, ByVal decimalPlaces As Integer) As String

            decimalPlaces = decimalPlaces - value.Exponent()
            If decimalPlaces > 0 Then
                Return "0." & New String("0"c, decimalPlaces)
            Else
                Return "0"
            End If

        End Function

        ''' <summary> Returns a value that is rounded down to the specified significant digit. </summary>
        ''' <param name="value">           value which to use in the calculation. </param>
        ''' <param name="relativeDecimal"> The decimal digit relative to the most significant digit to
        ''' use. </param>
        ''' <returns> A rounded down value to the specified relative digit. </returns>
        ''' <example> Unary.Floor(1.123,1) returns 1.1.<p>
        ''' Unary.Floor(11.23,1) returns 11.</p><p>
        ''' Unary.Floor(-1.123,1) returns -1.3.</p><p>
        ''' Unary.Floor(0.1234,1) returns 0.12.</p><p>
        ''' Unary.Floor(11.12,0) returns 10.</p></example>
        <Extension()>
        Public Function Floor(ByVal value As Double, ByVal relativeDecimal As Integer) As Double

            Dim decimalDigitValue As Double

            ' check if the value is zero
            If value = 0 Then

                ' if so, return This value
                Return value

            Else

                ' get the representation of the significant digit
                decimalDigitValue = DecimalValue(value, -relativeDecimal)

                ' get the new value
                Return decimalDigitValue * System.Math.Floor(value / decimalDigitValue)

            End If

        End Function

        ''' <summary> Computes the length of a right triangle's hypotenuse. </summary>
        ''' <remarks> <para>The length is computed accurately, even in cases where x<sup>2</sup> or
        ''' y<sup>2</sup> would overflow.</para> </remarks>
        ''' <param name="value">      The length of one side. </param>
        ''' <param name="orthogonal"> The length of orthogonal side. </param>
        ''' <returns> The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>). </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal value As Double, ByVal orthogonal As Double) As Double
            If (value = 0.0) AndAlso (orthogonal = 0.0) Then
                Return (0.0)
            Else
                Dim ax As Double = Math.Abs(value)
                Dim ay As Double = Math.Abs(orthogonal)
                If ax > ay Then
                    Dim r As Double = orthogonal / value
                    Return (ax * Math.Sqrt(1.0 + r * r))
                Else
                    Dim r As Double = value / orthogonal
                    Return (ay * Math.Sqrt(1.0 + r * r))
                End If
            End If
        End Function

        ''' <summary> Interpolate over the unity range. </summary>
        ''' <param name="value">    The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="minValue"> The minimum range point. </param>
        ''' <param name="maxValue"> The maximum range point. </param>
        ''' <returns> Returns the interpolated output over the specified range relative to the value over
        ''' the unity [0,1) range. </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Double, ByVal minValue As Double, ByVal maxValue As Double) As Double
            Return value * (maxValue - minValue) + minValue
        End Function

        ''' <summary> Linearly interpolated value. </summary>
        ''' <param name="value">     The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="minValue">  The first abscissa value. </param>
        ''' <param name="maxValue">  The second abscissa value. </param>
        ''' <param name="minOutput"> The first ordinate value. </param>
        ''' <param name="maxOutput"> The second ordinate value. </param>
        ''' <returns> The abscissa value derived using linear interpolation based on a pair of coordinates. </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Double, ByVal minValue As Double, ByVal maxValue As Double, ByVal minOutput As Double, ByVal maxOutput As Double) As Double

            ' check if we have two distinct pairs
            If maxValue = minValue Then
                ' if we have identical value values, return the
                ' average of the ordinate value
                Return 0.5 * (minOutput + maxOutput)
            Else
                Return minOutput + (value - minValue) * (maxOutput - minOutput) / (maxValue - minValue)
            End If

        End Function

        ''' <summary> Linearly interpolated value. </summary>
        ''' <param name="value">  The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="point1"> The first point. </param>
        ''' <param name="point2"> The second point. </param>
        ''' <returns> The abscissa value derived using linear interpolation based on a pair of coordinates. </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Double, ByVal point1 As System.Drawing.PointF, ByVal point2 As System.Drawing.PointF) As Double

            ' check if we have two distinct pairs
            If point2.X = point2.X Then
                ' if we have identical X values, return the
                ' average of the ordinate value
                Return 0.5 * (point1.Y + point2.Y)
            Else
                Return point1.Y + (value - point1.X) * (point2.Y - point1.Y) / (point2.X - point1.X)
            End If

        End Function

        ''' <summary> Linearly interpolated value. </summary>
        ''' <param name="value">     The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="rectangle"> The rectangle of coordinates. </param>
        ''' <returns> The abscissa value derived using linear interpolation based on a pair of coordinates. </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Double, ByVal rectangle As System.Drawing.RectangleF) As Double

            ' check if we have two distinct pairs
            If rectangle.Width = 0 Then
                ' if we have identical X values, return the
                ' average of the ordinate value
                Return rectangle.Y + 0.5 * rectangle.Height
            Else
                Return rectangle.Y + (value - rectangle.X) * rectangle.Height / rectangle.Width
            End If

        End Function

        ''' <summary> Calculate a base 10 logarithm in a safe manner to avoid math exceptions. </summary>
        ''' <param name="value"> The value for which the logarithm is to be calculated. </param>
        ''' <returns> The value of the logarithm, or 0 if the <paramref name="value"/>
        ''' argument was negative or zero. </returns>
        <Extension()>
        Public Function Log10(ByVal value As Double) As Double
            Return Math.Log10(Math.Max(value, Double.Epsilon))
        End Function

        ''' <summary> Returns the maximum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The maximum value. </returns>
        <Extension()>
        Public Function Max(ByVal value As Double, ByVal other As Double) As Double
            If value >= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns the minimum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The minimum value. </returns>
        <Extension()>
        Public Function Min(ByVal value As Double, ByVal other As Double) As Double
            If value <= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Calculate the modulus (remainder) in a safe manner so that divide by zero errors are
        ''' avoided. </summary>
        ''' <param name="value">    The divisor. </param>
        ''' <param name="dividend"> The dividend. </param>
        ''' <returns> the value of the modulus, or zero for the divide-by-zero case. </returns>
        <Extension()>
        Public Function [Mod](ByVal value As Double, ByVal dividend As Double) As Double

            If dividend = 0 Then
                Return 0
            End If
            Dim temp As Double = value / dividend
            Return dividend * (temp - Math.Floor(temp))
        End Function

        ''' <summary> Raises an argument to an integer power. </summary>
        ''' <remarks> <para>Low integer powers can be computed by optimized algorithms much faster than the
        ''' general algorithm for an arbitrary real power employed by
        ''' <see cref="T:System.Math.Pow"/>.</para> </remarks>
        ''' <param name="value"> The argument. </param>
        ''' <param name="power"> The power. </param>
        ''' <returns> The value of x<sup>n</sup>. </returns>
        <Extension()>
        Public Function Pow(ByVal value As Double, ByVal power As Integer) As Double

            If power < 0 Then
                Return (1.0 / Pow(value, -power))
            End If

            Select Case power
                Case 0
                    ' we follow convention that 0^0 = 1
                    Return (1.0)
                Case 1
                    Return (value)
                Case 2
                    ' 1 multiply
                    Return (value * value)
                Case 3
                    ' 2 multiplies
                    Return (value * value * value)
                Case 4
                    ' 2 multiplies
                    Dim x2 As Double = value * value
                    Return (x2 * x2)
                Case 5
                    ' 3 multiplies
                    Dim x2 As Double = value * value
                    Return (x2 * x2 * value)
                Case 6
                    ' 3 multiplies
                    Dim x2 As Double = value * value
                    Return (x2 * x2 * x2)
                Case 7
                    ' 4 multiplies
                    Dim x3 As Double = value * value * value
                    Return (x3 * x3 * value)
                Case 8
                    ' 3 multiplies
                    Dim x2 As Double = value * value
                    Dim x4 As Double = x2 * x2
                    Return (x4 * x4)
                Case 9
                    ' 4 multiplies
                    Dim x3 As Double = value * value * value
                    Return (x3 * x3 * x3)
                Case 10
                    ' 4 multiplies
                    Dim x2 As Double = value * value
                    Dim x4 As Double = x2 * x2
                    Return (x4 * x4 * x2)
                Case Else
                    Return (Math.Pow(value, power))
            End Select

        End Function

        ''' <summary> An method for squaring. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> System.Double. </returns>
        <Extension()>
        Public Function Squared(ByVal value As Double) As Double
            Return (value * value)
        End Function

        ''' <summary> An method for cubing. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> System.Double. </returns>
        <Extension()>
        Public Function Cubed(ByVal value As Double) As Double
            Return (value * value * value)
        End Function

#End Region

#Region " DOUBLE? "

        ''' <summary> Returns true if the absolute difference is less than or equal than
        ''' <paramref name="delta">delta</paramref>. </summary>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <param name="delta"> The delta. </param>
        ''' <returns> <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Double?, ByVal other As Double?, ByVal delta As Double) As Boolean
            If value Is Nothing Then
                Return other Is Nothing
            ElseIf other Is Nothing Then
                Return False
            Else
                Return NumericExtensions.Approximates(value.Value, other.Value, delta)
            End If
        End Function

        ''' <summary> Returns true if the absolute difference is greater than
        ''' <paramref name="delta">delta</paramref>. </summary>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <param name="delta"> The delta. </param>
        ''' <returns> <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Differs(ByVal value As Double?, ByVal other As Double?, ByVal delta As Double) As Boolean
            If value Is Nothing Then
                Return other IsNot Nothing
            ElseIf other Is Nothing Then
                Return True
            Else
                Return value.Value.Differs(other.Value, delta)
            End If
        End Function

#End Region

#Region " INTEGER "

        ''' <summary> Returns the maximum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The maximum value. </returns>
        <Extension()>
        Public Function Max(ByVal value As Integer, ByVal other As Integer) As Integer
            If value >= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns the minimum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The minimum value. </returns>
        <Extension()>
        Public Function Min(ByVal value As Integer, ByVal other As Integer) As Integer
            If value <= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns an HEX string. </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        <Extension()>
        Public Function ToHex(ByVal value As Integer, ByVal nibbleCount As Integer) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:X" & CStr(nibbleCount) & "}", value)
        End Function

        ''' <summary> Returns an HEX string caption with preceding "0x". </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        <Extension()>
        Public Function ToHexCaption(ByVal value As Integer, ByVal nibbleCount As Integer) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X" & CStr(nibbleCount) & "}", value)
        End Function

        ''' <summary> Truncates a value to the desired precision. </summary>
        ''' <param name="value"> Extended value. </param>
        <Extension()>
        Public Function Truncate(ByVal value As Int32) As Int16

            value = value And UInt16.MaxValue
            If value > Int16.MaxValue Then
                value -= UInt16.MaxValue + 1 ' -65536
            End If
            Return Convert.ToInt16(value)

        End Function

#End Region

#Region " LONG "

        ''' <summary> Returns the maximum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The maximum value. </returns>
        <Extension()>
        Public Function Max(ByVal value As Long, ByVal other As Long) As Long
            If value >= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns the minimum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The minimum value. </returns>
        <Extension()>
        Public Function Min(ByVal value As Long, ByVal other As Long) As Long
            If value <= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns an HEX string. </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        <Extension()>
        Public Function ToHex(ByVal value As Long, ByVal nibbleCount As Long) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:X" & CStr(nibbleCount) & "}", value)
        End Function

        ''' <summary> Returns an HEX string caption with preceding "0x". </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        <Extension()>
        Public Function ToHexCaption(ByVal value As Long, ByVal nibbleCount As Long) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}" & CStr(nibbleCount), value)
        End Function

        ''' <summary> Truncates a value to the desired precision. </summary>
        ''' <param name="value"> Extended value. </param>
        <Extension()>
        Public Function Truncate(ByVal value As Int64) As Int32

            value = value And UInt32.MaxValue
            If value > Int32.MaxValue Then
                value -= UInt32.MaxValue + 1
            End If
            Return Convert.ToInt32(value)

        End Function


#End Region

#Region " SHORT "

        ''' <summary> Returns the maximum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The maximum value. </returns>
        <Extension()>
        Public Function Max(ByVal value As Short, ByVal other As Short) As Short
            If value >= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns the minimum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The minimum value. </returns>
        <Extension()>
        Public Function Min(ByVal value As Short, ByVal other As Short) As Short
            If value <= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns an HEX string. </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        <Extension()>
        Public Function ToHex(ByVal value As Short, ByVal nibbleCount As Short) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:X" & CStr(nibbleCount) & "}", value)
        End Function

        ''' <summary> Returns an HEX string caption with preceding "0x". </summary>
        ''' <param name="value">       Extended value. </param>
        ''' <param name="nibbleCount"> Number of nibbles. </param>
        ''' <returns> The caption. </returns>
        <Extension()>
        Public Function ToHexCaption(ByVal value As Short, ByVal nibbleCount As Short) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}" & CStr(nibbleCount), value)
        End Function

        ''' <summary> Truncates a value to the desired precision. </summary>
        ''' <param name="value"> Extended value. </param>
        <Extension()>
        Public Function Truncate(ByVal value As Int16) As Byte

            Dim maxValue As Short = 2S * Byte.MaxValue - 1S
            value = value And maxValue
            If value > Int16.MaxValue Then
                value -= maxValue + 1S ' -65536
            End If
            Return Convert.ToByte(value)

        End Function

#End Region

#Region " SINGLE "

        ''' <summary> Returns true if the values are unequal within the specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the values are different; <c>False</c> otherwise. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Differs(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return Not NumericExtensions.Approximates(value, other, precision)
        End Function

        ''' <summary> Returns true if the absolute difference is greater than
        ''' <paramref name="delta">delta</paramref>. </summary>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <param name="delta"> The delta. </param>
        ''' <returns> <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Differs(ByVal value As Single?, ByVal other As Single?, ByVal delta As Single) As Boolean
            If value Is Nothing Then
                Return other IsNot Nothing
            ElseIf other Is Nothing Then
                Return True
            Else
                Return value.Value.Differs(other.Value, delta)
            End If
        End Function

        ''' <summary> Returns true if the two values are within the specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the values are close; <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) <= precision
        End Function

        ''' <summary> Returns true if the two values are within the specified relative precision. </summary>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        ''' the <see cref="Hypotenuse"></see> </param>
        ''' <returns> <c>True</c> if the two values are within the specified relative precision;
        ''' <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Single, ByVal other As Single, ByVal significantDigits As Integer) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) < Epsilon(value, other, significantDigits)
        End Function

        ''' <summary> Returns the Hypotenuse of the values scaled by the specified significant digits. </summary>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        ''' the <see cref="Hypotenuse"></see> </param>
        ''' <returns> the Hypotenuse of the values scaled by the specified significant digits. </returns>
        <Extension()>
        Public Function Epsilon(ByVal value As Single, ByVal other As Single, ByVal significantDigits As Integer) As Single
            Return CSng(Hypotenuse(value, other) / 10 ^ (significantDigits - 1))
        End Function

        ''' <summary> Returns true if the first value is smaller or equal the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the first value is smaller or equal the other within the specified
        ''' absolute precision; <c>False</c> otherwise. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function SmallerEquals(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value < other)
        End Function

        ''' <summary> Returns true if the first value is greater than or equal the other within the
        ''' specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the first value is greater than or equal the other within the
        ''' specified absolute precision; <c>False</c> otherwise. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterEquals(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value > other)
        End Function

        ''' <summary> Returns true if the first value is less than the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        <System.Runtime.CompilerServices.Extension()>
        Public Function SmallerDiffers(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return Not value.GreaterEquals(other, precision)
        End Function

        ''' <summary> Returns true if the first value is greater than the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterDiffers(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return Not value.SmallerEquals(other, precision)
        End Function

        ''' <summary> Returns a value that is rounded up to the specified significant digit. </summary>
        ''' <param name="value">           value which to use in the calculation. </param>
        ''' <param name="relativeDecimal"> The decimal digit relative to the most significant digit to
        ''' use. </param>
        ''' <returns> A rounded up value to the specified relative digit. </returns>
        ''' <example> Unary.Ceiling(1.123456,2) returns 1.13.<p>
        ''' Unary.Ceiling(-1.123456,2) returns -1.12.</p></example>
        <Extension()>
        Public Function Ceiling(ByVal value As Single, ByVal relativeDecimal As Integer) As Single
            Return CSng(CType(value, Double).Ceiling(relativeDecimal))
        End Function

        ''' <summary> Returns the number of decimal places for displaying the auto value. </summary>
        ''' <param name="value"> A <see cref="T:System.Single">Single</see> value. </param>
        <Extension()>
        Public Function DecimalPlaces(ByVal value As Single) As Integer
            Return CType(value, Double).DecimalPlaces
        End Function

        ''' <summary> Returns the decimal value of a number. </summary>
        ''' <param name="value">            Value which to use in the calculation. </param>
        ''' <param name="decimalLeftShift"> Decimal power by which to shift the digit. </param>
        ''' <returns> The decimal value of the value shifted by the specified decimalPlaces. </returns>
        ''' <example> Unary.DecimalValue(1.123,1) returns 10.<p>
        ''' Unary.DecimalValue(11.23,1) returns 100.</p><p>
        ''' Unary.DecimalValue(-1.123,1) returns 10.</p><p>
        ''' Unary.DecimalValue(0.1234,1) returns 1.</p><p>
        ''' Unary.DecimalValue(11.12,0) returns 10.</p></example>
        <Extension()>
        Public Function DecimalValue(ByVal value As Single, ByVal decimalLeftShift As Integer) As Double
            Return CDbl(value).DecimalValue(decimalLeftShift)
        End Function

        ''' <summary> Returns the exponent. </summary>
        ''' <param name="value"> A <see cref="T:System.Single">Single</see> value. </param>
        ''' <returns> The most significant decade of the value. </returns>
        ''' <example> NumericExtensions.Exponent(1.12) returns 0.<p>
        ''' Unary.Decade(11.23) returns 1.</p><p>
        ''' NumericExtensions.Exponent(-1.123) returns 0.</p><p>
        ''' NumericExtensions.Exponent(0.1234) returns -1</p></example>
        ''' <seealso cref="Decimal"/>
        <Extension()>
        Public Function Exponent(ByVal value As Single) As Integer
            Return CDbl(value).Exponent
        End Function

        ''' <summary> Returns the exponent. </summary>
        ''' <param name="value">               A <see cref="T:System.Single">Single</see> value. </param>
        ''' <param name="useEngineeringScale"> true to use engineering scale. </param>
        ''' <returns> The most significant decade of the value. </returns>
        ''' <example> NumericExtensions.Exponent(11.12) returns 0.<p>
        ''' Unary.Decade(111.23) returns 1.</p><p>
        ''' NumericExtensions.Exponent(1111.123) returns 3.</p><p>
        ''' NumericExtensions.Exponent(-11.12) returns 0.</p><p>
        ''' Unary.Decade(-111.23) returns 1.</p><p>
        ''' NumericExtensions.Exponent(-1111.123) returns 3.</p><p>
        ''' NumericExtensions.Exponent(0.1234) returns 0</p><p>
        ''' NumericExtensions.Exponent(0.01234) returns 0</p><p>
        ''' NumericExtensions.Exponent(0.0001234) returns -3</p><p>
        ''' NumericExtensions.Exponent(-0.1234) returns 0</p><p>
        ''' NumericExtensions.Exponent(-0.01234) returns 0</p><p>
        ''' NumericExtensions.Exponent(-0.001234) returns -3</p><p>
        ''' NumericExtensions.Exponent(-0.0001234) returns -3</p></example>
        ''' <seealso cref="DecimalValue"/>
        <Extension()>
        Public Function Exponent(ByVal value As Single, ByVal useEngineeringScale As Boolean) As Integer
            Return CDbl(value).Exponent(useEngineeringScale)
        End Function

        ''' <summary> Returns a suggested fixed format based requested most significant decimalPlaces. </summary>
        ''' <param name="value">         Number to format. </param>
        ''' <param name="decimalPlaces"> The number of decimal places beyond the most significant digit. </param>
        ''' <returns> A fix format string. </returns>
        ''' <example> Unary.FixedFormat(1.123,1) returns 0.0.<p>
        ''' Unary.FixedFormat(11.23,1) returns 0.</p><p>
        ''' Unary.FixedFormat(-1.123,1) returns 0.0.</p><p>
        ''' Unary.FixedFormat(0.1234,1) returns 0.0.</p></example>
        <Extension()>
        Public Function FixedFormat(ByVal value As Single, ByVal decimalPlaces As Integer) As String
            Return CDbl(value).FixedFormat(decimalPlaces)
        End Function

        ''' <summary> Returns a value that is rounded down to the specified significant digit. </summary>
        ''' <param name="value">           value which to use in the calculation. </param>
        ''' <param name="relativeDecimal"> The decimal digit relative to the most significant digit to
        ''' use. </param>
        ''' <returns> A rounded down value to the specified relative digit. </returns>
        ''' <example> Unary.Floor(1.123,1) returns 1.1.<p>
        ''' Unary.Floor(11.23,1) returns 11.</p><p>
        ''' Unary.Floor(-1.123,1) returns -1.3.</p><p>
        ''' Unary.Floor(0.1234,1) returns 0.12.</p><p>
        ''' Unary.Floor(11.12,0) returns 10.</p></example>
        <Extension()>
        Public Function Floor(ByVal value As Single, ByVal relativeDecimal As Integer) As Double
            Return CDbl(value).Floor(relativeDecimal)
        End Function

        ''' <summary> Computes the length of a right triangle's hypotenuse. </summary>
        ''' <remarks> <para>The length is computed accurately, even in cases where x<sup>2</sup> or
        ''' y<sup>2</sup> would overflow.</para> </remarks>
        ''' <param name="value">      The length of one side. </param>
        ''' <param name="orthogonal"> The length of orthogonal side. </param>
        ''' <returns> The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>). </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal value As Single, ByVal orthogonal As Single) As Double
            Return Hypotenuse(CDbl(value), CDbl(orthogonal))
        End Function

        ''' <summary> Interpolate over the unity range. </summary>
        ''' <param name="value">    The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="minValue"> The minimum range point. </param>
        ''' <param name="maxValue"> The maximum range point. </param>
        ''' <returns> Returns the interpolated output over the specified range relative to the value over
        ''' the unity [0,1) range. </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Single, ByVal minValue As Single, ByVal maxValue As Single) As Double
            Return value * (maxValue - minValue) + minValue
        End Function

        ''' <summary> Linearly interpolated value. </summary>
        ''' <param name="value">     The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="minValue">  The first abscissa value. </param>
        ''' <param name="maxValue">  The second abscissa value. </param>
        ''' <param name="minOutput"> The first ordinate value. </param>
        ''' <param name="maxOutput"> The second ordinate value. </param>
        ''' <returns> The abscissa value derived using linear interpolation based on a pair of coordinates. </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Single, ByVal minValue As Single, ByVal maxValue As Single, ByVal minOutput As Single, ByVal maxOutput As Single) As Double
            Return CType(value, Double).Interpolate(minValue, maxValue, minOutput, maxOutput)
        End Function

        ''' <summary> Linearly interpolated value. </summary>
        ''' <param name="value">  The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="point1"> The first point. </param>
        ''' <param name="point2"> The second point. </param>
        ''' <returns> The abscissa value derived using linear interpolation based on a pair of coordinates. </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Single, ByVal point1 As System.Drawing.PointF, ByVal point2 As System.Drawing.PointF) As Double
            Return CType(value, Double).Interpolate(point1, point2)
        End Function

        ''' <summary> Linearly interpolated value. </summary>
        ''' <param name="value">     The abscissa value to interpolate to an ordinate value. </param>
        ''' <param name="rectangle"> The rectangle of coordinates. </param>
        ''' <returns> The abscissa value derived using linear interpolation based on a pair of coordinates. </returns>
        <Extension()>
        Public Function Interpolate(ByVal value As Single, ByVal rectangle As System.Drawing.RectangleF) As Double
            Return CType(value, Double).Interpolate(rectangle)
        End Function

        ''' <summary> Calculate a base 10 logarithm in a safe manner to avoid math exceptions. </summary>
        ''' <param name="value"> The value for which the logarithm is to be calculated. </param>
        ''' <returns> The value of the logarithm, or 0 if the <paramref name="value"/>
        ''' argument was negative or zero. </returns>
        <Extension()>
        Public Function Log10(ByVal value As Single) As Double
            Return CType(value, Double).Log10()
        End Function

        ''' <summary> Returns the maximum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The maximum value. </returns>
        <Extension()>
        Public Function Max(ByVal value As Single, ByVal other As Single) As Single
            If value >= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns the minimum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The minimum value. </returns>
        <Extension()>
        Public Function Min(ByVal value As Single, ByVal other As Single) As Single
            If value <= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Calculate the modulus (remainder) in a safe manner so that divide by zero errors are
        ''' avoided. </summary>
        ''' <param name="value">    The divisor. </param>
        ''' <param name="dividend"> The dividend. </param>
        ''' <returns> the value of the modulus, or zero for the divide-by-zero case. </returns>
        <Extension()>
        Public Function [Mod](ByVal value As Single, ByVal dividend As Single) As Double
            Return CType(value, Double).Mod(dividend)
        End Function

        ''' <summary> Raises an argument to an integer power. </summary>
        ''' <remarks> <para>Low integer powers can be computed by optimized algorithms much faster than the
        ''' general algorithm for an arbitrary real power employed by
        ''' <see cref="T:System.Math.Pow"/>.</para> </remarks>
        ''' <param name="value"> The argument. </param>
        ''' <param name="power"> The power. </param>
        ''' <returns> The value of x<sup>n</sup>. </returns>
        <Extension()>
        Public Function Pow(ByVal value As Single, ByVal power As Integer) As Double
            Return CType(value, Double).Pow(power)
        End Function

        ''' <summary> An method for squaring. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> System.Double. </returns>
        <Extension()>
        Public Function Squared(ByVal value As Single) As Double
            Return (value * value)
        End Function

        ''' <summary> An method for cubing. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> System.Double. </returns>
        <Extension()>
        Public Function Cubed(ByVal value As Single) As Double
            Return (value * value * value)
        End Function

#End Region

    End Module
End Namespace
