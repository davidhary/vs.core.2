﻿Imports System.Runtime.CompilerServices
Namespace StackExtensions

    ''' <summary> Includes extensions for <see cref="Stack">Stack</see> objects. </summary>
    ''' <license> (c) 2014 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="10/11/2014" by="David" revision="2.0.5395.x"> Created. </history>
    Public Module Methods

        ''' <summary> Peek the second item from the stack. </summary>
        ''' <param name="stack"> The stack. </param>
        ''' <returns> A T. </returns>
        <Extension()>
        Public Function Over(Of T)(ByVal stack As Stack(Of T)) As T
            If stack Is Nothing Then
                Throw New ArgumentNullException(NameOf(stack))
            End If
            Dim top As T = stack.Pop
            Dim value As T = stack.Peek
            stack.Push(top)
            Return value
        End Function

    End Module

End Namespace
