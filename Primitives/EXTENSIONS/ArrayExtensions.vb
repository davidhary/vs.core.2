﻿Imports System.Runtime.CompilerServices
Namespace ArrayExtensions

    ''' <summary> Includes extensions for native arrays. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created </history>
    Public Module Methods

        ''' <summary> Appends a list of elements to the end of an array. </summary>
        ''' <param name="values"> The array. </param>
        ''' <param name="items">  A variable-length parameters list containing items. </param>
        ''' <returns> Modified array. </returns>
        <Extension()>
        Public Function Append(Of T)(ByVal values As T(), ByVal ParamArray items As T()) As T()

            If values Is Nothing Then Return items
            If items Is Nothing Then Return values

            Dim oldLength As Integer = values.Length

            ' make room for new items
            System.Array.Resize(values, oldLength + items.Length)

            For i As Integer = 0 To items.Length - 1
                values(oldLength + i) = items(i)
            Next i
            Return values

        End Function

        ''' <summary> Determines if the two specified arrays have the same values. </summary>
        ''' <remarks> <see cref="T:Array"/> or <see cref="T:Arraylist"/> equals methods cannot be used
        ''' because it expects the two entities to be the same for equality. </remarks>
        ''' <param name="left">  The left value. </param>
        ''' <param name="right"> The right value. </param>
        ''' <returns> <c>True</c> if equals. </returns>
        <Extension()>
        Public Function Equals(Of T)(ByVal left As T(), ByVal right As T()) As Boolean

            Dim result As Boolean = True
            If left Is Nothing Then
                result = right Is Nothing
            ElseIf right Is Nothing Then
                result = False
            Else
                For i As Integer = 0 To left.Length - 1
                    If Not left(i).Equals(right(i)) Then
                        result = False
                        Exit For
                    End If
                Next
            End If
            Return result
        End Function

        ''' <summary> Determines whether the specified array is null or empty. </summary>
        ''' <param name="array"> The array. </param>
        ''' <returns> <c>True</c> if the array is null or empty; otherwise, <c>False</c>. </returns>
        <Extension()>
        Public Function IsNullOrEmpty(ByVal array As System.Array) As Boolean
            Return array Is Nothing OrElse array.Length = 0
        End Function


        ''' <summary> Remove an Array at a specific Location. </summary>
        ''' <param name="values"> The Array Object. </param>
        ''' <param name="index">  index to remove at. </param>
        ''' <returns> Modified array. </returns>
        <Extension()>
        Public Function RemoveAt(Of T)(ByVal values As T(), ByVal index As Integer) As T()

            If index < 0 OrElse values Is Nothing OrElse values.Length = 0 OrElse values.Length <= (index + 1) Then
                Return values
            End If

            ' move everything from the index on to the left one then remove last empty
            For i As Integer = index + 1 To values.Length - 1
                values(i - 1) = values(i)
            Next i

            System.Array.Resize(values, values.Length - 1)

            Return values

        End Function

        ''' <summary> Remove all elements in an array satisfying a predicate. </summary>
        ''' <param name="values">    The Array Object. </param>
        ''' <param name="condition"> A Predicate when the element shall get removed under. </param>
        ''' <returns> Modified array. </returns>
        <Extension()>
        Public Function RemoveAll(Of T)(ByVal values As T(), ByVal condition As Predicate(Of T)) As T()

            If condition Is Nothing OrElse values Is Nothing OrElse values.Length = 0 Then
                Return values
            End If

            Dim Count As Integer = 0
            For i As Integer = 0 To values.Length - 1
                If condition(values(i)) Then
                    RemoveAt(Of T)(values, i)
                    Count += 1
                End If
            Next i
            Return values

        End Function

#Region " COPY BETWEEN ARRAYS OF DIFFERENT TYPES OR SIZES "

        ''' <summary> Copy values between arrays. </summary>
        ''' <param name="fromValues"> From values. </param>
        ''' <param name="toValues">   To values. </param>
        <Extension()>
        Public Sub Copy(ByVal fromValues() As Double, ByVal toValues() As Double)
            If fromValues IsNot Nothing AndAlso toValues IsNot Nothing AndAlso fromValues.Length > 0 AndAlso toValues.Length > 0 Then
                For i As Integer = Math.Max(fromValues.GetLowerBound(0),
                                            toValues.GetLowerBound(0)) To Math.Min(fromValues.GetUpperBound(0),
                                                                                   toValues.GetUpperBound(0))
                    toValues(i) = fromValues(i)
                Next
            End If
        End Sub

        ''' <summary> Copy values between arrays. </summary>
        ''' <param name="fromValues"> From values. </param>
        ''' <param name="toValues">   To values. </param>
        <Extension()>
        Public Sub Copy(ByVal fromValues() As Double, ByVal toValues() As Single)
            If fromValues IsNot Nothing AndAlso toValues IsNot Nothing AndAlso fromValues.Length > 0 AndAlso toValues.Length > 0 Then
                For i As Integer = Math.Max(fromValues.GetLowerBound(0),
                                            toValues.GetLowerBound(0)) To Math.Min(fromValues.GetUpperBound(0),
                                                                                   toValues.GetUpperBound(0))
                    toValues(i) = CSng(fromValues(i))
                Next
            End If
        End Sub

        ''' <summary> Copy values between arrays. </summary>
        ''' <param name="fromValues"> From values. </param>
        ''' <param name="toValues">   To values. </param>
        <Extension()>
        Public Sub Copy(ByVal fromValues() As Single, ByVal toValues() As Double)
            If fromValues IsNot Nothing AndAlso toValues IsNot Nothing AndAlso fromValues.Length > 0 AndAlso toValues.Length > 0 Then
                For i As Integer = Math.Max(fromValues.GetLowerBound(0),
                                            toValues.GetLowerBound(0)) To Math.Min(fromValues.GetUpperBound(0),
                                                                                   toValues.GetUpperBound(0))
                    toValues(i) = fromValues(i)
                Next
            End If
        End Sub

        ''' <summary> Copy values between arrays. </summary>
        ''' <param name="fromValues"> From values. </param>
        ''' <param name="toValues">   To values. </param>
        <Extension()>
        Public Sub Copy(ByVal fromValues() As Single, ByVal toValues() As Single)
            If fromValues IsNot Nothing AndAlso toValues IsNot Nothing AndAlso fromValues.Length > 0 AndAlso toValues.Length > 0 Then
                For i As Integer = Math.Max(fromValues.GetLowerBound(0),
                                            toValues.GetLowerBound(0)) To Math.Min(fromValues.GetUpperBound(0),
                                                                                   toValues.GetUpperBound(0))
                    toValues(i) = fromValues(i)
                Next
            End If
        End Sub

#End Region

    End Module
End Namespace
