﻿Imports System.Runtime.CompilerServices
Namespace ImmediateIfExtensions
    ''' <summary> Implement a type safe immediate if function. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    Public Module Methods

        ''' <summary> Type safe immediate if function. </summary>
        ''' <param name="condition"> Specifies the condition. </param>
        ''' <param name="truePart">  Specifies the value to return if the condition is true. </param>
        ''' <param name="falsePart"> Specifies the value to return if the condition is false. </param>
        ''' <returns> The <paramref name="truePart"/> if the <paramref name="condition"/> is true; 
        '''           otherwise <paramref name="falsePart"/>. </returns>
        <Extension()>
        Public Function IIf(Of T)(ByVal condition As Boolean, ByVal truePart As T, ByVal falsePart As T) As T
            If condition Then
                Return truePart
            Else
                Return falsePart
            End If
        End Function

        ''' <summary> Type safe immediate if function. </summary>
        ''' <param name="condition"> Specifies the condition. </param>
        ''' <param name="truePart">  Specifies the value to return if the condition is true. </param>
        ''' <param name="falsePart"> Specifies the value to return if the condition is false. </param>
        ''' <returns> The <paramref name="truePart"/> if the <paramref name="condition"/> is true; 
        '''           otherwise <paramref name="falsePart"/>. </returns>
        <Extension()>
        Public Function IIf(ByVal condition As Boolean, ByVal truePart As String, ByVal falsePart As String) As String
            If condition Then
                Return truePart
            Else
                Return falsePart
            End If
        End Function

    End Module

End Namespace
