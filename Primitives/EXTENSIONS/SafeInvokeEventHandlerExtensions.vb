Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Namespace SafeInvokeEventHandlerExtensions

    ''' <summary> Extends event handlers for safe triggering and safe cross threading handling of event. </summary>
    ''' <remarks> Shout outs to: <para>
    ''' http://www.CodeProject.com/KB/cs/EventSafeTrigger.aspx </para><para>
    ''' AEONHACK http://www.DreamInCode.net/forums/user/334492-AEONHACK/ </para><para>
    ''' http://www.DreamInCode.net/code/snippet5016.htm </para>
    ''' </remarks>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="03/01/2010" by="David" revision="1.2.3712.x"> Created </history>
    Public Module Methods

#Region " INVOKE REQUIRED "

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <remarks>
        ''' David, 1/1/2016. This workaround is required because the null conditional does not seem to
        ''' work correctly causing an null reference exception.
        ''' </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function SafeInvocationList(Of TEventArgs)(ByVal handler As EventHandler(Of TEventArgs)) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <remarks> David, 1/1/2016. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function SafeInvocationList(ByVal handler As EventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <remarks> David, 1/1/2016. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function SafeInvocationList(ByVal handler As PropertyChangedEventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Returns the event handler invocation list. </summary>
        ''' <remarks> David, 1/1/2016. </remarks>
        ''' <param name="handler"> The handler. </param>
        ''' <returns> A [Delegate]() </returns>
        <Extension>
        Public Function SafeInvocationList(ByVal handler As PropertyChangingEventHandler) As [Delegate]()
            If handler Is Nothing Then
                Return New [Delegate]() {}
            Else
                Return handler.GetInvocationList
            End If
        End Function

        ''' <summary> Determines if invoke is required on any of the targets. </summary>
        ''' <param name="value"> The event. </param>
        <Extension()>
        <Obsolete("Exception overhead is too large for event handling.")>
        Public Function HasSynchronizableTarget(Of TEventArgs As EventArgs)(ByVal value As EventHandler(Of TEventArgs)) As Boolean
            Dim affirmative As Boolean = False
            For Each d As [Delegate] In value.SafeInvocationList
                affirmative = affirmative OrElse (d.Target IsNot Nothing AndAlso
                                                  TryCast(d.Target, ISynchronizeInvoke) IsNot Nothing)
                If affirmative Then Exit For
            Next
            Return affirmative
        End Function

        ''' <summary> Determines if invoke is required on any of the targets. </summary>
        ''' <param name="value"> The event. </param>
        <Extension()>
        <Obsolete("Exception overhead is too large for event handling.")>
        Public Function InvokeRequired(Of TEventArgs As EventArgs)(ByVal value As EventHandler(Of TEventArgs)) As Boolean
            Dim affirmative As Boolean = False
            For Each d As [Delegate] In value.SafeInvocationList
                If d.Target IsNot Nothing Then
                    Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If target IsNot Nothing Then
                        affirmative = affirmative OrElse target.InvokeRequired
                    End If
                End If
                If affirmative Then Exit For
            Next
            Return affirmative
        End Function

#End Region

#Region " SAFE BEGIN INVOKE "

        ''' <summary> Safely triggers an event across threads. Does not wait for the event handler to
        ''' complete. </summary>
        ''' <remarks> Raise events in cross-thread operations, no more of those pesky: Cross-thread
        ''' operation not valid: Control 'NAME' accessed from a thread other than the thread it was
        ''' created on." I have put together a simple class that reverses a string on a separate thread
        ''' and raises an event. Visual Basic hides the delegates for events. Using this method required
        ''' appending 'Event' to the name of the Event, e.g., StoppedEvent.SafeBeginInvoke(me).
        ''' <para>
        ''' Benchmark:
        ''' </para>
        ''' Safe Begin Invoke Benchmarked at 0.05273ms. </remarks>
        ''' <param name="value">  The event to trigger. </param>
        ''' <param name="sender"> The sender of the event. </param>
        <Extension()>
        <Obsolete("Exception overhead is too large for event handling.")>
        Public Sub SafeBeginInvoke(ByVal value As EventHandler, ByVal sender As Object)
            Methods.SafeBeginInvoke(value, sender, System.EventArgs.Empty)
        End Sub

        ''' <summary> Safely triggers an event across threads. Does not wait for the event handler to
        ''' complete. </summary>
        ''' <param name="value">  The event to trigger. </param>
        ''' <param name="sender"> The sender of the event. </param>
        ''' <param name="e">      The arguments for the event. </param>
        ''' <remarks> Uses <see cref="[Delegate].DynamicInvoke">dynamic invoke, which is very slow. </see></remarks>
        <Extension()>
        <Obsolete("Exception overhead is too large for event handling.")>
        Public Sub SafeBeginInvoke(ByVal value As EventHandler, ByVal sender As Object, ByVal e As EventArgs)
            For Each d As [Delegate] In value.SafeInvocationList
                If d.Target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, e})
                Else
                    Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If target Is Nothing Then
                        d.DynamicInvoke(New Object() {sender, e})
                    Else
                        target.BeginInvoke(d, New Object() {sender, e})
                    End If
                End If
            Next
        End Sub

        ''' <summary> Safely triggers an event across threads. Does not wait for the event handler to
        ''' complete. </summary>
        ''' <param name="value">  The event to trigger. </param>
        ''' <param name="sender"> The sender of the event. </param>
        ''' <param name="e">      The arguments for the event. </param>
        ''' <remarks> Uses <see cref="[Delegate].DynamicInvoke">dynamic invoke, which is very slow. </see></remarks>
        <Extension()>
        <Obsolete("Exception overhead is too large for event handling.")>
        Public Sub SafeBeginInvoke(ByVal value As PropertyChangedEventHandler, ByVal sender As Object,
                                   ByVal e As PropertyChangedEventArgs)
            For Each d As [Delegate] In value.SafeInvocationList
                If d.Target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, e})
                Else
                    Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If target Is Nothing Then
                        d.DynamicInvoke(New Object() {sender, e})
                    Else
                        target.BeginInvoke(d, New Object() {sender, e})
                    End If
                End If
            Next
        End Sub

        ''' <summary> Safely triggers an event across threads. Does not wait for the event handler to
        ''' complete. </summary>
        ''' <param name="value">  The event to trigger. </param>
        ''' <param name="sender"> The sender of the event. </param>
        ''' <param name="e">      The arguments for the event. </param>
        ''' <remarks> Uses <see cref="[Delegate].DynamicInvoke">dynamic invoke, which is very slow. </see></remarks>
        <Extension()>
        <Obsolete("Exception overhead is too large for event handling.")>
        Public Sub SafeBeginInvoke(Of TEventArgs As EventArgs)(ByVal value As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            For Each d As [Delegate] In value.SafeInvocationList
                If d.Target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, e})
                Else
                    Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If target Is Nothing Then
                        d.DynamicInvoke(New Object() {sender, e})
                    Else
                        target.BeginInvoke(d, New Object() {sender, e})
                    End If
                End If
            Next
        End Sub

        ''' <summary> Safely triggers an event across threads. Does not wait for the event handler to
        ''' complete. </summary>
        ''' <param name="value">  The event to trigger. </param>
        ''' <param name="sender"> The sender of the event. </param>
        <Extension()>
        <Obsolete("Exception overhead is too large for event handling.")>
        Public Sub SafeBeginInvoke(Of TEventArgs As {EventArgs, New})(ByVal value As EventHandler(Of TEventArgs), ByVal sender As Object)
            Methods.SafeBeginInvoke(value, sender, New TEventArgs())
        End Sub

#End Region

#Region " SAFE INVOKE "

        ''' <summary> Safely triggers an event across threads. Waits for the event handler to complete. </summary>
        ''' <remarks> Raise events in cross-thread operations, no more of those pesky: Cross-thread
        ''' operation not valid: Control 'NAME' accessed from a thread other than the thread it was
        ''' created on." I have put together a simple class that reverses a string on a separate thread
        ''' and raises an event. Visual Basic hides the delegates for events. Using this method required
        ''' appending 'Event' to the name of the Event, e.g., StoppedEvent.SafeInvoke(me).
        ''' <para>
        ''' Benchmark:
        ''' </para>
        ''' Safe Begin End Invoke Benchmarked at 0.054185ms. </remarks>
        ''' <param name="value">  The event to trigger. </param>
        ''' <param name="sender"> The sender of the event. </param>
        <Extension()>
        <Obsolete("Exception overhead is too large for event handling.")>
        Public Sub SafeInvoke(ByVal value As EventHandler, ByVal sender As Object)
            Methods.SafeInvoke(value, sender, System.EventArgs.Empty)
        End Sub

        ''' <summary> Safely triggers an event across threads. Waits for the event handler to complete. </summary>
        ''' <param name="value">  The event to trigger. </param>
        ''' <param name="sender"> The sender of the event. </param>
        ''' <param name="e">      The arguments for the event. </param>
        ''' <remarks> Uses <see cref="[Delegate].DynamicInvoke">dynamic invoke, which is very slow. </see></remarks>
        <Extension()>
        <Obsolete("Exception overhead is too large for event handling.")>
        Public Sub SafeInvoke(ByVal value As EventHandler, ByVal sender As Object, ByVal e As EventArgs)
            For Each d As [Delegate] In value.SafeInvocationList
                If d.Target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, e})
                Else
                    Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    Dim result As IAsyncResult = Nothing
                    If target Is Nothing Then
                        d.DynamicInvoke(New Object() {sender, e})
                    Else
                        ' asynchronously execute the delegate on the thread that generated the process.
                        result = target.BeginInvoke(d, New Object() {sender, e})
                        If result IsNot Nothing Then
                            ' wait until the process started by Begin Invoke.
                            target.EndInvoke(result)
                        End If
                    End If
                End If
            Next
        End Sub

        ''' <summary> Safely triggers an event across threads. Waits for the event handler to complete. </summary>
        ''' <param name="value">  The event to trigger. </param>
        ''' <param name="sender"> The sender of the event. </param>
        ''' <param name="e">      The arguments for the event. </param>
        ''' <remarks> Uses <see cref="[Delegate].DynamicInvoke">dynamic invoke, which is very slow. </see></remarks>
        <Extension()>
        <Obsolete("Exception overhead is too large for event handling.")>
        Public Sub SafeInvoke(ByVal value As ComponentModel.PropertyChangedEventHandler, ByVal sender As Object,
                              ByVal e As ComponentModel.PropertyChangedEventArgs)
            For Each d As [Delegate] In value.SafeInvocationList
                If d.Target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, e})
                Else
                    Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    Dim result As IAsyncResult = Nothing
                    If target Is Nothing Then
                        d.DynamicInvoke(New Object() {sender, e})
                    Else
                        ' asynchronously execute the delegate on the thread that generated the process.
                        result = target.BeginInvoke(d, New Object() {sender, e})
                        If result IsNot Nothing Then
                            ' wait until the process started by Begin Invoke.
                            target.EndInvoke(result)
                        End If
                    End If
                End If
            Next
        End Sub

        ''' <summary> Safely triggers an event across threads. Waits for the event handler to complete. </summary>
        ''' <param name="value">  The event to trigger. </param>
        ''' <param name="sender"> The sender of the event. </param>
        ''' <param name="e">      The arguments for the event. </param>
        ''' <remarks> Uses <see cref="[Delegate].DynamicInvoke">dynamic invoke, which is very slow. </see></remarks>
        <Extension()>
        <Obsolete("Exception overhead is too large for event handling.")>
        Public Sub SafeInvoke(Of TEventArgs As EventArgs)(ByVal value As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            For Each d As [Delegate] In value.SafeInvocationList
                If d.Target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, e})
                Else
                    Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    Dim result As IAsyncResult = Nothing
                    If target Is Nothing Then
                        d.DynamicInvoke(New Object() {sender, e})
                    Else
                        ' asynchronously execute the delegate on the thread that generated the process.
                        result = target.BeginInvoke(d, New Object() {sender, e})
                        If result IsNot Nothing Then
                            ' wait until the process started by Begin Invoke.
                            target.EndInvoke(result)
                        End If
                    End If
                End If
            Next
        End Sub

        ''' <summary> Safely triggers an event across threads. Waits for the event handler to complete. </summary>
        ''' <param name="value">  The event to trigger. </param>
        ''' <param name="sender"> The sender of the event. </param>
        <Extension()>
        <Obsolete("Exception overhead is too large for event handling.")>
        Public Sub SafeInvoke(Of TEventArgs As {EventArgs, New})(ByVal value As EventHandler(Of TEventArgs), ByVal sender As Object)
            Methods.SafeInvoke(value, sender, New TEventArgs())
        End Sub

#End Region

#Region " EVENT HANDLER SYSTEM EVENT ARGS RETURN VALUE "

        ''' <summary> Safely triggers an event across threads. Waits for the event handler to complete. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">                The event to trigger. </param>
        ''' <param name="sender">               The sender of the event. </param>
        ''' <param name="retrieveDataFunction"> A function used to retrieve data from the event. </param>
        ''' <returns> Returns data retrieved from the event arguments. </returns>
        <Extension()>
        <Obsolete("Exception overhead is too large for event handling.")>
        Public Function SafeInvoke(Of TEventArgs As {EventArgs, New}, TReturnType)(ByVal value As EventHandler(Of TEventArgs),
                                                                                   ByVal sender As Object, ByVal retrieveDataFunction As Func(Of TEventArgs, TReturnType)) As TReturnType
            If retrieveDataFunction Is Nothing Then
                Throw New ArgumentNullException(NameOf(retrieveDataFunction))
            End If
            If value IsNot Nothing Then
                Dim eventArgs As TEventArgs = New TEventArgs()
                Methods.SafeInvoke(value, sender, eventArgs)
                Dim returnData As TReturnType = retrieveDataFunction(eventArgs)
                Return returnData
            Else
                Return Nothing
            End If
        End Function

        ''' <summary> Safely triggers an event across threads. Waits for the event handler to complete. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">                The event to trigger. </param>
        ''' <param name="sender">               The sender of the event. </param>
        ''' <param name="eventArgs">            The arguments for the event. </param>
        ''' <param name="retrieveDataFunction"> A function used to retrieve data from the event. </param>
        ''' <returns> Returns data retrieved from the event arguments. </returns>
        <Extension()>
        <Obsolete("Exception overhead is too large for event handling.")>
        Public Function SafeInvoke(Of TReturnType)(ByVal value As EventHandler, ByVal sender As Object, ByVal eventArgs As EventArgs, ByVal retrieveDataFunction As Func(Of EventArgs, TReturnType)) As TReturnType
            If retrieveDataFunction Is Nothing Then
                Throw New ArgumentNullException(NameOf(retrieveDataFunction))
            End If

            If value IsNot Nothing Then
                Methods.SafeInvoke(value, sender, eventArgs)
                Dim returnData As TReturnType = retrieveDataFunction(eventArgs)
                Return returnData
            Else
                Return Nothing
            End If
        End Function

        ''' <summary> Safely triggers an event across threads. Waits for the event handler to complete. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">                The event to trigger. </param>
        ''' <param name="sender">               The sender of the event. </param>
        ''' <param name="eventArgs">            The arguments for the event. </param>
        ''' <param name="retrieveDataFunction"> A function used to retrieve data from the event. </param>
        ''' <returns> Returns data retrieved from the event arguments. </returns>
        <Extension()>
        <Obsolete("Exception overhead is too large for event handling.")>
        Public Function SafeInvoke(Of TEventArgs As EventArgs, TReturnType)(ByVal value As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal eventArgs As TEventArgs, ByVal retrieveDataFunction As Func(Of TEventArgs, TReturnType)) As TReturnType
            If retrieveDataFunction Is Nothing Then
                Throw New ArgumentNullException(NameOf(retrieveDataFunction))
            End If
            If value IsNot Nothing Then
                Methods.SafeInvoke(value, sender, eventArgs)
                Dim returnData As TReturnType = retrieveDataFunction(eventArgs)
                Return returnData
            Else
                Return Nothing
            End If
        End Function

#End Region

    End Module

End Namespace
