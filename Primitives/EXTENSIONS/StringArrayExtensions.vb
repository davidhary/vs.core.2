﻿Imports System.Runtime.CompilerServices
Namespace StringArrayExtensions
    ''' <summary> Includes extensions for string arrays. </summary>
    ''' <license> (c) 2011 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="08/27/2011" by="David" revision="1.2.4256.x"> Created </history>  
    Public Module Methods

        ''' <summary> Combines the specified string array using the
        ''' <paramref name="terminator">terminator</paramref>. </summary>
        ''' <param name="value">      The string array. </param>
        ''' <param name="terminator"> The terminator. </param>
        <Extension()>
        Public Function Combine(ByVal value As String(), ByVal terminator As String) As String
            If value Is Nothing Then Return ""
            Dim builder As New System.Text.StringBuilder
            For Each s As String In value
                If builder.Length > 0 Then
                    builder.Append(terminator)
                End If
                builder.Append(s)
            Next
            Return builder.ToString
        End Function

    End Module
End Namespace
