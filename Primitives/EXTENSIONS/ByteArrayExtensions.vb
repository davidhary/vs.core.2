﻿Imports System.Runtime.CompilerServices
Namespace ByteArrayExtensions

    ''' <summary> Includes extensions for <see cref="Byte">Byte</see> arrays. </summary>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/14/2013" by="David" revision="2.0.5066.x"> Created. </history>
    Public Module Methods

        ''' <summary> Converts the values to a string base 16. </summary>
        ''' <param name="values"> The values. </param>
        ''' <returns> Hex string aggregate of the byte array. </returns>
        <Extension()>
        Public Function ToStringBase16(ByVal values() As Byte) As String
            If values Is Nothing OrElse values.Length = 0 Then
                Return ""
            Else
                Return values.Aggregate(String.Empty, Function(current As String, value As Byte) (current & value.ToString("X2")))
            End If
        End Function

    End Module

End Namespace
