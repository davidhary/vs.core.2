Imports System.ComponentModel
''' <summary> A class to encapsulate specific enumerations in cases where speed improvements are
''' desired. Reported 100000 operations time reduced from 3.9 to .57 million ticks. With using a
''' creatable class, improvement increased to 10 fold. </summary>
''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="3/25/2015" by="David" revision="">                            Created. </history>
''' <history date="05/04/2008" by="Stewart and StageSoft" revision="1.2.3411.x"> <para>
''' From Strong-Type and Efficient .NET Enums By Hanna GIAT</para><para>
''' http://www.CodeProject.com/KB/cs/efficient_strong_enum.aspx. </para> </history>
Public Class EnumExtender(Of T)

    ''' <summary> constructor for the class instantiates all elements. With built-in elements,
    ''' subsequent operations are fairly speedy. </summary>
    Public Sub New()
        MyBase.new()
        Me._Type = GetType(T)
        Me._Values = System.Enum.GetValues(Me._Type)
        Me._Names = System.Enum.GetNames(Me._Type)

        ' Me._descriptions = isr.Core.Pith.EnumExtensions.Descriptions(T)
        Dim values As New System.Collections.Generic.List(Of String)
        For Each value As System.Enum In System.Enum.GetValues(Me._Type)
            values.Add(Me.Description(value))
        Next value
        Me._Descriptions = values.ToArray
        Me._NameValuePairs = New Dictionary(Of String, T)(Me._Names.Count)
        Me._LowerCaseNameValuePairs = New Dictionary(Of String, T)(Me._Names.Count)
        Me._ValueDescriptionPairs = New Dictionary(Of T, String)(Me._Names.Count)
        Me._ValueEnumValuePairs = New Dictionary(Of Integer, T)(Me._Names.Count)

        Dim index As Integer = 0
        For Each name As String In Me._Names
            Dim enumValue As T = CType(System.Enum.Parse(Me._Type, name), T)
            Me._NameValuePairs.Add(name, enumValue)
            Me._ValueDescriptionPairs.Add(enumValue, Me._Descriptions(index))
            Me._LowerCaseNameValuePairs.Add(name.ToLower(Globalization.CultureInfo.CurrentCulture), enumValue)
            Me._ValueEnumValuePairs.Add(Convert.ToInt32(enumValue, Globalization.CultureInfo.CurrentCulture), enumValue)
            index += 1
        Next name

    End Sub

    ''' <summary> Gets the <see cref="DescriptionAttribute"/> of an <see cref="T:System.Enum"/> type
    ''' value. </summary>
    ''' <param name="value"> The <see cref="T:System.Enum"/> type value. </param>
    ''' <returns> A string containing the text of the <see cref="DescriptionAttribute"/>. </returns>
    Public Function Description(ByVal value As System.Enum) As String

        If value Is Nothing Then Return ""

        Dim candidate As String = value.ToString()
        Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(candidate)
        Dim attributes As DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())

        If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
            candidate = attributes(0).Description
        End If
        Return candidate

    End Function

    ''' <summary> The descriptions. </summary>
    Private _Descriptions As IEnumerable(Of String)

    ''' <summary> Gets the descriptions. </summary>
    ''' <returns> The descriptions. </returns>
    Public Function Descriptions() As IEnumerable(Of String)
        Return Me._Descriptions
    End Function

    ''' <summary> Returns the descriptions of an flags enumerated list masked by the specified value. </summary>
    ''' <param name="mask"> Allows returning only selected items. </param>
    ''' <returns> The descriptions. </returns>
    Public Function Descriptions(ByVal mask As Integer) As IEnumerable(Of String)
        Dim index As Integer = 0
        Dim values As New System.Collections.Generic.List(Of String)
        For Each value As Object In Me._Values
            If (CInt(value) And mask) <> 0 Then
                values.Add(Me._Descriptions(index))
            End If
            index += 1
        Next value
        Return values
    End Function

    ''' <summary> Gets the Name of the Enum value. </summary>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    ''' illegal values. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> The name. </returns>
    Public Function Name(ByVal value As Object) As String
        ' We can hold an additional dictionary to map T -> string.
        ' In my specific usage, this usages is rare, so I selected the lower performance option
        If value Is Nothing Then Return ""
        Try
            Dim enumValue As T = CType(value, T)
            For Each pair As KeyValuePair(Of String, T) In Me._NameValuePairs
                ' Dim x As Integer = Convert.ToInt32(pair.Value, Globalization.CultureInfo.CurrentCulture)
                If pair.Value.Equals(enumValue) Then
                    Return pair.Key
                End If
            Next pair
        Catch
            Throw New ArgumentException("Cannot convert " & value.ToString & " to " & Me._Type.ToString(), "value")
        End Try
        Return Nothing ' should never happen

    End Function

    ''' <summary> The names. </summary>
    Private _Names As IEnumerable(Of String)

    ''' <summary> Returns the names. </summary>
    ''' <returns> The names. </returns>
    Public Function Names() As IEnumerable(Of String)
        Return Me._Names
    End Function

    ''' <summary> Returns the Names of an flags enumerated list masked by the specified value. </summary>
    ''' <param name="mask"> Allows returning only selected items. </param>
    ''' <returns> The names. </returns>
    Public Function Names(ByVal mask As Integer) As IEnumerable(Of String)
        Dim index As Integer = 0
        Dim values As New System.Collections.Generic.List(Of String)
        For Each value As Object In Me._Values
            If (CInt(value) And mask) <> 0 Then
                values.Add(Me._Names(index))
            End If
            index += 1
        Next value
        Return values
    End Function

    ''' <summary> The values. </summary>
    Private _Values As System.Array

    ''' <summary> Returns the array of Enum values. </summary>
    ''' <returns> The values. </returns>
    Public Function Values() As Array
        Return Me._Values
    End Function

    ''' <summary> Returns the Values of an flags enumerated list masked by the specified value. </summary>
    ''' <param name="mask"> Allows returning only selected items. </param>
    ''' <returns> The values. </returns>
    Public Function Values(ByVal mask As Integer) As IEnumerable(Of Integer)
        Dim index As Integer = 0
        Dim v As New System.Collections.Generic.List(Of Integer)
        For Each value As Object In Me._Values
            If (CInt(value) And mask) <> 0 Then
                v.Add(CInt(value))
            End If
            index += 1
        Next value
        Return v
    End Function

    ''' <summary> Dictionary of Enum value and description pairs. </summary>
    Private _ValueDescriptionPairs As Dictionary(Of T, String)

    ''' <summary> Returns a <see cref="IList"/> of value and description pairs. </summary>
    ''' <returns> An <see cref="IList"/> containing the enumerated type value and description. </returns>
    ''' <example> The first step is to add a description attribute to your Enum.
    ''' <code>
    ''' Public Enum SimpleEnum2
    ''' [System.ComponentModel.Description("Today")] Today
    ''' [System.ComponentModel.Description("Last 7 days")] Last7
    ''' [System.ComponentModel.Description("Last 14 days")] Last14
    ''' [System.ComponentModel.Description("Last 30 days")] Last30
    ''' [System.ComponentModel.Description("All")] All
    ''' End Enum
    ''' Public SimpleEfficientEnum As New EfficientEnum(of SimpleEnum2)
    ''' Dim combo As ComboBox = New ComboBox()
    ''' combo.DataSource = SimpleEfficientEnum.GetValueDescriptionPairs(GetType(SimpleEnum2))
    ''' combo.DisplayMember = "Value"
    ''' combo.ValueMember = "Key"
    ''' dim selectedEnum as SimpleEnum2 = SimpleEfficientEnum.ToObjectFromPair(combo.SelectedItem)
    ''' </code>
    ''' This works with any control that supports data binding, including the
    ''' <see cref="Windows.Forms.ToolStripComboBox">tool strip combo box</see>,
    ''' although you will need to cast the
    ''' <see cref="Windows.Forms.ToolStripComboBox.Control">control property</see>
    ''' to a <see cref="Windows.Forms.ComboBox">Combo Box</see> to get to the DataSource property. In
    ''' that case, you will also want to perform the same cast when you are referencing the selected
    ''' value to work with it as your Enum type.</example>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")>
    Public Function ValueDescriptionPairs() As IList
        Return Me._ValueDescriptionPairs.ToList
    End Function

    ''' <summary>
    ''' Gets or sets the type.
    ''' </summary>
    Private _Type As Type

    ''' <summary> Gets the underlying type. </summary>
    ''' <returns> The underlying type. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")>
    Public Function UnderlyingType() As Type
        ' Seems like this is good enough. 
        Return System.Enum.GetUnderlyingType(Me._Type)
    End Function

    ''' <summary> Returns True if the enumerated value is defined in this enumeration. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>True</c> if the enumerated value is defined in this enumeration. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function IsDefined(ByVal value As Object) As Boolean
        If value Is Nothing Then Return False
        Try
            Return IsDefined(CInt(Microsoft.VisualBasic.Fix(value)))
        Catch
            Return False
        End Try
    End Function

    ''' <summary> Returns True if the enumerated value is defined in this enumeration. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>True</c> if the enumerated value is defined in this enumeration. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function IsDefined(ByVal value As Integer) As Boolean
        Try
            Return Me._ValueEnumValuePairs.ContainsKey(value)
        Catch
            Return False
        End Try
    End Function

    ''' <summary> Dictionary of name value pairs used for parsing case-sensitive values. </summary>
    Private _NameValuePairs As Dictionary(Of String, T)

    ''' <summary> Returns the Enum value for the given name. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> Enum value for the given name. </returns>
    Public Function Parse(ByVal value As String) As T
        If String.IsNullOrWhiteSpace(value) Then Return Nothing
        Return Me._NameValuePairs(value) ' Exception will be thrown if the value not found
    End Function

    ''' <summary> Will serve us in the Parse method, with ignoreCase == true.
    ''' It is possible not to hold this member and to define a Comparer class -
    ''' But my main goal here is the performance at runtime. </summary>
    Private _LowerCaseNameValuePairs As Dictionary(Of String, T)

    ''' <summary> Returns the Enum value for the given name. </summary>
    ''' <param name="value">      The value. </param>
    ''' <param name="ignoreCase"> . </param>
    ''' <returns> Enum value for the given name. </returns>
    Public Function Parse(ByVal value As String, ByVal ignoreCase As Boolean) As T
        If String.IsNullOrWhiteSpace(value) Then Return Nothing
        If ignoreCase Then
            Dim valLower As String = value.ToLower(Globalization.CultureInfo.CurrentCulture)
            Return Me._LowerCaseNameValuePairs(valLower)
        Else
            Return Parse(value)
        End If
    End Function

    ''' <summary> Returns the value converted to Enum. </summary>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    ''' illegal values. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> value converted to Enum. </returns>
    Public Function ToObject(ByVal value As Object) As T
        If value Is Nothing Then Return Nothing
        Try
            Dim wholeValue As Integer = CInt(Microsoft.VisualBasic.Fix(value))
            Return ToObject(wholeValue)
        Catch e1 As InvalidCastException
            Throw New ArgumentException(String.Format("Cannot convert {0} to integer", value), "value")
        End Try
        'If an exception is coming from ToObject(value) do not catch it here.
    End Function

    ''' <summary> Converts a value to an integer. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> value as an Integer. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="integer")>
    Public Function ToInteger(ByVal value As Object) As Integer
        If value Is Nothing Then Throw New ArgumentNullException(NameOf(value))
        Try
            Return CInt(Microsoft.VisualBasic.Fix(value))
        Catch e As InvalidCastException
            Throw New ArgumentException(String.Format("Cannot convert {0} to integer", value), "value")
        End Try
    End Function

    ''' <summary> Will serve us in the ToObject method </summary>
    Private _ValueEnumValuePairs As Dictionary(Of Integer, T)

    ''' <summary> Returns the value converted to Enum. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> value converted to Enum. </returns>
    Public Function ToObject(ByVal value As Integer) As T
        Return Me._ValueEnumValuePairs(value)
    End Function

    ''' <summary> Returns the value converted to Enum from the given value name pair. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> converted to Enum from the given value name pair. </returns>
    Public Function ToObjectFromPair(ByVal value As Object) As T
        If value Is Nothing Then Return Nothing
        Return ToObject(CType(value, KeyValuePair(Of T, String)).Key)
    End Function

    ''' <summary> Converts a type to an integer. </summary>
    ''' <param name="type"> The <see cref="T:System.Enum"/> type. </param>
    ''' <returns> The given data converted to an integer. </returns>
    Private Function ToInteger(ByVal type As System.Enum) As Integer
        Return Me.ToInteger(CObj(type))
    End Function

#Region " BIT MANIPULATION "

    ''' <summary> Appends a value. </summary>
    ''' <param name="value"> Specifies the bit values which to add.</param>
    ''' <history date="07/07/2009" by="David" revision="1.2.3475.x">
    ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx
    ''' Under The Creative Commons Attribution-ShareAlike 2.5 License
    ''' </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function Add(ByVal value As T) As T
        Try
            Dim oldValue As Integer = Me.ToInteger(Me._Type)
            Dim bitsValue As Integer = Me.ToInteger(value)
            Dim newValue As Integer = oldValue Or bitsValue
            Return CType(CObj(newValue), T)
        Catch ex As Exception
            Throw New ArgumentException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                      "Could not append value from enumerated type '{0}'.", GetType(T).Name), ex)
        End Try
    End Function

    ''' <summary> Set the specified bits. </summary>
    ''' <param name="value"> Specifies the bit values which to add.</param>
    Public Function [Set](ByVal value As T) As T
        Return Add(value)
    End Function

    ''' <summary> Returns true if the enumerated flag has the specified bits. </summary>
    ''' <param name="value"> Specifies the bit values which to compare.</param>
    ''' <history date="07/07/2009" by="David" revision="1.2.3475.x">
    ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx
    ''' Under The Creative Commons Attribution-ShareAlike 2.5 License
    ''' </history>
    ''' <history date="06/05/2010" by="David" revision="1.2.3807.x">
    ''' Returns true if equals (IS).
    ''' </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function Has(ByVal value As T) As Boolean
        Try
            Dim oldValue As Integer = Me.ToInteger(Me._Type)
            Dim bitsValue As Integer = Me.ToInteger(value)
            Return (oldValue = bitsValue) OrElse ((oldValue And bitsValue) = bitsValue)
        Catch
            Return False
        End Try
    End Function

    ''' <summary> Returns true if the specified bits are set. </summary>
    ''' <param name="value"> Specifies the bit values which to compare.</param>
    ''' <history date="06/05/2010" by="David" revision="1.2.3807.x">
    ''' Returns true if equals (IS).
    ''' </history>
    Public Function IsSet(ByVal value As T) As Boolean
        Return Me.Has(value)
    End Function

    ''' <summary>  Returns true if value is only the provided type. </summary>
    ''' <param name="value"> Specifies the bit values which to compare.</param>
    ''' <history date="07/07/2009" by="David" revision="1.2.3475.x">
    ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx
    ''' Under The Creative Commons Attribution-ShareAlike 2.5 License
    ''' </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function [Is](ByVal value As T) As Boolean
        Try
            Dim oldValue As Integer = Me.ToInteger(Me._Type)
            Dim bitsValue As Integer = Me.ToInteger(value)
            Return oldValue = bitsValue
        Catch
            Return False
        End Try
    End Function

    ''' <summary> Clears the specified bits. </summary>
    ''' <param name="value"> Specifies the bit values which to remove.</param>
    Public Function Clear(ByVal value As T) As T
        Return Me.Remove(value)
    End Function

    ''' <summary> Completely removes the value. </summary>
    ''' <param name="value"> Specifies the bit values which to remove.</param>
    ''' <history date="07/07/2009" by="David" revision="1.2.3475.x">
    ''' http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx
    ''' Under The Creative Commons Attribution-ShareAlike 2.5 License
    ''' </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function Remove(ByVal value As T) As T
        Try
            Dim oldValue As Integer = Me.ToInteger(Me._Type)
            Dim bitsValue As Integer = Me.ToInteger(value)
            Dim newValue As Integer = oldValue And (Not bitsValue)
            Return CType(CObj(newValue), T)
        Catch ex As Exception
            Throw New ArgumentException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                      "Could not remove value from enumerated type '{0}'.", GetType(T).Name), ex)
        End Try
    End Function


#End Region

#Region " LIST VALUE DESCRIPTIONS "

    ''' <summary> List values. </summary>
    ''' <param name="control">  The control. </param>
    ''' <param name="excluded"> The excluded. </param>
    ''' <returns> The item count. </returns>
    Public Function ListValues(ByVal control As System.Windows.Forms.ListControl, ByVal excluded As T()) As Integer
        Dim v As New Dictionary(Of T, String)
        For Each vv As KeyValuePair(Of T, String) In Me._ValueDescriptionPairs
            If Not excluded.Contains(vv.Key) Then v.Add(vv.Key, vv.Value)
        Next
        Return Me.ListValues(control, v)
    End Function

    ''' <summary> List values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="control"> The control. </param>
    ''' <returns> The item count. </returns>
    Public Function ListValues(ByVal control As System.Windows.Forms.ListControl, ByVal values As Dictionary(Of T, String)) As Integer
        If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
        Dim comboEnabled As Boolean = control.Enabled
        control.Enabled = False
        control.DataSource = Nothing
        control.ValueMember = "Key"
        control.DisplayMember = "Value"
        ' converting to array ensures that values displayed in two controls are not connected.
        control.DataSource = values.ToList
        control.SelectedIndex = -1
        control.Enabled = comboEnabled
        control.Invalidate()
        If values IsNot Nothing Then
            Return values.Count
        Else
            Return 0
        End If
    End Function

    ''' <summary> List values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="control"> The control. </param>
    ''' <returns> The item count. </returns>
    Public Function ListValues(ByVal control As System.Windows.Forms.ListControl) As Integer
        Return Me.ListValues(control, Me._ValueDescriptionPairs)
    End Function

    ''' <summary> Returns the selected item. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="control"> The control. </param>
    Public Function SelectedItem(ByVal control As System.Windows.Forms.ComboBox) As KeyValuePair(Of T, String)
        If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
        Return CType(control.SelectedItem, KeyValuePair(Of T, String))
    End Function

    ''' <summary> Returns the selected item. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="control"> The control. </param>
    Public Function SelectedValue(ByVal control As System.Windows.Forms.ComboBox) As T
        If control Is Nothing Then Throw New ArgumentNullException(NameOf(control))
        Return Me.ToObjectFromPair(control.SelectedItem)
    End Function

    ''' <summary> Select value. </summary>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   Specifies the bit values which to add. </param>
    Public Sub SelectValue(ByVal control As System.Windows.Forms.ListControl, ByVal value As T)
        If control IsNot Nothing Then
            control.SelectedValue = Me.ToInteger(value)
        End If
    End Sub

#End Region

End Class

