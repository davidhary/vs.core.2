﻿Imports System.Runtime.CompilerServices
Imports Microsoft.VisualBasic.ApplicationServices
Namespace AssemblyExtensions

    ''' <summary> Includes extensions for <see cref="T:System.Reflection.Assembly">Assembly</see>
    ''' and <see cref="T:System.Reflection.Assembly">assembly info</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    Public Module Methods

        ''' <summary> Returns the assembly public key. </summary>
        ''' <remarks> <examples>
        ''' <code>
        ''' Dim pk As String = System.Reflection.Assembly.GetExecutingAssembly.GetPublicKey()
        ''' </code></examples> </remarks>
        ''' <param name="assembly"> The assembly. </param>
        ''' <returns> The public key of the assembly. </returns>
        <Extension()>
        Public Function GetPublicKey(ByVal assembly As System.Reflection.Assembly) As String
            If assembly Is Nothing Then
                Throw New ArgumentNullException(NameOf(assembly))
            End If
            Dim values As Byte() = assembly.GetName.GetPublicKey
            Return values.Aggregate(String.Empty, Function(current As String, value As Byte) (current & value.ToString("X2")))
        End Function

        ''' <summary> Gets public key token. </summary>
        ''' <param name="assembly"> The assembly. </param>
        ''' <returns> The public key token of the assembly. </returns>
        <Extension()>
        Public Function GetPublicKeyToken(ByVal assembly As System.Reflection.Assembly) As String
            If assembly Is Nothing Then
                Throw New ArgumentNullException(NameOf(assembly))
            End If
            Dim values As Byte() = assembly.GetName.GetPublicKeyToken
            Return values.Aggregate(String.Empty, Function(current As String, value As Byte) (current & value.ToString("X2")))
        End Function

        ''' <summary> Gets the application <see cref="FileVersionInfo">File Version Info.</see> </summary>
        ''' <remarks> Use this as a version information source for the application so that it won't be
        ''' affected by any changes that might occur in the <see cref="My.MyApplication.Info">application
        ''' info</see> code. </remarks>
        ''' <returns> The application <see cref="FileVersionInfo">File Version Info</see>. </returns>
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                    Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        <Extension()>
        Public Function GetFileVersionInfo(ByVal assembly As System.Reflection.Assembly) As FileVersionInfo
            If assembly Is Nothing Then
                Throw New ArgumentNullException(NameOf(assembly))
            End If
            Return FileVersionInfo.GetVersionInfo(assembly.Location)
        End Function

        ''' <summary> Gets a unique identifier. </summary>
        ''' <param name="assembly"> The assembly. </param>
        ''' <returns> The unique identifier. </returns>
        <Extension()>
        Public Function GetGuid(ByVal assembly As System.Reflection.Assembly) As Guid
            If assembly Is Nothing Then Throw New ArgumentNullException(NameOf(assembly))
            Dim attribute As Runtime.InteropServices.GuidAttribute = CType(assembly.GetCustomAttributes(GetType(Runtime.InteropServices.GuidAttribute),
                                                                                                          True)(0), Runtime.InteropServices.GuidAttribute)
            Return New Guid(attribute.Value)
        End Function

        ''' <summary> Gets the assembly. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="assemblyInfo"> Information describing the assembly. </param>
        ''' <returns> The application <see cref="T:System.Reflection.Assembly">Assembly</see>. </returns>
        <Extension()>
        Public Function GetAssembly(ByVal assemblyInfo As ApplicationServices.AssemblyInfo) As System.Reflection.Assembly
            If assemblyInfo Is Nothing Then
                Throw New ArgumentNullException(NameOf(assemblyInfo))
            End If
            Dim value As System.Reflection.Assembly = Nothing
            For Each a As System.Reflection.Assembly In assemblyInfo.LoadedAssemblies
                If a.GetName.Name.Equals(assemblyInfo.AssemblyName) Then
                    value = a
                    Exit For
                End If
            Next
            Return value
        End Function

        ''' <summary> Gets the folder where application files are located. </summary>
        ''' <remarks> <examples>
        ''' <code>
        ''' Dim folder As String = My.Application.Info.ApplicationFolder()
        ''' </code></examples> </remarks>
        ''' <param name="assemblyInfo"> Information describing the assembly. </param>
        ''' <returns> The application folder path. </returns>
        <Extension()>
        Public Function ApplicationFolder(ByVal assemblyInfo As ApplicationServices.AssemblyInfo) As String
            If assemblyInfo Is Nothing Then
                Throw New ArgumentNullException(NameOf(assemblyInfo))
            End If
            Return assemblyInfo.DirectoryPath
        End Function

        Public Const DefaultVersionFormat As String = "{0}.{1:00}.{2:0000}"

        ''' <summary> Gets the product version. </summary>
        ''' <remarks> <examples>
        ''' <code>
        ''' Dim version As String = My.Application.Info.ProducVersion("")
        ''' </code></examples> </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="assemblyInfo"> Information describing the assembly. </param>
        ''' <param name="format">       The version format. Set to empty to use the
        ''' <see cref="DefaultVersionFormat">default version format</see>. </param>
        ''' <returns> The application version. </returns>
        <Extension()>
        Public Function ProductVersion(ByVal assemblyInfo As ApplicationServices.AssemblyInfo, ByVal format As String) As String
            If assemblyInfo Is Nothing Then
                Throw New ArgumentNullException(NameOf(assemblyInfo))
            End If
            If String.IsNullOrWhiteSpace(format) Then format = DefaultVersionFormat
            Return String.Format(Globalization.CultureInfo.CurrentCulture,
                                 format, assemblyInfo.Version.Major, assemblyInfo.Version.Minor, assemblyInfo.Version.Build)
        End Function

    End Module

End Namespace
