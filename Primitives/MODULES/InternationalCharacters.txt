﻿0xB8 ,    // Epsilon with Tonos
0xB9 ,    // Eta with Tonos
0xBA ,    // Iota with Tonos
0xBC ,    // Omicron with Tonos
0xBE ,    // Upsiloin with Tonos
0xBF ,    // Omega with Tonos
0x20 ,    // space
0xC1 ,    // Alpha (UC)
0xE0 ,    // Aleph
0xE1 ,    // Alpha
0xE2 ,    // Beta
0xE3 ,    // Gamma
0xE4 ,    // Delta
0xE5 ,    // Epsilon
0xE6 ,    // Zeta
0xE7 ,    // Eta
0xE8 ,    // Theta
0xE9 ,    // Iota
0xEA ,    // Kappa
0xEB ,    // Lamda
0xEC ,    // Mu
0xED ,    // Nu
0xEE ,    // Xi
0xEF ,    // Omicron
0xD0 ,    // Pi
0xD1 ,    // Rho
0xD3 ,    // Sigma
0xD4 ,    // Tau
0xD5 ,    // Upsilon
0xD6 ,    // Phi
0xD7 ,    // Chi
0xD8 ,    // Psi
0xD9 ,    // Omega
0x20 ,    // space
0xDA ,    // Iota with Dialytika
0xDB  } ; // Upsilon with Dialytika
omega: System.Text.Encoding.GetEncoding(1253).GetChars(New Byte() {&HD9})(0)
delta: System.Text.Encoding.GetEncoding(1252).GetChars(New Byte() {&HE4})(0)
Aleph: System.Text.Encoding.GetEncoding(1255).GetChars(New Byte() {&HE0})(0)
