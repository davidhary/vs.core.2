﻿''' <summary> Special character functions. </summary>
''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="10/26/2010" by="David" revision="1.2.3951.x"> Created </history>
Public Module InternationalCharacters

    ''' <summary> Gets the symbol for Aleph. </summary>
    ''' <returns> The symbol for Aleph. </returns>
    Public Function Aleph() As Char
        Return Text.Encoding.GetEncoding(1255).GetChars(New Byte() {&HE0})(0)
    End Function

    ''' <summary> Gets the symbol for Eta (Nano). </summary>
    ''' <returns> The symbol for Eta (Nano). </returns>
    Public Function Eta() As Char
        Return System.Text.Encoding.GetEncoding(1253).GetChars(New Byte() {&HE7})(0)
    End Function

    ''' <summary> Gets the symbol for Mu (micro). </summary>
    ''' <returns> The symbol for Mu (micro). </returns>
    Public Function MU() As Char
        Return System.Text.Encoding.GetEncoding(1253).GetChars(New Byte() {&HEC})(0)
    End Function

    ''' <summary> Gets the symbol for Omega. </summary>
    ''' <returns> The symbol for Omega. </returns>
    Public Function Omega() As Char
        Return System.Text.Encoding.GetEncoding(1253).GetChars(New Byte() {&HD9})(0)
    End Function

    ''' <summary> Gets the symbol for Rho. </summary>
    ''' <returns> The symbol for Rho. </returns>
    Public Function Rho() As Char
        Return System.Text.Encoding.GetEncoding(1253).GetChars(New Byte() {&HF1})(0)
    End Function

End Module
