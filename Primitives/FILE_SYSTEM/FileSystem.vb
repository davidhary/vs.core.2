﻿''' <summary> General File System Functions.</summary>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license> 
''' <history date="01/15/2005" by="David" revision="1.0.1841.x"> Created. </history>
Public NotInheritable Class FileSystem

    ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
    Private Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Determines whether the specified folder path is writable. </summary>
    ''' <remarks> Uses a temporary random file name to test if the file can be created. The file is
    ''' deleted thereafter. </remarks>
    ''' <param name="path"> The path. </param>
    ''' <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Shared Function IsFolderWritable(ByVal path As String) As Boolean
        Dim filePath As String = ""
        Dim affirmative As Boolean = False
        Try
            filePath = System.IO.Path.Combine(path, System.IO.Path.GetRandomFileName())
            Using s As System.IO.FileStream = System.IO.File.Open(filePath, System.IO.FileMode.OpenOrCreate)
            End Using
            affirmative = True
        Catch
        Finally
            ' SS reported an exception from this test possibly indicating that Windows allowed writing the file 
            ' by failed report deletion. Or else, Windows raised another exception type.
            Try
                If System.IO.File.Exists(filePath) Then
                    System.IO.File.Delete(filePath)
                End If
            Catch
            End Try
        End Try
        Return affirmative
    End Function

    ''' <summary> Validates the name of the file. </summary>
    ''' <param name="filePath"> The file path. </param>
    ''' <returns> <c>True</c> if file name is valid, <c>False</c> otherwise. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Shared Function ValidateFileName(ByVal filePath As String) As Boolean
        Try
            ' Check that the file name is legal.
            System.IO.File.Exists(filePath)
            Return True
        Catch
            Return False
        End Try
    End Function

End Class
