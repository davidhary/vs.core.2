﻿Imports System.Runtime.CompilerServices
Namespace StringEncryptionExtensions
    ''' <summary> Includes Encryption extensions for <see cref="String">String</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    Public Module Methods

        ''' <summary> Encrypts the specified value. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">  The value. </param>
        ''' <param name="phrase"> The pass phrase. </param>
        ''' <returns> System.String. </returns>
        <Extension()>
        Public Function Encrypt(ByVal value As String, ByVal phrase As String) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return ""
            ElseIf String.IsNullOrWhiteSpace(phrase) Then
                Throw New ArgumentNullException(NameOf(phrase))
            End If
            Return System.Convert.ToBase64String(StringEncryptionExtensions.encrypt(System.Text.Encoding.UTF8.GetBytes(value), phrase))
        End Function

        ''' <summary> Encrypts the byte array using the
        ''' <see cref="T:System.Security.Cryptography.RijndaelManaged">Rijndael symmetric encryption
        ''' algorithm</see> </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values"> The values. </param>
        ''' <param name="phrase"> The pass phrase. </param>
        ''' <returns> An array of <see cref="T:System.Byte"/>. </returns>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")>
        Private Function encrypt(ByVal values As Byte(), ByVal phrase As String) As Byte()

            If values Is Nothing Then
                Return values
            ElseIf String.IsNullOrWhiteSpace(phrase) Then
                Throw New ArgumentNullException(NameOf(phrase))
            End If

            Dim result As Byte() = {}

            Using encryptionAlgorithm As New System.Security.Cryptography.RijndaelManaged
                encryptionAlgorithm.KeySize = 256
                encryptionAlgorithm.Key = StringEncryptionExtensions.encryptionKey(phrase)
                encryptionAlgorithm.IV = StringEncryptionExtensions.encryptionInitializationVector(phrase)

                Using memoryStream As New IO.MemoryStream

                    Using cryptoStream As New Security.Cryptography.CryptoStream(memoryStream,
                                                                          encryptionAlgorithm.CreateEncryptor,
                                                                          Security.Cryptography.CryptoStreamMode.Write)
                        cryptoStream.Write(values, 0, values.Length)
                        cryptoStream.FlushFinalBlock()
                        result = memoryStream.ToArray()
                        cryptoStream.Close()
                        memoryStream.Close()

                    End Using

                End Using
            End Using

            Return result

        End Function

        ''' <summary> Decrypts the specified value. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">  The value. </param>
        ''' <param name="phrase"> The pass phrase. </param>
        ''' <returns> System.String. </returns>
        <Extension()>
        Public Function Decrypt(ByVal value As String, ByVal phrase As String) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return ""
            ElseIf String.IsNullOrWhiteSpace(phrase) Then
                Throw New ArgumentNullException(NameOf(phrase))
            End If
            Return System.Text.Encoding.UTF8.GetString(StringEncryptionExtensions.decrypt(System.Convert.FromBase64String(value), phrase))
        End Function

        ''' <summary> Decrypts the byte array using the
        ''' <see cref="T:System.Security.Cryptography.RijndaelManaged">Rijndael symmetric encryption
        ''' algorithm</see> </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values"> The values. </param>
        ''' <param name="phrase"> The pass phrase. </param>
        ''' <returns> An array of <see cref="T:System.Byte"/>. </returns>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")>
        Private Function decrypt(ByVal values As Byte(), ByVal phrase As String) As Byte()

            If values Is Nothing Then
                Return values
            ElseIf String.IsNullOrWhiteSpace(phrase) Then
                Throw New ArgumentNullException(NameOf(phrase))
            End If

            Dim result As Byte() = {}

            Using encryptionAlgorithm As New System.Security.Cryptography.RijndaelManaged

                encryptionAlgorithm.KeySize = 256
                encryptionAlgorithm.Key = StringEncryptionExtensions.encryptionKey(phrase)
                encryptionAlgorithm.IV = StringEncryptionExtensions.encryptionInitializationVector(phrase)

                Using memoryStream As New IO.MemoryStream(values)

                    Using cryptoStream As New Security.Cryptography.CryptoStream(memoryStream,
                                                                               encryptionAlgorithm.CreateDecryptor,
                                                                               Security.Cryptography.CryptoStreamMode.Read)
                        Dim workspace As Byte()
                        ReDim workspace(values.Length)
                        Dim decryptedByteCount As Integer
                        decryptedByteCount = cryptoStream.Read(workspace, 0, values.Length)

                        cryptoStream.Close()
                        memoryStream.Close()

                        ReDim result(decryptedByteCount)
                        Array.Copy(workspace, result, decryptedByteCount)

                    End Using

                End Using

            End Using

            Return result

        End Function

        ''' <summary> Generates a byte array of required length as the encryption key. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="phrase"> The pass phrase. </param>
        ''' <returns> An array of <see cref="T:System.Byte"/>. </returns>
        Private Function encryptionKey(ByVal phrase As String) As Byte()
            If String.IsNullOrWhiteSpace(phrase) Then
                Throw New ArgumentNullException(NameOf(phrase))
            End If
            'A SHA256 hash of the pass phrase has just the required length. It is used twice in a manner of self-salting.
            Using SHA256 As New System.Security.Cryptography.SHA256Managed
                Dim L1 As String = System.Convert.ToBase64String(SHA256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(phrase)))
                Dim L2 As String = phrase & L1
                Return SHA256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(L2))
            End Using
        End Function

        ''' <summary> Generates a byte array of required length as the initialization vector. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="phrase"> The pass phrase. </param>
        ''' <returns> An array of <see cref="T:System.Byte"/>. </returns>
        Private Function encryptionInitializationVector(ByVal phrase As String) As Byte()
            If String.IsNullOrWhiteSpace(phrase) Then
                Throw New ArgumentNullException(NameOf(phrase))
            End If
            ' A MD5 hash of the pass phrase has just the required length. It is used twice in a manner of self-salting.
            Using MD5 As New System.Security.Cryptography.MD5CryptoServiceProvider
                Dim L1 As String = System.Convert.ToBase64String(MD5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(phrase)))
                Dim L2 As String = phrase & L1
                Return MD5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(L2))
            End Using
        End Function

    End Module
End Namespace
