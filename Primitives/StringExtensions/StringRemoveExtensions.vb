﻿Imports System.Runtime.CompilerServices
Namespace StringRemoveExtensions
    ''' <summary> Includes 'remove' extensions for <see cref="String">String</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    Public Module Methods

        ''' <summary> Returns the right most <paramref name="count"></paramref> characters from the string. </summary>
        ''' <param name="value"> The string being chopped. </param>
        ''' <param name="count"> The number of characters to return. </param>
        ''' <returns> A string containing the right most characters in this string. </returns>
        <Extension()>
        Public Function Right(ByVal value As String, ByVal count As Integer) As String
            If String.IsNullOrWhiteSpace(value) OrElse value.Length <= count Then Return value
            Return value.Substring(value.Length - count)
        End Function

        ''' <summary> Returns the left most <paramref name="count"></paramref> characters from the string. </summary>
        ''' <param name="value"> The string being chopped. </param>
        ''' <param name="count"> The number of characters to return. </param>
        ''' <returns> A string containing the left most characters in this string. </returns>
        <Extension()>
        Public Function Left(ByVal value As String, ByVal count As Integer) As String
            If String.IsNullOrWhiteSpace(value) OrElse value.Length <= count Then Return value
            Return value.Substring(0, count)
        End Function

        ''' <summary> Removes the left characters. </summary>
        ''' <param name="value"> The string. </param>
        ''' <param name="count"> The count. </param>
        ''' <returns> String with left characters removed. </returns>
        <Extension()>
        Public Function RemoveLeft(ByVal value As String, ByVal count As Integer) As String
            If String.IsNullOrWhiteSpace(value) OrElse value.Length <= count Then Return ""
            Return value.Substring(count)
        End Function

        ''' <summary> Removes the right characters. </summary>
        ''' <param name="value"> The string. </param>
        ''' <param name="count"> The count. </param>
        ''' <returns> String with right characters removed. </returns>
        <Extension()>
        Public Function RemoveRight(ByVal value As String, ByVal count As Integer) As String
            If String.IsNullOrWhiteSpace(value) OrElse value.Length <= count Then Return ""
            Return value.Substring(0, value.Length - count)
        End Function

    End Module
End Namespace
