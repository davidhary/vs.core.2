﻿Imports System.Runtime.CompilerServices
Namespace StringReverseExtensions
    ''' <summary> Includes 'Reverse' extensions for <see cref="String">String</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    Public Module Methods

        ''' <summary> Reverse the string from http://en.wikipedia.org/wiki/Extension_method. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> The string being chopped. </param>
        ''' <returns> String with reversed characters. </returns>
        <Extension()>
        Public Function Reverse(ByVal value As String) As String

            If value Is Nothing Then
                Throw New ArgumentNullException(NameOf(value))
            ElseIf String.IsNullOrWhiteSpace(value) Then
                Return value
            End If
            Dim chars As Char() = value.ToCharArray()
            Array.Reverse(chars)
            Return New String(chars)

        End Function

    End Module
End Namespace
