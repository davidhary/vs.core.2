﻿Imports System.Runtime.CompilerServices
Namespace StringIncludesExtensions
    ''' <summary> Includes 'include' extensions for <see cref="String">String</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    Public Module Methods

        ''' <summary> The illegal file characters. </summary>
        Public Const IllegalFileCharacters As String = "/\\:*?""<>|"

        ''' <summary> Returns True if the validated string contains only alpha characters. </summary>
        ''' <param name="source"> The string to validate. </param>
        ''' <returns> <c>True</c> if the validated string contains only alpha; otherwise, <c>False</c>. </returns>
        <Extension()>
        Public Function IncludesIllegalFileCharacters(ByVal source As String) As Boolean

            Return source.IncludesCharacters(StringIncludesExtensions.IllegalFileCharacters)

        End Function

        ''' <summary> Returns true if the string includes the specified characters. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">     The string that is being searched. </param>
        ''' <param name="characters"> The characters to search for. </param>
        ''' <returns> <c>True</c> if the string includes the specified characters; otherwise, <c>False</c>. </returns>
        <Extension()>
        Public Function IncludesCharacters(ByVal source As String, ByVal characters As String) As Boolean

            If source Is Nothing Then
                Throw New ArgumentNullException(NameOf(source))
            End If
            If characters Is Nothing Then
                Throw New ArgumentNullException(NameOf(characters))
            End If

            Dim expression As String = String.Format(System.Globalization.CultureInfo.CurrentCulture, "[{0}]", characters)
            Dim r As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex(
                expression, System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            Return r.IsMatch(source)

        End Function

        ''' <summary> Returns true if the string includes any of the specified characters. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">     The string that is being searched. </param>
        ''' <param name="characters"> The characters to search for. </param>
        ''' <returns> null if it fails, else. </returns>
        <Extension()>
        Public Function IncludesAny(ByVal source As String, ByVal characters As String) As Boolean

            If source Is Nothing Then
                Throw New ArgumentNullException(NameOf(source))
            End If
            If characters Is Nothing Then
                Throw New ArgumentNullException(NameOf(characters))
            End If

            ' 2954: was ^[{0}]+$ 
            Dim expression As String = String.Format(System.Globalization.CultureInfo.CurrentCulture, "[{0}]", characters)
            Dim r As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex(
                expression, System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            Return r.IsMatch(source)

        End Function

    End Module
End Namespace
