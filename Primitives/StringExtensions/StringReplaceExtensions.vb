﻿Imports System.Runtime.CompilerServices
Namespace StringReplaceExtensions
    ''' <summary> Includes 'Replace' extensions for <see cref="String">String</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    Public Module Methods

        ''' <summary> Replaces the given <paramref name="characters">characters</paramref> with the
        ''' <paramref name="replacing">new characters.</paramref> </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">     The original string. </param>
        ''' <param name="characters"> The characters to be replaced. </param>
        ''' <param name="replacing">  The new characters. </param>
        ''' <returns> String with replaced characters. </returns>
        <Extension()>
        Public Function Replace(ByVal source As String, ByVal characters As String, ByVal replacing As Char) As String
            If String.IsNullOrWhiteSpace(source) Then
                Throw New ArgumentNullException(NameOf(source))
            End If
            If String.IsNullOrWhiteSpace(characters) Then
                Throw New ArgumentNullException(NameOf(characters))
            End If
            If String.IsNullOrWhiteSpace(replacing) Then
                Throw New ArgumentNullException(NameOf(replacing))
            End If
            For Each c As Char In characters.ToCharArray
                source = source.Replace(c, replacing)
            Next
            Return source
        End Function

    End Module
End Namespace
