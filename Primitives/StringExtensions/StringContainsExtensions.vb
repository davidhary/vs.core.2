﻿Imports System.Runtime.CompilerServices
Namespace StringContainsExtensions
    ''' <summary> Includes 'Contains' extensions for <see cref="String">String</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    Public Module Methods

        ''' <summary> Returns true if the find string is in the search string. </summary>
        ''' <param name="search">         The item to search. </param>
        ''' <param name="find">           The item to find. </param>
        ''' <param name="comparisonType"> Type of the comparison. </param>
        ''' <returns> <c>True</c> the find string is in the search string.. </returns>
        <Extension()>
        Public Function Contains(ByVal search As String, ByVal find As String, ByVal comparisonType As StringComparison) As Boolean
            Return Location(search, find, comparisonType) >= 0
        End Function

        ''' <summary> Returns the location of the find string in the search string. </summary>
        ''' <param name="search">         The item to search. </param>
        ''' <param name="find">           The item to find. </param>
        ''' <param name="comparisonType"> Type of the comparison. </param>
        ''' <returns> The location of the find string in the search string. </returns>
        <Extension()>
        Public Function Location(ByVal search As String, ByVal find As String, ByVal comparisonType As StringComparison) As Integer
            If String.IsNullOrWhiteSpace(search) Then Return 0
            Return search.IndexOf(find, comparisonType)
        End Function

    End Module
End Namespace
