﻿Imports System.Runtime.CompilerServices
Namespace StringValidationExtensions
    ''' <summary> Includes Validation extensions for <see cref="String">String</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    Public Module Methods

        ''' <summary> true, if is valid email address from
        ''' http://www.DavidHayden.com/blog/dave/archive/2006/11/30/ExtensionMethodsCSharp.aspx. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> email address to test. </param>
        ''' <returns> true, if is valid email address. </returns>
        <Extension()>
        Public Function IsValidEmailAddress(ByVal value As String) As Boolean
            If value Is Nothing Then
                Throw New ArgumentNullException(NameOf(value))
            ElseIf String.IsNullOrWhiteSpace(value) Then
                Return False
            End If
            Return New Text.RegularExpressions.Regex("^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$").IsMatch(value)
        End Function

        ''' <summary> From http://www.OSIX.net/modules/article/?id=586
        ''' A complete URL (not only HTTP) Regex can be found at http://internet.ls-la.net/folklore/url-
        ''' Regexpr.html. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> Specifies the floating number candidate. </param>
        ''' <returns> null if it fails, else. </returns>
        <Extension()>
        Public Function IsValidUrl(ByVal value As String) As Boolean
            If value Is Nothing Then
                Throw New ArgumentNullException(NameOf(value))
            ElseIf String.IsNullOrWhiteSpace(value) Then
                Return False
            End If
            Return Uri.IsWellFormedUriString(value, UriKind.RelativeOrAbsolute)
#If False Then
      Dim regularExpression As String = "^(HTTP?://)" 
                             & "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" 
                             & "(([0-9]{1,3}\.){3}[0-9]{1,3}" 
                             & "|" & "([0-9a-z_!~*'()-]+\.)*" 
                             & "([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\." 
                             & "[a-z]{2,6})" & "(:[0-9]{1,4})?" & "((/?)|" 
                             & "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$"
      Return New Regex(regularExpression).IsMatch(value)
#End If
        End Function

        ''' <summary> Check if URL (HTTP) is available. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> URL to check. </param>
        ''' <returns> <c>True</c> if available. </returns>
        ''' <example> string URL = "www.CodeProject.com;
        ''' if( !url.UrlAvailable())
        '''     ...CodeProject is not available</example>
        <Extension()>
        Public Function IsUrlAvailable(ByVal value As String) As Boolean
            ' use URI instead.
            If value Is Nothing Then
                Throw New ArgumentNullException(NameOf(value))
            ElseIf String.IsNullOrWhiteSpace(value) Then
                Return False
            Else
                Return Uri.IsWellFormedUriString(value, UriKind.RelativeOrAbsolute)
            End If
#If False Then
      If (Not value.StartsWith("HTTP://", StringComparison.OrdinalIgnoreCase)) OrElse (Not value.StartsWith("HTTP://", StringComparison.OrdinalIgnoreCase)) Then
        value = "HTTP://" & value
      End If
      Try
        Dim myRequest As Net.HttpWebRequest = CType(Net.WebRequest.Create(value), Net.HttpWebRequest)
        myRequest.Method = "GET"
        myRequest.ContentType = "application/x-www-form-UrlEncoded"
        Dim myHttpWebResponse As Net.HttpWebResponse = CType(myRequest.GetResponse(), Net.HttpWebResponse)
        Return myHttpWebResponse IsNot Nothing
      Catch
        Return False
      End Try
#End If
        End Function

    End Module
End Namespace
