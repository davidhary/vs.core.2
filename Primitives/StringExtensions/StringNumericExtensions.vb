﻿Imports System.Runtime.CompilerServices
Namespace StringNumericExtensions
    ''' <summary> Includes extensions for <see cref="String">String</see> Numeric. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    Public Module Methods

        ''' <summary> Returns true if the string represents a floating number. </summary>
        ''' <param name="value"> Specifies the floating number candidate. </param>
        ''' <returns> <c>True</c> if the string represents a floating number; otherwise, <c>False</c>. </returns>
        <Extension()>
        Public Function IsFloat(ByVal value As String) As Boolean
            Dim a As Double
            Return System.Double.TryParse(value, Globalization.NumberStyles.Float, System.Globalization.CultureInfo.CurrentCulture, a)
        End Function

        ''' <summary> Returns true if the string represents a whole number. </summary>
        ''' <param name="value"> Specifies the integer candidate. </param>
        ''' <returns> <c>True</c> if the string represents a whole number; otherwise, <c>False</c>. </returns>
        <Extension()>
        Public Function IsInteger(ByVal value As String) As Boolean

            Dim a As Double
            Return System.Double.TryParse(value, Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture, a)

        End Function

        ''' <summary> Returns true if the string represents any number. </summary>
        ''' <param name="value"> Specifies the number candidate. </param>
        ''' <returns> <c>True</c> if the string represents any number; otherwise, <c>False</c>. </returns>
        <Extension()>
        Public Function IsNumber(ByVal value As String) As Boolean

            Dim a As Double
            Return System.Double.TryParse(value, Globalization.NumberStyles.Number Or
                                          Globalization.NumberStyles.AllowExponent, System.Globalization.CultureInfo.CurrentCulture, a)

        End Function

        ''' <summary> Returns true if the string represents any number. </summary>
        ''' <param name="value"> Specifies the number candidate. </param>
        ''' <returns> <c>True</c> if the string represents any number; otherwise, <c>False</c>. </returns>
        <Extension()>
        Public Function IsNumeric(ByVal value As String) As Boolean
            Return value.IsNumber()
        End Function

    End Module
End Namespace
