﻿Imports System.Runtime.CompilerServices
Namespace StringCompactExtensions
    ''' <summary> Includes extensions for <see cref="String">String</see> Compacting. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    Public Module Methods

        ''' <summary> Compacts the string to permit display within the given width. For example, using
        ''' <see cref="System.Windows.Forms.TextFormatFlags.PathEllipsis">Path Ellipsis</see>
        ''' as the <paramref name="formatInstruction">format instruction</paramref>
        ''' the string <c>c:\program files\test app\RunMe.exe' might turn into 'c:\program files\...\
        ''' RunMe.exe'</c> depending on the font and width. </summary>
        ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        ''' are null. </exception>
        ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ''' the required range. </exception>
        ''' <param name="value">             Specifies the string to compact. </param>
        ''' <param name="width">             Specifies the width. </param>
        ''' <param name="font">              Specifies the <see cref="Drawing.Font">font</see> </param>
        ''' <param name="formatInstruction"> Specifies the designed
        ''' <see cref="System.Windows.Forms.TextFormatFlags">formatting</see> </param>
        ''' <returns> Compacted string. </returns>
        <Extension()>
        Public Function Compact(ByVal value As String, ByVal width As Integer,
                            ByVal font As Drawing.Font, ByVal formatInstruction As System.Windows.Forms.TextFormatFlags) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return ""
            End If
            If font Is Nothing Then
                Throw New ArgumentNullException(NameOf(font))
            End If
            If width <= 0 Then
                Throw New ArgumentOutOfRangeException("width", width, "Must be non-zero")
            End If
            Dim result As String = String.Copy(value)
            System.Windows.Forms.TextRenderer.MeasureText(result, font, New Drawing.Size(width, font.Height),
                                                          formatInstruction Or formatInstruction Or System.Windows.Forms.TextFormatFlags.ModifyString)
            Return result
        End Function

        ''' <summary> Compacts the string to permit display within the given width. For example, the string
        ''' <c>'c:\program files\test app\RunMe.exe' might turn into 'c:\program files\...\RunMe.exe'</c>
        ''' depending on the font and width. </summary>
        ''' <param name="value"> Specifies the string to compact. </param>
        ''' <param name="width"> Specifies the width. </param>
        ''' <param name="font">  Specifies the <see cref="Drawing.Font">font</see> </param>
        ''' <returns> Compacted string. </returns>
        <Extension()>
        Public Function Compact(ByVal value As String, ByVal width As Integer,
                            ByVal font As Drawing.Font) As String
            Return value.Compact(width, font, System.Windows.Forms.TextFormatFlags.PathEllipsis)
        End Function

        ''' <summary> Compacts the string to permit display within the given control. For example, using
        ''' <see cref="System.Windows.Forms.TextFormatFlags.PathEllipsis">Path Ellipsis</see>
        ''' as the <paramref name="formatInstruction">format instruction</paramref>
        ''' the string <c>'c:\program files\test app\RunMe.exe' might turn into 'c:\program files\...\
        ''' RunMe.exe'</c> depending on the font and width. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">             Specifies the text. </param>
        ''' <param name="control">           Specifies the control within which to format the text. </param>
        ''' <param name="formatInstruction"> Specifies the designed
        ''' <see cref="System.Windows.Forms.TextFormatFlags">formatting</see> </param>
        ''' <returns> Compacted string. </returns>
        <Extension()>
        Public Function Compact(ByVal value As String,
                            ByVal control As System.Windows.Forms.Control,
                            ByVal formatInstruction As System.Windows.Forms.TextFormatFlags) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return ""
            End If
            If control Is Nothing Then
                Throw New ArgumentNullException(NameOf(control))
            End If
            Return value.Compact(control.Width, control.Font, formatInstruction)
        End Function

        ''' <summary> Compacts the string to permit display within the given control. For example, the
        ''' string <c>'c:\program files\test app\RunMe.exe' might turn into 'c:\program files\...\RunMe.exe'</c>
        ''' depending on the font and width. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   Specifies the string to compact. </param>
        ''' <param name="control"> Specifies the control receiving the string. </param>
        ''' <returns> Compacted string. </returns>
        <Extension()>
        Public Function Compact(ByVal value As String, ByVal control As System.Windows.Forms.Control) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return ""
            End If
            If control Is Nothing Then
                Throw New ArgumentNullException(NameOf(control))
            End If
            Return value.Compact(control, System.Windows.Forms.TextFormatFlags.PathEllipsis)
        End Function

    End Module
End Namespace
