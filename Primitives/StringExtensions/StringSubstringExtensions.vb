﻿Imports System.Runtime.CompilerServices
Namespace StringSubstringExtensions
    ''' <summary> Includes 'Substring' extensions for <see cref="String">String</see>. </summary>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/19/2010" by="David" revision="1.2.3975.x"> Created. </history>
    Public Module Methods

        ''' <summary> Returns a substring of the input string taking not of start index and length
        ''' exceptions. </summary>
        ''' <param name="value">      The input string to sub string. </param>
        ''' <param name="startIndex"> The zero based starting index. </param>
        ''' <param name="length">     The total length. </param>
        ''' <returns> A substring of the input string taking not of start index and length exceptions. </returns>
        <Extension()>
        Public Function SafeSubstring(ByVal value As String, ByVal startIndex As Integer, ByVal length As Integer) As String

            If String.IsNullOrWhiteSpace(value) OrElse length <= 0 Then
                Return ""
            End If

            Dim inputLength As Integer = value.Length
            If startIndex > inputLength Then
                Return ""
            Else
                If startIndex + length > inputLength Then
                    length = inputLength - startIndex
                End If
                Return value.Substring(startIndex, length)
            End If

        End Function

        ''' <summary> Returns the substring after the last occurrence of the specified string. </summary>
        ''' <param name="source"> The string to substring. </param>
        ''' <param name="search"> The string to search for. </param>
        ''' <returns> The substring after the last occurrence of the specified string. </returns>
        <Extension()>
        Public Function SubstringAfter(ByVal source As String, ByVal search As String) As String

            If String.IsNullOrWhiteSpace(source) Then
                Return ""
            End If

            If String.IsNullOrWhiteSpace(search) Then
                Return ""
            End If

            Dim location As Integer = source.LastIndexOf(search, StringComparison.OrdinalIgnoreCase)
            If location >= 0 Then
                If location + search.Length < source.Length Then
                    Return source.Substring(location + search.Length)
                Else
                    Return ""
                End If
            Else
                Return ""
            End If

        End Function

        ''' <summary> Returns the substring before the last occurrence of the specified string. </summary>
        ''' <param name="source"> The string to substring. </param>
        ''' <param name="search"> The string to search for. </param>
        ''' <returns> The substring before the last occurrence of the specified string. </returns>
        <Extension()>
        Public Function SubstringBefore(ByVal source As String, ByVal search As String) As String

            If String.IsNullOrWhiteSpace(source) Then
                Return ""
            End If

            If String.IsNullOrWhiteSpace(search) Then
                Return ""
            End If

            Dim location As Integer = source.LastIndexOf(search, StringComparison.OrdinalIgnoreCase)
            If location >= 0 Then
                Return source.Substring(0, location)
            Else
                Return ""
            End If
        End Function

        ''' <summary> Returns the substring after the last occurrence of the specified string. </summary>
        ''' <param name="source">         The string to substring. </param>
        ''' <param name="startDelimiter"> The start delimiter to search for. </param>
        ''' <param name="endDelimiter">   The end delimiter to search for. </param>
        ''' <returns> The substring after the last occurrence of the specified string. </returns>
        <Extension()>
        Public Function SubstringBetween(ByVal source As String, ByVal startDelimiter As String, ByVal endDelimiter As String) As String

            If String.IsNullOrWhiteSpace(source) Then
                Return ""
            End If

            If String.IsNullOrWhiteSpace(startDelimiter) Then
                Return ""
            End If

            Dim startLocation As Integer = source.LastIndexOf(startDelimiter, StringComparison.OrdinalIgnoreCase) + startDelimiter.Length
            Dim endLocation As Integer = source.LastIndexOf(endDelimiter, StringComparison.OrdinalIgnoreCase)

            If startLocation >= 0 AndAlso startLocation < endLocation Then
                Return source.Substring(startLocation, endLocation - startLocation)
            Else
                Return ""
            End If

        End Function

    End Module
End Namespace
