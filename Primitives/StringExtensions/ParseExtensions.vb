﻿Imports System.Globalization
Imports System.ComponentModel
Namespace ParseExtensions
    ''' <summary> Includes extensions for <see cref="String">String</see> parsing. </summary>
    ''' <license> (c) 2009 ELMARG.<para>
    ''' Licensed under The Code Project Open License. </para><para>
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.2.3981.x">
    ''' From http://www.CodeProject.com/KB/string/StringConversion.aspx. </history>
    Public Module Methods

#Region " WITH DEFAULT CULTURE "

        ''' <summary> Converts the specified string value to its strong-typed counterpart. If the
        ''' conversion fails the <paramref name="defaultValue"/> will be returned. </summary>
        ''' <param name="value">        The value. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> The converted value. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function ConvertTo(Of T)(ByVal value As String, ByVal defaultValue As T) As T
            Return ParseExtensions.ConvertTo(Of T)(value, CultureInfo.InvariantCulture, defaultValue)
        End Function

#End Region

#Region " WITH CULTURE AS STRING VALUE "

        ''' <summary> Converts the specified string value to its strong-typed counterpart. If the
        ''' conversion fails an exception will be raised. Supply a default value to suppress the
        ''' exception. </summary>
        ''' <param name="value">   The value. </param>
        ''' <param name="culture"> The culture. </param>
        ''' <returns> The converted value. </returns>
        <System.Runtime.CompilerServices.Extension()>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")>
        Public Function ConvertTo(Of T)(ByVal value As String, ByVal culture As String) As T
            Return ParseExtensions.ConvertTo(Of T)(value, CultureInfo.GetCultureInfo(culture))
        End Function

        ''' <summary> Converts the specified string value to its strong-typed counterpart. If the
        ''' conversion fails the <paramref name="defaultValue"/> will be returned. </summary>
        ''' <param name="value">        The value. </param>
        ''' <param name="culture">      The culture. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> The converted value. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function ConvertTo(Of T)(ByVal value As String, ByVal culture As String, ByVal defaultValue As T) As T
            Return ParseExtensions.ConvertTo(Of T)(value, CultureInfo.GetCultureInfo(culture), defaultValue)
        End Function

#End Region

#Region " WITH STRONG TYPED CULTURE "

        ''' <summary> Converts the specified string value to its strong-typed counterpart. If the
        ''' conversion fails an exception will be raised. Supply a default value to suppress the
        ''' exception. </summary>
        ''' <param name="value">   The value. </param>
        ''' <returns> The converted value. </returns>
        <System.Runtime.CompilerServices.Extension()>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")>
        Public Function ConvertTo(Of T)(ByVal value As String) As T
            Return ParseExtensions.ConvertTo(Of T)(value, CultureInfo.CurrentCulture, Nothing, True)
        End Function

        ''' <summary> Converts the specified string value to its strong-typed counterpart. If the
        ''' conversion fails an exception will be raised. Supply a default value to suppress the
        ''' exception. </summary>
        ''' <param name="value">   The value. </param>
        ''' <param name="culture"> The culture. </param>
        ''' <returns> The converted value. </returns>
        <System.Runtime.CompilerServices.Extension()>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")>
        Public Function ConvertTo(Of T)(ByVal value As String, ByVal culture As CultureInfo) As T
            Return ParseExtensions.ConvertTo(Of T)(value, culture, Nothing, True)
        End Function

        ''' <summary> Converts the specified string value to its strong-typed counterpart. If the
        ''' conversion fails the <paramref name="defaultValue"/> will be returned. </summary>
        ''' <param name="value">        The value. </param>
        ''' <param name="culture">      The culture. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> The converted value. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function ConvertTo(Of T)(ByVal value As String, ByVal culture As CultureInfo, ByVal defaultValue As T) As T
            Return ParseExtensions.ConvertTo(Of T)(value, culture, defaultValue, False)
        End Function

        ''' <summary> Converts the specified string value to its strong-typed counterpart. If the
        ''' conversion fails the <paramref name="defaultValue"/> will be returned. </summary>
        ''' <param name="value">        The value. </param>
        ''' <param name="styles">       The number style. </param>
        ''' <param name="culture">      The culture. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> The converted value. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function ConvertTo(Of T)(ByVal value As String, ByVal styles As NumberStyles, ByVal culture As CultureInfo, ByVal defaultValue As T) As T
            Return ParseExtensions.ConvertTo(Of T)(value, styles, culture, defaultValue, False)
        End Function

        ''' <summary> Converts the specified string value to its strong-typed counterpart. If the
        ''' conversion fails an exception will be thrown. </summary>
        ''' <param name="value">        The value. </param>
        ''' <param name="styles">       The number style. </param>
        ''' <param name="culture">      The culture. </param>
        ''' <returns> The converted value. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function ConvertTo(Of T)(ByVal value As String, ByVal styles As NumberStyles, ByVal culture As CultureInfo) As T
            Return ParseExtensions.ConvertTo(Of T)(value, styles, culture, Nothing, True)
        End Function

        ''' <summary> Converts the specified string value to its strong-typed counterpart. If the
        ''' conversion fails the <paramref name="defaultValue"/> will be returned. </summary>
        ''' <param name="value">        The value. </param>
        ''' <param name="styles">       The number style. </param>
        ''' <param name="culture">      The culture. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <param name="result">       The result. </param>
        ''' <returns> <c>True</c> if parsed; otherwise, <c>False</c>. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function TryParse(Of T)(ByVal value As String, ByVal styles As NumberStyles, ByVal culture As CultureInfo,
                                       ByVal defaultValue As T, ByVal result As T) As Boolean
            Return ParseExtensions._TryParse(Of T)(value, styles, culture, defaultValue, result)
        End Function

        ''' <summary> Converts the specified string value to its strong-typed counterpart. If the
        ''' conversion fails the <paramref name="result"/> will be returned. </summary>
        ''' <param name="value">  The value. </param>
        ''' <param name="result"> The result. </param>
        ''' <returns> <c>True</c> if parsed; otherwise, <c>False</c>. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function TryParse(Of T)(ByVal value As String, ByVal result As T) As Boolean
            Return ParseExtensions._TryParse(Of T)(value, Globalization.CultureInfo.CurrentCulture, result)
        End Function

#End Region

#Region " WORKERS "

        ''' <summary> Converts the specified string value to its strong-typed counterpart. If the
        ''' conversion fails either an exception is raised or the default value will be returned. </summary>
        ''' <param name="value">   The value. </param>
        ''' <param name="culture"> The culture. </param>
        ''' <param name="result">  [in,out] The result. </param>
        ''' <returns> <c>True</c> if parsed; otherwise, <c>False</c>. </returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Function _TryParse(Of T)(ByVal value As String, ByVal culture As CultureInfo, ByRef result As T) As Boolean
            Dim converter As TypeConverter = TypeDescriptor.GetConverter(GetType(T))
            Try
                'nasty but needed because the B.C.L is not turning on the right NumberStyle flags
                If TypeOf converter Is BaseNumberConverter Then
                    result = CType(HandleThousandsSeparatorIssue(Of T)(converter, value, NumberStyles.Number, culture), T)
                ElseIf TypeOf converter Is BooleanConverter Then
                    result = CType(HandleBooleanValues(converter, value, culture), T)
                Else
                    result = CType(converter.ConvertFromString(Nothing, culture, value), T)
                End If
                Return True
            Catch
                Return False
            End Try
        End Function

        ''' <summary> Converts the specified string value to its strong-typed counterpart. If the
        ''' conversion fails either an exception is raised or the default value will be returned. </summary>
        ''' <param name="value">        The value. </param>
        ''' <param name="styles">       The number style. </param>
        ''' <param name="culture">      The culture. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <param name="result">       [in,out] The result. </param>
        ''' <returns> <c>True</c> if parsed; otherwise, <c>False</c>. </returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Function _TryParse(Of T)(ByVal value As String, ByVal styles As NumberStyles, ByVal culture As CultureInfo, ByVal defaultValue As T, ByRef result As T) As Boolean
            Dim converter As TypeConverter = TypeDescriptor.GetConverter(GetType(T))
            Try
                'nasty but needed because the B.C.L is not turning on the right NumberStyle flags
                If TypeOf converter Is BaseNumberConverter Then
                    result = CType(HandleThousandsSeparatorIssue(Of T)(converter, value, styles, culture), T)
                ElseIf TypeOf converter Is BooleanConverter Then
                    result = CType(HandleBooleanValues(converter, value, culture), T)
                Else
                    result = CType(converter.ConvertFromString(Nothing, culture, value), T)
                End If
                Return True
            Catch
                result = defaultValue
                Return False
            End Try
        End Function

        ''' <summary> Converts the specified string value to its strong-typed counterpart. If the
        ''' conversion fails either an exception is raised or the default value will be returned. </summary>
        ''' <param name="value">          The value. </param>
        ''' <param name="styles">         The number style. </param>
        ''' <param name="culture">        The culture. </param>
        ''' <param name="defaultValue">   The default value. </param>
        ''' <param name="raiseException"> if set to <c>True</c> [raise exception]. </param>
        ''' <returns> The converted value. </returns>
        Private Function ConvertTo(Of T)(ByVal value As String, ByVal styles As NumberStyles, ByVal culture As CultureInfo, ByVal defaultValue As T, ByVal raiseException As Boolean) As T
            Dim converter As TypeConverter = TypeDescriptor.GetConverter(GetType(T))
            Try
                'nasty but needed because the B.C.L is not turning on the right NumberStyle flags
                If TypeOf converter Is BaseNumberConverter Then
                    Return CType(HandleThousandsSeparatorIssue(Of T)(converter, value, styles, culture), T)
                ElseIf TypeOf converter Is BooleanConverter Then
                    Return CType(HandleBooleanValues(converter, value, culture), T)
                Else
                    Return CType(converter.ConvertFromString(Nothing, culture, value), T)
                End If
            Catch
                If raiseException Then
                    Throw
                End If
                Return defaultValue
            End Try
        End Function

        ''' <summary> Converts the specified string value to its strong-typed counterpart. If the
        ''' conversion fails either an exception is raised or the default value will be returned. A
        ''' default value is returned for the empty string. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">          The value. </param>
        ''' <param name="culture">        The culture. </param>
        ''' <param name="defaultValue">   The default value. </param>
        ''' <param name="raiseException"> if set to <c>True</c> [raise exception]. </param>
        ''' <returns> The converted value. </returns>
        Private Function ConvertTo(Of T)(ByVal value As String, ByVal culture As CultureInfo, ByVal defaultValue As T, ByVal raiseException As Boolean) As T
            Dim converter As TypeConverter = TypeDescriptor.GetConverter(GetType(T))
            Try
                If String.IsNullOrWhiteSpace(value) Then
                    If raiseException Then
                        Throw New ArgumentNullException(NameOf(value))
                    Else
                        Return defaultValue
                    End If
                    'nasty but needed because the B.C.L is not turning on the right NumberStyle flags
                ElseIf TypeOf converter Is BaseNumberConverter Then
                    Return CType(HandleThousandsSeparatorIssue(Of T)(converter, value, NumberStyles.Number, culture), T)
                ElseIf TypeOf converter Is BooleanConverter Then
                    Return CType(HandleBooleanValues(converter, value, culture), T)
                Else
                    Return CType(converter.ConvertFromString(Nothing, culture, value), T)
                End If
            Catch
                If raiseException Then
                    Throw
                End If
                Return defaultValue
            End Try
        End Function

        ''' <summary> The <see cref="BooleanConverter">Boolean Type Converter</see> is only able to handle
        ''' True and False strings. With this function we are able to handle other frequently used values
        ''' as well, such as Yes, No, On, Off, 0 and 1. </summary>
        ''' <param name="converter"> If the provided string cannot be translated, we'll hand it back to
        ''' the type converter. </param>
        ''' <param name="value">     The string value which represents a Boolean value. </param>
        ''' <param name="culture">   . </param>
        ''' <returns> The converted Boolean value. </returns>
        Private Function HandleBooleanValues(ByVal converter As TypeConverter, ByVal value As String, ByVal culture As CultureInfo) As Object
            Dim trueValues As String() = {"TRUE", "YES", "Y", "ON", "1", "PASS", "P", "T", "AUTO"}
            Dim falseValues As String() = {"FALSE", "NO", "N", "OFF", "0", "FAIL", "F", "F", "MAN"}

            If (Not String.IsNullOrWhiteSpace(value)) Then
                If trueValues.Contains(value.ToUpperInvariant()) Then
                    Return True
                End If
                If falseValues.Contains(value.ToUpperInvariant()) Then
                    Return False
                End If
            End If
            Return converter.ConvertFromString(Nothing, culture, value)
        End Function

        ''' <summary> Handles the specific numeric types. </summary>
        ''' <remarks> Thousands Separator issue http://social.MSDN.microsoft.com/Forums/en-
        ''' US/NETFXBCL/thread/c980b925-6df5-428d-bf87-7ff83db4504c/. </remarks>
        ''' <param name="converter"> . </param>
        ''' <param name="value">     The string value which represents a Boolean value. </param>
        ''' <param name="styles">    The number style. </param>
        ''' <param name="culture">   . </param>
        ''' <returns> The converted value. </returns>
        Private Function HandleThousandsSeparatorIssue(Of T)(ByVal converter As TypeConverter, ByVal value As String,
                                                             ByVal styles As NumberStyles, ByVal culture As CultureInfo) As Object

            Dim format As IFormatProvider = culture.NumberFormat

            If GetType(T).Equals(GetType(Double)) Then
                Return Double.Parse(value, styles, format)
            End If
            If GetType(T).Equals(GetType(Single)) Then
                Return Single.Parse(value, styles, format)
            End If
            If GetType(T).Equals(GetType(Decimal)) Then
                Return Decimal.Parse(value, styles, format)
            End If
            If GetType(T).Equals(GetType(Integer)) Then
                If value.StartsWith("&", StringComparison.OrdinalIgnoreCase) Then
                    styles = NumberStyles.HexNumber
                    value = value.TrimStart("&Hh".ToCharArray)
                    Return Integer.Parse(value, styles, format)
                ElseIf value.StartsWith("0x", StringComparison.OrdinalIgnoreCase) Then
                    styles = NumberStyles.HexNumber
                    value = value.Substring(2)
                    Return Integer.Parse(value, styles, format)
                Else
                    Return Integer.Parse(value, styles Xor NumberStyles.AllowDecimalPoint, format)
                End If
            End If
            If GetType(T).Equals(GetType(UInteger)) Then
                If value.StartsWith("&", StringComparison.OrdinalIgnoreCase) Then
                    styles = NumberStyles.HexNumber
                    value = value.TrimStart("&Hh".ToCharArray)
                    Return Integer.Parse(value, styles, format)
                ElseIf value.StartsWith("0x", StringComparison.OrdinalIgnoreCase) Then
                    styles = NumberStyles.HexNumber
                    value = value.Substring(2)
                    Return UInteger.Parse(value, styles, format)
                Else
                    Return UInteger.Parse(value, styles Xor NumberStyles.AllowDecimalPoint, format)
                End If
            End If
            If GetType(T).Equals(GetType(Long)) Then
                If value.StartsWith("&", StringComparison.OrdinalIgnoreCase) Then
                    styles = NumberStyles.HexNumber
                    value = value.TrimStart("&Hh".ToCharArray)
                    Return Long.Parse(value, styles, format)
                ElseIf value.StartsWith("0x", StringComparison.OrdinalIgnoreCase) Then
                    styles = NumberStyles.HexNumber
                    value = value.Substring(2)
                    Return Long.Parse(value, styles, format)
                Else
                    Return Long.Parse(value, styles Xor NumberStyles.AllowDecimalPoint, format)
                End If
            End If
            If GetType(T).Equals(GetType(ULong)) Then
                If value.StartsWith("&", StringComparison.OrdinalIgnoreCase) Then
                    styles = NumberStyles.HexNumber
                    value = value.TrimStart("&Hh".ToCharArray)
                    Return Long.Parse(value, styles, format)
                ElseIf value.StartsWith("0x", StringComparison.OrdinalIgnoreCase) Then
                    styles = NumberStyles.HexNumber
                    value = value.Substring(2)
                    Return ULong.Parse(value, styles, format)
                Else
                    Return ULong.Parse(value, styles Xor NumberStyles.AllowDecimalPoint, format)
                End If
            End If
            If GetType(T).Equals(GetType(Short)) Then
                If value.StartsWith("&", StringComparison.OrdinalIgnoreCase) Then
                    styles = NumberStyles.HexNumber
                    value = value.TrimStart("&Hh".ToCharArray)
                    Return Short.Parse(value, styles, format)
                ElseIf value.StartsWith("0x", StringComparison.OrdinalIgnoreCase) Then
                    styles = NumberStyles.HexNumber
                    value = value.Substring(2)
                    Return Short.Parse(value, styles, format)
                Else
                    Return Short.Parse(value, styles Xor NumberStyles.AllowDecimalPoint, format)
                End If
            End If
            If GetType(T).Equals(GetType(UShort)) Then
                If value.StartsWith("&", StringComparison.OrdinalIgnoreCase) Then
                    styles = NumberStyles.HexNumber
                    value = value.TrimStart("&Hh".ToCharArray)
                    Return Short.Parse(value, styles, format)
                ElseIf value.StartsWith("0x", StringComparison.OrdinalIgnoreCase) Then
                    styles = NumberStyles.HexNumber
                    value = value.Substring(2)
                    Return UShort.Parse(value, styles, format)
                Else
                    Return UShort.Parse(value, styles Xor NumberStyles.AllowDecimalPoint, format)
                End If
            End If

            Return CType(converter.ConvertFromString(Nothing, culture, value), T)
        End Function

#End Region

#Region " EXAMPLES "
#If False Then
    Private Function example() As Boolean

      Dim b As Boolean = "true".ConvertTo(Of Boolean)()

      b = "on".ConvertTo(Of Boolean)()
      b = "off".ConvertTo(Of Boolean)()
      b = "pass".ConvertTo(Of Boolean)()
      b = "fail".ConvertTo(Of Boolean)()

      Dim dt As DateTime = " 01:23:45 ".ConvertTo(Of DateTime)()
      Dim p As Drawing.Point = "100,25".ConvertTo(Of Drawing.Point)()

      '' convert, but use specific culture. 
      '' in this case the comma is used as a decimal separator
      Dim d As Double = "1.234,567".ConvertTo(Of Double)("en_US")

      '' provide a default value, if conversion fails
      Dim g As Guid = "I'm not a GUID".ConvertTo(Of Guid)(Guid.Empty)

      '' use default value if TypeConverter will fail
      Dim a As DateTime = "I'm not a date".ConvertTo(Of DateTime)(DateTime.Today)

      '' or with localization support
      Dim d1 As Double = "1a".ConvertTo(Of Double)(CultureInfo.InstalledUICulture, -1)

      Return True
    End Function
#End If
#End Region

    End Module
End Namespace
