﻿Imports System.Runtime.CompilerServices
Namespace StringConsistsExtensions
    ''' <summary> Includes 'Consists' extensions for <see cref="String">String</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    Public Module Methods

        ''' <summary> Returns True if the validated string contains only alpha characters. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> The string to validate. </param>
        ''' <returns> null if it fails, else. </returns>
        <Extension()>
        Public Function ConsistsOfAlphaCharacters(ByVal value As String) As Boolean

            If value Is Nothing Then
                Throw New ArgumentNullException(NameOf(value))
            ElseIf String.IsNullOrWhiteSpace(value) Then
                Return False
            End If

            Dim r As Text.RegularExpressions.Regex = New Text.RegularExpressions.Regex("^[a-z]+$",
                                                                                       Text.RegularExpressions.RegexOptions.IgnoreCase)
            Return r.IsMatch(value)

        End Function

        ''' <summary> Returns True if the validated string contains only alpha characters. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value"> The string to validate. </param>
        ''' <returns> null if it fails, else. </returns>
        <Extension()>
        Public Function ConsistsOfNumbers(ByVal value As String) As Boolean

            If value Is Nothing Then
                Throw New ArgumentNullException(NameOf(value))
            ElseIf String.IsNullOrWhiteSpace(value) Then
                Return False
            End If

            Dim r As Text.RegularExpressions.Regex = New Text.RegularExpressions.Regex("^[0-9]+$",
                                                                                       Text.RegularExpressions.RegexOptions.IgnoreCase)
            Return r.IsMatch(value)

        End Function

    End Module
End Namespace
