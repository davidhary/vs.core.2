﻿Imports System.Runtime.CompilerServices
Namespace StringAlignmentExtensions
    ''' <summary> Includes Alignment extensions for <see cref="String">String</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    Public Module Methods

        ''' <summary> Returns a centered string. </summary>
        ''' <param name="value">  Specifies the original string. </param>
        ''' <param name="length"> Specifies the total length of the new string. </param>
        ''' <returns> The input string centered with appropriate number of spaces on each side. </returns>
        <Extension()>
        Public Function Center(ByVal value As String, ByVal length As Integer) As String

            Dim leftSpacesCount As Double
            Dim rightSpacesCount As Double
            If length <= 0 Then
                Return ""
            ElseIf String.IsNullOrWhiteSpace(value) Then
                Return "".PadLeft(length, " "c)
            ElseIf value.Length > length Then
                Return value.Substring(0, length)
            ElseIf value.Length = length Then
                Return value
            Else
                leftSpacesCount = (length - value.Length) \ 2S
                rightSpacesCount = length - value.Length - leftSpacesCount
                Return "".PadLeft(CInt(leftSpacesCount), " "c) & value &
                       "".PadLeft(CInt(rightSpacesCount), " "c)
            End If

        End Function

        ''' <summary> Returns a left aligned string.  If the string is too large, the only 'length' values
        ''' are returned with the last character set to '~' to signify the lost character. </summary>
        ''' <param name="caption">      The caption. </param>
        ''' <param name="captionWidth"> Width of the caption. </param>
        ''' <returns> A left aligned string. </returns>
        <Extension()>
        Public Function LeftAlign(ByVal caption As String, ByVal captionWidth As Integer) As String

            Const spaceCharacter As Char = " "c
            Const lostCharacter As Char = "~"c

            ' make sure we have one left space.
            If captionWidth <= 0 Then
                Return ""
            ElseIf String.IsNullOrWhiteSpace(caption) Then
                Return ""
            ElseIf caption.Length > captionWidth Then
                Return caption.Substring(0, captionWidth - 1) & lostCharacter
            ElseIf caption.Length = captionWidth Then
                Return caption
            Else
                Return caption.PadRight(captionWidth, spaceCharacter)
            End If

        End Function

        ''' <summary> Returns a right aligned string.  If the string is too large, the only 'length' values
        ''' are returned with the first character set to '~' to signify the lost character. </summary>
        ''' <param name="caption">      The caption. </param>
        ''' <param name="captionWidth"> Width of the caption. </param>
        ''' <returns> A right aligned string. </returns>
        <Extension()>
        Public Function RightAlign(ByVal caption As String, ByVal captionWidth As Integer) As String

            Const spaceCharacter As Char = " "c
            Const lostCharacter As Char = "~"c

            ' make sure we have one left space.
            If captionWidth <= 0 Then
                Return ""
            ElseIf String.IsNullOrWhiteSpace(caption) Then
                Return ""
            ElseIf caption.Length > captionWidth Then
                Return lostCharacter & caption.Substring(0, captionWidth - 1)
            ElseIf caption.Length = captionWidth Then
                Return caption
            Else
                Return caption.PadLeft(captionWidth, spaceCharacter)
            End If

        End Function

    End Module
End Namespace
