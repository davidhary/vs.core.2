﻿Imports Microsoft.SqlServer.MessageBox
Imports System.Windows.Forms
''' <summary> Extends the <see cref="ExceptionMessageBox">Exception message dialog</see>. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="02/14/2014" by="David" revision="2.0.5158.x"> Exception Message. </history>
Public Class MyExceptionMessageBox
    Inherits ExceptionMessageBox

#Region " CONSTRUCTORS "

    ''' <summary> Constructor. </summary>
    ''' <param name="exception"> The exception. </param>
    Public Sub New(ByVal exception As Exception)
        MyBase.New(exception)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    Public Sub New(ByVal text As String, ByVal caption As String)
        MyBase.New(text, caption)
    End Sub

#End Region

#Region " SHARED "

    ''' <summary> Builds message box icon symbol hash. </summary>
    ''' <returns> A <see cref="Dictionary(Of MessageBoxIcon, ExceptionMessageBoxSymbol)">dictionary</see>. </returns>
    Public Shared Function BuildMessageBoxIconSymbolHash() As Dictionary(Of MessageBoxIcon, ExceptionMessageBoxSymbol)
        Dim dix2 As New Dictionary(Of MessageBoxIcon, ExceptionMessageBoxSymbol)
        Dim dix3 As Dictionary(Of MessageBoxIcon, ExceptionMessageBoxSymbol) = dix2
        ' same as information: dix3.Add(MessageBoxIcon.Asterisk, ExceptionMessageBoxSymbol.Asterisk)
        ' same as warning: dix3.Add(MessageBoxIcon.Exclamation, ExceptionMessageBoxSymbol.Exclamation)
        dix3.Add(MessageBoxIcon.Error, ExceptionMessageBoxSymbol.Error)
        ' same as error: dix3.Add(MessageBoxIcon.Hand, ExceptionMessageBoxSymbol.Hand)
        dix3.Add(MessageBoxIcon.Information, ExceptionMessageBoxSymbol.Information)
        dix3.Add(MessageBoxIcon.None, ExceptionMessageBoxSymbol.None)
        dix3.Add(MessageBoxIcon.Question, ExceptionMessageBoxSymbol.Question)
        ' same as error: dix3.Add(MessageBoxIcon.Stop, ExceptionMessageBoxSymbol.Stop)
        dix3.Add(MessageBoxIcon.Warning, ExceptionMessageBoxSymbol.Warning)
        dix3 = Nothing
        Return dix2
    End Function

    ''' <summary> Converts a value to a symbol. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> Symbol. </returns>
    Public Shared Function ToSymbol(ByVal value As MessageBoxIcon) As ExceptionMessageBoxSymbol
        Static hash As Dictionary(Of MessageBoxIcon, ExceptionMessageBoxSymbol)
        If hash Is Nothing Then
            hash = MyExceptionMessageBox.BuildMessageBoxIconSymbolHash
        End If
        If hash.ContainsKey(value) Then
            Return hash(value)
        Else
            Return ExceptionMessageBoxSymbol.Information
        End If
    End Function


    ''' <summary> Synchronously Invokes the exception display on the apartment thread to permit using
    ''' the clipboard. </summary>
    ''' <param name="owner">     The owner. </param>
    ''' <param name="exception"> The exception. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal owner As IWin32Window, ByVal exception As Exception) As DialogResult
        Dim box As New MyExceptionMessageBox(exception)
        Return box.ShowDialog(owner)
    End Function

    ''' <summary> Synchronously Invokes the exception display on the apartment thread to permit using
    ''' the clipboard. </summary>
    ''' <param name="owner">   The owner. </param>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String) As DialogResult
        Dim box As New MyExceptionMessageBox(text, caption)
        Return box.ShowDialog(owner)
    End Function

    ''' <summary> Synchronously Invokes the exception display on the apartment thread to permit using
    ''' the clipboard. </summary>
    ''' <param name="owner">         The owner. </param>
    ''' <param name="exception">     The exception. </param>
    ''' <param name="icon">          The icon. </param>
    ''' <param name="dialogResults"> The dialog results. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal owner As IWin32Window, ByVal exception As Exception, ByVal icon As MessageBoxIcon,
                                      ByVal dialogResults() As DialogResult) As DialogResult
        Dim box As New MyExceptionMessageBox(exception)
        Return box.ShowDialog(owner, MyExceptionMessageBox.ToSymbol(icon), dialogResults)
    End Function

    ''' <summary> Synchronously Invokes the exception display on the apartment thread to permit using
    ''' the clipboard. </summary>
    ''' <param name="owner">         The owner. </param>
    ''' <param name="text">          The exception. </param>
    ''' <param name="caption">       The caption. </param>
    ''' <param name="icon">          The icon. </param>
    ''' <param name="dialogResults"> The dialog results. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String,
                                      ByVal icon As MessageBoxIcon, ByVal dialogResults() As DialogResult) As DialogResult
        Dim box As New MyExceptionMessageBox(text, caption)
        Return box.ShowDialog(owner, MyExceptionMessageBox.ToSymbol(icon), dialogResults)
    End Function


    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialogAbortIgnore(ByVal owner As IWin32Window, ByVal exception As Exception, ByVal icon As MessageBoxIcon) As DialogResult
        Return MyExceptionMessageBox.ShowDialog(owner, exception, icon, New DialogResult() {DialogResult.Abort, DialogResult.Ignore})
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner">   The owner. </param>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialogAbortIgnore(ByVal owner As IWin32Window, ByVal text As String,
                                                 ByVal caption As String, ByVal icon As MessageBoxIcon) As DialogResult
        Return MyExceptionMessageBox.ShowDialog(owner, text, caption, icon, New DialogResult() {DialogResult.Abort, DialogResult.Ignore})
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> Either <see cref="DialogResult.Ignore">ignore</see> or <see cref="DialogResult.OK">Okay</see>. </returns>
    Public Shared Function ShowDialogIgnoreExit(ByVal owner As IWin32Window, ByVal exception As Exception, ByVal icon As MessageBoxIcon) As DialogResult
        Dim box As New MyExceptionMessageBox(exception)
        Return box.ShowDialogIgnoreExit(owner, icon)
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> Either <see cref="DialogResult.Ignore">ignore</see> or <see cref="DialogResult.OK">Okay</see>. </returns>
    Public Shared Function ShowDialogIgnoreExit(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String, ByVal icon As MessageBoxIcon) As DialogResult
        Dim box As New MyExceptionMessageBox(text, caption)
        Return box.ShowDialogIgnoreExit(owner, icon)
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> <see cref="DialogResult.OK">Okay</see>. </returns>
    Public Shared Function ShowDialogExit(ByVal owner As IWin32Window, ByVal exception As Exception, ByVal icon As MessageBoxIcon) As DialogResult
        Dim box As New MyExceptionMessageBox(exception)
        Return box.ShowDialogExit(owner, icon)
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> <see cref="DialogResult.OK">Okay</see>. </returns>
    Public Shared Function ShowDialogExit(ByVal owner As IWin32Window, ByVal text As String, ByVal caption As String, ByVal icon As MessageBoxIcon) As DialogResult
        Dim box As New MyExceptionMessageBox(text, caption)
        Return box.ShowDialogExit(owner, icon)
    End Function

#End Region

#Region " SHOW DIALOG "

    ''' <summary> Shows the exception display. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    ''' <example> Example 1: Simple Message <code>
    ''' Dim box As MyExceptionMessageBox = New MyExceptionMessageBox(message)
    ''' me.ShowDialog(owner)      </code></example>
    ''' <example> Example 2: Message box with exception message. <code>
    ''' Dim box As MyExceptionMessageBox = New MyExceptionMessageBox(exception)
    ''' me.ShowDialog(owner)      </code></example>
    '''
    ''' ### <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
    ''' null. </exception>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function ShowDialog(ByVal owner As IWin32Window) As DialogResult

        Dim r As DialogResult = DialogResult.Abort
        Try
            If owner Is Nothing OrElse TypeOf owner Is Form Then
                r = Me.Show(Nothing)
            Else
                r = Me.Show(owner)
            End If
        Catch ex As Exception
            Dim errorMessage As System.Text.StringBuilder = New System.Text.StringBuilder()
            If Me.Message Is Nothing Then
                errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                          "Exception occurred attempting to display an application exception")
            Else
                errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                          "Exception occurred attempting to display the following application exception: {0}{1}", Environment.NewLine,
                                          Me.Message)
            End If
            errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                      "{0}The following error occurred:{0}{1}", Environment.NewLine, ex)
            errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                      "{0}{0}Click Abort to exit application. Otherwise, the application will continue.", Environment.NewLine)
            r = MessageBox.Show(errorMessage.ToString(), "Application Error",
                                              MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1,
                                              MessageBoxOptions.DefaultDesktopOnly)
        End Try
        Return r
    End Function

    Private Sub MyExceptionMessageBox_OnCopyToClipboard(sender As Object, e As Microsoft.SqlServer.MessageBox.CopyToClipboardEventArgs) Handles Me.OnCopyToClipboard
        SafeClipboardSetDataObject.SetDataObject(e.ClipboardText)
        e.EventHandled = True
    End Sub

    ''' <summary> Displays the message box. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <remarks>
    ''' <example> 
    ''' Example 1: Simple Message
    '''           <code>
    ''' Dim box As MyExceptionMessageBox = New MyExceptionMessageBox(exception)
    ''' me.ShowDialog(owner, New String(){"A","B"},ExceptionMessageBoxSymbol.Asterisk, ExceptionMessageBoxDefaultButton.Button1)
    '''       </code></example>
    '''          </remarks>
    ''' <param name="owner"> The owner. </param>
    ''' <param name="buttonText">    The buttons text. </param>
    ''' <param name="symbol">         The symbol. </param>
    ''' <param name="defaultButton">  The default button. </param>
    ''' <returns> <see cref="ExceptionMessageBoxDialogResult">custom dialog result</see> if okay,
    ''' otherwise,
    ''' <see cref="ExceptionMessageBoxDialogResult.None"></see> </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function ShowDialog(ByVal owner As IWin32Window, ByVal buttonText As String(), ByVal symbol As ExceptionMessageBoxSymbol,
                               ByVal defaultButton As ExceptionMessageBoxDefaultButton) As ExceptionMessageBoxDialogResult
        If buttonText Is Nothing Then
            Throw New ArgumentNullException(NameOf(buttonText))
        ElseIf buttonText.Count = 0 OrElse buttonText.Count > 5 Then
            Throw New ArgumentOutOfRangeException("buttonText", "Must have between 1 and 5 values")
        End If

        ' Set the names of the custom buttons.
        Select Case buttonText.Count
            Case 1
                Me.SetButtonText(buttonText(0))
            Case 2
                Me.SetButtonText(buttonText(0), buttonText(1))
            Case 3
                Me.SetButtonText(buttonText(0), buttonText(1), buttonText(2))
            Case 4
                Me.SetButtonText(buttonText(0), buttonText(1), buttonText(2), buttonText(3))
            Case 5
                Me.SetButtonText(buttonText(0), buttonText(1), buttonText(2), buttonText(4))
        End Select
        Me.DefaultButton = defaultButton
        Me.Symbol = symbol
        Me.Buttons = ExceptionMessageBoxButtons.Custom
        Me.ShowDialog(owner)
        Return Me.CustomDialogResult

    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner">       The owner. </param>
    ''' <param name="buttonsText"> The buttons text. </param>
    ''' <param name="symbol">      The symbol. </param>
    ''' <remrks> Uses the first button as default. </remrks>
    ''' <returns> <see cref="ExceptionMessageBoxDialogResult">custom dialog result</see> if okay,
    ''' otherwise,
    ''' <see cref="ExceptionMessageBoxDialogResult.None"></see> </returns>
    ''' <example> Example 1: Simple Message
    '''           <code>
    ''' Dim box As MyExceptionMessageBox = New MyExceptionMessageBox(exception)
    ''' me.ShowDialog(owner, New String(){"A","B"},ExceptionMessageBoxSymbol.Asterisk)
    '''       </code></example>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
    ''' null. </exception>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function ShowDialog(ByVal owner As IWin32Window, ByVal buttonsText As String(), ByVal symbol As ExceptionMessageBoxSymbol) As ExceptionMessageBoxDialogResult
        Return Me.ShowDialog(owner, buttonsText, symbol, ExceptionMessageBoxDefaultButton.Button1)
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner">         The owner. </param>
    ''' <param name="buttonText">    The buttons text. </param>
    ''' <param name="symbol">        The symbol. </param>
    ''' <param name="defaultButton"> The default button. </param>
    ''' <param name="dialogResults"> The dialog results corresponding to the
    ''' <paramref name="buttonText">buttons</paramref>. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    ''' <example> Example 1: Simple Message
    ''' <code>
    ''' Dim box As MyExceptionMessageBox = New MyExceptionMessageBox(exception)
    ''' me.InvokeShow(owner, New String(){"A","B"},ExceptionMessageBoxSymbol.Asterisk,
    '''                                   ExceptionMessageBoxDefaultButton.Button1)
    ''' </code></example>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required
    ''' arguments are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are
    ''' outside the required range. </exception>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function ShowDialog(ByVal owner As IWin32Window,
                               ByVal buttonText As String(), ByVal symbol As ExceptionMessageBoxSymbol,
                               ByVal defaultButton As ExceptionMessageBoxDefaultButton,
                               ByVal dialogResults As DialogResult()) As DialogResult
        If buttonText Is Nothing Then
            Throw New ArgumentNullException(NameOf(buttonText))
        ElseIf buttonText.Count = 0 OrElse buttonText.Count > 5 Then
            Throw New ArgumentOutOfRangeException("buttonText", "Must have between 1 and 5 values")
        ElseIf dialogResults Is Nothing Then
            Throw New ArgumentNullException(NameOf(dialogResults))
        ElseIf dialogResults.Count = 0 OrElse dialogResults.Count > 5 Then
            Throw New ArgumentOutOfRangeException("dialogResults", "Must have between 1 and 5 values")
        ElseIf dialogResults.Count <> buttonText.Count Then
            Throw New ArgumentOutOfRangeException("dialogResults", "Must have the same count as button text")
        End If

        ' Set the names of the custom buttons.
        Select Case buttonText.Count
            Case 1
                Me.SetButtonText(buttonText(0))
            Case 2
                Me.SetButtonText(buttonText(0), buttonText(1))
            Case 3
                Me.SetButtonText(buttonText(0), buttonText(1), buttonText(2))
            Case 4
                Me.SetButtonText(buttonText(0), buttonText(1), buttonText(2), buttonText(3))
            Case 5
                Me.SetButtonText(buttonText(0), buttonText(1), buttonText(2), buttonText(4))
        End Select
        Me.DefaultButton = defaultButton
        Me.Symbol = symbol
        Me.Buttons = ExceptionMessageBoxButtons.Custom
        Me.ShowDialog(owner)
        Select Case Me.CustomDialogResult
            Case ExceptionMessageBoxDialogResult.Button1
                Return dialogResults(0)
            Case ExceptionMessageBoxDialogResult.Button2
                Return dialogResults(1)
            Case ExceptionMessageBoxDialogResult.Button3
                Return dialogResults(2)
            Case ExceptionMessageBoxDialogResult.Button4
                Return dialogResults(3)
            Case ExceptionMessageBoxDialogResult.Button5
                Return dialogResults(4)
            Case Else
                Return MessageBox.Show("Failed displaying the message box. Click Abort to exit application. Otherwise, the application will continue.",
                                                     "Application Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1,
                                                     MessageBoxOptions.DefaultDesktopOnly)
        End Select

    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner">         The owner. </param>
    ''' <param name="symbol">        The symbol. </param>
    ''' <param name="dialogResults"> The dialog results corresponding to the buttons. </param>
    ''' <remrks> Uses the dialog results names as the button names. Uses the first button as default. </remrks>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    ''' <example> Example 1: Simple Message
    ''' <code>
    ''' Dim box As MyExceptionMessageBox = New MyExceptionMessageBox(exception)
    ''' me.InvokeShow(owner, New String(){"A","B"},ExceptionMessageBoxSymbol.Asterisk,
    '''                                   ExceptionMessageBoxDefaultButton.Button1)
    ''' </code></example>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required
    ''' arguments are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are
    ''' outside the required range. </exception>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function ShowDialog(ByVal owner As IWin32Window,
                               ByVal symbol As ExceptionMessageBoxSymbol,
                               ByVal dialogResults As DialogResult()) As DialogResult
        If dialogResults Is Nothing Then
            Throw New ArgumentNullException(NameOf(dialogResults))
        ElseIf dialogResults.Count = 0 OrElse dialogResults.Count > 5 Then
            Throw New ArgumentOutOfRangeException("dialogResults", "Must have between 1 and 5 values")
        End If

        Dim buttonText As New List(Of String)
        For Each d As DialogResult In dialogResults
            buttonText.Add(d.ToString)
        Next
        Return Me.ShowDialog(owner, buttonText.ToArray, symbol, ExceptionMessageBoxDefaultButton.Button1, dialogResults)
    End Function

#End Region

#Region " SHOW DIALOG - PREDEFINED BUTTONS "

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> <see cref="DialogResult">Dialog result</see>. </returns>
    Public Function ShowDialogAbortIgnore(ByVal owner As IWin32Window, ByVal icon As MessageBoxIcon) As DialogResult
        Return Me.ShowDialog(owner, MyExceptionMessageBox.ToSymbol(icon), New DialogResult() {DialogResult.Abort, DialogResult.Ignore})
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> Either <see cref="DialogResult.Ignore">ignore</see> or <see cref="DialogResult.OK">Okay</see>. </returns>
    Public Function ShowDialogIgnoreExit(ByVal owner As IWin32Window, ByVal icon As MessageBoxIcon) As DialogResult
        Return Me.ShowDialog(owner, New String() {"Ignore", "Exit"}, MyExceptionMessageBox.ToSymbol(icon),
                             ExceptionMessageBoxDefaultButton.Button1, New DialogResult() {DialogResult.Ignore, DialogResult.OK})
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <param name="owner"> The owner. </param>
    ''' <returns> <see cref="DialogResult.OK">Okay</see>. </returns>
    Public Function ShowDialogExit(ByVal owner As IWin32Window, ByVal icon As MessageBoxIcon) As DialogResult
        Return Me.ShowDialog(owner, New String() {"Exit"}, MyExceptionMessageBox.ToSymbol(icon),
                             ExceptionMessageBoxDefaultButton.Button1, New DialogResult() {DialogResult.OK})
    End Function

#End Region

End Class
