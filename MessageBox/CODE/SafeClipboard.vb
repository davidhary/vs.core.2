﻿Imports System.Windows.Forms
''' <summary> Safe clipboard set data object. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="3/24/2015" by="David" revision=""> 
''' http://stackoverflow.com/questions/899350/how-to-copy-the-contents-of-a-string-to-the-clipboard-in-c
''' </history>
Public Class SafeClipboardSetDataObject
    Inherits SingleThreadApartmentBase

    ''' <summary> Constructor. </summary>
    ''' <param name="format"> Describes the <see cref="DataFormats">format</see> of the
    ''' specified data. </param>
    ''' <param name="data">   The data. </param>
    Public Sub New(ByVal format As String, ByVal data As Object)
        MyBase.New()
        Me._format = format
        Me._data = data
    End Sub

    Private ReadOnly _format As String
    Private ReadOnly _data As Object

    ''' <summary> Implemented in this class to do actual work. </summary>
    Protected Overrides Sub Work()
        Dim obj As Object = New System.Windows.Forms.DataObject(Me._format, Me._data)
        Clipboard.SetDataObject(obj, True)
    End Sub

    ''' <summary> Copies text to the clipboard. </summary>
    ''' <param name="text"> The text. </param>
    Public Shared Sub SetDataObject(ByVal text As String)
        Dim scr As SafeClipboardSetDataObject = New SafeClipboardSetDataObject(DataFormats.Text, text)
        scr.Go()
    End Sub

End Class


