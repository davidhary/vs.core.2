''' <summary> A data entry form for text with a numeric validator. </summary>
''' <license> (c) 2006 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/20/2006" by="David" revision="1.0.2242.x"> Created. </history>
Public Class TextInputBox
    Inherits Pith.FormBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of this class. </summary>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        If Version.Parse(My.Computer.Info.OSVersion).Major <= EnableDropShadowVersion Then
            Me.ClassStyle = Pith.ClassStyleConstants.DropShadow
        End If
        Me._numberStyle = Globalization.NumberStyles.None

    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 12/19/2015. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Returns the entered value. </summary>
    ''' <value> The entered value. </value>
    Public Property EnteredValue() As String
        Get
            Return Me._enteredValueTextBox.Text
        End Get
        Set(ByVal Value As String)
            Me._enteredValueTextBox.Text = Value
        End Set
    End Property

    ''' <summary> Number of styles. </summary>
    Private _numberStyle As Globalization.NumberStyles

    ''' <summary> Gets or sets the number style requested. </summary>
    ''' <value> The total number of style. </value>
    Public Property NumberStyle() As Globalization.NumberStyles
        Get
            Return Me._numberStyle
        End Get
        Set(ByVal Value As Globalization.NumberStyles)
            Me._numberStyle = Value
        End Set
    End Property

#End Region

#Region " FORM AND CONTROL EVENT HANDLERS "

    ''' <summary> Closes and returns the <see cref="System.Windows.Forms.DialogResult.OK">OK</see>
    ''' dialog result. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _acceptButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _acceptButton.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary> Closes and returns the <see cref="System.Windows.Forms.DialogResult.Cancel">Cancel</see>
    ''' dialog result. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _cancelButton.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    ''' <summary> Validates the entered value. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub _enteredValueTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _enteredValueTextBox.Validating
        Me._acceptButton.Enabled = False
        Me._validationErrorProvider.SetError(CType(sender, System.Windows.Forms.Control), String.Empty)
        If Me.NumberStyle <> Globalization.NumberStyles.None Then
            Dim outcome As Double
            e.Cancel = Not Double.TryParse(Me._enteredValueTextBox.Text, Me.NumberStyle,
              Globalization.CultureInfo.CurrentCulture, outcome)
        End If
    End Sub

    ''' <summary> Enables the OK button. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _enteredValueTextBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _enteredValueTextBox.Validated
        Me._acceptButton.Enabled = True
    End Sub

#End Region

End Class
