''' <summary> A data entry form. </summary>
''' <license> (c) 2006 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/20/2006" by="David" revision="1.0.2242.x"> Created. </history>
Public Class CompoundInputBox
    Inherits Pith.FormBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of this class. </summary>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        If Version.Parse(My.Computer.Info.OSVersion).Major <= EnableDropShadowVersion Then
            Me.ClassStyle = Pith.ClassStyleConstants.DropShadow
        End If

        Me._AcceptButton.Enabled = True

        Me.TextBoxLabel.Text = ""
        Me.NumericUpDownLabel.Text = ""
        Me.DropDownBoxLabel.Text = ""

    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 12/19/2015. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


#End Region

#Region " FORM EVENTS "

    ''' <summary> Handles the shown event of the control. </summary>
    ''' <remarks> David, 9/21/2015. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyClass.Shown
        Try
            If String.IsNullOrWhiteSpace(Me.TextBoxLabel.Text) Then
                Me.TextBoxLabel.Visible = False
                Me.TextBox.Visible = False
            End If
            If String.IsNullOrWhiteSpace(Me.NumericUpDownLabel.Text) Then
                Me.NumericUpDownLabel.Visible = False
                Me.NumericUpDown.Visible = False
            End If
            If String.IsNullOrWhiteSpace(Me.DropDownBoxLabel.Text) Then
                Me.DropDownBoxLabel.Visible = False
                Me.DropDownBox.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub


#End Region

#Region " FORM AND CONTROL EVENT HANDLERS "

    ''' <summary> Closes and returns the <see cref="System.Windows.Forms.DialogResult.OK">OK</see>
    ''' dialog result. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _acceptButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AcceptButton.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary> Closes and returns the <see cref="System.Windows.Forms.DialogResult.Cancel">Cancel</see>
    ''' dialog result. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    ''' <summary> Enables the OK button. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _enteredValueTextBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox.Validated
        Me._AcceptButton.Enabled = True
    End Sub

#End Region

End Class
