''' <summary> Displays a splash screen. </summary>
''' <license> (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/17/2004" by="David" revision="1.0.1508.x"> created. </history>
Public Class SplashScreen
    Inherits Pith.FormBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of this class. </summary>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        If Version.Parse(My.Computer.Info.OSVersion).Major <= EnableDropShadowVersion Then
            Me.ClassStyle = Pith.ClassStyleConstants.DropShadow
        End If
        Me._status = ""
        Me._LicenseHeader = "Licensed to:"
        Me._licenseeName = "Integrated Scientific Resources, Inc."
        Me._ProductFamilyName = "An Integrated Scientific Resources Product"
    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 12/19/2015. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PROPERTIES and METHODS "

    ''' <summary> true to topmost. </summary>
    Dim _topmost As Boolean

    ''' <summary> Sets the top most status in a thread safe way. </summary>
    ''' <param name="value"> true to value. </param>
    Public Sub TopmostSetter(ByVal value As Boolean)
        Me._topmost = value
        SplashScreen.SafeTopMostSetter(Me, value)
    End Sub

    ''' <summary> Gets or sets the header for displaying the licensee name. </summary>
    ''' <value> <c>LicenseHeader</c>is a String property. </value>
    Public Property LicenseHeader() As String

    ''' <summary> Gets or sets the license code. </summary>
    ''' <value> <c>LicenseCode</c>is a String property. </value>
    Public Property LicenseCode() As String

    ''' <summary> Name of the licensee. </summary>
    Private _licenseeName As String

    ''' <summary> Gets or sets the licensee name. </summary>
    ''' <value> <c>LicenseeName</c>is a String property. </value>
    Public Property LicenseeName() As String
        Get
            Return Me._licenseeName
        End Get
        Set(ByVal value As String)
            Me._licenseeName = value
            ' Display license information
            SplashScreen.SafeTextSetter(Me._licenseeLabel, Me.buildLicenseInfo)
            SplashScreen.SafeHeightSetter(Me._licenseeLabel, Convert.ToInt32(Me.CreateGraphics().MeasureString(Me._licenseeLabel.Text,
                                                                                                             Me._licenseeLabel.Font).Height))
        End Set
    End Property

    ''' <summary> The platform title. </summary>
    Private _platformTitle As String = String.Empty

    ''' <summary> Gets or sets the platform title, e.g., 'For 32 Bit operating Systems'. </summary>
    ''' <value> <c>PlatformTitle</c>is a String property. </value>
    Public Property PlatformTitle() As String
        Get
            If String.IsNullOrWhiteSpace(Me._platformTitle) Then
                Me._platformTitle = "for 32/64-Bit Operating Systems"
            End If
            Return Me._platformTitle
        End Get
        Set(ByVal value As String)
            Me._platformTitle = value
        End Set
    End Property

    ''' <summary> Gets reference to the primary Picture Box. </summary>
    ''' <value> The primary picture box. </value>
    Public ReadOnly Property PrimaryPictureBox() As System.Windows.Forms.PictureBox
        Get
            Return Me._companyLogoPictureBox
        End Get
    End Property

    ''' <summary> Gets reference to the secondary Picture Box. </summary>
    ''' <value> The secondary picture box. </value>
    Public ReadOnly Property SecondaryPictureBox() As System.Windows.Forms.PictureBox
        Get
            Return Me._disksPictureBox
        End Get
    End Property

    ''' <summary> Gets or sets the product family name, such as MyCompany Components. </summary>
    ''' <value> <c>ProductFamilyName</c>is a String property. </value>
    Public Property ProductFamilyName() As String

    ''' <summary> Updates all information. </summary>
    Public Sub UpdateAllInfo()
        Me.startUpdateInfo()
    End Sub

    ''' <summary> The status. </summary>
    Private _status As String

    ''' <summary> Displays a message on the splash screen. </summary>
    ''' <param name="value"> The message. </param>
    Public Sub DisplayMessage(ByVal value As String)
        Me._status = value
        SplashScreen.SafeTextSetter(Me._statusLabel, value)
    End Sub

    ''' <summary> Builds license information. </summary>
    ''' <returns> The license information. </returns>
    Private Function buildLicenseInfo() As String
        ' Display license information
        Dim licenseBuilder As System.Text.StringBuilder = New System.Text.StringBuilder("")
        If Not ((String.IsNullOrWhiteSpace(Me.LicenseeName) OrElse String.IsNullOrWhiteSpace(Me.LicenseeName.Trim)) AndAlso
                 (String.IsNullOrWhiteSpace(Me.LicenseCode) OrElse String.IsNullOrWhiteSpace(Me.LicenseCode.Trim))) Then
            If Not (String.IsNullOrWhiteSpace(Me.LicenseHeader) OrElse String.IsNullOrWhiteSpace(Me.LicenseHeader.Trim)) Then
                licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{1}{0}", Environment.NewLine, LicenseHeader.Trim)
            End If
            If Not (String.IsNullOrWhiteSpace(LicenseeName) OrElse String.IsNullOrWhiteSpace(LicenseeName.Trim)) Then
                licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "  {1}{0}", Environment.NewLine, LicenseeName.Trim)
            End If
            If Not (String.IsNullOrWhiteSpace(LicenseCode) OrElse String.IsNullOrWhiteSpace(LicenseCode.Trim)) Then
                licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "  {1}{0}", Environment.NewLine, LicenseCode.Trim)
            End If
        ElseIf Not (String.IsNullOrWhiteSpace(LicenseHeader) OrElse String.IsNullOrWhiteSpace(LicenseHeader.Trim)) Then
            licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{1}{0}", Environment.NewLine, LicenseHeader.Trim)
        Else
            licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{1}{0}", Environment.NewLine, My.Application.Info.CompanyName)
        End If
        Return licenseBuilder.ToString
    End Function

    ''' <summary> Update the information on screen. </summary>
    Private Sub updateInfo()

        Me._productFamilyLabel.Text = Me.ProductFamilyName
        Me._productFamilyLabel.Invalidate()
        Me._productTitleLabel.Text = My.Application.Info.Title
        Me._productTitleLabel.Invalidate()
        Me._productNameLabel.Text = My.Application.Info.ProductName
        Me._productNameLabel.Invalidate()
        Me._productVersionLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "Version {0}", My.Application.Info.Version)
        Me._productVersionLabel.Invalidate()
        Me._platformTitleLabel.Text = Me.PlatformTitle
        Me._platformTitleLabel.Invalidate()
        Me._companyNameLabel.Text = My.Application.Info.CompanyName
        Me._companyNameLabel.Invalidate()
        Me._copyrightLabel.Text = My.Application.Info.Copyright
        Me._copyrightLabel.Invalidate()
        Me._copyrightWarningLabel.Text = My.Application.Info.Trademark
        Me._copyrightWarningLabel.Invalidate()
        System.Windows.Forms.Application.DoEvents()

        ' Display license information
        Me._licenseeLabel.Text = Me.buildLicenseInfo
        Me._licenseeLabel.Height = Convert.ToInt32(Me.CreateGraphics().MeasureString(Me._licenseeLabel.Text, Me._licenseeLabel.Font).Height)
        Me._licenseeLabel.Invalidate()
        System.Windows.Forms.Application.DoEvents()

        Me._statusLabel.Text = Me._status
        Me._statusLabel.Invalidate()

        Me.TopMost = Me._topmost
        System.Windows.Forms.Application.DoEvents()

    End Sub

#End Region

#Region " THREAD SAFE METHODS "

    ''' <summary> Safe height setter. </summary>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   true to value. </param>
    Private Shared Sub SafeHeightSetter(ByVal control As System.Windows.Forms.Control, ByVal value As Integer)
        If control IsNot Nothing Then
            If control.InvokeRequired Then
                control.Invoke(New Action(Of System.Windows.Forms.Control, Integer)(AddressOf SplashScreen.SafeHeightSetter), New Object() {control, value})
            Else
                control.Height = value
                control.Invalidate()
                System.Windows.Forms.Application.DoEvents()
            End If
        End If
    End Sub

    ''' <summary> Sets the <see cref="System.Windows.Forms.Control">control</see> text to the
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe. </summary>
    ''' <remarks> The value is set to empty if null or empty. </remarks>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   The value. </param>
    ''' <returns> value. </returns>
    Private Shared Function SafeTextSetter(ByVal control As System.Windows.Forms.Control, ByVal value As String) As String
        If control IsNot Nothing Then
            If String.IsNullOrWhiteSpace(value) Then
                value = ""
            End If
            If control.InvokeRequired Then
                control.Invoke(New Action(Of System.Windows.Forms.Control, String)(AddressOf SplashScreen.SafeTextSetter), New Object() {control, value})
            Else
                control.Text = value
                control.Invalidate()
                System.Windows.Forms.Application.DoEvents()
            End If
        End If
        Return value
    End Function

    ''' <summary> Safe top most setter. </summary>
    ''' <param name="form"> The form. </param>
    ''' <param name="value">   true to value. </param>
    Private Shared Sub SafeTopMostSetter(ByVal form As System.Windows.Forms.Form, ByVal value As Boolean)
        If form IsNot Nothing Then
            If form.InvokeRequired Then
                form.Invoke(New Action(Of System.Windows.Forms.Form, Boolean)(AddressOf SplashScreen.SafeTopMostSetter), New Object() {form, value})
            Else
                form.TopMost = value
                System.Windows.Forms.Application.DoEvents()
            End If
        End If
    End Sub

    ''' <summary> Start the information updating thread. </summary>
    Private Sub startUpdateInfo()
        If Me.InvokeRequired Then
            Me.Invoke(New Action(AddressOf Me.updateInfo))
        Else
            Me.updateInfo()
        End If
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Does all the post processing after all the form controls are rendered as the user
    ''' expects them. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        System.Windows.Forms.Application.DoEvents()

        Try

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            Me.startUpdateInfo()

        Catch ex As Exception

            System.Windows.Forms.MessageBox.Show(ex.ToString, "Exception Occurred", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default
            System.Windows.Forms.Application.DoEvents()

        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Hides the splash screen. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _cancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _cancelButton.Click
        Me.Close()
    End Sub

#End Region

End Class
