﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Namespace ErrorProviderExtensions

    ''' <summary> Includes extensions for <see cref="ErrorProvider">Error Provider</see>. </summary>
    ''' <license> (c) 2015 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="05/26/2015" by="David" revision="1.0.5624.x"> Created. </history>
    Public Module Methods

#Region " ANNUNCIATE - OBJECT "

        ''' <summary> Annunciates error. </summary>
        ''' <param name="sender">  The event sender. </param>
        ''' <param name="details"> The details. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function Annunciate(ByVal provider As ErrorProvider, ByVal sender As Object, ByVal details As String) As String
            Dim control As Control = TryCast(sender, Control)
            If control IsNot Nothing Then
                provider.Annunciate(control, details)
            Else
                Dim toolStripItem As ToolStripItem = TryCast(sender, ToolStripItem)
                If toolStripItem IsNot Nothing Then
                    provider.Annunciate(toolStripItem, details)
                End If
            End If
            Return details
        End Function

        ''' <summary> Annunciates error. </summary>
        ''' <param name="sender"> The event sender. </param>
        ''' <param name="format"> Describes the format to use. </param>
        ''' <param name="args">   A variable-length parameters list containing arguments. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function Annunciate(ByVal provider As ErrorProvider, ByVal sender As Object, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return provider.Annunciate(sender, String.Format(format, args))
        End Function

#End Region

#Region " ANNUNCIATE - CONTROL "

        ''' <summary> Annunciates error. </summary>
        ''' <param name="sender">  The event sender. </param>
        ''' <param name="details"> The details. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function Annunciate(ByVal provider As ErrorProvider, ByVal sender As Control, ByVal details As String) As String
            If provider IsNot Nothing AndAlso sender IsNot Nothing Then
                If TypeOf sender.Container Is ToolStripItem Then
                    provider.Annunciate(TryCast(sender.Container, ToolStripItem), details)
                Else
                    provider.SetError(sender, details)
                End If
                Return details
            Else
                Return ""
            End If
        End Function

        ''' <summary> Annunciates error. </summary>
        ''' <param name="sender"> The event sender. </param>
        ''' <param name="format"> Describes the format to use. </param>
        ''' <param name="args">   A variable-length parameters list containing arguments. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function Annunciate(ByVal provider As ErrorProvider, ByVal sender As Control,
                                   ByVal format As String, ByVal ParamArray args() As Object) As String
            If provider IsNot Nothing AndAlso sender IsNot Nothing Then
                Return provider.Annunciate(sender, String.Format(format, args))
            Else
                Return ""
            End If
        End Function

#End Region

#Region " ANNUNCIATE -- TOOL STRIP "

        ''' <summary> Searches for the first tool strip item position. </summary>
        ''' <param name="container"> The container. </param>
        ''' <param name="item">      The item. </param>
        ''' <returns> The found tool strip item position. </returns>
        <Extension()>
        Public Function FindToolStripItemPosition(ByVal container As ToolStrip, ByVal item As ToolStripItem) As Integer
            If container Is Nothing Then Throw New ArgumentNullException("container")
            Dim loc As Integer = 0
            Dim isFound As Boolean = False
            For Each c As ToolStripItem In container.Items
                If c Is item Then
                    isFound = True
                    Exit For
                ElseIf c.Visible Then
                    loc += c.Width + c.Margin.Left + c.Margin.Right
                End If
            Next
            If isFound Then
                If container.GripStyle = ToolStripGripStyle.Visible Then
                    loc += container.GripRectangle.Width + container.GripMargin.Left + container.GripMargin.Right
                End If
                Return loc
            Else
                Return 0
            End If
        End Function

        ''' <summary> Annunciates error. </summary>
        ''' <param name="sender">  The sender. </param>
        ''' <param name="details"> The details. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function Annunciate(ByVal provider As ErrorProvider, ByVal sender As ToolStripItem, ByVal details As String) As String
            If provider IsNot Nothing AndAlso sender IsNot Nothing AndAlso sender.Owner IsNot Nothing Then
                provider.SetIconAlignment(sender.Owner, ErrorIconAlignment.MiddleLeft)
                provider.SetIconPadding(sender.Owner, -(Methods.FindToolStripItemPosition(sender.Owner, sender) +
                                                                 sender.Width + sender.Margin.Left + 10))
                provider.SetError(sender.Owner, details)
            End If
            Return details
        End Function

        ''' <summary> Annunciates error. </summary>
        ''' <param name="sender"> The sender. </param>
        ''' <param name="format"> Describes the format to use. </param>
        ''' <param name="args">   A variable-length parameters list containing arguments. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function Annunciate(ByVal provider As ErrorProvider, ByVal sender As ToolStripItem, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return provider.Annunciate(sender, String.Format(format, args))
        End Function

#End Region

    End Module

End Namespace

