﻿Imports System.Runtime.CompilerServices
Namespace StopwatchExtensions

    ''' <summary> Includes extensions for <see cref="Stopwatch">Stop Watch</see>. </summary>
    ''' <license> (c) 2015 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="03/19/2015" by="David" revision="2.0.5556.x"> Created. </history>
    Public Module Methods

        ''' <summary> Waits. </summary>
        ''' <param name="stopwatch"> The stop watch. </param>
        ''' <param name="value">     The value. </param>
        <Extension()>
        Public Sub Wait(ByVal stopwatch As Stopwatch, ByVal value As TimeSpan)
            If stopwatch IsNot Nothing AndAlso value > TimeSpan.Zero Then
                Do
                    System.Windows.Forms.Application.DoEvents()
                Loop Until stopwatch.Elapsed > value
            End If
        End Sub

    End Module

End Namespace
