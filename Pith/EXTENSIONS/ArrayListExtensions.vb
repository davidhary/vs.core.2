﻿Imports System.Runtime.CompilerServices
Namespace ArrayListExtensions

    ''' <summary> Includes extensions for <see cref="ArrayList">array lists</see>. </summary>
    ''' <license> (c) 2014 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/07/2014" by="David" revision="2.1.5425.x"> Created </history>
    Public Module Methods

        ''' <summary> Concatenates. </summary>
        ''' <param name="values">    The values. </param>
        ''' <param name="delimiter"> The delimiter. </param>
        ''' <returns> A String. </returns>
        <Extension()>
        Public Function Concatenate(ByVal values As ArrayList, ByVal delimiter As String) As String
            Dim builder As New System.Text.StringBuilder
            If values IsNot Nothing Then
                For Each v As String In values
                    If builder.Length > 0 Then
                        builder.Append(delimiter)
                    End If
                    builder.Append(v)
                Next
            End If
            Return builder.ToString()
        End Function

    End Module

End Namespace
