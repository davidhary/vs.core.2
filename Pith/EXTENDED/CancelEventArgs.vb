''' <summary> Additional information for cancel events. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="2/14/2015" by="David" revision=""> Created. </history>
Public Class CancelEventArgs
    Inherits System.ComponentModel.CancelEventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me._Details = ""
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="details"> The details. </param>
    Public Sub New(ByVal details As String)
        MyBase.new()
        Me._Details = details
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="format"> Specifies the message formatting string. </param>
    ''' <param name="args">   Specifies the arguments. </param>
    Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(String.Format(format, args))
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets an empty <see cref="CancelEventArgs">event arguments</see>. </summary>
    ''' <value> The empty. </value>
    Public Shared Shadows ReadOnly Property Empty() As CancelEventArgs
        Get
            Return New CancelEventArgs
        End Get
    End Property

    ''' <summary> Gets or sets the details. </summary>
    ''' <value> The details. </value>
    Public Property Details As String

    ''' <summary> Set event to cancel with details. Empty details clears and sets cancel false. </summary>
    ''' <param name="details"> The details. </param>
    Public Sub CancelDetails(ByVal details As String)
        Me.Cancel = Not String.IsNullOrWhiteSpace(details)
        Me.Details = details
    End Sub

    ''' <summary> Set event to cancel with details. </summary>
    ''' <param name="format"> Specifies the message formatting string. </param>
    ''' <param name="args">   Specifies the arguments. </param>
    Public Sub CancelDetails(ByVal format As String, ByVal ParamArray args() As Object)
        Me.CancelDetails(String.Format(format, args))
    End Sub

#End Region

End Class

