''' <summary> Defines a <see cref="T:System.Int32">Int32</see> Line: y = slope * x + offset. </summary>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/19/2005" by="David" revision="1.0.2088.x"> Created. </history>
Public Class LineI

#Region " Shared "

    ''' <summary> Gets the unit Line. </summary>
    ''' <value> A <see cref="LineI"/> value. </value>
    Public Shared ReadOnly Property Unity() As LineI
        Get
            Return New LineI(0, 0, 1, 1)
        End Get
    End Property

    ''' <summary> Returns the calculates offset, i.e., the pressure at zero volts. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="zero">  Zero span values. </param>
    ''' <param name="slope"> Full span values. </param>
    ''' <returns> null if it fails, else the calculated offset. </returns>
    Private Shared Function ComputeOffset(ByVal zero As PointI, ByVal slope As Double) As Double

        If zero Is Nothing Then
            Throw New ArgumentNullException(NameOf(zero))
        End If
        Return zero.Y - slope * zero.X

    End Function

    ''' <summary> Returns the calculates slope as the ratio of pressure to voltage change. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="zero"> Zero span values. </param>
    ''' <param name="full"> Full span values. </param>
    ''' <returns> The calculated slope. </returns>
    Private Shared Function ComputeSlope(ByVal zero As PointI, ByVal full As PointI) As Double

        If zero Is Nothing Then
            Throw New ArgumentNullException(NameOf(zero))
        End If
        If full Is Nothing Then
            Throw New ArgumentNullException(NameOf(full))
        End If
        Return (full.Y - zero.Y) / (full.X - zero.X)

    End Function

    ''' <summary> Transposes the (x,y) line to a (y,x) line. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="line"> Specifies the <see cref="LineI">Line</see>
    ''' to transpose. </param>
    ''' <returns> The transposed (y,x) line. </returns>
    Public Shared Function Transpose(ByVal line As LineI) As LineI

        If line Is Nothing Then
            Throw New ArgumentNullException(NameOf(line))
        End If
        Return New LineI(line.Origin.Y, line.Origin.X, line.Insertion.Y, line.Insertion.X)

    End Function

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Constructs a unity <see cref="LineI"/> instance. </summary>
    Public Sub New()
        MyBase.New()
        Me.SetLine(PointI.Zero, PointI.Unity)
    End Sub

    ''' <summary> Constructs a <see cref="LineI"/> instance by its limits. </summary>
    ''' <param name="x1"> A <see cref="T:System.Int32">Int32</see> expression that specifies the x
    ''' element of the first (x,y) point defining the line. </param>
    ''' <param name="y1"> A <see cref="T:System.Int32">Int32</see> expression that specifies the y
    ''' element of the first (x,y) point defining the line. </param>
    ''' <param name="x2"> A <see cref="T:System.Int32">Int32</see> expression that specifies the x
    ''' element of the second (x,y) point defining the line. </param>
    ''' <param name="y2"> A <see cref="T:System.Int32">Int32</see> expression that specifies the y
    ''' element of the second (x,y) point defining the line. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    Public Sub New(ByVal x1 As Int32, ByVal y1 As Int32, ByVal x2 As Int32, ByVal y2 As Int32)
        Me.New()
        Me.SetLine(x1, y1, x2, y2)
    End Sub

    ''' <summary> Constructs a <see cref="LineI"/> instance by its slope and offset. </summary>
    ''' <param name="slope">  A <see cref="T:System.Double">Double</see> expression that specifies the
    ''' slope of the line. </param>
    ''' <param name="offset"> A <see cref="T:System.Double">Double</see> expression that specifies the
    ''' offset of the line. </param>
    Public Sub New(ByVal slope As Double, ByVal offset As Double)
        Me.New()
        Me.SetLine(slope, offset)
    End Sub

    ''' <summary> Constructs a <see cref="LineI"/> instance by its origin and insertion points. </summary>
    ''' <param name="origin">    A <see cref="PointI">Point</see> expression that
    ''' specifies the origin point of the line. </param>
    ''' <param name="insertion"> A <see cref="PointI">Point</see> expression that
    ''' specifies the insertion (end) point of the line. </param>
    Public Sub New(ByVal origin As PointI, ByVal insertion As PointI)
        MyBase.New()
        Me.SetLine(origin, insertion)
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <param name="model"> The LineI object from which to copy. </param>
    Public Sub New(ByVal model As LineI)
        MyBase.New()
        If model IsNot Nothing Then
            Me.SetLine(model._Origin, model._Insertion)
        End If
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse LineI.Equals(Me, CType(obj, LineI)))
    End Function

    ''' <summary> Compares two lines. The lines are compared using their slopes and offsets. </summary>
    ''' <remarks> The two lines are the same if the have the same minimum and maximum values. </remarks>
    ''' <param name="other"> Specifies the other <see cref="LineI">Line</see>
    ''' to compare for equality with this instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As LineI) As Boolean
        If other Is Nothing Then
            Return False
        Else
            Return Math.Abs(other.Slope - Me.Slope) < Single.Epsilon AndAlso Math.Abs(other.Offset - Me.Offset) < Single.Epsilon
        End If
    End Function

    ''' <summary> Compares two lines. The lines are compared using their slopes and offsets. </summary>
    ''' <remarks> The two lines are the same if the have the same minimum and maximum values. </remarks>
    ''' <param name="referenceLine"> Specifies the <see cref="LineI">Line</see>
    ''' to compare for equality with this instance. </param>
    ''' <param name="tolerance">     Specifies the relative tolerance for comparing the two lines.
    ''' The lines are compared based on their slope and offset.  The offset tolerance is based on it
    ''' relative change from the Y range.  The tolerance if based on the reference line. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal referenceLine As LineI, ByVal tolerance As Single) As Boolean
        If referenceLine Is Nothing Then
            Return False
        Else
            Return Math.Abs(referenceLine.Offset - Me.Offset) < Math.Max(Single.Epsilon, tolerance * Math.Abs(Insertion.Y - Origin.Y)) AndAlso
                   Math.Abs(referenceLine.Slope - Me.Slope) < Math.Max(Single.Epsilon, tolerance * Math.Abs(referenceLine.Slope))
        End If
    End Function

    ''' <summary> Compares two lines. The lines are compared using their slopes and offsets. </summary>
    ''' <remarks> The two lines are the same if the have the same minimum and maximum values. </remarks>
    ''' <param name="referenceLine"> Specifies the <see cref="LineI">Line</see>
    ''' to compare for equality with this instance. </param>
    ''' <param name="tolerance">     Specifies the relative tolerance for comparing the two lines.
    ''' The lines are compared based on their slope and offset.  The offset tolerance is based on it
    ''' relative change from the Y range.  The tolerance if based on the reference line. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal referenceLine As LineI, ByVal tolerance As Double) As Boolean
        If referenceLine Is Nothing Then
            Return False
        Else
            Return Math.Abs(referenceLine.Offset - Me.Offset) < Math.Max(Single.Epsilon, tolerance * Math.Abs(Insertion.Y - Origin.Y)) AndAlso
                   Math.Abs(referenceLine.Slope - Me.Slope) < Math.Max(Single.Epsilon, tolerance * Math.Abs(referenceLine.Slope))
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As LineI, ByVal right As LineI) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse (left IsNot Nothing) AndAlso left.Equals(right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As LineI, ByVal right As LineI) As Boolean
        Return ((left Is Nothing) AndAlso (right IsNot Nothing)) OrElse Not ((left IsNot Nothing) AndAlso left.Equals(right))
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <returns> An <see cref="T:System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.Origin.GetHashCode Xor Me.Insertion.GetHashCode
    End Function

#End Region

#Region " METHODS "

    ''' <summary> Sets the Line based on the slope and offset values. </summary>
    ''' <param name="slope">  A <see cref="T:System.Double">Double</see> expression that specifies the
    ''' slope of the line. </param>
    ''' <param name="offset"> A <see cref="T:System.Double">Double</see> expression that specifies the
    ''' offset of the line. </param>
    Public Overloads Sub SetLine(ByVal slope As Double, ByVal offset As Double)

        Me.SetLine(New PointI(0, Convert.ToInt32(offset)), New PointI(1, Convert.ToInt32(slope + offset)))

    End Sub

    ''' <summary> Sets the Line based on the slope and offset values. </summary>
    ''' <param name="x1">     A <see cref="T:System.Int32">Int32</see> expression that specifies the x
    ''' element of the first (x,y) point defining the line. </param>
    ''' <param name="x2">     A <see cref="T:System.Int32">Int32</see> expression that specifies the x
    ''' element of the second (x,y) point defining the line. </param>
    ''' <param name="slope">  A <see cref="T:System.Double">Double</see> expression that specifies the
    ''' slope of the line. </param>
    ''' <param name="offset"> A <see cref="T:System.Double">Double</see> expression that specifies the
    ''' offset of the line. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    Public Overloads Sub SetLine(ByVal x1 As Int32, ByVal x2 As Int32, ByVal slope As Double, ByVal offset As Double)

        Me.SetLine(New PointI(x1, Convert.ToInt32(x1 * slope + offset)), New PointI(x2, Convert.ToInt32(x2 * slope + offset)))

    End Sub

    ''' <summary> Sets the Line based on the slope and offset values. </summary>
    ''' <param name="slope">  A <see cref="T:System.Double">Double</see> expression that specifies the
    ''' slope of the line. </param>
    ''' <param name="offset"> A <see cref="T:System.Double">Double</see> expression that specifies the
    ''' offset of the line. </param>
    ''' <param name="y1">     A <see cref="T:System.Int32">Int32</see> expression that specifies the x
    ''' element of the first (x,y) point defining the line. </param>
    ''' <param name="y2">     A <see cref="T:System.Int32">Int32</see> expression that specifies the x
    ''' element of the second (x,y) point defining the line. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Overloads Sub SetLine(ByVal slope As Double, ByVal offset As Double, ByVal y1 As Int32, ByVal y2 As Int32)

        If slope = 0 Then
            Me.SetLine(New PointI(0, 0), New PointI(1, Convert.ToInt32(offset)))
        Else
            Me.SetLine(New PointI(Convert.ToInt32((y1 - offset) / slope), y1), New PointI(Convert.ToInt32((y2 - offset) / slope), y2))
        End If

    End Sub

    ''' <summary> Sets the Line based on the values. </summary>
    ''' <remarks> Use this class to set the line. </remarks>
    ''' <param name="x1"> A <see cref="T:System.Int32">Int32</see> expression that specifies the x
    ''' element of the first (x,y) point defining the line. </param>
    ''' <param name="y1"> A <see cref="T:System.Int32">Int32</see> expression that specifies the y
    ''' element of the first (x,y) point defining the line. </param>
    ''' <param name="x2"> A <see cref="T:System.Int32">Int32</see> expression that specifies the x
    ''' element of the second (x,y) point defining the line. </param>
    ''' <param name="y2"> A <see cref="T:System.Int32">Int32</see> expression that specifies the y
    ''' element of the second (x,y) point defining the line. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Overloads Sub SetLine(ByVal x1 As Int32, ByVal y1 As Int32, ByVal x2 As Int32, ByVal y2 As Int32)

        Me.SetLine(New PointI(x1, y1), New PointI(x2, y2))

    End Sub

    ''' <summary> Sets the Line based its origin and insertion points. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="origin">    A <see cref="PointI">Point</see> expression that
    ''' specifies the origin point of the line. </param>
    ''' <param name="insertion"> A <see cref="PointI">Point</see> expression that
    ''' specifies the insertion (end) point of the line. </param>
    Public Overloads Sub SetLine(ByVal origin As PointI, ByVal insertion As PointI)

        If origin Is Nothing Then
            Throw New ArgumentNullException(NameOf(origin))
        End If
        If insertion Is Nothing Then
            Throw New ArgumentNullException(NameOf(insertion))
        End If

        Me._Origin = New PointI(origin)
        Me._Insertion = New PointI(insertion)
        Me._slope = ComputeSlope(origin, insertion)
        Me._offset = ComputeOffset(origin, Me.Slope)

    End Sub

    ''' <summary> Returns the default string representation of the line. </summary>
    ''' <returns> A representation of the line, e.g., '[(x1,y1)-(x2,y2)]' . </returns>
    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
                             "[({0}, {1}) - ({2},{3})]",
                             Me.Origin.X.ToString(Globalization.CultureInfo.CurrentCulture),
                             Me.Origin.Y.ToString(Globalization.CultureInfo.CurrentCulture),
                             Me.Insertion.X.ToString(Globalization.CultureInfo.CurrentCulture),
                             Me.Insertion.Y.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> The offset. </summary>
    Private _offset As Double

    ''' <summary> Gets the Offset of the line y = slope * x + offset. This is the Y value at x
    ''' = 0. </summary>
    ''' <value> A <see cref="T:System.Int32">Int32</see> property. </value>
    Public ReadOnly Property Offset() As Double
        Get
            Return Me._offset
        End Get
    End Property

    ''' <summary> The insertion. </summary>
    Private _Insertion As PointI

    ''' <summary> Gets or sets the origin point of the line. </summary>
    ''' <value> A <see cref="PointI">Point</see> property. </value>
    Public Property Insertion() As PointI
        Get
            Return Me._Insertion
        End Get
        Set(value As PointI)
            If value Is Nothing Then
                Me._Insertion = Nothing
            ElseIf Not value.Equals(Me.Insertion) Then
                Me.SetLine(Me.Origin, value)
            End If
        End Set
    End Property

    ''' <summary> The origin. </summary>
    Private _Origin As PointI

    ''' <summary> Gets or sets the origin point of the line. </summary>
    ''' <value> A <see cref="PointI">Point</see> property. </value>
    Public Property Origin() As PointI
        Get
            Return Me._Origin
        End Get
        Set(value As PointI)
            If value Is Nothing Then
                Me._Origin = Nothing
            ElseIf Not value.Equals(Me.Origin) Then
                Me.SetLine(value, Me.Insertion)
            End If
        End Set
    End Property



    ''' <summary> The slope. </summary>
    Private _slope As Double

    ''' <summary> Gets the Slope of the line y = slope * x + offset. </summary>
    ''' <value> A <see cref="T:System.Int32">Int32</see> property. </value>
    Public ReadOnly Property Slope() As Double
        Get
            Return Me._slope
        End Get
    End Property

    ''' <summary> Returns the X line value for the give Y value. </summary>
    ''' <value> The x coordinate. </value>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="X")>
    Public ReadOnly Property X(ByVal y As Int32) As Int32
        Get
            If Me.Slope <> 0 Then
                Return Convert.ToInt32((y - Me.Offset) / Me.Slope)
            Else
                Return Convert.ToInt32(Me.Offset)
            End If
        End Get
    End Property

    ''' <summary> Returns the Y line value for the give X value. </summary>
    ''' <value> The y coordinate. </value>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x"),
    CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Y")>
    Public ReadOnly Property Y(ByVal x As Int32) As Int32
        Get
            Return Convert.ToInt32(x * Me.Slope + Me.Offset)
        End Get
    End Property

#End Region

End Class

