''' <summary> Defines a <see cref="T:System.Int32">Int32</see> point. </summary>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/07/2005" by="David" revision="1.0.2077.x"> Created. </history>
Public Class PointI

#Region " SHARED "

    ''' <summary> Gets the zero [0,0] point value. </summary>
    ''' <value> A <see cref="PointI"/> value. </value>
    Public Shared ReadOnly Property Zero() As PointI
        Get
            Return New PointI(0, 0)
        End Get
    End Property

    ''' <summary> Gets the Unity [1,1] point value. </summary>
    ''' <value> A <see cref="PointI"/> value. </value>
    Public Shared ReadOnly Property Unity() As PointI
        Get
            Return New PointI(1, 1)
        End Get
    End Property

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Constructs a <see cref="PointI"/> instance by its limits. </summary>
    ''' <param name="x"> A <see cref="T:System.Int32">Int32</see> expression that specifics the x
    ''' element. </param>
    ''' <param name="y"> A <see cref="T:System.Int32">Int32</see> expression that specifics the y
    ''' element. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    Public Sub New(ByVal x As Int32, ByVal y As Int32)
        MyBase.New()
        Me._SetPoint(x, y)
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <param name="model"> The PointI object from which to copy. </param>
    Public Sub New(ByVal model As PointI)
        MyBase.New()
        If model IsNot Nothing Then
            Me._SetPoint(model._X, model._Y)
        End If
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse PointI.Equals(Me, CType(obj, PointI)))
    End Function

    ''' <summary> Compares two ranges. </summary>
    ''' <remarks> The two ranges are the same if the have the same minimum and maximum values. </remarks>
    ''' <param name="other"> The other point to compare to this object. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As PointI) As Boolean
        If other Is Nothing Then
            Return False
        Else
            Return other.X.Equals(Me.X) And other.Y.Equals(Me.Y)
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As PointI, ByVal right As PointI) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse (left IsNot Nothing) AndAlso left.Equals(right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operator. </returns>
    Public Shared Operator <>(ByVal left As PointI, ByVal right As PointI) As Boolean
        Return ((left Is Nothing) AndAlso (right IsNot Nothing)) OrElse Not ((left IsNot Nothing) AndAlso left.Equals(right))
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <returns> An <see cref="T:System.Single">Single</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.X.GetHashCode Xor Me.Y.GetHashCode
    End Function

#End Region

#Region " METHODS "

    ''' <summary> Gets the exponent based on the point extremum values.  This is the
    ''' <see cref="T:System.Int32">Int32</see> value representing the exponent of
    ''' the most significant digit of point values.  For example, the 4 for 20,000 or -3 for 0.0012. </summary>
    ''' <returns> The exponent. </returns>
    Public Function GetExponent() As Int32

        Return Convert.ToInt32(Math.Max(NumericExtensions.Exponent(Me.X), NumericExtensions.Exponent(Me.Y)))

    End Function

    ''' <summary> Gets the exponent based on the point values.  This is the
    ''' <see cref="T:System.Int32">Int32</see> value representing the exponent of
    ''' the most significant digit of point values limits.  For example, the 4 for 20,000 or -3 for
    ''' 0.0012.  With engineering scales, the exponents are multiples of three, e.g., 20,000 yields
    ''' +3 and 0.0001 -3. </summary>
    ''' <param name="useEngineeringScale"> True to use scale exponent increments of 3. </param>
    ''' <returns> The exponent. </returns>
    Public Function GetExponent(ByVal useEngineeringScale As Boolean) As Int32

        Return Convert.ToInt32(Math.Max(NumericExtensions.Exponent(Me.X, useEngineeringScale), NumericExtensions.Exponent(Me.Y, useEngineeringScale)))

    End Function

    ''' <summary> Sets the point based on the values. </summary>
    ''' <remarks> Use this class to set the point. </remarks>
    ''' <param name="x"> A <see cref="T:System.Int32">Int32</see> expression that specifies the x value
    ''' of the point. </param>
    ''' <param name="y"> A <see cref="T:System.Int32">Int32</see> expression that specifies the y value
    ''' of the point. </param>
    Private Sub _SetPoint(ByVal x As Int32, ByVal y As Int32)
        If Not Me._X.Equals(x) Then
            Me._X = x
        End If
        If Not Me._Y.Equals(y) Then
            Me._Y = y
        End If
    End Sub

    ''' <summary> Sets the point based on the values. </summary>
    ''' <remarks> Use this class to set the point. </remarks>
    ''' <param name="x"> A <see cref="T:System.Int32">Int32</see> expression that specifies the x value
    ''' of the point. </param>
    ''' <param name="y"> A <see cref="T:System.Int32">Int32</see> expression that specifies the y value
    ''' of the point. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="SetPoint")>
    Public Overloads Sub SetPoint(ByVal x As Int32, ByVal y As Int32)
        Me.X = x
        Me.Y = y
    End Sub

    ''' <summary> Returns the default string representation of the point. </summary>
    ''' <returns> A representation of the point, e.g., '(x,y)' . </returns>
    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},{1})",
                             Me.X.ToString(Globalization.CultureInfo.CurrentCulture),
                             Me.Y.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the x value of the point. </summary>
    ''' <value> A <see cref="T:System.Int32">Int32</see> property. </value>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="X")>
    Public Property X() As Int32

    ''' <summary> Gets or sets the y value of the point. </summary>
    ''' <value> A <see cref="T:System.Int32">Int32</see> property. </value>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Y")>
    Public Property Y() As Int32

#End Region

End Class
