''' <summary> Defines a <see cref="T:System.Single">Single</see> range class. </summary>
''' <license> (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/30/2004" by="David" revision="1.0.1581.x"> Created. </history>
Public Class RangeF

#Region " SHARED "

    ''' <summary> Gets the empty range. </summary>
    ''' <value> A <see cref="RangeF"/> value with <see cref="Single.MaxValue"/>
    ''' minimum value and <see cref="Single.MaxValue"/> for the maximum value. </value>
    Public Shared ReadOnly Property [Empty]() As RangeF
        Get
            Return New RangeF(Single.MaxValue, -Single.MaxValue)
        End Get
    End Property

    ''' <summary> Return the range of the specified data array. </summary>
    ''' <param name="values"> The data array. </param>
    ''' <returns> The calculated range. </returns>
    Public Shared Function GetRange(ByVal values() As Single) As RangeF

        ' return the unit range if no data
        If values Is Nothing Then
            Return RangeF.Unity
        End If

        ' initialize the range values to the first value
        Dim temp As Single
        temp = values(0)
        Dim min As Single = temp
        Dim max As Single = temp

        ' Loop over each point in the arrays
        For i As Integer = 0 To values.Length - 1
            temp = values(i)
            If temp < min Then
                min = temp
            ElseIf temp > max Then
                max = temp
            End If
        Next i

        Return New RangeF(min, max)

    End Function

    ''' <summary> Gets the Unity range. </summary>
    ''' <value> A <see cref="RangeF"/> [0,1] value. </value>
    Public Shared ReadOnly Property Unity() As RangeF
        Get
            Return New RangeF(0, 1)
        End Get
    End Property

    ''' <summary> Gets the zero range value. </summary>
    ''' <value> A <see cref="RangeF"/> value. </value>
    Public Shared ReadOnly Property Zero() As RangeF
        Get
            Return New RangeF(0, 0)
        End Get
    End Property

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Constructs a <see cref="RangeF"/> instance by its limits. </summary>
    ''' <param name="minValue"> A <see cref="T:System.Single">Single</see> expression that specifics
    ''' the minimum range. </param>
    ''' <param name="maxValue"> A <see cref="T:System.Single">Single</see> expression that specifics
    ''' the maximum range. </param>
    Public Sub New(ByVal minValue As Single, ByVal maxValue As Single)
        MyBase.New()
        Me.SetRange(minValue, maxValue)
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <param name="model"> The RangeF object from which to copy. </param>
    Public Sub New(ByVal model As RangeF)
        MyBase.New()
        If model IsNot Nothing Then
            Me.SetRange(model._Min, model._Max)
        End If
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As RangeF, ByVal right As RangeF) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return RangeF.Equals(left, right)
        End If
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As RangeF, ByVal right As RangeF) As Boolean
        Return Not RangeF.Equals(left, right)
    End Operator

    ''' <summary> Returns True if equal. </summary>
    ''' <remarks> Ranges are the same if the have the same
    ''' <see cref="Type">min</see> and <see cref="Type">max</see> values. </remarks>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> <c>True</c> if equals. </returns>
    Public Overloads Shared Function Equals(ByVal left As RangeF, ByVal right As RangeF) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left.Max.Equals(right.Max) AndAlso left.Min.Equals(right.Min)
        End If
    End Function

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse RangeF.Equals(Me, CType(obj, RangeF)))
    End Function

    ''' <summary> Returns True if the value of the <paramref name="other"/> equals to the instance
    ''' value. </summary>
    ''' <remarks> Ranges are the same if the have the same
    ''' <see cref="Type">min</see> and <see cref="Type">max</see> values. </remarks>
    ''' <param name="other"> The other <see cref="RangeF">Range</see> to compare for equality with this
    ''' instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As RangeF) As Boolean
        If other Is Nothing Then
            Return False
        Else
            Return RangeF.Equals(Me, other)
        End If
    End Function

#End Region

#Region " METHODS "

    ''' <summary> Returns true if the point value is within the range. </summary>
    ''' <param name="point"> A <see cref="T:System.Single">Single</see> point value&gt; </param>
    ''' <returns> <c>True</c> if value above or equal to minimum or below or equal to maximum. </returns>
    Public Function Contains(ByVal point As Single) As Boolean

        Return (point >= Me.Min) AndAlso (point <= Me.Max)

    End Function

    ''' <summary> Returns true if the point value is within the range. </summary>
    ''' <param name="point">     A <see cref="T:System.Single">Single</see> point value&gt; </param>
    ''' <param name="tolerance"> Tolerance for comparison. </param>
    ''' <returns> <c>True</c> if value above or equal to minimum - tolerance or below or equal to maximum +
    ''' tolerance. </returns>
    Public Function Contains(ByVal point As Single, ByVal tolerance As Single) As Boolean

        Return (point >= Me.Min - tolerance) AndAlso (point <= Me.Max + tolerance)

    End Function

    ''' <summary> Extend this range to include both its present values and the specified range. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="range"> A <see cref="RangeF"/> value. </param>
    ''' <returns> Extended range. </returns>
    Public Function ExtendRange(ByVal range As RangeF) As RangeF

        If range Is Nothing Then
            Throw New ArgumentNullException(NameOf(range))
        End If

        If Me.Min > range.Min Then
            Me.Min = range.Min
        End If

        If Me.Max < range.Max Then
            Me.Max = range.Max
        End If

        Return Me

    End Function

    ''' <summary> Gets the exponent based on the range extremum values.  This is the
    ''' <see cref="T:System.Integer">integer</see> value representing the exponent of
    ''' the most significant digit of range limits.  For example, the 4 for 20,000 or -3 for 0.0012. </summary>
    ''' <returns> The exponent. </returns>
    Public Function GetExponent() As Integer

        Return Convert.ToInt32(Math.Max(NumericExtensions.Exponent(Me.Min), NumericExtensions.Exponent(Me.Max)))

    End Function

    ''' <summary> Gets the exponent based on the range extremum values.  This is the
    ''' <see cref="T:System.Integer">integer</see> value representing the exponent of
    ''' the most significant digit of range limits.  For example, the 4 for 20,000 or -3 for 0.0012.
    ''' With engineering scales, the exponents are multiples of three, e.g., 20,000 yields +3 and
    ''' 0.0001 -3. </summary>
    ''' <param name="useEngineeringScale"> True to use scale exponent increments of 3. </param>
    ''' <returns> The exponent. </returns>
    Public Function GetExponent(ByVal useEngineeringScale As Boolean) As Integer

        Return Convert.ToInt32(Math.Max(NumericExtensions.Exponent(Me.Min, useEngineeringScale),
                                        NumericExtensions.Exponent(Me.Max, useEngineeringScale)))

    End Function

    ''' <summary> Creates a unique hash code. </summary>
    ''' <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me.Min.GetHashCode Xor Me.Max.GetHashCode
    End Function

    ''' <summary> Sets the range based on the extrema. </summary>
    ''' <remarks> Use this class to set the range. </remarks>
    ''' <param name="minValue"> A <see cref="T:System.Single">Single</see> expression that specifies
    ''' the minimum value of the range. </param>
    ''' <param name="maxValue"> A <see cref="T:System.Single">Single</see> expression that specifies
    ''' the maximum value of the range. </param>
    Public Overloads Sub SetRange(ByVal minValue As Single, ByVal maxValue As Single)

        Me.Min = minValue
        Me.Max = maxValue

    End Sub

    ''' <summary> Returns the default string representation of the range. </summary>
    ''' <returns> A representation of the range, e.g., '(min,max)' . </returns>
    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},{1})",
                             Me.Min.ToString(Globalization.CultureInfo.CurrentCulture),
                             Me.Max.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the maximum value of the range. </summary>
    ''' <value> A <see cref="T:System.Single">Single</see> property. </value>
    Public Property Max() As Single

    ''' <summary> Returns the mid range point of the range. </summary>
    ''' <value> A read only <see cref="T:System.Single">Single</see> property. </value>
    Public ReadOnly Property Midrange() As Single
        Get
            Return 0.5F * (Me.Max + Me.Min)
        End Get
    End Property

    ''' <summary> Gets or sets the minimum value of the range. </summary>
    ''' <value> A <see cref="T:System.Single">Single</see> property. </value>
    Public Property Min() As Single

    ''' <summary> Gets the range value of the range. </summary>
    ''' <value> A <see cref="T:System.Single">Single</see> </value>
    Public ReadOnly Property Span() As Single
        Get
            Return (Me.Max - Me.Min)
        End Get
    End Property

#End Region

End Class
