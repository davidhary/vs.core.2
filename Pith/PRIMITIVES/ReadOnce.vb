﻿''' <summary> Read once class. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
Public Class ReadOnce(Of T As Structure)
    Implements IEquatable(Of T)

    Public Sub New()
        MyBase.New()
        Me._HasValue = False
    End Sub

    ''' <summary> Gets or sets a value indicating whether this object has value. </summary>
    ''' <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
    Public Property HasValue As Boolean

    Private _Value As T
    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property Value As T
        Get
            Me._HasValue = False
            Return Me._Value
        End Get
        Set(value As T)
            Me._Value = value
            Me._HasValue = True
        End Set
    End Property

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return String.Format("{0}{1}", IIf(Me.HasValue, "", "?"), Me._Value)
    End Function

#Region " EQUALS "

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>. </returns>
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse ReadOnce(Of T).Equals(Me, CType(obj, ReadOnce(Of T))))
    End Function

    Public Overloads Function Equals(other As T) As Boolean Implements System.IEquatable(Of T).Equals
        Return Me.HasValue AndAlso other.Equals(Me.Value)
    End Function

    ''' <summary> Compares two ranges. </summary>
    ''' <remarks> The two ranges are the same if the have the same minimum and maximum values. </remarks>
    ''' <param name="other"> The other point to compare to this object. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As ReadOnce(Of T)) As Boolean
        If other Is Nothing Then
            Return False
        Else
            Return other.Value.Equals(Me.Value) And other.HasValue.Equals(Me.HasValue)
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As ReadOnce(Of T), ByVal right As ReadOnce(Of T)) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse (left IsNot Nothing) AndAlso left.Equals(right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operator. </returns>
    Public Shared Operator <>(ByVal left As ReadOnce(Of T), ByVal right As ReadOnce(Of T)) As Boolean
        Return ((left Is Nothing) AndAlso (right IsNot Nothing)) OrElse Not ((left IsNot Nothing) AndAlso left.Equals(right))
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <returns> An <see cref="T:System.Single">Single</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.HasValue.GetHashCode Xor Me.Value.GetHashCode
    End Function

#End Region

End Class
