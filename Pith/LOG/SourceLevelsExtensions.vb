﻿Imports System.Runtime.CompilerServices
Namespace DiagnosticsExtensions

    ''' <summary> A source level extensions. </summary>
    ''' <remarks> David, 12/11/2015. </remarks>
    ''' <license>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    Public Module SourceLevelMethods

#Region " SOURCE LEVELS AND TRACE EVENT TYPES "

        ''' <summary> Builds source level trace event type hash. </summary>
        ''' <returns> A Dictionary for translating source level to trace event type. </returns>
        Public Function BuildSourceLevelsTraceEventTypeHash() As Dictionary(Of SourceLevels, TraceEventType)
            Dim dix2 As New Dictionary(Of SourceLevels, TraceEventType)
            Dim dix3 As Dictionary(Of SourceLevels, TraceEventType) = dix2
            dix3.Add(Diagnostics.SourceLevels.Critical, TraceEventType.Critical)
            dix3.Add(Diagnostics.SourceLevels.Error, TraceEventType.Error)
            dix3.Add(Diagnostics.SourceLevels.Information, TraceEventType.Information)
            dix3.Add(Diagnostics.SourceLevels.Off, TraceEventType.Stop)
            dix3.Add(Diagnostics.SourceLevels.Verbose, TraceEventType.Verbose)
            dix3.Add(Diagnostics.SourceLevels.Warning, TraceEventType.Warning)
            dix3 = Nothing
            Return dix2
        End Function

        ''' <summary> Derives a <see cref="TraceEventType">trace event type</see> given the
        ''' <see cref="SourceLevels">source level</see> of a trace switch. </summary>
        ''' <param name="value"> The <see cref="SourceLevels">levels</see> of the trace message filtered
        ''' by the source switch and event type filter. </param>
        ''' <returns> The Trace <see cref="TraceEventType">event type</see>.. </returns>
        <Extension()>
        Public Function ToTraceEventType(ByVal value As SourceLevels) As TraceEventType
            Static hash As Dictionary(Of SourceLevels, TraceEventType)
            If hash Is Nothing Then
                hash = SourceLevelMethods.BuildSourceLevelsTraceEventTypeHash
            End If
            If hash.ContainsKey(value) Then
                Return hash(value)
            Else
                Return TraceEventType.Information
            End If
        End Function

#End Region

    End Module

End Namespace
