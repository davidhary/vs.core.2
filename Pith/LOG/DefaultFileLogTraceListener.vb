﻿Imports System.Globalization
Imports System.Security.Permissions
''' <summary> Replaces the standard event log. </summary>
''' <license> (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="8/8/2011" by="David" revision=""> Created. </history>
Public Class DefaultFileLogTraceListener
    Inherits Logging.FileLogTraceListener

    ''' <summary> Gets the default file log trace listener name. </summary>
    Public Const DefaultFileLogWriterName As String = "FileLog"

    ''' <summary> The default folder title. </summary>
    Public Const DefaultFolderTitle As String = "Logs"

    ''' <summary> Initializes a new instance of the
    ''' <see cref="T:Logging.FileLogTraceListener">file log trace
    ''' listener</see>. </summary>
    ''' <param name="userLevel"> The user level. </param>
    Public Sub New(ByVal userLevel As UserLevel)
        Me.New(DefaultFileLogTraceListener.DefaultFileLogWriterName, DefaultFileLogTraceListener.DefaultFolderTitle, userLevel)
    End Sub

    ''' <summary> Initializes a new instance of the
    ''' <see cref="T:Logging.FileLogTraceListener">file log trace
    ''' listener</see> using the application folder or the specified user level application data
    ''' folder if the application folder is protected. </summary>
    ''' <param name="name">        The name of the
    ''' <see cref="T:System.Diagnostics.TraceListener"></see>. </param>
    ''' <param name="folderTitle"> Name of the folder. </param>
    ''' <param name="userLevel">   The user level. </param>
    Public Sub New(ByVal name As String, ByVal folderTitle As String, ByVal userLevel As UserLevel)
        Me.New(name, DefaultFileLogTraceListener.BuildCustomFolder(folderTitle, userLevel))
    End Sub

    ''' <summary> Initializes a new instance of the
    ''' <see cref="T:Logging.FileLogTraceListener">file log trace
    ''' listener</see> using the application folder or the specified user level application data
    ''' folder if the application folder is protected. </summary>
    ''' <param name="name">        The name of the
    ''' <see cref="T:System.Diagnostics.TraceListener"></see>. </param>
    Public Sub New(ByVal name As String, ByVal folder As String)
        MyBase.New(name)

        Me.TimeFormat = "HH:mm:ss:fff"
        Me.UsingUniversalTime = True

        ' set the default properties of the log file name 
        Me.LogFileCreationSchedule = Logging.LogFileCreationScheduleOption.Daily
        Me.Location = Logging.LogFileLocation.Custom
        Me.Delimiter = ", "
        Me.CustomLocation = folder
        Me.IncludeHostName = False
        Me.AutoFlush = True
        Me.TraceOutputOptions = TraceOptions.DateTime Or TraceOptions.LogicalOperationStack

        ' note that the trace event cache time format is not set correctly to -8 hours offset.
        ' Rather, it gives us a -4 hours offset.

    End Sub

#Region " WRITE "

    ''' <summary> true to using universal time. </summary>
    Private _usingUniversalTime As Boolean

    ''' <summary> Gets or sets the condition for using universal time.  When using universal time, the
    ''' format is automatically modified to add a 'T' suffix if not set. Defaults to True. </summary>
    ''' <value> <c>True</c> if [using universal time]; otherwise, <c>False</c>. </value>
    Public Property UsingUniversalTime() As Boolean
        Get
            Return Me._usingUniversalTime
        End Get
        Set(ByVal value As Boolean)
            Me._usingUniversalTime = value
            If value AndAlso Not Me._TimeFormat.EndsWith("T", StringComparison.OrdinalIgnoreCase) Then
                Me.TimeFormat &= "T"
            End If
        End Set
    End Property

    ''' <summary> Gets the time format. </summary>
    ''' <value> The time format. </value>
    Public Property TimeFormat() As String

    ''' <summary> Builds trace message. </summary>
    ''' <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache"></see> object that
    ''' contains the current process ID, thread ID, and stack trace information. </param>
    ''' <param name="source">     A name of the trace source that invoked this method. </param>
    ''' <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType"></see>
    ''' enumeration values. </param>
    ''' <param name="id">         A numeric identifier for the event. </param>
    ''' <param name="message">    A message to write. </param>
    ''' <returns> The trace message record. </returns>
    Private Function BuildTraceMessage(ByVal eventCache As TraceEventCache, ByVal source As String, ByVal eventType As TraceEventType,
                                       ByVal id As Integer, ByVal message As String) As String

        Dim builder As New System.Text.StringBuilder
        builder.AppendFormat("{0}{1}", eventType.ToString.Substring(0, 2), Me.Delimiter)
        If id = 0 Then id = eventType
        builder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0:X}{1}", id, Me.Delimiter)

        If Me.IncludeHostName Then
            builder.AppendFormat("{0}.{1}{2}", Me.HostName, source, Me.Delimiter)
        End If

        If (Me.TraceOutputOptions And TraceOptions.DateTime) = TraceOptions.DateTime Then
            Dim logTime As DateTime = eventCache.DateTime
            If Not Me._usingUniversalTime Then
                logTime = logTime.Add(DateTimeOffset.Now.Offset)
            End If
            builder.AppendFormat("{0}{1}", logTime.ToString(Me._TimeFormat, CultureInfo.CurrentCulture), Me.Delimiter)
        End If

        If (Me.TraceOutputOptions And TraceOptions.ProcessId) = TraceOptions.ProcessId Then
            builder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0:X}{1}", eventCache.ProcessId, Me.Delimiter)
        End If
        If (Me.TraceOutputOptions And TraceOptions.ThreadId) = TraceOptions.ThreadId Then
            builder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0:X}{1}", eventCache.ThreadId, Me.Delimiter)
        End If
        If (Me.TraceOutputOptions And TraceOptions.Timestamp) = TraceOptions.Timestamp Then
            builder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{1}", eventCache.Timestamp, Me.Delimiter)
        End If

        builder.Append(message)

        If (Me.TraceOutputOptions And TraceOptions.LogicalOperationStack) = TraceOptions.LogicalOperationStack Then
            builder.AppendFormat("{0}{1}", Me.Delimiter, DefaultFileLogTraceListener.StackToString(eventCache.LogicalOperationStack))
        End If

        If (Me.TraceOutputOptions And TraceOptions.Callstack) = TraceOptions.Callstack Then
            builder.AppendFormat("{0}{1}", Me.Delimiter, eventCache.Callstack)
        End If
        Return builder.ToString

    End Function

    ''' <summary> Writes trace information, a message and event information to the output file or
    ''' stream. </summary>
    ''' <remarks> The <see cref="Logging.Log">Visual Basic Log</see> use the
    ''' <see cref="Switch">switch</see> to Determine if the event cache should be traced using the
    ''' switch <see cref="TraceEventType">event type</see>. The
    ''' <see cref="DefaultTraceListener.Filter">filter</see> is Overridable and can be set as
    ''' follows if additional filtering is required; otherwise, it is Nothing:
    ''' <code>
    ''' ' apply the trace level to the trace listener.
    ''' Dim listener As Diagnostics.TraceListener = log.TraceSource.Listeners(DefaultFileLogTraceListener.DefaultTraceListenerName)
    ''' If listener IsNot Nothing Then
    ''' listener.Filter = New EventTypeFilter(log.TraceSource.Switch.Level)
    ''' End If
    ''' </code> </remarks>
    ''' <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache"></see> object that
    ''' contains the current process ID, thread ID, and stack trace information. </param>
    ''' <param name="source">     A name of the trace source that invoked this method. </param>
    ''' <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType"></see>
    ''' enumeration values. </param>
    ''' <param name="id">         A numeric identifier for the event. </param>
    ''' <param name="message">    A message to write. </param>
    <HostProtection(SecurityAction.LinkDemand, Synchronization:=True)>
    Public Overrides Sub TraceEvent(ByVal eventCache As TraceEventCache, ByVal source As String, ByVal eventType As TraceEventType,
                                    ByVal id As Integer, ByVal message As String)
        If eventCache Is Nothing OrElse String.IsNullOrWhiteSpace(message) Then Return
        If Me.Filter Is Nothing OrElse Me.Filter.ShouldTrace(eventCache, source, eventType, id, message, Nothing, Nothing, Nothing) Then
            If message.Contains(Environment.NewLine) Then
                For Each line As String In message.Split(Environment.NewLine.ToCharArray)
                    Me.TraceEvent(eventCache, source, eventType, id, line.Trim)
                Next
                Return
            End If
            Dim record As String = Me.BuildTraceMessage(eventCache, source, eventType, id, message)
            Me.WriteLine(record)
        End If
    End Sub

    ''' <summary> Name of the host. </summary>
    Private _HostName As String

    ''' <summary> Gets the name of the host. </summary>
    ''' <value> The name of the host. </value>
    Private ReadOnly Property HostName() As String
        Get
            If (Me._HostName = "") Then
                Me._HostName = Environment.MachineName
            End If
            Return Me._HostName
        End Get
    End Property

    ''' <summary> Stacks to string. </summary>
    ''' <param name="stack"> The stack. </param>
    ''' <returns> System.String. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")>
    Private Shared Function StackToString(ByVal stack As Stack) As String
        Dim enumerator As IEnumerator = Nothing
        Dim length As Integer = ", ".Length
        Dim builder As New System.Text.StringBuilder
        Try
            enumerator = stack.GetEnumerator
            Do While enumerator.MoveNext
                builder.AppendFormat("{0}, ", enumerator.Current.ToString)
            Loop
        Finally
            If TypeOf enumerator Is IDisposable Then
                TryCast(enumerator, IDisposable).Dispose()
            End If
        End Try
        builder.Replace("""", """""")
        If (builder.Length >= length) Then
            builder.Remove((builder.Length - length), length)
        End If
        Return ("""" & builder.ToString & """")
    End Function

#End Region

#Region " FOLDER "

    ''' <summary> Determines whether the specified folder path is writable. </summary>
    ''' <remarks> Uses a temporary random file name to test if the file can be created. The file is
    ''' deleted thereafter. </remarks>
    ''' <param name="path"> The path. </param>
    ''' <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Shared Function IsFolderWritable(ByVal path As String) As Boolean
        Dim filePath As String = ""
        Dim affirmative As Boolean = False
        Try
            filePath = System.IO.Path.Combine(path, System.IO.Path.GetRandomFileName())
            Using s As System.IO.FileStream = System.IO.File.Open(filePath, System.IO.FileMode.OpenOrCreate)
            End Using
            affirmative = True
        Catch
        Finally
            ' SS reported an exception from this test possibly indicating that Windows allowed writing the file 
            ' by failed report deletion. Or else, Windows raised another exception type.
            Try
                If System.IO.File.Exists(filePath) Then
                    System.IO.File.Delete(filePath)
                End If
            Catch
            End Try
        End Try
        Return affirmative
    End Function

    ''' <summary> Returns the default folder name for tracing. </summary>
    ''' <returns> The default path for the log file. </returns>
    Public Shared Function BuildCustomFolder(ByVal defaultFolderTitle As String, ByVal userLevel As UserLevel) As String
        Dim candidatePath As String = My.Application.Info.DirectoryPath
        If IsFolderWritable(candidatePath) Then
            Return System.IO.Path.Combine(candidatePath, "..\" & defaultFolderTitle)
        ElseIf userLevel = UserLevel.AllUsers Then
            Return System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData, defaultFolderTitle)
        Else
            Return System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData, defaultFolderTitle)
        End If
    End Function


#End Region

#Region " FILE SIZE "

    ''' <summary> Returns the file size, -2 if path not specified or -1 if file not found. </summary>
    ''' <param name="path"> The path. </param>
    ''' <returns> System.Int64. </returns>
    Public Shared Function FileSize(ByVal path As String) As Long
        If String.IsNullOrWhiteSpace(path) Then Return 0
        Dim info As System.IO.FileInfo = New System.IO.FileInfo(path)
        Return DefaultFileLogTraceListener.FileSize(info)
    End Function

    ''' <summary> Returns the file size, -2 if path not specified or -1 if file not found. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> System.Int64. </returns>
    Public Shared Function FileSize(ByVal value As System.IO.FileInfo) As Long

        If value Is Nothing Then
            Return -2
        ElseIf value.Exists Then
            Return value.Length
        ElseIf String.IsNullOrWhiteSpace(value.Name) Then
            Return -2
        Else
            Return -1
        End If

    End Function

    ''' <summary> Gets the file log trace listener. </summary>
    ''' <value> The file log trace listener. </value>
    Public Shared ReadOnly Property FileLogTraceListener As Logging.FileLogTraceListener
        Get
            Return TryCast(My.Application.Log.TraceSource.Listeners(DefaultFileLogTraceListener.DefaultFileLogWriterName), Logging.FileLogTraceListener)
        End Get
    End Property

    ''' <summary> Returns true if log file exists; Otherwise, <c>False</c>. </summary>
    ''' <returns> <c>True</c> if okay, <c>False</c> otherwise. </returns>
    Public Shared Function LogFileExists() As Boolean
        Return DefaultFileLogTraceListener.FileSize(DefaultFileLogTraceListener.FileLogTraceListener?.FullLogFileName) > 2
    End Function

#End Region

End Class

#Region " Not used: This method from the log code is replaced by the custom writer "
#If LegacyCode Then
  ''' <summary>
  ''' Writes trace information, a message and event information to the output file or stream.
  ''' </summary>
  ''' <param name="source">A name of the trace source that invoked this method. </param>
  ''' <param name="message">A message to write.</param>
  ''' <param name="eventCache">A <see cref="T:System.Diagnostics.TraceEventCache"></see> object that contains the current process ID, thread ID, and stack trace information.</param>
  ''' <param name="eventType">One of the <see cref="T:System.Diagnostics.TraceEventType"></see> enumeration values.</param>
  ''' <param name="id">A numeric identifier for the event.</param>
  ''' <filterPriority>1</filterPriority>
  ''' <PermissionSet><IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /><IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" /></PermissionSet>
  <HostProtection(SecurityAction.LinkDemand, Synchronization:=True)> 
  Public Overrides Sub TraceEvent(ByVal eventCache As TraceEventCache, ByVal source As String, ByVal eventType As TraceEventType, ByVal id As Integer, ByVal message As String)
    If ((Me.Filter Is Nothing) OrElse Me.Filter.ShouldTrace(eventCache, source, eventType, id, message, Nothing, Nothing, Nothing)) Then
      Dim builder As New StringBuilder
      builder.Append((source & Me.Delimiter))
      builder.Append(([Enum].GetName(GetType(TraceEventType), eventType) & Me.Delimiter))
      builder.Append((id.ToString(CultureInfo.InvariantCulture) & Me.Delimiter))
      builder.Append(message)
      If ((Me.TraceOutputOptions And TraceOptions.Callstack) = TraceOptions.Callstack) Then
        builder.Append((Me.Delimiter & eventCache.Callstack))
      End If
      If ((Me.TraceOutputOptions And TraceOptions.LogicalOperationStack) = TraceOptions.LogicalOperationStack) Then
        builder.Append((Me.Delimiter & StackToString(eventCache.LogicalOperationStack)))
      End If
      If ((Me.TraceOutputOptions And TraceOptions.DateTime) = TraceOptions.DateTime) Then
        builder.Append((Me.Delimiter & eventCache.DateTime.ToString("o", CultureInfo.InvariantCulture)))
        ' builder.Append((Me.Delimiter & eventCache.DateTime.ToString("u", CultureInfo.InvariantCulture)))
      End If
      If ((Me.TraceOutputOptions And TraceOptions.ProcessId) = TraceOptions.ProcessId) Then
        builder.Append((Me.Delimiter & eventCache.ProcessId.ToString(CultureInfo.InvariantCulture)))
      End If
      If ((Me.TraceOutputOptions And TraceOptions.ThreadId) = TraceOptions.ThreadId) Then
        builder.Append((Me.Delimiter & eventCache.ThreadId))
      End If
      If ((Me.TraceOutputOptions And TraceOptions.Timestamp) = TraceOptions.Timestamp) Then
        builder.Append((Me.Delimiter & eventCache.Timestamp.ToString(CultureInfo.InvariantCulture)))
      End If
      If Me.IncludeHostName Then
        builder.Append((Me.Delimiter & Me.HostName))
      End If
      Me.WriteLine(builder.ToString)
    End If
  End Sub
#End If
#End Region

