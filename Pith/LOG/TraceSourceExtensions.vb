﻿Imports System.Runtime.CompilerServices
Namespace DiagnosticsExtensions

    ''' <summary> Extends the <see cref="Microsoft.VisualBasic.Logging">Logging</see> functionality. </summary>
    ''' <license> (c) 2014 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/09/2014" by="David" revision="2.0.5152.x"> Created. </history>
    Public Module TraceSourceExtensions

#Region " TRACE LEVEL "

        ''' <summary> Applies the trace level. </summary>
        ''' <param name="TraceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
        <Extension()>
        Public Sub ApplyTraceLevel(ByVal traceSource As TraceSource, ByVal value As TraceEventType)
            If traceSource IsNot Nothing AndAlso traceSource.Switch IsNot Nothing Then
                traceSource.Switch.ApplyTraceLevel(value)
            End If
        End Sub

        ''' <summary> Returns the trace level. </summary>
        ''' <param name="TraceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <returns> The trace level; <see cref="TraceEventType.Information">information</see> if nothing. </returns>
        <Extension()>
        Public Function TraceLevel(ByVal traceSource As TraceSource) As TraceEventType
            If traceSource Is Nothing OrElse traceSource.Switch Is Nothing Then
                Return TraceEventType.Information
            Else
                Return traceSource.Switch.TraceLevel
            End If
        End Function

        ''' <summary> Checks if the log should trace the event type. </summary>
        ''' <param name="TraceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
        ''' <returns> <c>True</c>&gt; If the log should trace the  c&gt;TrueThe trace level;
        ''' <see cref="TraceEventType.Information">information</see> if log is nothing. </returns>
        <Extension()>
        Public Function ShouldTrace(ByVal traceSource As TraceSource, ByVal eventType As TraceEventType) As Boolean
            Return eventType <= traceSource.TraceLevel
        End Function


#End Region

#Region " TRACE EVENT "

        ''' <summary> Writes a trace event to the trace listeners. </summary>
        ''' <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="eventType">   The <see cref="TraceEventType">event type</see> of the trace
        ''' data. </param>
        ''' <param name="id">          A numeric (integer range) identifier for the event. Specifies as decimal to allow
        ''' overloading the default method. Using long did not work under VS2010. </param>
        ''' <param name="format">      The trace message format. </param>
        ''' <param name="args">        The trace message arguments. </param>
        ''' <returns> The trace message details or empty. </returns>
        <Extension()>
        Public Function TraceEvent(ByVal traceSource As TraceSource, ByVal eventType As TraceEventType,
                                   ByVal id As Decimal, ByVal format As String, ByVal ParamArray args() As Object) As String
            If format IsNot Nothing Then
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
                If traceSource IsNot Nothing Then
                    traceSource.TraceEvent(eventType, CInt(id), message)
                End If
                Return message
            End If
            Return ""
        End Function

        ''' <summary> Writes a trace event to the trace listeners. </summary>
        ''' <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="eventType">   The <see cref="TraceEventType">event type</see> of the trace
        ''' data. </param>
        ''' <param name="format">      The trace message format. </param>
        ''' <param name="args">        The trace message arguments. </param>
        ''' <returns> The trace message details or empty. </returns>
        <Extension()>
        Public Function TraceEvent(ByVal traceSource As TraceSource, ByVal eventType As TraceEventType,
                                   ByVal format As String, ByVal ParamArray args() As Object) As String
            If format IsNot Nothing Then
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
                If traceSource IsNot Nothing Then
                    traceSource.TraceEvent(eventType, 0I, message)
                End If
                Return message
            End If
            Return ""
        End Function

        ''' <summary> Writes a trace event to the trace listeners. </summary>
        ''' <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="eventType">   The <see cref="TraceEventType">event type</see> of the trace
        ''' data. </param>
        ''' <param name="messages">    Messages to trace. </param>
        ''' <returns> The trace message details or empty. </returns>
        <Extension()>
        Public Function TraceEvent(ByVal traceSource As TraceSource, ByVal eventType As TraceEventType, ByVal id As Integer,
                                   ByVal messages As String()) As String
            If messages IsNot Nothing Then
                Dim message As String = String.Join(",", messages)
                If traceSource IsNot Nothing Then
                    traceSource.TraceEvent(eventType, id, message)
                End If
                Return message
            End If
            Return ""
        End Function

#End Region

#Region " EXCEPTION TRACE EVENT "

        ''' <summary> Writes a trace event to the trace listeners. </summary>
        ''' <param name="traceSource">    The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="ex">             The exception. </param>
        ''' <param name="eventType">      The <see cref="TraceEventType">event type</see> of the trace
        ''' data. </param>
        ''' <param name="id">             A numeric (integer range) identifier for the event. </param>
        ''' <param name="format">      The additional information format. </param>
        ''' <param name="args">        The additional information arguments. </param>
        <Extension()>
        Public Sub TraceEvent(ByVal traceSource As TraceSource, ByVal ex As Exception,
                              ByVal eventType As TraceEventType, ByVal id As Integer,
                              ByVal format As String, ByVal ParamArray args() As Object)
            If ex IsNot Nothing AndAlso traceSource IsNot Nothing Then
                Dim builder As New System.Text.StringBuilder
                builder.Append(ex.Message)
                If Not String.IsNullOrWhiteSpace(format) Then
                    builder.Append("; ")
                    builder.AppendFormat(format, args)
                End If
                traceSource.TraceEvent(eventType, id, builder.ToString)
                builder.Clear()

                If ex IsNot Nothing AndAlso ex.StackTrace IsNot Nothing Then
                    Dim stackTrace As String() = ex.StackTrace.Split(CChar(Environment.NewLine))
                    traceSource.TraceEvent(eventType, id, stackTrace)
                End If

                If ex.Data IsNot Nothing AndAlso ex.Data.Count > 0 Then
                    For Each keyValuePair As System.Collections.DictionaryEntry In ex.Data
                        traceSource.TraceEvent(eventType, CLng(id), keyValuePair.Key.ToString & "=" & keyValuePair.Value.ToString)
                    Next
                End If

                If ex.InnerException IsNot Nothing Then
                    traceSource.TraceEvent(ex.InnerException, eventType, id, "(Inner Exception)")
                End If

            End If
        End Sub

        ''' <summary> Writes a trace event to the trace listeners. </summary>
        ''' <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="ex">          The exception. </param>
        ''' <param name="eventType">   The <see cref="TraceEventType">event type</see> of the trace
        ''' data. </param>
        ''' <param name="format">      The additional information format. </param>
        ''' <param name="args">        The additional information arguments. </param>
        <Extension()>
        Public Sub TraceEvent(ByVal traceSource As TraceSource, ByVal ex As Exception, ByVal eventType As TraceEventType,
                              ByVal format As String, ByVal ParamArray args() As Object)
            TraceSourceExtensions.TraceEvent(traceSource, ex, eventType, 0, format, args)
        End Sub

        ''' <summary> Writes a trace event to the trace listeners. </summary>
        ''' <param name="traceSource">   The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="ex">            The exception. </param>
        ''' <param name="id">            A numeric (integer range) identifier for the event. </param>
        ''' <param name="format">        The additional information format. </param>
        ''' <param name="args">          The additional information arguments. </param>
        <Extension()>
        Public Sub TraceEvent(ByVal traceSource As TraceSource, ByVal ex As Exception, ByVal id As Integer,
                              ByVal format As String, ByVal ParamArray args() As Object)
            TraceSourceExtensions.TraceEvent(traceSource, ex, TraceEventType.Error, id, format, args)
        End Sub

        ''' <summary> Writes a trace event to the trace listeners. </summary>
        ''' <param name="traceSource">   The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="ex">            The exception. </param>
        ''' <param name="id">            A numeric (integer range) identifier for the event. </param>
        <Extension()>
        Public Sub TraceEvent(ByVal traceSource As TraceSource, ByVal ex As Exception, ByVal id As Integer)
            TraceSourceExtensions.TraceEvent(traceSource, ex, TraceEventType.Error, id, "")
        End Sub

        ''' <summary> Writes a trace event to the trace listeners. </summary>
        ''' <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="ex">  The exception. </param>
        <Extension()>
        Public Sub TraceEvent(ByVal traceSource As TraceSource, ByVal ex As Exception)
            TraceSourceExtensions.TraceEvent(traceSource, ex, TraceEventType.Error, 0, "")
        End Sub

#End Region

#Region " TRACE EVENT -- OVERRIDE TRACE LEVEL "

        ''' <summary> Writes a trace event to the trace listeners. Overrides the current trace source
        ''' level. </summary>
        ''' <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="eventType">   The <see cref="TraceEventType">event type</see> of the trace
        ''' data. </param>
        ''' <param name="id">          A numeric (integer range) identifier for the event. </param>
        ''' <param name="format">   The trace message format. </param>
        ''' <returns> Message or empty string. </returns>
        <Extension()>
        Public Function TraceEventOverride(ByVal traceSource As TraceSource, ByVal eventType As TraceEventType,
                                           ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object) As String
            If format IsNot Nothing Then
                Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
                If traceSource IsNot Nothing Then
                    ' save the current trace level.
                    Dim lastSourceLevel As Diagnostics.SourceLevels = traceSource.Switch.Level
                    ' set the requested level.
                    traceSource.Switch.Level = eventType.ToSourceLevel
                    ' write the entry.
                    traceSource.TraceEvent(eventType, id, details)
                    ' restore the level.
                    traceSource.Switch.Level = lastSourceLevel
                End If
                Return details
            End If
            Return ""
        End Function

        ''' <summary> Writes a trace event to the trace listeners. Overrides the current trace source level. </summary>
        ''' <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="eventType">   The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="format">   The trace message format. </param>
        ''' <param name="args">     The arguments. </param>
        ''' <returns> Message or empty string. </returns>
        <Extension()>
        Public Function TraceEventOverride(ByVal traceSource As TraceSource, ByVal eventType As TraceEventType,
                                          ByVal format As String, ByVal ParamArray args() As Object) As String
            If format IsNot Nothing Then
                Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
                If traceSource IsNot Nothing Then
                    Return traceSource.TraceEventOverride(eventType, 0, details)
                End If
                Return details
            End If
            Return ""
        End Function

#End Region

#Region " TRACE EVENT TRACE MESSAGES "

        ''' <summary> Writes a trace event to the trace listeners. </summary>
        ''' <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="message"> The message. </param>
        <Extension()>
        Public Sub TraceEvent(ByVal traceSource As TraceSource, ByVal message As TraceMessage)
            If traceSource IsNot Nothing AndAlso message IsNot Nothing Then
                If Not String.IsNullOrWhiteSpace(message.Details) Then
                    traceSource.TraceEvent(message.EventType, message.Id, message.Details)
                End If
            End If
        End Sub

        ''' <summary> Writes a trace event to the trace listeners. </summary>
        ''' <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="message">        The message. </param>
        ''' <param name="additionalInfo"> Information describing the additional. </param>
        <Extension()>
        Public Sub TraceEvent(ByVal traceSource As TraceSource, ByVal message As TraceMessage, ByVal additionalInfo As String)
            If traceSource IsNot Nothing AndAlso message IsNot Nothing Then
                If String.IsNullOrWhiteSpace(additionalInfo) Then
                    If Not String.IsNullOrWhiteSpace(message.Details) Then
                        traceSource.TraceEvent(message.EventType, message.Id, message.Details)
                    End If
                Else
                    If Not String.IsNullOrWhiteSpace(message.Details) Then
                        traceSource.TraceEvent(message.EventType, message.Id, "{0}; {1}", message.Details, additionalInfo)
                    End If
                End If
            End If
        End Sub

#End Region

#Region " FILE  "

        ''' <summary> Returns the default file log writer. </summary>
        ''' <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <returns> The default <see cref="Logging.FileLogTraceListener">file log writer</see>. </returns>
        <Extension()>
        Public Function DefaultFileLogWriter(ByVal traceSource As TraceSource) As Logging.FileLogTraceListener
            If traceSource Is Nothing Then
                Return Nothing
            Else
                Return CType(traceSource.Listeners(DefaultFileLogTraceListener.DefaultFileLogWriterName), Logging.FileLogTraceListener)
            End If
        End Function


        ''' <summary> Returns the filename of the default file log writer. </summary>
        ''' <returns> The filename of the log file. </returns>
        ''' <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        <Extension()>
        Public Function DefaultFileLogWriterFilePath(ByVal traceSource As TraceSource) As String
            If traceSource Is Nothing Then
                Return ""
            Else
                If traceSource.DefaultFileLogWriter Is Nothing Then
                    Return ""
                ElseIf traceSource.DefaultFileLogWriter IsNot Nothing Then
                    Return traceSource.DefaultFileLogWriter.FullLogFileName
                Else
                    Return ""
                End If
            End If
        End Function

        ''' <summary> Checks if the default file log writer file exists. </summary>
        ''' <returns> <c>True</c> if the log file exists. </returns>
        <Extension()>
        Public Function DefaultFileLogWriterFileExists(ByVal traceSource As TraceSource) As Boolean
            Return DefaultFileLogTraceListener.FileSize(traceSource.DefaultFileLogWriterFilePath) > 2
        End Function

#End Region

#Region " REPLACE TRACE LISTENER "

        ''' <summary> Replaces the default file log trace listener with a new one. </summary>
        ''' <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="logWriter"> The <see cref="Logging.FileLogTraceListener">log writer</see>. </param>
        ''' <returns>The <see cref="Logging.FileLogTraceListener">file log trace listener. </see></returns>
        <Extension()>
        Public Function ReplaceDefaultTraceListener(ByVal traceSource As TraceSource, ByVal logWriter As Logging.FileLogTraceListener) As Logging.FileLogTraceListener
            If traceSource IsNot Nothing Then
                traceSource.Listeners.Remove(DefaultFileLogTraceListener.DefaultFileLogWriterName)
                traceSource.Listeners.Add(logWriter)
            End If
            Return logWriter
        End Function

        ''' <summary> Replaces the default file log trace listener with a new one for the current user. </summary>
        ''' <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <remarks> The current user application data folder is used. </remarks>
        ''' <returns> The <see cref="Logging.FileLogTraceListener">file log trace listener. </see> </returns>
        <Extension()>
        Public Function ReplaceDefaultTraceListener(ByVal traceSource As TraceSource) As Logging.FileLogTraceListener
            Return ReplaceDefaultTraceListener(traceSource, UserLevel.CurrentUser)
        End Function

        ''' <summary> Replaces the default file log trace listener with a new one. </summary>
        ''' <param name="traceSource"> The <see cref="TraceSource">Trace Source</see>. </param>
        ''' <param name="userLevel"> if set to <c>True</c> uses the all users application data folder;
        ''' otherwise the current user application data folder is used. </param>
        ''' <returns> The <see cref="logging.FileLogTraceListener">file log trace listener. </see> </returns>
        <Extension()>
        Public Function ReplaceDefaultTraceListener(ByVal traceSource As TraceSource, ByVal userLevel As UserLevel) As Logging.FileLogTraceListener
            Dim tempListener As Logging.FileLogTraceListener = Nothing
            Dim listener As Logging.FileLogTraceListener = Nothing
            Try
                tempListener = New DefaultFileLogTraceListener(userLevel)
                listener = tempListener
                If traceSource IsNot Nothing Then
                    traceSource.ReplaceDefaultTraceListener(listener)
                End If
            Finally
                If tempListener IsNot Nothing Then tempListener.Dispose()
            End Try
            Return listener
        End Function

#End Region

    End Module
End Namespace
