﻿Imports System.Runtime.CompilerServices
Namespace DiagnosticsExtensions

    ''' <summary> Provides translators for <see cref="Diagnostics.TraceLevel">Trace Level</see>. </summary>
    ''' <license> (c) 2014 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/09/2014" by="David" revision="2.0.5152.x"> Created. </history>
    Public Module TraceLevelMethods

#Region " TRACE LEVELS AND TRACE EVENT TYPES "

        ''' <summary> Builds trace level trace event type hash. </summary>
        ''' <returns> A Dictionary for translating trace levels to trace event types. </returns>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Function BuildTraceLevelTraceEventTypeHash() As Dictionary(Of TraceLevel, TraceEventType)
            Dim dix2 As New Dictionary(Of TraceLevel, TraceEventType)
            Dim dix3 As Dictionary(Of TraceLevel, TraceEventType) = dix2
            dix3.Add(Diagnostics.TraceLevel.Error, TraceEventType.Error)
            dix3.Add(Diagnostics.TraceLevel.Info, TraceEventType.Information)
            dix3.Add(Diagnostics.TraceLevel.Off, TraceEventType.Stop)
            dix3.Add(Diagnostics.TraceLevel.Verbose, TraceEventType.Verbose)
            dix3.Add(Diagnostics.TraceLevel.Warning, TraceEventType.Warning)
            dix3 = Nothing
            Return dix2
        End Function

        ''' <summary> Derives a <see cref="TraceEventType">trace event type</see>. </summary>
        ''' <param name="value"> A <see cref="TraceLevel">trace level</see> of a trace switch. Specifies
        ''' what messages to output for the <see cref="System.Diagnostics.Debug">debug</see>
        ''' and <see cref="System.Diagnostics.Trace">trace</see> and
        ''' <see cref="System.Diagnostics.TraceSwitch">trace switch</see> classes. </param>
        ''' <returns> The <see cref="TraceEventType">event type</see>. </returns>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        <Extension()>
        Public Function ToTraceEventType(ByVal value As TraceLevel) As TraceEventType
            Static hash As Dictionary(Of TraceLevel, TraceEventType)
            If hash Is Nothing Then
                hash = TraceLevelMethods.BuildTraceLevelTraceEventTypeHash
            End If
            If hash.ContainsKey(value) Then
                Return hash(value)
            Else
                Return TraceEventType.Information
            End If
        End Function

#End Region

    End Module

End Namespace

