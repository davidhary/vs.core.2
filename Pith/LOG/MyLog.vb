﻿Imports isr.Core.Pith.DiagnosticsExtensions
''' <summary> Extends the <see cref="Microsoft.VisualBasic.Logging.Log">log</see>. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="2/16/2014" by="David" revision=""> Created. </history>
Public Class MyLog
    Inherits Logging.Log
    Implements ITraceMessageListener

#Region " CONSTRUCTORS "

    Public Const DefaultFileLogTraceListenerName As String = "FileLog"
    Public Const DefaultSourceName As String = "DefaultSource"
    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.Initialize()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 1/22/2016. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name)
        Me.Initialize()
    End Sub

    ''' <summary> Gets or sets the trace source. </summary>
    ''' <value> The trace source. </value>
    Public Overloads Property TraceSource As MyTraceSource

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 1/22/2016. </remarks>
    Private Sub Initialize()
        Dim name As String = MyBase.TraceSource.Name
        Me._TraceSource = New MyTraceSource(name)
        If Not Me.TraceSource.HasBeenConfigured Then
            MyBase.InitializeWithDefaultsSinceNoConfigExists()
        End If
        Me.TraceSource?.ApplyTraceLevel(TraceEventType.Verbose)
        MyBase.TraceSource?.ApplyTraceLevel(TraceEventType.Verbose)
    End Sub

#End Region

#Region " WRITE LOG ENTRY "

    ''' <summary> Writes an entry. </summary>
    ''' <remarks> Overloads the underlying method. </remarks>
    ''' <param name="message">  The message details. </param>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    Public Overloads Sub WriteEntry(ByVal message As String, ByVal severity As TraceEventType)
        Me.TraceSource.TraceEvent(severity, message)
    End Sub

    ''' <summary> Writes an entry. </summary>
    ''' <remarks> Overloads the underlying method. </remarks>
    ''' <param name="message">  The message details. </param>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    Public Overloads Sub WriteEntry(ByVal message As String, ByVal severity As TraceEventType, ByVal id As Integer)
        Me.TraceSource.TraceEvent(severity, id, message)
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="details">  The message details. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal details As String) As String
        If details IsNot Nothing Then
            Me.WriteEntry(details, severity)
            Return details
        End If
        Return ""
    End Function

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="details">  The message details. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal id As Integer, ByVal details As String) As String
        If details IsNot Nothing Then
            Me.WriteEntry(details, severity, id)
            Return details
        End If
        Return ""
    End Function

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="format">   The message format. </param>
    ''' <param name="args">     Specified the message arguments. </param>
    ''' <returns> The message details or empty. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
        If format IsNot Nothing Then
            Me.WriteLogEntry(severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End If
        Return ""
    End Function

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="format">   The message format. </param>
    ''' <param name="args">     Specified the message arguments. </param>
    ''' <returns> The message details or empty. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal id As Integer,
                                      ByVal format As String, ByVal ParamArray args() As Object) As String
        If format IsNot Nothing Then
            Me.WriteLogEntry(severity, id, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End If
        Return ""
    End Function


    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="messages"> Message information to me. </param>
    ''' <returns> The message details or empty. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal messages As String()) As String
        If messages IsNot Nothing Then
            Return Me.WriteLogEntry(severity, String.Join(",", messages))
        End If
        Return ""
    End Function

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="messages"> Message information to me. </param>
    ''' <returns> The message details or empty. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal id As Integer, ByVal messages As String()) As String
        If messages IsNot Nothing Then
            Return Me.WriteLogEntry(severity, id, String.Join(",", messages))
        End If
        Return ""
    End Function

#End Region

#Region " WRITE EXCEPTION DETAILS "

    ''' <summary> Writes an exception. </summary>
    ''' <param name="ex"> The exception. </param>
    Public Overloads Sub WriteException(ByVal ex As Exception)
        Me.WriteException(ex, TraceEventType.Error, "", 0)
    End Sub

    ''' <summary> Writes an exception. </summary>
    ''' <param name="ex">             The exception. </param>
    ''' <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
    ''' data. </param>
    ''' <param name="additionalInfo"> Additional information. </param>
    Public Overloads Sub WriteException(ByVal ex As Exception, ByVal severity As TraceEventType, ByVal additionalInfo As String)
        Me.WriteException(ex, severity, additionalInfo, 0)
    End Sub

    ''' <summary> Writes an exception. </summary>
    ''' <param name="ex">             The exception. </param>
    ''' <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
    ''' data. </param>
    ''' <param name="additionalInfo"> Additional information. </param>
    ''' <param name="id">             The identifier. </param>
    Public Overloads Sub WriteException(ByVal ex As Exception, ByVal severity As TraceEventType, ByVal additionalInfo As String, ByVal id As Integer)
        If ex IsNot Nothing Then
            Dim builder As New System.Text.StringBuilder
            builder.Append(ex.Message)
            If Not String.IsNullOrWhiteSpace(additionalInfo) Then
                builder.Append("; ")
                builder.Append(additionalInfo)
            End If
            Me.WriteEntry(builder.ToString, severity, id)
            builder.Clear()
        End If
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="ex">             The exception. </param>
    ''' <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
    ''' data. </param>
    ''' <param name="additionalInfo"> Additional information. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType, ByVal additionalInfo As String)
        If ex IsNot Nothing Then
            Me.WriteException(ex, severity, additionalInfo)
            If ex.StackTrace IsNot Nothing Then
                Dim stackTrace As String() = ex.StackTrace.Split(CChar(Environment.NewLine))
                Me.WriteLogEntry(severity, stackTrace)
            End If
            If ex.Data IsNot Nothing AndAlso ex.Data.Count > 0 Then
                For Each keyValuePair As System.Collections.DictionaryEntry In ex.Data
                    Me.WriteEntry(keyValuePair.Key.ToString & "=" & keyValuePair.Value.ToString, severity)
                Next
            End If
            If ex.InnerException IsNot Nothing Then
                Me.WriteExceptionDetails(ex.InnerException, severity, "(Inner Exception)")
            End If
        End If

    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="ex">             The exception. </param>
    ''' <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
    ''' data. </param>
    ''' <param name="id">             The identifier. </param>
    ''' <param name="additionalInfo"> Additional information. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType, ByVal id As Integer, ByVal additionalInfo As String)
        If ex IsNot Nothing Then
            Me.WriteException(ex, severity, additionalInfo, id)
            If ex.StackTrace IsNot Nothing Then
                Dim stackTrace As String() = ex.StackTrace.Split(CChar(Environment.NewLine))
                Me.WriteLogEntry(severity, stackTrace)
            End If
            If ex.Data IsNot Nothing AndAlso ex.Data.Count > 0 Then
                For Each keyValuePair As System.Collections.DictionaryEntry In ex.Data
                    Me.WriteEntry(keyValuePair.Key.ToString & "=" & keyValuePair.Value.ToString, severity, id)
                Next
            End If
            If ex.InnerException IsNot Nothing Then
                Me.WriteExceptionDetails(ex.InnerException, severity, id, "(Inner Exception)")
            End If
        End If

    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="ex">       The exception. </param>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="format">   The additional information format. </param>
    ''' <param name="args">     The additional information arguments. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType,
                                     ByVal format As String, ByVal ParamArray args() As Object)
            Me.WriteExceptionDetails(ex, severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="ex">       The exception. </param>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="format">   The additional information format. </param>
    ''' <param name="args">     The additional information arguments. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType,
                                         ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object)
            Me.WriteExceptionDetails(ex, severity, id, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="ex">  The exception. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception)
            Me.WriteExceptionDetails(ex, TraceEventType.Error, "")
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="ex">  The exception. </param>
    ''' <param name="id">  The identifier. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception, ByVal id As Integer)
        Me.WriteExceptionDetails(ex, TraceEventType.Error, id, "")
    End Sub

#End Region

#Region " WRITE LOG ENTRY -- OVERRIDE TRACE LEVEL "

    ''' <summary> Writes a message to the application log listeners. Overrides the current log level. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="details">  The message details. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntryOverride(ByVal severity As TraceEventType, ByVal details As String) As String
        If details IsNot Nothing Then
            ' save the current trace level.
            Dim lastSourceLevel As Diagnostics.SourceLevels = Me.TraceSource.Switch.Level
            ' set the requested level.
            Me.TraceSource.Switch.Level = severity.ToSourceLevel
            ' write the entry.
            Me.WriteEntry(details, severity)
            ' restore the level.
            Me.TraceSource.Switch.Level = lastSourceLevel
            Return details
        End If
        Return ""
    End Function

    ''' <summary> Writes a message to the application log listeners. Overrides the current log level. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="details">  The message details. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntryOverride(ByVal severity As TraceEventType,
                                              ByVal id As Integer, ByVal details As String) As String
        If details IsNot Nothing Then
            ' save the current trace level.
            Dim lastSourceLevel As Diagnostics.SourceLevels = Me.TraceSource.Switch.Level
            ' set the requested level.
            Me.TraceSource.Switch.Level = severity.ToSourceLevel
            ' write the entry.
            Me.WriteEntry(details, severity, id)
            ' restore the level.
            Me.TraceSource.Switch.Level = lastSourceLevel
            Return details
        End If
        Return ""
    End Function

    ''' <summary> Writes a message to the application log listeners. Overrides the current log level. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="format">   The message details. </param>
    ''' <param name="args">     The arguments. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntryOverride(ByVal severity As TraceEventType,
                                          ByVal format As String, ByVal ParamArray args() As Object) As String
        If format IsNot Nothing Then
            Return Me.WriteLogEntryOverride(severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End If
        Return ""
    End Function

    ''' <summary> Writes a message to the application log listeners. Overrides the current log level. </summary>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="format">   The message details. </param>
    ''' <param name="args">     The arguments. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntryOverride(ByVal severity As TraceEventType,
                                              ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object) As String
        If format IsNot Nothing Then
            Return Me.WriteLogEntryOverride(severity, id, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End If
        Return ""
    End Function

#End Region

#Region " LOG TRACE MESSAGES "

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="message"> The message. </param>
    Public Sub WriteLogEntry(ByVal message As TraceMessage)
        If message IsNot Nothing Then
            If Not String.IsNullOrWhiteSpace(message.Details) Then
                Me.WriteLogEntry(message.EventType, message.Id, message.Details)
            ElseIf message.HasSynopsis Then
                Me.WriteLogEntry(message.EventType, message.Id, message.ExtractSynopsis)
            End If
        End If
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <param name="message">        The message. </param>
    ''' <param name="additionalInfo"> Information describing the additional. </param>
    Public Sub WriteLogEntry(ByVal message As TraceMessage, ByVal additionalInfo As String)
        If message IsNot Nothing Then
            If String.IsNullOrWhiteSpace(additionalInfo) Then
                If Not String.IsNullOrWhiteSpace(message.Details) Then
                    Me.WriteLogEntry(message.EventType, message.Id, message.Details)
                ElseIf message.HasSynopsis Then
                    Me.WriteLogEntry(message.EventType, message.Id, message.ExtractSynopsis)
                End If
            Else
                If Not String.IsNullOrWhiteSpace(message.Details) Then
                    Me.WriteLogEntry(message.EventType, message.Id, "{0},{1}", message.Details, additionalInfo)
                ElseIf message.HasSynopsis Then
                    Me.WriteLogEntry(message.EventType, message.Id, "{0},{1}", message.ExtractSynopsis, additionalInfo)
                End If
            End If
        End If
    End Sub

#End Region

#Region " FILE  "

    ''' <summary> Gets the file log trace listener. </summary>
    ''' <value> The file log trace listener. </value>
    Public ReadOnly Property FileLogTraceListener As Logging.FileLogTraceListener
        Get
            Return Me.TraceSource.DefaultFileLogWriter
        End Get
    End Property

    ''' <summary> Gets the filename of the full log file. </summary>
    ''' <value> The filename of the full log file. </value>
    Public ReadOnly Property FullLogFileName As String
        Get
            Return Me.FileLogTraceListener.FullLogFileName
        End Get
    End Property

    ''' <summary> Checks if the default file log writer file exists. </summary>
    ''' <returns> <c>True</c> if the log file exists. </returns>
    Public ReadOnly Property LogFileExists() As Boolean
        Get
            Return DefaultFileLogTraceListener.FileSize(Me.FullLogFileName) > 2
        End Get
    End Property

#End Region

#Region " REPLACE TRACE LISTENER "

    ''' <summary> Replaces the default file log trace listener with a new one. </summary>
    ''' <param name="logWriter"> The
    ''' <see cref="Logging.FileLogTraceListener">log writer</see>. </param>
    ''' <returns>The <see cref="Logging.FileLogTraceListener">file log trace listener. </see></returns>
    Public Function ReplaceDefaultTraceListener(ByVal logWriter As Logging.FileLogTraceListener) As Logging.FileLogTraceListener
        If Me.TraceSource IsNot Nothing Then
            Me.TraceSource.ReplaceDefaultTraceListener(logWriter)
            MyBase.TraceSource.ReplaceDefaultTraceListener(logWriter)
        End If
        Return logWriter
    End Function

    ''' <summary> Replaces the default file log trace listener with a new one for the current user. </summary>
    ''' <remarks> The current user application data folder is used. </remarks>
    ''' <returns> The <see cref="Logging.FileLogTraceListener">file log trace listener. </see> </returns>
    Public Function ReplaceDefaultTraceListener() As Logging.FileLogTraceListener
        Return Me.ReplaceDefaultTraceListener(UserLevel.CurrentUser)
    End Function

    ''' <summary> Replaces the default file log trace listener with a new one. </summary>
    ''' <param name="userLevel"> if set to <c>True</c> uses the all users application data folder;
    ''' otherwise the current user application data folder is used. </param>
    ''' <returns> The <see cref="Logging.FileLogTraceListener">file log trace listener. </see> </returns>
    Public Function ReplaceDefaultTraceListener(ByVal userLevel As UserLevel) As Logging.FileLogTraceListener
        Dim tempListener As Logging.FileLogTraceListener = Nothing
        Dim listener As Logging.FileLogTraceListener = Nothing
        Try
            tempListener = New DefaultFileLogTraceListener(userLevel)
            listener = tempListener
            Me.ReplaceDefaultTraceListener(listener)
        Finally
            If tempListener IsNot Nothing Then tempListener.Dispose()
        End Try
        Return listener
    End Function

#End Region

#Region " I TRACE MESSAGES LISTENER "

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 12/29/2015. </remarks>
    ''' <param name="value"> The event message. </param>
    ''' <returns> The trace message details. </returns>
    Public Function TraceEvent(ByVal value As TraceMessage) As String Implements ITraceMessageListener.TraceEvent
        If value Is Nothing Then
            Return ""
        Else
            If Me.ShouldTrace(value.EventType) Then Me.TraceSource.TraceEvent(value)
            System.Windows.Forms.Application.DoEvents()
            Return value.Details
        End If
    End Function

    ''' <summary> Gets or sets the trace level. </summary>
    ''' <value> The trace level. </value>
    Public Property TraceLevel As TraceEventType Implements ITraceMessageListener.TraceLevel
        Get
            If Me.TraceSource Is Nothing Then
                Return TraceEventType.Information
            Else
                Return Me.TraceSource.TraceLevel
            End If
        End Get
        Set(value As TraceEventType)
            Me.TraceSource?.ApplyTraceLevel(value)
            MyBase.TraceSource?.ApplyTraceLevel(value)
        End Set
    End Property

    ''' <summary> Checks if the log should trace the event type. </summary>
    ''' <param name="value"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> If the log should trace; <c>False</c> otherwise. </returns>
    Public Function ShouldTrace(ByVal value As TraceEventType) As Boolean Implements ITraceMessageListener.ShouldTrace
        Return value <= Me.TraceLevel
    End Function

#End Region

#Region " TRACE LEVEL "

    ''' <summary> Applies the trace level. </summary>
    ''' <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
    Public Sub ApplyTraceLevel(ByVal value As TraceEventType)
        Me.TraceLevel = value
    End Sub

#End Region

End Class

