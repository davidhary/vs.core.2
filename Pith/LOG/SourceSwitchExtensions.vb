﻿Imports System.Runtime.CompilerServices
Namespace DiagnosticsExtensions

    ''' <summary> Extends the <see cref="Diagnostics.SourceSwitch">source switch</see> functionality. </summary>
    ''' <license> (c) 2014 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/09/2014" by="David" revision="2.0.5152.x"> Created. </history>
    Public Module SourceSwitchMethods

#Region " TRACE LEVEL "

        ''' <summary> Applies the trace level. </summary>
        ''' <param name="switch"> The <see cref="SourceSwitch">Source switch</see>. </param>
        ''' <param name="value">  The <see cref="TraceEventType">trace level</see> value. </param>
        <Extension()>
        Public Sub ApplyTraceLevel(ByVal switch As SourceSwitch, ByVal value As TraceEventType)
            If switch IsNot Nothing Then
                switch.Level = value.ToSourceLevel
            End If
        End Sub

        ''' <summary> Returns the trace level. </summary>
        ''' <param name="switch"> The <see cref="SourceSwitch">Source switch</see>. </param>
        ''' <returns> The trace level; <see cref="TraceEventType.Information">information</see> if nothing. </returns>
        <Extension()>
        Public Function TraceLevel(ByVal switch As SourceSwitch) As TraceEventType
            If switch Is Nothing Then
                Return TraceEventType.Verbose
            Else
                Return switch.Level.ToTraceEventType
            End If
        End Function

#End Region

    End Module

End Namespace
