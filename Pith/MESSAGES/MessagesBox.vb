Imports System.ComponentModel
Imports System.Threading
Imports System.Windows.Forms
Imports isr.Core.Pith.EventHandlerExtensions
''' <summary> Messages display text box. </summary>
''' <license> (c) 2002 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/21/2002" by="David" revision="1.0.839.x"> created. </history>
<Description("Messages Test Box")>
Public Class MessagesBox
    Inherits TextBox
    Implements INotifyPropertyChanged

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Constructor for this class. </summary>
    Public Sub New()
        MyBase.New()

        ' set defaults
        Me.syncLocker = New Object
        Me._lines = New List(Of String)
        Me.Multiline = True
        Me.ReadOnly = True
        Me.CausesValidation = False
        Me.ScrollBars = ScrollBars.Both
        Me.Size = New System.Drawing.Size(150, 150)

        Me._InitializeKnownState()

        MyBase.ContextMenuStrip = createContextMenuStrip()
        Me.ContentsQueue = New Queue(Of String)
        Me.InitializeWorker()

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.ContainerPanel = Nothing
                Me.DisposeWorker()
                Me._ContentsQueue?.Clear() : Me._ContentsQueue = Nothing
                Me._lines?.Clear() : Me._lines = Nothing
                Me._MyContextMenuStrip?.Dispose() : Me._MyContextMenuStrip = Nothing
                Me.RemoveEventHandler(Me.PropertyChangedEvent)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary> Initializes the initialize known state. </summary>
    ''' <remarks> David, 12/23/2015. </remarks>
    Private Sub _InitializeKnownState()
        Me.BackColor = Drawing.SystemColors.Info
        Me.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ResetCount = 200
        Me.PresetCount = 100
        Me.Appending = False
        Me.TabCaption = "Log"
        Me.CaptionFormat = "{0} " & System.Text.Encoding.GetEncoding(437).GetString(New Byte() {240})
    End Sub

    ''' <summary> Initializes the known state. </summary>
    ''' <remarks> David, 12/23/2015. </remarks>
    Protected Overridable Sub InitializeKnownState()
        Me._InitializeKnownState()
    End Sub

#End Region

#Region " LIST MANAGER "

    Private _Appending As Boolean
    ''' <summary> Gets the appending sentinel. Items are appended to an appending list. 
    '''           Otherwise, items are added to the top of the list. </summary>
    ''' <value> The ascending sentinel; True if the list is ascending or descending. </value>
    <Category("Appearance"), Description("True to add items to the bottom of the list"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(False)>
    Public Property Appending As Boolean
        Get
            Return Me._Appending
        End Get
        Set(value As Boolean)
            If value <> Me.Appending Then
                Me._Appending = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.Appending))
            End If
        End Set
    End Property

    ''' <summary> The synchronization locker. </summary>
    Private syncLocker As Object

    ''' <summary> Holds the list of lines to display. </summary>
    Private _lines As List(Of String)

    ''' <summary> Adds the message to the message list. A message may include one or more lines. </summary>
    ''' <param name="message"> The message to add. </param>
    Private Sub AddMessageLines(ByVal message As String)
        If String.IsNullOrWhiteSpace(message) Then
            message = ""
        End If
        Dim values As String() = message.Split(CChar(Environment.NewLine))
        If values.Count = 1 Then
            If Me.Appending Then
                Me._lines.Add(message)
            Else
                Me._lines.Insert(0, message)
            End If
        Else
            If Me.Appending Then
                Me._lines.AddRange(values)
            Else
                Me._lines.InsertRange(0, values)
            End If
        End If
    End Sub

    ''' <summary> Executes the clear action. </summary>
    Protected Overridable Sub OnClear()
        Me._lines.Clear()
        MyBase.Clear()
        Me.NewMessagesAdded = False
    End Sub

    ''' <summary> Clears all text from the text box control. </summary>
    Public Shadows Sub Clear()
        If Me.InvokeRequired Then
            Me.Invoke(New Action(AddressOf Me.Clear), New Object() {})
        Else
            If Not Me.IsDisposed AndAlso Me._lines IsNot Nothing Then
                SyncLock syncLocker
                    Me.OnClear()
                End SyncLock
            End If
        End If
    End Sub

    Private _resetCount As Integer

    ''' <summary> Gets or sets the reset count. </summary>
    ''' <remarks> The message list gets reset to the preset count when the message count exceeds the
    ''' reset count. </remarks>
    ''' <value> <c>ResetSize</c>is an integer property. </value>
    <Category("Appearance"), Description("Number of lines at which to reset"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("100")>
    Public Property ResetCount() As Integer
        Get
            Return Me._resetCount
        End Get
        Set(ByVal value As Integer)
            If Me.ResetCount <> value Then
                Me._resetCount = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.ResetCount))
            End If
        End Set
    End Property

    Private _presetCount As Integer

    ''' <summary> Gets or sets the preset count. </summary>
    ''' <remarks> The message list gets reset to the preset count when the message count exceeds the
    ''' reset count. </remarks>
    ''' <value> <c>PresetSize</c>is an integer property. </value>
    <Category("Appearance"), Description("Number of lines to reset to"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("50")>
    Public Property PresetCount() As Integer
        Get
            Return Me._presetCount
        End Get
        Set(ByVal value As Integer)
            If Me.PresetCount <> value Then
                Me._presetCount = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.PresetCount))
            End If
        End Set
    End Property

    ''' <summary> Gets the sentinel indicating if the control is visible to the user. </summary>
    ''' <value> The showing. </value>
    Protected ReadOnly Property UserVisible As Boolean
        Get
            Return Me.Visible AndAlso (Me.GetContainerControl IsNot Nothing AndAlso
                                       Me.GetContainerControl.ActiveControl IsNot Nothing AndAlso Me.GetContainerControl.ActiveControl.Visible)
        End Get
    End Property

    ''' <summary> Displays the available lines and clear the <see cref="NewMessagesAdded">message
    ''' sentinel</see>. </summary>
    Public Overridable Sub Display()
        If Me.UserVisible Then
            Me.NewMessagesAdded = False
            Me.Lines = Me._lines.ToArray
            Me.SelectionStart = 0
            Me.SelectionLength = 0
        End If
    End Sub

    ''' <summary> Raises the <see cref="E: Control.VisibleChanged" /> event. 
    '''           Updates the display. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnVisibleChanged(e As System.EventArgs)
        Me.Display()
        MyBase.OnVisibleChanged(e)
    End Sub

#End Region

#Region " ADD MESSAGE "

    ''' <summary> Prepends or appends a new value to the messages box. </summary>
    ''' <remarks> Starts a Background Worker by calling RunWorkerAsync. The Text property of
    ''' the TextBox control is set when the Background Worker raises the RunWorkerCompleted event. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub AddMessage(ByVal value As String)
        Me.ContentsQueue.Enqueue(value)
        Me.FlushMessages()
    End Sub

    ''' <summary> Adds an error message to the message box. </summary>
    ''' <param name="value"> The <see cref="System.Exception">error</see> to add. </param>
    Public Sub AddMessage(ByVal value As Exception)

        If Not (value Is Nothing OrElse String.IsNullOrWhiteSpace(value.Message)) Then

            If value.InnerException IsNot Nothing Then
                Me.AddMessage(value.InnerException)
            End If

            Dim messageBuilder As New System.Text.StringBuilder
            messageBuilder.Append(value.Message)
            If value IsNot Nothing AndAlso value.StackTrace IsNot Nothing Then
                messageBuilder.AppendLine()
                messageBuilder.Append("Stack Trace ")
                messageBuilder.AppendLine()
                messageBuilder.Append(value.StackTrace)
            End If
            If value.Data IsNot Nothing AndAlso value.Data.Count > 0 Then
                messageBuilder.AppendLine()
                messageBuilder.Append("Data: ")
                For Each keyValuePair As System.Collections.DictionaryEntry In value.Data
                    messageBuilder.AppendLine()
                    messageBuilder.Append(keyValuePair.Key.ToString & "=" & keyValuePair.Value.ToString)
                Next
            End If

            Me.AddMessage(messageBuilder.ToString)

        End If

    End Sub

#End Region

#Region " CAPTION "

    Private _NewMessagesAdded As Boolean

    ''' <summary> Gets or sets the sentinel indicating that new messages were added. </summary>
    ''' <value> The new messages available. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property NewMessagesAdded As Boolean
        Get
            Return Me._NewMessagesAdded
        End Get
        Set(value As Boolean)
            If value <> Me.NewMessagesAdded Then
                Me._NewMessagesAdded = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.NewMessagesAdded))
                Me.UpdateCaption()
            End If
        End Set
    End Property

    Private _TabCaption As String

    ''' <summary> Gets or sets the tab caption. </summary>
    ''' <value> The tab caption. </value>
    <Category("Appearance"), Description("Default title for the parent tab"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("Log")>
    Public Property TabCaption() As String
        Get
            Return Me._TabCaption
        End Get
        Set(ByVal Value As String)
            If Me.TabCaption <> Value Then
                Me._TabCaption = Value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.TabCaption))
                Me.UpdateCaption()
            End If
        End Set
    End Property

    Private _Captionformat As String

    ''' <summary> Gets or sets the caption format indicating that messages were added. </summary>
    ''' <value> The tab caption format. </value>
    <Category("Appearance"), Description("Formats the tab caption with number of new messages"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("{0} =")>
    Public Property CaptionFormat() As String
        Get
            Return Me._Captionformat
        End Get
        Set(ByVal Value As String)
            If String.IsNullOrEmpty(Value) Then Value = ""
            If Not Value.Equals(Me.CaptionFormat) Then
                Me._Captionformat = Value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.CaptionFormat))
                Me.UpdateCaption()
            End If
        End Set
    End Property

    Private Property _Caption As String

    ''' <summary> Gets or sets the caption. </summary>
    ''' <value> The caption. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property Caption As String
        Get
            Return Me._Caption
        End Get
        Set(value As String)
            If String.IsNullOrEmpty(value) Then value = ""
            If Not value.Equals(Me.Caption) Then
                Me._Caption = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.Caption))
                Me.SafeTextSetter(Me.ContainerPanel, Me.Caption)
                Me.SafeTextSetter(Me.ContainerTreeNode, Me.Caption)
            End If
        End Set
    End Property

    ''' <summary> Updates the caption. </summary>
    Private Sub UpdateCaption()
        If True = Me.NewMessagesAdded Then
            Me.Caption = String.Format(Globalization.CultureInfo.CurrentCulture, Me.CaptionFormat, Me.TabCaption)
        Else
            Me.Caption = Me.TabCaption
        End If
    End Sub

    ''' <summary> Gets or sets the container panel. </summary>
    ''' <value> The parent panel. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ContainerPanel As Panel

    ''' <summary> Gets or sets the container tree node. </summary>
    ''' <value> The container tree node. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ContainerTreeNode As TreeNode

    ''' <summary> Updates the container panel caption. </summary>
    ''' <param name="panel">The container panel.</param>
    ''' <param name="value">The value.</param>
    Private Sub SafeTextSetter(ByVal panel As Panel, ByVal value As String)
        If panel IsNot Nothing Then
            If panel.InvokeRequired Then
                panel.Invoke(New Action(Of Panel, String)(AddressOf Me.SafeTextSetter), New Object() {panel, value})
            Else
                panel.Text = value
            End If
        End If
    End Sub

    ''' <summary> Updates the container tree node caption. </summary>
    ''' <remarks> David, 12/25/2015. </remarks>
    ''' <param name="node"> The container tree node. </param>
    ''' <param name="value"> The value. </param>
    Private Sub SafeTextSetter(ByVal node As TreeNode, ByVal value As String)
        If node IsNot Nothing Then
            If node.TreeView?.InvokeRequired Then
                node.TreeView.Invoke(New Action(Of TreeNode, String)(AddressOf Me.SafeTextSetter), New Object() {node, value})
            Else
                node.Text = value
                System.Windows.Forms.Application.DoEvents()
            End If
        End If
    End Sub

#End Region

#Region " CONTEXT MENU STRIP "

    Dim _MyContextMenuStrip As ContextMenuStrip

    ''' <summary> Creates a context menu strip. </summary>
    ''' <returns> The new context menu strip. </returns>
    Private Function createContextMenuStrip() As ContextMenuStrip

        ' Create a new ContextMenuStrip control.
        Me._MyContextMenuStrip = New ContextMenuStrip()

        ' Attach an event handler for the 
        ' ContextMenuStrip control's Opening event.
        AddHandler _MyContextMenuStrip.Opening, AddressOf Me.contectMenuOpeningHandler

        Return Me._MyContextMenuStrip

    End Function

    ''' <summary> Adds menu items. </summary>
    ''' <remarks> This event handler is invoked when the <see cref="ContextMenuStrip"/> control's
    ''' Opening event is raised. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub contectMenuOpeningHandler(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)

        Me._MyContextMenuStrip = CType(sender, ContextMenuStrip)

        ' Clear the ContextMenuStrip control's Items collection.
        Me._MyContextMenuStrip.Items.Clear()

        ' Populate the ContextMenuStrip control with its default items.
        ' myContextMenuStrip.Items.Add("-")
        Me._MyContextMenuStrip.Items.Add(New ToolStripMenuItem("Clear &All", Nothing, AddressOf Me.clearAllHandler, "Clear"))
        Me._MyContextMenuStrip.Items.Add(New ToolStripMenuItem("Flush &Queue", Nothing, AddressOf Me.flushQueuesHandler, "Flush"))

        ' Set Cancel to false. 
        ' It is optimized to true based on empty entry.
        e.Cancel = False

    End Sub

    ''' <summary> Applies the high point Output. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub clearAllHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Clear()
    End Sub

    ''' <summary> Flush messages and content. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub flushQueuesHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Me.FlushContent()
        Me.FlushMessages()
    End Sub

#End Region

#Region " PROPERTY CHANGED "

    ''' <summary> Gets the sentinel indicating that multiple synchronization contexts are expected. </summary>
    ''' <remarks> When having multiple user interfaces or a thread running within the user interface,
    ''' the current synchronization context may not reflect the contexts of the current UI causing a
    ''' cross thread exceptions. In this case the more complex
    ''' <see cref="SafeInvokePropertyChanged">thread safe methods</see> must be used. </remarks>
    ''' <value> <c>True</c> if more than one synchronization contexts should be expected. </value>
    Public Property MultipleSyncContextsExpected As Boolean

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As PropertyChangedEventHandler)
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanged, CType(d, PropertyChangedEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub InvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokePropertyChanged(ByVal obj As Object)
        Me.InvokePropertyChanged(CType(obj, PropertyChangedEventArgs))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeInvokePropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeInvokePropertyChanged(New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        For Each d As [Delegate] In evt.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    target.Invoke(d, New Object() {Me, e})
                End If
            End If
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeBeginInvokePropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeBeginInvokePropertyChanged(New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeBeginInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        For Each d As [Delegate] In evt.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    target.BeginInvoke(d, New Object() {Me, e})
                End If
            End If
        Next
    End Sub

#End Region

#Region " NOTIFICATIONS "

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    Protected Overridable Sub AsyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If e IsNot Nothing Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeBeginInvokePropertyChanged(e)
            Else
                SynchronizationContext.Current.Post(New SendOrPostCallback(AddressOf InvokePropertyChanged), e)
            End If
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub AsyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.AsyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

    ''' <summary>
    ''' Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes or
    ''' Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property value.
    ''' </summary>
    ''' <remarks> David, 12/11/2015. </remarks>
    ''' <param name="e"> Property changed event information. </param>
    Protected Overridable Sub SyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If e IsNot Nothing Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeInvokePropertyChanged(e)
            Else
                SynchronizationContext.Current.Send(New SendOrPostCallback(AddressOf InvokePropertyChanged), e)
            End If
        End If
    End Sub

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region


#End Region

#Region " WORKER "

    ''' <summary> Initializes the worker. </summary>
    ''' <remarks> David, 1/1/2016. </remarks>
    Private Sub InitializeWorker()
        Me.worker = New BackgroundWorker()
        Me.worker.WorkerSupportsCancellation = True
    End Sub

    ''' <summary> The background worker is used for setting the messages text box
    ''' in a thread safe way. </summary>
    Private WithEvents worker As BackgroundWorker

    ''' <summary> Dispose worker. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub DisposeWorker()
        Try
            If Me.worker IsNot Nothing Then
                Me.worker.CancelAsync()
                Application.DoEvents()
                If Not (Me.worker.IsBusy OrElse Me.worker.CancellationPending) Then
                    Me.worker.Dispose()
                End If
            End If
        Catch
        End Try
    End Sub

#End Region

#Region " WORKER WORK: DISPLAY QUEUED MESSAGES "

    ''' <summary> Gets a queue of contents. </summary>
    ''' <value> A Queue of contents. </value>
    Private Property ContentsQueue As Queue(Of String)

    ''' <summary> This event handler sets the Text property of the TextBox control. It is called on the
    ''' thread that created the TextBox control, so the call is thread-safe. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Run worker completed event information. </param>
    Private Sub UpdateDisplay(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles worker.RunWorkerCompleted

        If Not (Me.IsDisposed OrElse e.Cancelled OrElse e.Error IsNot Nothing) Then
            SyncLock syncLocker
                Do While Me.ContentsQueue.Count > 0
                    Me.AddMessageLines(Me.ContentsQueue.Dequeue)
                Loop
                If Me._lines.Count > Me.ResetCount Then
                    If Me.Appending Then
                        Me._lines.RemoveRange(0, Me._lines.Count - Me.PresetCount)
                    Else
                        Me._lines.RemoveRange(Me.PresetCount, Me._lines.Count - Me.PresetCount)
                    End If
                End If
                Me.NewMessagesAdded = Not Me.UserVisible
            End SyncLock
            Me.Display()
            Me.Invalidate()
            Application.DoEvents()
        End If
    End Sub

    ''' <summary> Uses the message worker to flush any queued messages. </summary>
    ''' <remarks> If the calling thread is different from the thread that created the TextBox control,
    ''' this method creates a SetTextCallback and calls itself asynchronously using the Invoke
    ''' method. </remarks>
    Public Sub FlushMessages()
        If Me.InvokeRequired Then
            Me.Invoke(New Action(AddressOf Me.FlushMessages), New Object() {})
        Else
            If Not (Me.IsDisposed OrElse Me.worker.IsBusy) Then
                Me.worker.RunWorkerAsync()
                Application.DoEvents()
            End If
        End If
    End Sub

#End Region

End Class
