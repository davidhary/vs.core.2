﻿Imports isr.Core.Pith.AssemblyExtensions
''' <summary> Provides information about the application's assembly. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="2/15/2014" by="David" revision=""> Created. </history>
Public Class MyAssemblyInfo
    Inherits ApplicationServices.AssemblyInfo

    ''' <summary> Constructor. </summary>
    ''' <param name="assembly"> The assembly. </param>
    Public Sub New(ByVal assembly As System.Reflection.Assembly)
        MyBase.New(assembly)
        Me.Assembly = assembly
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As ApplicationServices.AssemblyInfo)
        Me.New(value.GetAssembly)
    End Sub

#Region " EXTENDED INFORMATION "

    ''' <summary> Gets the assembly. </summary>
    ''' <value> The assembly. </value>
    Public Property Assembly As System.Reflection.Assembly

    ''' <summary> Gets the pathname of the application folder. </summary>
    ''' <value> The pathname of the application folder. </value>
    Public ReadOnly Property ApplicationFolder As String
        Get
            Return Me.DirectoryPath
        End Get
    End Property

    ''' <summary> Returns the application file name (with extension). </summary>
    ''' <value> The name of the application. </value>
    Public ReadOnly Property ApplicationFileName() As String
        Get
            Return Me.FileInfo.Name
        End Get
    End Property

    ''' <summary> Returns the application file name (without extension). </summary>
    ''' <value> The name of the application. </value>
    Public ReadOnly Property ApplicationFileTitle() As String
        Get
            Return Me.AssemblyName
        End Get
    End Property

    ''' <summary> Returns the application Title. </summary>
    ''' <value> The Title of the application. </value>
    Public ReadOnly Property ApplicationTitle() As String
        Get
            Return Me.Title
        End Get
    End Property

    Dim _fileInfo As System.IO.FileInfo
    ''' <summary> Gets the file info for the assembly file. </summary>
    ''' <value> Information describing the file version. </value>
    Public ReadOnly Property FileInfo As System.IO.FileInfo
        Get
            If Me._fileInfo Is Nothing Then
                Me._fileInfo = New System.IO.FileInfo(Me.Location)
            End If
            Return Me._fileInfo
        End Get
    End Property

    Dim _fileVersionInfo As FileVersionInfo
    ''' <summary> Gets information describing the file version. </summary>
    ''' <value> Information describing the file version. </value>
    Public ReadOnly Property FileVersionInfo As FileVersionInfo
        Get
            If Me._fileVersionInfo Is Nothing Then
                Me._fileVersionInfo = Me.Assembly.GetFileVersionInfo
            End If
            Return Me._fileVersionInfo
        End Get
    End Property

    ''' <summary> Gets the location. </summary>
    ''' <value> The location. </value>
    Public ReadOnly Property Location As String
        Get
            Return Me.Assembly.Location
        End Get
    End Property

    ''' <summary> Gets the public key. </summary>
    ''' <value> The public key. </value>
    Public ReadOnly Property PublicKey As String
        Get
            Return Me.Assembly.GetPublicKey
        End Get
    End Property

    ''' <summary> Gets the public key token. </summary>
    ''' <value> The public key token. </value>
    Public ReadOnly Property PublicKeyToken As String
        Get
            Return Assembly.GetPublicKeyToken
        End Get
    End Property

    ''' <summary> Gets the product version. </summary>
    ''' <returns> The product version. </returns>
    Public Function ProductVersion(ByVal format As String) As String
        Return MyBase.ProductVersion(format)
    End Function

    ''' <summary> Returns the application product version. </summary>
    ''' <returns> The product version. </returns>
    Public Function ProductVersion(ByVal fieldCount As Integer) As String
        Return Me.Version.ToString(fieldCount)
    End Function

#End Region

#Region " CAPTION "

    ''' <summary> Builds the default caption. </summary>
    ''' <param name="subtitle"> The subtitle. </param>
    ''' <returns> System.String. </returns>
    Public Function BuildDefaultCaption(ByVal subtitle As String) As String

        Dim builder As New System.Text.StringBuilder
        builder.Append(Me.ApplicationTitle)
        builder.Append(" ")
        builder.Append(Me.ProductVersion(3))
        If Me.Version.Major < 1 Then
            builder.Append(".")
            Select Case Me.Version.Minor
                Case 0
                    builder.Append("Alpha")
                Case 1
                    builder.Append("Beta")
                Case 2 To 8
                    builder.Append(String.Format(Globalization.CultureInfo.CurrentCulture, "RC{0}", Me.Version.Minor - 1))
                Case Else
                    builder.Append("Gold")
            End Select
        End If
        If Not String.IsNullOrWhiteSpace(subtitle) Then
            builder.Append(": ")
            builder.Append(subtitle)
        End If
        Return builder.ToString

    End Function

#End Region

#Region " APPLICATION DATA FOLDER "

    ''' <summary> Gets the application data folder Path. This seems to create the folder if it does not
    ''' exist. </summary>
    ''' <param name="userLevel"> The user level. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function OpenApplicationDataFolderPath(ByVal userLevel As UserLevel) As String
        If userLevel.AllUsers = userLevel Then
            Return My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData
        Else
            Return My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData
        End If
    End Function

#End Region

#Region " APPLICATION CONFIGURATION FILE "

    ''' <summary>
    ''' The default configuration extension
    ''' </summary>
    Private ReadOnly defaultConfigExtension As String = ".config"

    ''' <summary> Returns the file path of the default application configuration file. The folder is
    ''' created if it does not exists. </summary>
    ''' <param name="userLevel">                  The user level. </param>
    ''' <param name="significantVersionElements"> The significant version elements. A value between 1
    ''' and 4 indicating the version level to use. For example, with 1, only the major version of the
    ''' folder name is used; whereas with 4 all version elements are used. Defaults to 4. </param>
    ''' <returns> The configuration file full path. Uses the application folder if this folder is
    ''' writable. Otherwise the <see cref="OpenApplicationConfigFolderPath">application data
    ''' folder</see> is used. System.String. </returns>
    Public Function BuildApplicationConfigFilePath(ByVal userLevel As UserLevel, ByVal significantVersionElements As Integer) As String
        Return Me.BuildApplicationConfigFilePath(userLevel, Me.defaultConfigExtension, significantVersionElements)
    End Function

    ''' <summary> Returns the file path of the application configuration file. The folder is created if
    ''' it does not exists. </summary>
    ''' <param name="userLevel">                  The user level. </param>
    ''' <param name="extension">                  The extension. </param>
    ''' <param name="significantVersionElements"> The significant version elements. A value between 1
    ''' and 4 indicating the version level to use. For example, with 1, only the major version of the
    ''' folder name is used; whereas with 4 all version elements are used. Defaults to 4. </param>
    ''' <returns> The configuration file full path. Uses the application folder if this folder is
    ''' writable. Otherwise the <see cref="OpenApplicationConfigFolderPath">application data
    ''' folder</see> is used. System.String. </returns>
    Public Function BuildApplicationConfigFilePath(ByVal userLevel As UserLevel, ByVal extension As String, ByVal significantVersionElements As Integer) As String
        Return System.IO.Path.Combine(MyAssemblyInfo.OpenApplicationConfigFolderPath(userLevel, significantVersionElements),
                                      Me.FileInfo.Name & extension)
    End Function

    ''' <summary> Opens the application configuration folder path. </summary>
    ''' <param name="userLevel">                  The user level. </param>
    ''' <param name="significantVersionElements"> The significant version elements. A value between 1
    ''' and 4 indicating the version level to use. For example, with 1, only the major version of the
    ''' folder name is used; whereas with 4 all version elements are used. Defaults to 4. </param>
    ''' <returns> The configuration folder full path. Uses the application folder if this folder is
    ''' writable. Otherwise the <see cref="OpenApplicationDataFolderPath">application data
    ''' folder</see> is used. System.String. </returns>
    Public Shared Function OpenApplicationConfigFolderPath(ByVal userLevel As UserLevel, ByVal significantVersionElements As Integer) As String
        Dim candidatePath As String = My.Application.Info.DirectoryPath
        If Not MyAssemblyInfo.IsFolderWritable(candidatePath) Then
            candidatePath = MyAssemblyInfo.OpenApplicationDataFolderPath(userLevel, significantVersionElements)
        End If
        Return candidatePath
    End Function

#End Region

#Region " APPLICATION DATA FOLDER -- CUSTOMIZED "


    ''' <summary> Returns the Folder Path with the relevant version information elements replaces with
    ''' '0'. Creates the folder if it does not exist. </summary>
    ''' <param name="dataFolderPath">             Name of the data path. </param>
    ''' <param name="significantVersionElements"> The significant version elements. A value between 1
    ''' and 4 indicating the version level to use. For example, with 1, only the major version of the
    ''' folder name is used; whereas with 4 all version elements are used. Defaults to 4. </param>
    ''' <returns> A folder name with the revision numbers (if there) truncated based on the number of
    ''' significant elements. For example, with two significant digits we get: ...major.minor.0.0;
    ''' or. </returns>
    Public Shared Function OpenDataFolderPath(ByVal dataFolderPath As String, ByVal significantVersionElements As Integer) As String
        significantVersionElements = Math.Max(1, Math.Min(4, significantVersionElements))
        Dim di As New System.IO.DirectoryInfo(MyAssemblyInfo.ReplaceTrailingElements(dataFolderPath, "."c, 4 - significantVersionElements, "0"))
        If Not di.Exists Then
            di.Create()
        End If
        Return di.FullName
    End Function

    ''' <summary> Gets the application data folder Path. The folder is created if it does not exist. </summary>
    ''' <param name="userLevel">                  The user level. </param>
    ''' <param name="significantVersionElements"> The significant version elements. A value between 1
    ''' and 4 indicating the version level to use. For example, with 1, only the major version of the
    ''' folder name is used; whereas with 4 all version elements are used. Defaults to 4. </param>
    ''' <returns> The application data folder for the product. With All Users: In Windows XP: .\
    ''' Application Data\&lt;company&gt;\&lt;product name&gt;\major.minor.0.0; or In Windows 7 or 8:
    ''' C:\ProgramData\&lt;company&gt;\&lt;product name&gt;\major.minor.0.0 For the Current User: In
    ''' Windows XP: .\&lt;user&gt;\documents and settings\AppData\&lt;company&gt;\&lt;product name&gt;
    ''' \major.minor.0.0; or In Windows 7 or 8: C:\Users\&lt;user&gt;\AppData\Roaming\\&lt;company&gt;
    ''' \&lt;product name&gt;\major.minor.0.0.
    ''' The version information is based on the Assembly File Version. </returns>
    Public Shared Function OpenApplicationDataFolderPath(ByVal userLevel As UserLevel, ByVal significantVersionElements As Integer) As String
        Return MyAssemblyInfo.OpenDataFolderPath(MyAssemblyInfo.OpenApplicationDataFolderPath(userLevel), significantVersionElements)
    End Function

#End Region

#Region " HELPER FUNCTIONS "

    ''' <summary> Determines whether the specified folder path is writable. </summary>
    ''' <remarks> Uses a temporary random file name to test if the file can be created. The file is
    ''' deleted thereafter. </remarks>
    ''' <param name="path"> The path. </param>
    ''' <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Shared Function IsFolderWritable(ByVal path As String) As Boolean
        Dim filePath As String = ""
        Dim affirmative As Boolean = False
        Try
            filePath = System.IO.Path.Combine(path, System.IO.Path.GetRandomFileName())
            Using s As System.IO.FileStream = System.IO.File.Open(filePath, System.IO.FileMode.OpenOrCreate)
            End Using
            affirmative = True
        Catch
        Finally
            ' SS reported an exception from this test possibly indicating that Windows allowed writing the file 
            ' by failed report deletion. Or else, Windows raised another exception type.
            Try
                If System.IO.File.Exists(filePath) Then
                    System.IO.File.Delete(filePath)
                End If
            Catch
            End Try
        End Try
        Return affirmative
    End Function

    ''' <summary> Drops the trailing elements. </summary>
    ''' <param name="value">     The value. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <param name="count">     The count. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function DropTrailingElements(ByVal value As String, ByVal delimiter As Char, ByVal count As Integer) As String
        If String.IsNullOrWhiteSpace(value) Then
            Return value
        ElseIf String.IsNullOrWhiteSpace(delimiter) Then
            Return value
        Else
            Do While count > 0
                count -= 1
                Dim i As Integer = value.LastIndexOf(delimiter)
                If i > 0 Then
                    value = value.Substring(0, i)
                Else
                    count = 0
                End If
            Loop
            Return value
        End If
    End Function

    ''' <summary> Drops the trailing elements. </summary>
    ''' <param name="value">     The value. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <param name="count">     The count. </param>
    ''' <param name="replace">   The replace. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function ReplaceTrailingElements(ByVal value As String, ByVal delimiter As Char, ByVal count As Integer, ByVal replace As String) As String
        If String.IsNullOrWhiteSpace(value) Then
            Return value
        ElseIf String.IsNullOrWhiteSpace(delimiter) Then
            Return value
        Else
            Dim replaceCount As Integer = 0
            Do While count > 0
                count -= 1
                Dim i As Integer = value.LastIndexOf(delimiter)
                If i > 0 Then
                    replaceCount += 1
                    value = value.Substring(0, i)
                Else
                    count = 0
                End If
            Loop
            Dim builder As New System.Text.StringBuilder(value)
            If replaceCount > 0 Then
                For i As Integer = 1 To replaceCount
                    builder.Append(delimiter)
                    builder.Append(replace)
                Next
            End If
            Return builder.ToString
        End If
    End Function

#End Region

End Class

