﻿Imports System.Linq
''' <summary> A sealed class designed to provide application log access to the library. </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/02/2011" by="David" revision="x.x.4050.x"> Created </history>
Public NotInheritable Class EmbeddedResourceManager

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="EmbeddedResourceManager" /> class.
    ''' </summary>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " EMBEDDED RESOURCES  "

    ''' <summary> Builds full resource name. </summary>
    ''' <param name="assembly">     The assembly. </param>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> The full resource name, which starts with the Assembly name. </returns>
    Public Shared Function BuildFullResourceName(ByVal assembly As System.Reflection.Assembly, ByVal resourceName As String) As String
        If String.IsNullOrWhiteSpace(resourceName) Then
            Return ""
        Else
            If assembly Is Nothing Then
                assembly = System.Reflection.Assembly.GetExecutingAssembly()
            End If
            Dim assemblyName As String = assembly.GetName().Name
            If resourceName.StartsWith(assemblyName, StringComparison.OrdinalIgnoreCase) Then
                Return resourceName
            Else
                Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0}.{1}", assembly.GetName().Name, resourceName)
            End If
        End If
    End Function

    ''' <summary> Builds full resource name. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> The full resource name, which starts with the Assembly name. </returns>
    Public Shared Function BuildFullResourceName(ByVal resourceName As String) As String
        Return EmbeddedResourceManager.BuildFullResourceName(System.Reflection.Assembly.GetExecutingAssembly(), resourceName)
    End Function

    ''' <summary> Queries if a given embedded resource exists. </summary>
    ''' <param name="assembly">     The assembly. </param>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <c>True</c> if the resource exists; otherwise, <c>False</c>. </returns>
    Public Shared Function EmbeddedResourceExists(ByVal assembly As System.Reflection.Assembly, ByVal resourceName As String) As Boolean
        If assembly Is Nothing Then
            assembly = System.Reflection.Assembly.GetExecutingAssembly()
        End If
        ' Retrieve a list of resource names contained by the assembly.
        Dim resourceNames As String() = assembly.GetManifestResourceNames()
        Return resourceNames.Contains(EmbeddedResourceManager.BuildFullResourceName(assembly, resourceName), StringComparer.OrdinalIgnoreCase)
    End Function

    ''' <summary> Queries if a given embedded resource exists. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <c>True</c> if the resource exists; otherwise, <c>False</c>. </returns>
    Public Shared Function EmbeddedResourceExists(ByVal resourceName As String) As Boolean
        Dim assembly As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly()
        Return EmbeddedResourceManager.EmbeddedResourceExists(assembly, EmbeddedResourceManager.BuildFullResourceName(assembly, resourceName))
    End Function

    ''' <summary> Read text from an embedded resource file. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> The embedded text resource. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")>
    Public Shared Function ReadEmbeddedTextResource(ByVal resourceName As String) As String

        Dim assembly As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly()
        Dim contents As String = ""
        Using resourceStream As System.IO.Stream = assembly.GetManifestResourceStream(EmbeddedResourceManager.BuildFullResourceName(assembly, resourceName))
            Using sr As New System.IO.StreamReader(resourceStream)
                contents = sr.ReadToEnd()
            End Using
        End Using
        Return contents
    End Function

    ''' <summary> Read text from an embedded resource file. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> The embedded image resource. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")>
    Public Shared Function ReadEmbeddedImageResource(ByVal resourceName As String) As System.Drawing.Image
        Dim assembly As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly()
        Using resourceStream As System.IO.Stream =
                    assembly.GetManifestResourceStream(EmbeddedResourceManager.BuildFullResourceName(assembly, resourceName))
            Return System.Drawing.Image.FromStream(resourceStream)
        End Using
    End Function

#End Region

End Class
