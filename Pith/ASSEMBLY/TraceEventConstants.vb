Namespace Global.isr

    ''' <summary> A trace event constants. </summary>
    ''' <remarks> David, 11/26/2015. </remarks>
    ''' <license>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    Public Module TraceEventConstants

        ''' <summary> The application type scale factor. </summary>
        Public Const BaseScaleFactor As Integer = &H10
        Public Const LibraryDigitMask As Integer = &H1 * BaseScaleFactor ' &H10
        Public Const FormApplicationDigitMask As Integer = &H2 * BaseScaleFactor ' &H20
        Public Const UnitTestDigitMask As Integer = &H3 * BaseScaleFactor ' &H30
        Public Const NamespaceScaleFactor As Integer = &H10 * BaseScaleFactor ' &H100
        Public Const NamespaceMask As Integer = &HFF * NamespaceScaleFactor ' &HFF00

    End Module

End Namespace
