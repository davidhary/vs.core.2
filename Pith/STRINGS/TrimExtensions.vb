﻿Imports System.Runtime.CompilerServices
Namespace TrimExtensions
    ''' <summary> Includes 'Trim' extensions for <see cref="String">String</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    Public Module Methods

        ''' <summary> Remove unit characters from SCPI data. Some instruments append units to the end of
        ''' the fetched values. This methods removes alpha characters as well as the number sign which
        ''' the 2700 appends to the reading number. </summary>
        ''' <param name="value"> A delimited string of values. </param>
        ''' <returns> The values striped from units. </returns>
        <Extension()>
        Public Function TrimUnits(ByVal value As String) As String
            Return Methods.TrimUnits(value, ",")
        End Function

        ''' <summary> Remove unit characters from SCPI data. Some instruments append units to the end of
        ''' the fetched values. This methods removes alpha characters as well as the number sign which
        ''' the 2700 appends to the reading number. </summary>
        ''' <param name="value"> A delimited string of values. </param>
        ''' <param name="delimiter"> The delimiter. </param>
        <Extension()>
        Public Function TrimUnits(ByVal value As String, ByVal delimiter As String) As String
            Const unitCharacters As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ#"
            Dim dataBuilder As New System.Text.StringBuilder
            If Not String.IsNullOrWhiteSpace(value) Then
                If value.Contains(delimiter) Then
                    For Each dataElement As String In value.Split(","c)
                        If dataBuilder.Length > 0 Then dataBuilder.Append(",")
                        dataBuilder.Append(dataElement.TrimEnd(unitCharacters.ToCharArray))
                    Next
                Else
                    dataBuilder.Append(value.TrimEnd(unitCharacters.ToCharArray))
                End If
            End If
            Return dataBuilder.ToString
        End Function

    End Module
End Namespace
