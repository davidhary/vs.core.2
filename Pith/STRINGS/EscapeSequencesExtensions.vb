﻿Imports System.Runtime.CompilerServices
Namespace EscapeSequencesExtensions
    ''' <summary> Includes Escape Sequences extensions for <see cref="String">String</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    ''' <history date="08/28/2015" by="David" revision="2.1.5718.x"> Requires VS 2015. </history>
    Public Module Methods

        Public Const NewLineEscape As String = "\n"
        Public Const NewLineChar As Char = ChrW(10)
        Public Const ReturnEscape As String = "\r"
        Public Const ReturnChar As Char = ChrW(13)

        ''' <summary> Replaces common escape strings such as <code>'\n'</code> or <code>'\r'</code>with
        ''' control characters such as <code>10</code> and <code>13</code>, respectively. </summary>
        ''' <param name="value"> Text including escape sequences. </param>
        ''' <returns> String with escape string replaces with control characters. </returns>
        <Extension()>
        Public Function ReplaceCommonEscapeSequences(ByVal value As String) As String
            Return value?.Replace(NewLineEscape, NewLineChar).Replace(ReturnEscape, ReturnChar)
        End Function

        ''' <summary> Replaces control characters such as <code>10</code> and <code>13</code> with common
        ''' escape strings such as <code>'\n'</code> or <code>'\r'</code>, respectively. </summary>
        ''' <param name="value"> Text including control characters. </param>
        ''' <returns> String with control characters replaces with escape codes. </returns>
        <Extension()>
        Public Function InsertCommonEscapeSequences(ByVal value As String) As String
            Return value?.Replace(NewLineChar, NewLineEscape).Replace(ReturnChar, ReturnEscape)
        End Function

    End Module
End Namespace
