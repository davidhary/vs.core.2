﻿Imports System.Runtime.CompilerServices
Namespace SplitExtensions
    ''' <summary> Includes Split extensions for <see cref="String">String</see>. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created. </history>
    Public Module Methods

        ''' <summary> Splits the string to words by adding spaces between lower and upper case characters. </summary>
        ''' <param name="value"> The <c>String</c> value to split. </param>
        ''' <returns> A <c>String</c> of words separated by spaces. </returns>
        <Extension()>
        Public Function SplitWords(ByVal value As String) As String
            If String.IsNullOrWhiteSpace(value) Then
                Return ""
            Else
                Dim isSpace As Boolean = False
                Dim isLowerCase As Boolean = False
                Dim newValue As New System.Text.StringBuilder
                For Each c As Char In value
                    If Not isSpace AndAlso isLowerCase AndAlso Char.IsUpper(c) Then
                        newValue.Append(" ")
                    End If
                    isSpace = c.Equals(" "c)
                    isLowerCase = Not Char.IsUpper(c)
                    newValue.Append(c)
                Next
                Return newValue.ToString()
            End If
        End Function

    End Module
End Namespace
