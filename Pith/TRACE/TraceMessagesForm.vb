﻿Imports System.ComponentModel
Imports isr.Core.Pith
''' <summary> Form for viewing the messages. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="10/14/2014" by="David" revision=""> Created. </history>
Public Class TraceMessagesForm
    Inherits FormBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 12/12/2015. </remarks>
    Public Sub New()

        MyBase.New()

        ' This call is required by the designer.
        InitializeComponent()

        If Version.Parse(My.Computer.Info.OSVersion).Major <= EnableDropShadowVersion Then
            Me.ClassStyle = Core.Pith.ClassStyleConstants.DropShadow
        End If
        Me.ClassStyle = Me.ClassStyle Or Core.Pith.ClassStyleConstants.HideCloseButton

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary> Displays this dialog with title 'illegal call'. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2222:DoNotDecreaseInheritedMemberVisibility")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Shadows Function ShowDialog() As System.Windows.Forms.DialogResult
        Me.Text = "Illegal Call"
        Return MyBase.ShowDialog()
    End Function

    ''' <summary> Displays this dialog with title 'illegal call'. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2222:DoNotDecreaseInheritedMemberVisibility")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Shadows Sub Show()
        Me.Text = "Illegal Call"
        MyBase.Show()
    End Sub

    ''' <summary> Shows the message box with these messages. </summary>
    ''' <param name="owner">   The owner. </param>
    ''' <param name="caption"> The caption. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="details")>
    Public Shadows Sub Show(ByVal mdiForm As System.Windows.Forms.Form, ByVal owner As System.Windows.Forms.IWin32Window, ByVal caption As String)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        Me.Text = caption
        MyBase.Show(owner)
    End Sub

    ''' <summary> Shows the message box with these messages. </summary>
    ''' <param name="caption"> The caption. </param>
    Public Shadows Sub Show(ByVal mdiForm As System.Windows.Forms.Form, ByVal caption As String)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        Me.Text = caption
        MyBase.Show()
    End Sub

#End Region

#Region " OBSERVER "

    ''' <summary> Gets the trace messages box. </summary>
    ''' <value> The trace messages box. </value>
    Public ReadOnly Property TraceMessagesBox As TraceMessagesBox
        Get
            Return Me._TraceMessagesBox
        End Get
    End Property

#End Region

End Class