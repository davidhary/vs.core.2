Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core.Pith
''' <summary> A message logging text box. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/04/2013" by="David" revision="1.2.4955"> created based on the legacy
''' messages box. </history>
<System.ComponentModel.Description("Trace Messages Text Box")>
Public Class TraceMessagesBox
    Inherits MessagesBox
    Implements ITraceMessageListener

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Constructor for this class. </summary>
    Public Sub New()
        MyBase.New()
        Me.InitializeComponent()
        Me._TraceMessageFormat = "{0}, {1:X}, {2:HH:mm:ss.fff}, {3}"
        Me._AlertLevel = TraceEventType.Warning
        Me._TraceLevel = TraceEventType.Verbose
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            ' Invoke the base class dispose method        
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DISPLAY AND MESSAGE SENTINELS "

    Private _AlertLevel As TraceEventType

    ''' <summary> Gets or sets the alert level. Message <see cref="TraceEventType">levels</see> equal
    ''' or lower than this are tagged as alerts and set the <see cref="AlertsAdded">alert
    ''' sentinel</see>. </summary>
    ''' <value> The alert level. </value>
    <Category("Appearance"), Description("Level for notifying of alerts"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("TraceEventType.Warning")>
    Public Property AlertLevel As TraceEventType
        Get
            Return Me._AlertLevel
        End Get
        Set(value As TraceEventType)
            If value <> Me.AlertLevel Then
                Me._AlertLevel = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.AlertLevel))
            End If
        End Set
    End Property

    Private _AlertsAdded As Boolean

    ''' <summary> Gets or sets the sentinel indicating that new messages were added. </summary>
    ''' <value> The new messages available. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property AlertsAdded As Boolean
        Get
            Return Me._AlertsAdded
        End Get
        Set(value As Boolean)
            If value <> Me.AlertsAdded Then
                Me._AlertsAdded = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.AlertsAdded))
                Me.SafeVisibleSetter(Me.AlertsToggleControl, Me.AlertsAdded)
                If value Then Me.PlayAlert()
            End If
        End Set
    End Property

    ''' <summary> Displays the available lines and clears the alerts and message sentinels. </summary>
    Public Overrides Sub Display()
        If Me.Visible Then
            Me.AlertsAdded = False
        End If
        MyBase.Display()
    End Sub

    ''' <summary> Executes the clear action. </summary>
    Protected Overrides Sub OnClear()
        Me.AlertsAdded = False
        MyBase.OnClear()
    End Sub

    ''' <summary> Gets or sets the alerts toggle control. </summary>
    ''' <value> The alerts toggle control. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property AlertsToggleControl As Control

    ''' <summary> Safe visible setter. </summary>
    ''' <remarks> David, 12/25/2015. </remarks>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   The value. </param>
    Public Sub SafeVisibleSetter(ByVal control As Control, ByVal value As Boolean)
        If control IsNot Nothing Then
            If control.InvokeRequired Then
                control.Invoke(New Action(Of Control, Boolean)(AddressOf Me.SafeVisibleSetter), New Object() {control, value})
            Else
                control.Visible = value
            End If
        End If
    End Sub

    ''' <summary> Gets or sets the alert sound enabled. </summary>
    ''' <value> The alert sound enabled. </value>
    <Category("Appearance"), Description("Enables playing alert sounds"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(False)>
    Public Property AlertSoundEnabled As Boolean

    ''' <summary> Play alert. </summary>
    ''' <remarks> David, 12/28/2015. </remarks>
    Public Sub PlayAlert()
        If AlertSoundEnabled Then My.Computer.Audio.PlaySystemSound(Media.SystemSounds.Exclamation)
    End Sub

#End Region

#Region " ADD MESSAGE "

    ''' <summary> Adds a message to the display. </summary>
    ''' <param name="value">  The value. </param>
    Public Overloads Sub AddMessage(ByVal value As TraceMessage)
        If value Is Nothing OrElse String.IsNullOrWhiteSpace(value.Details) Then
            Debug.Assert(Not Debugger.IsAttached, "Empty trace message @",
                         New StackTrace(True).ToString.Split(Environment.NewLine.ToCharArray())(0))
        Else
            ' sustains the alerts flag until cleared.
            Me.AlertsAdded = Not Me.UserVisible AndAlso (Me.AlertsAdded OrElse (value.EventType <= Me.AlertLevel))
            Me.AddMessage(value.ToString(Me.TraceMessageFormat))
            Me.PublishStatus(value)
        End If
    End Sub

    ''' <summary> Gets or sets the default format for displaying the message. </summary>
    ''' <remarks> The format must include 4 elements to display the first two characters of
    ''' the trace event type, the id, timestamp and message details. For example,<code>
    ''' "{0}, {1:X}, {2:HH:mm:ss.fff}, {3}"</code>. </remarks>
    ''' <value> The trace message format. </value>
    <Category("Appearance"), Description("Formats the test message"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("{0}, {1:X}, {2:HH:mm:ss.fff}, {3}")>
    Public Property TraceMessageFormat As String

#End Region

#Region " STATUS PUBLISHER "

    Private _statusPrompt As String
    ''' <summary> Gets or sets the status prompt. </summary>
    ''' <value> The status bar label. </value>
    Public ReadOnly Property StatusPrompt As String
        Get
            Return Me._statusPrompt
        End Get
    End Property

    ''' <summary> Publishes status. </summary>
    ''' <remarks> David, 12/29/2015. </remarks>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to publish. </param>
    Public Sub PublishStatus(ByVal value As TraceMessage)
        If value?.HasSynopsis Then
            Me.PublishStatus(value.ExtractSynopsis)
            System.Windows.Forms.Application.DoEvents()
        End If
    End Sub

    ''' <summary> Publishes the trace message status. </summary>
    Public Sub PublishStatus(ByVal value As String)
        If String.IsNullOrWhiteSpace(value) Then value = ""
        If Not String.Equals(value, Me.StatusPrompt) Then
            Me._statusPrompt = value
            Me.AsyncNotifyPropertyChanged(NameOf(StatusPrompt))
        End If
    End Sub

#End Region

#Region " I TRACE MESSAGES LISTENER "

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 12/29/2015. </remarks>
    ''' <param name="value"> The event message. </param>
    ''' <returns> The trace message details. </returns>
    Public Function TraceEvent(value As TraceMessage) As String Implements ITraceMessageListener.TraceEvent
        If value Is Nothing Then
            Return ""
        Else
            If Me.ShouldTrace(value.EventType) Then Me.AddMessage(value)
            System.Windows.Forms.Application.DoEvents()
            Return value.Details
        End If
    End Function

    ''' <summary> Gets or sets the trace level. </summary>
    ''' <value> The trace level. </value>
    Public Property TraceLevel As TraceEventType Implements ITraceMessageListener.TraceLevel

    ''' <summary> Checks if the log should trace the event type. </summary>
    ''' <param name="value"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> If the log should trace; <c>False</c> otherwise. </returns>
    Public Function ShouldTrace(ByVal value As TraceEventType) As Boolean Implements ITraceMessageListener.ShouldTrace
        Return value <= Me.TraceLevel
    End Function

#End Region

End Class

