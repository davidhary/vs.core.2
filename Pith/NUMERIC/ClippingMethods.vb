﻿Imports System.Runtime.CompilerServices
Namespace NumericExtensions

    ''' <summary> Numeric clipping methods. </summary>
    ''' <remarks> David, 1/1/2016. </remarks>
    ''' <license>
    ''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    ''' </license>
    Public Module ClippingMethods

#Region " BYTE "

        ''' <summary> Clips the value between the minimum and maximum. </summary>
        ''' <remarks> David, 1/1/2016. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="min">   The minimum. </param>
        ''' <param name="max">   The maximum. </param>
        ''' <returns> A Byte. </returns>
        <Extension()>
        Public Function Clip(ByVal value As Byte, ByVal min As Byte, ByVal max As Byte) As Byte
            Return value.Max(min).Min(max)
        End Function

        ''' <summary> Returns the maximum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> the other value. </param>
        ''' <returns> The maximum value. </returns>
        <Extension()>
        Public Function Max(ByVal value As Byte, ByVal other As Byte) As Byte
            If value >= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns the minimum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The minimum value. </returns>
        <Extension()>
        Public Function Min(ByVal value As Byte, ByVal other As Byte) As Byte
            If value <= other Then
                Return value
            Else
                Return other
            End If
        End Function

#End Region

#Region " DECIMAL "

        ''' <summary> Clips the value between the minimum and maximum. </summary>
        ''' <remarks> David, 1/1/2016. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="min">   The minimum. </param>
        ''' <param name="max">   The maximum. </param>
        ''' <returns> A Decimal. </returns>
        <Extension()>
        Public Function Clip(ByVal value As Decimal, ByVal min As Decimal, ByVal max As Decimal) As Decimal
            Return value.Max(min).Min(max)
        End Function

        ''' <summary> Returns the maximum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> the other value. </param>
        ''' <returns> The maximum value. </returns>
        <Extension()>
        Public Function Max(ByVal value As Decimal, ByVal other As Decimal) As Decimal
            If value >= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns the minimum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The minimum value. </returns>
        <Extension()>
        Public Function Min(ByVal value As Decimal, ByVal other As Decimal) As Decimal
            If value <= other Then
                Return value
            Else
                Return other
            End If
        End Function

#End Region

#Region " DOUBLE "

        ''' <summary> Clips the value between the minimum and maximum. </summary>
        ''' <remarks> David, 1/1/2016. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="min">   The minimum. </param>
        ''' <param name="max">   The maximum. </param>
        ''' <returns> A Double. </returns>
        <Extension()>
        Public Function Clip(ByVal value As Double, ByVal min As Double, ByVal max As Double) As Double
            Return value.Max(min).Min(max)
        End Function

        ''' <summary> Returns the maximum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The maximum value. </returns>
        <Extension()>
        Public Function Max(ByVal value As Double, ByVal other As Double) As Double
            If value >= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns the minimum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The minimum value. </returns>
        <Extension()>
        Public Function Min(ByVal value As Double, ByVal other As Double) As Double
            If value <= other Then
                Return value
            Else
                Return other
            End If
        End Function

#End Region

#Region " INTEGER "

        ''' <summary> Clips the value between the minimum and maximum. </summary>
        ''' <remarks> David, 1/1/2016. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="min">   The minimum. </param>
        ''' <param name="max">   The maximum. </param>
        ''' <returns> A Integer. </returns>
        <Extension()>
        Public Function Clip(ByVal value As Integer, ByVal min As Integer, ByVal max As Integer) As Integer
            Return value.Max(min).Min(max)
        End Function

        ''' <summary> Returns the maximum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The maximum value. </returns>
        <Extension()>
        Public Function Max(ByVal value As Integer, ByVal other As Integer) As Integer
            If value >= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns the minimum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The minimum value. </returns>
        <Extension()>
        Public Function Min(ByVal value As Integer, ByVal other As Integer) As Integer
            If value <= other Then
                Return value
            Else
                Return other
            End If
        End Function

#End Region

#Region " LONG "

        ''' <summary> Clips the value between the minimum and maximum. </summary>
        ''' <remarks> David, 1/1/2016. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="min">   The minimum. </param>
        ''' <param name="max">   The maximum. </param>
        ''' <returns> A Long. </returns>
        <Extension()>
        Public Function Clip(ByVal value As Long, ByVal min As Long, ByVal max As Long) As Long
            Return value.Max(min).Min(max)
        End Function

        ''' <summary> Returns the maximum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The maximum value. </returns>
        <Extension()>
        Public Function Max(ByVal value As Long, ByVal other As Long) As Long
            If value >= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns the minimum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The minimum value. </returns>
        <Extension()>
        Public Function Min(ByVal value As Long, ByVal other As Long) As Long
            If value <= other Then
                Return value
            Else
                Return other
            End If
        End Function

#End Region

#Region " SHORT "

        ''' <summary> Clips the value between the minimum and maximum. </summary>
        ''' <remarks> David, 1/1/2016. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="min">   The minimum. </param>
        ''' <param name="max">   The maximum. </param>
        ''' <returns> A Single. </returns>
        <Extension()>
        Public Function Clip(ByVal value As Short, ByVal min As Short, ByVal max As Short) As Short
            Return value.Max(min).Min(max)
        End Function

        ''' <summary> Returns the maximum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The maximum value. </returns>
        <Extension()>
        Public Function Max(ByVal value As Short, ByVal other As Short) As Short
            If value >= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns the minimum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The minimum value. </returns>
        <Extension()>
        Public Function Min(ByVal value As Short, ByVal other As Short) As Short
            If value <= other Then
                Return value
            Else
                Return other
            End If
        End Function


#End Region

#Region " SINGLE "

        ''' <summary> Clips the value between the minimum and maximum. </summary>
        ''' <remarks> David, 1/1/2016. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="min">   The minimum. </param>
        ''' <param name="max">   The maximum. </param>
        ''' <returns> A Single. </returns>
        <Extension()>
        Public Function Clip(ByVal value As Single, ByVal min As Single, ByVal max As Single) As Single
            Return value.Max(min).Min(max)
        End Function

        ''' <summary> Returns the maximum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The maximum value. </returns>
        <Extension()>
        Public Function Max(ByVal value As Single, ByVal other As Single) As Single
            If value >= other Then
                Return value
            Else
                Return other
            End If
        End Function

        ''' <summary> Returns the minimum of the two values. </summary>
        ''' <param name="value"> Extended value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <returns> The minimum value. </returns>
        <Extension()>
        Public Function Min(ByVal value As Single, ByVal other As Single) As Single
            If value <= other Then
                Return value
            Else
                Return other
            End If
        End Function

#End Region

    End Module
End Namespace
