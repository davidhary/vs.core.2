﻿Imports System.Runtime.CompilerServices
Namespace NumericExtensions
    ''' <summary> Includes extensions for numeric objects. </summary>
    ''' <license> (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/09/2009" by="David" revision="1.1.3386.x"> Created </history>
    Public Module ApproximateMethods

#Region " DECIMAL "

        ''' <summary> Returns true if the two values are within the specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the two values are within the specified absolute precision. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) <= precision
        End Function

        ''' <summary> Returns true if the two values are within the specified relative precision. </summary>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        ''' the
        ''' <see cref="Hypotenuse"></see> </param>
        ''' <returns> <c>True</c> if the two values are within the specified relative precision. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Decimal, ByVal other As Decimal, ByVal significantDigits As Integer) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) < Epsilon(value, other, significantDigits)
        End Function

        ''' <summary> Returns the Hypotenuse of the values scaled by the specified significant digits. </summary>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        ''' the
        ''' <see cref="Hypotenuse"></see> </param>
        ''' <returns> The Hypotenuse of the values scaled by the specified significant digits. </returns>
        <Extension()>
        Public Function Epsilon(ByVal value As Decimal, ByVal other As Decimal, ByVal significantDigits As Integer) As Decimal
            Return CDec(Hypotenuse(value, other) / 10 ^ (significantDigits - 1))
        End Function

        ''' <summary> Returns true if the first value is less than or equal the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the first value is less than or equal the other within the specified absolute
        ''' precision. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function SmallerEquals(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value < other)
        End Function

        ''' <summary> Returns True if the first value is greater than or equal the other within the
        ''' specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the first value is greater than or equal the other within the specified
        ''' absolute precision. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterEquals(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value > other)
        End Function

        ''' <summary> Returns True if the values are unequal within the specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the values are unequal within the specified absolute precision. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Differs(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return Not NumericExtensions.Approximates(value, other, precision)
        End Function

        ''' <summary> Returns True if the first value is less than the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the first value is less than the other within the specified
        ''' absolute precision. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function SmallerDiffers(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return Not value.GreaterEquals(other, precision)
        End Function

        ''' <summary> Returns True if the first value is greater than the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the first value is greater than the other within the specified
        ''' absolute precision. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterDiffers(ByVal value As Decimal, ByVal other As Decimal, ByVal precision As Decimal) As Boolean
            Return Not value.SmallerEquals(other, precision)
        End Function

#End Region

#Region " DECIMAL? "

        ''' <summary> Returns true if the absolute difference is less than or equal than
        '''           <paramref name="delta">delta</paramref>. </summary>
        ''' <param name="value">     The value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="delta">     The delta. </param>
        ''' <returns> <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Decimal?, ByVal other As Decimal?, ByVal delta As Decimal) As Boolean
            If value Is Nothing Then
                Return other Is Nothing
            ElseIf other Is Nothing Then
                Return False
            Else
                Return NumericExtensions.Approximates(value.Value, other.Value, delta)
            End If
        End Function

        ''' <summary> Returns true if the absolute difference is greater than
        ''' <paramref name="delta">delta</paramref>. </summary>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <param name="delta"> The delta. </param>
        ''' <returns> <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Differs(ByVal value As Decimal?, ByVal other As Decimal?, ByVal delta As Decimal) As Boolean
            If value Is Nothing Then
                Return other IsNot Nothing
            ElseIf other Is Nothing Then
                Return True
            Else
                Return value.Value.Differs(other.Value, delta)
            End If
        End Function

#End Region

#Region " DOUBLE "

        ''' <summary> Returns True if the two values are within the specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the two values are within the specified absolute precision. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) <= precision
        End Function

        ''' <summary> Returns true if the two values are within the specified relative precision. </summary>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        ''' the
        ''' <see cref="Hypotenuse"></see> </param>
        ''' <returns> Returns true if the two values are within the specified relative precision. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Double, ByVal other As Double, ByVal significantDigits As Integer) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) < Epsilon(value, other, significantDigits)
        End Function

        ''' <summary> Returns the Hypotenuse of the values scaled by the specified significant digits. </summary>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        ''' the
        ''' <see cref="Hypotenuse"></see> </param>
        ''' <returns> the Hypotenuse of the values scaled by the specified significant digits. </returns>
        <Extension()>
        Public Function Epsilon(ByVal value As Double, ByVal other As Double, ByVal significantDigits As Integer) As Double
            ' Return Hypotenuse(value, other) / If(significantDigits >= 15, 1.0E+16, 10 ^ (significantDigits - 1))
            Return Hypotenuse(value, other) / 10 ^ (significantDigits - 1)
        End Function

        ''' <summary> Computes the length of a right triangle's hypotenuse. </summary>
        ''' <remarks> <para>The length is computed accurately, even in cases where x<sup>2</sup> or
        ''' y<sup>2</sup> would overflow.</para> </remarks>
        ''' <param name="value">      The length of one side. </param>
        ''' <param name="orthogonal"> The length of orthogonal side. </param>
        ''' <returns> The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>). </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal value As Double, ByVal orthogonal As Double) As Double
            If (value = 0.0) AndAlso (orthogonal = 0.0) Then
                Return (0.0)
            Else
                Dim ax As Double = Math.Abs(value)
                Dim ay As Double = Math.Abs(orthogonal)
                If ax > ay Then
                    Dim r As Double = orthogonal / value
                    Return (ax * Math.Sqrt(1.0 + r * r))
                Else
                    Dim r As Double = value / orthogonal
                    Return (ay * Math.Sqrt(1.0 + r * r))
                End If
            End If
        End Function

        ''' <summary> Returns true if the first value is less than or equal the other within the
        ''' specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        <Extension()>
        Public Function SmallerEquals(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value < other)
        End Function

        ''' <summary> Returns true if the first value is greater than or equal the other within the
        ''' specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterEquals(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value > other)
        End Function

        ''' <summary> Returns true if the values are unequal within the specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Differs(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return Not NumericExtensions.Approximates(value, other, precision)
        End Function

        ''' <summary> Returns true if the first value is less than the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        <System.Runtime.CompilerServices.Extension()>
        Public Function SmallerDiffers(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return Not value.GreaterEquals(other, precision)
        End Function

        ''' <summary> Returns true if the first value is greater than the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterDiffers(ByVal value As Double, ByVal other As Double, ByVal precision As Double) As Boolean
            Return Not value.SmallerEquals(other, precision)
        End Function

#End Region

#Region " DOUBLE? "

        ''' <summary> Returns true if the absolute difference is less than or equal than
        ''' <paramref name="delta">delta</paramref>. </summary>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <param name="delta"> The delta. </param>
        ''' <returns> <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Double?, ByVal other As Double?, ByVal delta As Double) As Boolean
            If value Is Nothing Then
                Return other Is Nothing
            ElseIf other Is Nothing Then
                Return False
            Else
                Return NumericExtensions.Approximates(value.Value, other.Value, delta)
            End If
        End Function

        ''' <summary> Returns true if the absolute difference is greater than
        ''' <paramref name="delta">delta</paramref>. </summary>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <param name="delta"> The delta. </param>
        ''' <returns> <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Differs(ByVal value As Double?, ByVal other As Double?, ByVal delta As Double) As Boolean
            If value Is Nothing Then
                Return other IsNot Nothing
            ElseIf other Is Nothing Then
                Return True
            Else
                Return value.Value.Differs(other.Value, delta)
            End If
        End Function

#End Region

#Region " SINGLE "

        ''' <summary> Returns true if the values are unequal within the specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the values are different; <c>False</c> otherwise. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function Differs(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return Not NumericExtensions.Approximates(value, other, precision)
        End Function

        ''' <summary> Returns true if the absolute difference is greater than
        ''' <paramref name="delta">delta</paramref>. </summary>
        ''' <param name="value"> The value. </param>
        ''' <param name="other"> The other value. </param>
        ''' <param name="delta"> The delta. </param>
        ''' <returns> <c>True</c> if the absolute difference is greater than delta, <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Differs(ByVal value As Single?, ByVal other As Single?, ByVal delta As Single) As Boolean
            If value Is Nothing Then
                Return other IsNot Nothing
            ElseIf other Is Nothing Then
                Return True
            Else
                Return value.Value.Differs(other.Value, delta)
            End If
        End Function

        ''' <summary> Returns true if the two values are within the specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the values are close; <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) <= precision
        End Function

        ''' <summary> Returns true if the two values are within the specified relative precision. </summary>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        ''' the <see cref="Hypotenuse"></see> </param>
        ''' <returns> <c>True</c> if the two values are within the specified relative precision;
        ''' <c>False</c> otherwise. </returns>
        <Extension()>
        Public Function Approximates(ByVal value As Single, ByVal other As Single, ByVal significantDigits As Integer) As Boolean
            Return other.Equals(value) OrElse Math.Abs(value - other) < Epsilon(value, other, significantDigits)
        End Function

        ''' <summary> Returns the Hypotenuse of the values scaled by the specified significant digits. </summary>
        ''' <param name="value">             Value. </param>
        ''' <param name="other">             The other value. </param>
        ''' <param name="significantDigits"> Defines relative precision based on the significant digits of
        ''' the <see cref="Hypotenuse"></see> </param>
        ''' <returns> the Hypotenuse of the values scaled by the specified significant digits. </returns>
        <Extension()>
        Public Function Epsilon(ByVal value As Single, ByVal other As Single, ByVal significantDigits As Integer) As Single
            Return CSng(Hypotenuse(value, other) / 10 ^ (significantDigits - 1))
        End Function

        ''' <summary> Returns true if the first value is smaller or equal the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the first value is smaller or equal the other within the specified
        ''' absolute precision; <c>False</c> otherwise. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function SmallerEquals(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value < other)
        End Function

        ''' <summary> Returns true if the first value is greater than or equal the other within the
        ''' specified absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        ''' <returns> <c>True</c> if the first value is greater than or equal the other within the
        ''' specified absolute precision; <c>False</c> otherwise. </returns>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterEquals(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return NumericExtensions.Approximates(value, other, precision) OrElse (value > other)
        End Function

        ''' <summary> Returns true if the first value is less than the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        <System.Runtime.CompilerServices.Extension()>
        Public Function SmallerDiffers(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return Not value.GreaterEquals(other, precision)
        End Function

        ''' <summary> Returns true if the first value is greater than the other within the specified
        ''' absolute precision. </summary>
        ''' <param name="value">     Value. </param>
        ''' <param name="other">     The other value. </param>
        ''' <param name="precision"> Absolute minimal difference for relative equality. </param>
        <System.Runtime.CompilerServices.Extension()>
        Public Function GreaterDiffers(ByVal value As Single, ByVal other As Single, ByVal precision As Single) As Boolean
            Return Not value.SmallerEquals(other, precision)
        End Function

        ''' <summary> Computes the length of a right triangle's hypotenuse. </summary>
        ''' <remarks> <para>The length is computed accurately, even in cases where x<sup>2</sup> or
        ''' y<sup>2</sup> would overflow.</para> </remarks>
        ''' <param name="value">      The length of one side. </param>
        ''' <param name="orthogonal"> The length of orthogonal side. </param>
        ''' <returns> The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>). </returns>
        <Extension()>
        Public Function Hypotenuse(ByVal value As Single, ByVal orthogonal As Single) As Double
            Return Hypotenuse(CDbl(value), CDbl(orthogonal))
        End Function

#End Region

    End Module
End Namespace
