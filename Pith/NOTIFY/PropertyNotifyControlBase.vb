﻿Imports System.ComponentModel
Imports System.Drawing
Imports System.Threading
Imports System.Windows.Forms
Imports isr.Core.Pith.EventHandlerExtensions
''' <summary> A base control implementing property notifications. Useful for a settings publisher. </summary>
''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="11/02/2010" by="David" revision="1.2.3988.x"> created. </history>
Public Class PropertyNotifyControlBase
    Inherits UserControl
    Implements INotifyPropertyChanged, INotifyPropertyChanging

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> A private constructor for this class making it not publicly creatable. This ensure
    ''' using the class as a singleton. </summary>
    Protected Sub New()
        MyBase.New()
        Me.InitializeComponent()
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveEventHandler(Me.PropertyChangedEvent)
                Me.RemoveEventHandler(Me.PropertyChangingEvent)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#Region " Windows Form Designer generated code "

    'Required by the Windows Form Designer
    Private components As IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        '
        'MyUserControlBase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = AutoScaleMode.Inherit
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "PropertyNotifyControlBase"
        Me.Size = New System.Drawing.Size(175, 173)
        Me.ResumeLayout(False)

    End Sub

#End Region

#End Region

#Region " SYNC CONTEXT "

    ''' <summary> Gets the sentinel indicating that multiple synchronization contexts are expected. </summary>
    ''' <remarks> When having multiple user interfaces or a thread running within the user interface,
    ''' the current synchronization context may not reflect the contexts of the current UI causing a
    ''' cross thread exceptions. In this case the more complex
    ''' <see cref="SafeInvokePropertyChanged">thread safe methods</see> must be used. </remarks>
    ''' <value> <c>True</c> if more than one synchronization contexts should be expected. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property MultipleSyncContextsExpected As Boolean

#End Region

#Region " PROPERTY CHANGED "

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As PropertyChangedEventHandler)
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanged, CType(d, PropertyChangedEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub InvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokePropertyChanged(ByVal obj As Object)
        Me.InvokePropertyChanged(CType(obj, PropertyChangedEventArgs))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeInvokePropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeInvokePropertyChanged(New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        For Each d As [Delegate] In evt.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    target.Invoke(d, New Object() {Me, e})
                End If
            End If
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeBeginInvokePropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeBeginInvokePropertyChanged(New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeBeginInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        For Each d As [Delegate] In evt.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    target.BeginInvoke(d, New Object() {Me, e})
                End If
            End If
        Next
    End Sub

#End Region

#Region " NOTIFICATIONS "

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    Protected Overridable Sub AsyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If e IsNot Nothing Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeBeginInvokePropertyChanged(e)
            Else
                SynchronizationContext.Current.Post(New SendOrPostCallback(AddressOf InvokePropertyChanged), e)
            End If
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub AsyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.AsyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

    ''' <summary>
    ''' Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes or
    ''' Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property value.
    ''' </summary>
    ''' <remarks> David, 12/11/2015. </remarks>
    ''' <param name="e"> Property changed event information. </param>
    Protected Overridable Sub SyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If e IsNot Nothing Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeInvokePropertyChanged(e)
            Else
                SynchronizationContext.Current.Send(New SendOrPostCallback(AddressOf InvokePropertyChanged), e)
            End If
        End If
    End Sub

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#End Region

#Region " PROPERTY CHANGING "

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanging(ByVal sender As Object, ByVal e As PropertyChangingEventArgs) Implements INotifyPropertyChanging.PropertyChanging

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As PropertyChangingEventHandler)
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanging, CType(d, PropertyChangingEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub InvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        Dim evt As PropertyChangingEventHandler = Me.PropertyChangingEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokePropertyChanging(ByVal obj As Object)
        Me.InvokePropertyChanging(CType(obj, PropertyChangingEventArgs))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeInvokePropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeInvokePropertyChanging(New PropertyChangingEventArgs(name))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeInvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        Dim evt As PropertyChangingEventHandler = Me.PropertyChangingEvent
        For Each d As [Delegate] In evt.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    target.Invoke(d, New Object() {Me, e})
                End If
            End If
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanging">Event</see> in a property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeBeginInvokePropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeBeginInvokePropertyChanging(New PropertyChangingEventArgs(name))
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanging">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeBeginInvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        Dim evt As PropertyChangingEventHandler = Me.PropertyChangingEvent
        For Each d As [Delegate] In evt.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    target.BeginInvoke(d, New Object() {Me, e})
                End If
            End If
        Next
    End Sub

#End Region

#Region " NOTIFICATIONS "

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanging">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SyncNotifyPropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeInvokePropertyChanging(New PropertyChangingEventArgs(name))
            Else
                SynchronizationContext.Current.Send(New SendOrPostCallback(AddressOf InvokePropertyChanging),
                                                    New PropertyChangingEventArgs(name))
            End If
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub AsyncNotifyPropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeBeginInvokePropertyChanging(New PropertyChangingEventArgs(name))
            Else
                SynchronizationContext.Current.Post(New SendOrPostCallback(AddressOf InvokePropertyChanging), New PropertyChangingEventArgs(name))
            End If
        End If
    End Sub

#End Region

#End Region

#Region " TOOL TIP "

    ''' <summary> Sets the Tool tip for all form controls that inherit a <see cref="Control">control base.</see> </summary>
    ''' <param name="parent">  Reference to the parent form or control. </param>
    ''' <param name="toolTip"> The parent form or control tool tip. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")>
    Public Shared Sub ToolTipSetter(ByVal parent As System.Windows.Forms.Control, ByVal toolTip As ToolTip)
        If parent Is Nothing Then Return
        If toolTip Is Nothing Then Return
        If TypeOf parent Is PropertyNotifyControlBase Then
            CType(parent, PropertyNotifyControlBase).ToolTipSetter(toolTip)
        ElseIf parent.HasChildren Then
            For Each control As System.Windows.Forms.Control In parent.Controls
                ToolTipSetter(control, toolTip)
            Next
        End If
    End Sub

    ''' <summary> Sets a tool tip for all controls on the user control. Uses the message already set
    ''' for this control. </summary>
    ''' <remarks> This is required because setting a tool tip from the parent form does not show the
    ''' tool tip if hovering above children controls hosted by the user control. </remarks>
    ''' <param name="toolTip"> The tool tip control from the parent form. </param>
    Public Sub ToolTipSetter(ByVal toolTip As ToolTip)
        If toolTip IsNot Nothing Then
            applyToolTipToChildControls(Me, toolTip, toolTip.GetToolTip(Me))
        End If
    End Sub

    ''' <summary> Sets a tool tip for all controls on the user control. </summary>
    ''' <remarks> This is required because setting a tool tip from the parent form does not show the
    ''' tool tip if hovering above children controls hosted by the user control. </remarks>
    ''' <param name="toolTip"> The tool tip control from the parent form. </param>
    ''' <param name="message"> The tool tip message to apply to all the children controls and their
    ''' children. </param>
    Public Sub ToolTipSetter(ByVal toolTip As ToolTip, ByVal message As String)
        applyToolTipToChildControls(Me, toolTip, message)
    End Sub

    ''' <summary> Applies the tool tip to all control hosted by the parent as well as all the children
    ''' with these control. </summary>
    ''' <param name="parent">  The parent control. </param>
    ''' <param name="toolTip"> The tool tip control from the parent form. </param>
    ''' <param name="message"> The tool tip message to apply to all the children controls and their
    ''' children. </param>
    Private Sub applyToolTipToChildControls(ByVal parent As Control, ByVal toolTip As ToolTip, ByVal message As String)
        For Each control As Control In parent.Controls
            toolTip.SetToolTip(control, message)
            If parent.HasChildren Then
                applyToolTipToChildControls(control, toolTip, message)
            End If
        Next
    End Sub

#End Region

End Class

