﻿''' <summary> Comparable and equatable value Notifier. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="10/10/2014" by="David" revision=""> Created. </history>
Public Class AtomNotifier(Of T As {IComparable(Of T), IEquatable(Of T)})
    Inherits PropertyNotifyBase

#Region " CONSTRUCTORS "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 1/1/2016. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

#End Region

#Region " VALUE "

    Private _Value As T

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property [Value] As T
        Get
            Return Me._Value
        End Get
        Set(ByVal value As T)
            If Not value.Equals(Me.Value) Then
                Me._Value = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.Value))
                System.Windows.Forms.Application.DoEvents()
            End If
        End Set
    End Property

#End Region

End Class

