﻿Imports System.Threading
Imports System.ComponentModel
Imports isr.Core.Pith.EventHandlerExtensions
''' <summary>  Defines the contract that must be implemented by property change notifiers. </summary>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/28/12" by="David" revision="1.2.4654"> Documented. </history>
Public MustInherit Class PropertyNotifyBase
    Implements IPropertyNotify

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of the <see cref="PropertyNotifyBase" /> class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

#Region " Disposable Support "

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks> Do not make this method Overridable (virtual) because a derived class should not be
    ''' able to override this method. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets or sets the dispose status sentinel of the base class.  This applies to the
    ''' derived class provided proper implementation. </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property IsDisposed() As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveEventHandler(Me.PropertyChangedEvent)
                Me.RemoveEventHandler(Me.PropertyChangingEvent)
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

#End Region

#Region " SYNC CONTEXT "

    ''' <summary> Gets the sentinel indicating that multiple synchronization contexts are expected. </summary>
    ''' <remarks> When having multiple user interfaces or a thread running within the user interface,
    ''' the current synchronization context may not reflect the contexts of the current UI causing a
    ''' cross thread exceptions. In this case the more complex
    ''' <see cref="SafeInvokePropertyChanged">thread safe methods</see> must be used. </remarks>
    ''' <value> <c>True</c> if more than one synchronization contexts should be expected. </value>
    Public Property MultipleSyncContextsExpected As Boolean

#End Region

#Region " PROPERTY CHANGED EVENT IMPLEMENTATION "

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Removes the event handler. </summary>
    ''' <remarks> David, 12/21/2015. </remarks>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As PropertyChangedEventHandler)
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanged, CType(d, PropertyChangedEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overridable Sub InvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokePropertyChanged(ByVal obj As Object)
        Me.InvokePropertyChanged(CType(obj, PropertyChangedEventArgs))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeInvokePropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeInvokePropertyChanged(New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overridable Sub SafeInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        For Each d As [Delegate] In evt.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    target.Invoke(d, New Object() {Me, e})
                End If
            End If
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeBeginInvokePropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeBeginInvokePropertyChanged(New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overridable Sub SafeBeginInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        For Each d As [Delegate] In evt.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    target.BeginInvoke(d, New Object() {Me, e})
                End If
            End If
        Next
    End Sub

#End Region

#Region " NOTIFICATIONS "

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    Protected Overridable Sub AsyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If e IsNot Nothing Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeBeginInvokePropertyChanged(e)
            Else
                SynchronizationContext.Current.Post(New SendOrPostCallback(AddressOf InvokePropertyChanged), e)
            End If
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub AsyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.AsyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

    ''' <summary>
    ''' Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes or
    ''' Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property value.
    ''' </summary>
    ''' <remarks> David, 12/11/2015. </remarks>
    ''' <param name="e"> Property changed event information. </param>
    Protected Overridable Sub SyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If e IsNot Nothing Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeInvokePropertyChanged(e)
            Else
                SynchronizationContext.Current.Send(New SendOrPostCallback(AddressOf InvokePropertyChanged), e)
            End If
        End If
    End Sub

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#End Region

#Region " PROPERTY CHANGING EVENT IMPLEMENTATION "

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanging(ByVal sender As Object, ByVal e As PropertyChangingEventArgs) Implements INotifyPropertyChanging.PropertyChanging

    ''' <summary> Removes the event handler. </summary>
    ''' <remarks> David, 12/21/2015. </remarks>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As PropertyChangingEventHandler)
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanging, CType(d, PropertyChangingEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overridable Sub InvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        Dim evt As PropertyChangingEventHandler = Me.PropertyChangingEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokePropertyChanging(ByVal obj As Object)
        Me.InvokePropertyChanging(CType(obj, PropertyChangingEventArgs))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeInvokePropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeInvokePropertyChanging(New PropertyChangingEventArgs(name))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overridable Sub SafeInvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        Dim evt As PropertyChangingEventHandler = Me.PropertyChangingEvent
        For Each d As [Delegate] In evt.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    target.Invoke(d, New Object() {Me, e})
                End If
            End If
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanging">Event</see> in a property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SafeBeginInvokePropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        Me.SafeBeginInvokePropertyChanging(New PropertyChangingEventArgs(name))
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanging">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overridable Sub SafeBeginInvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        Dim evt As PropertyChangingEventHandler = Me.PropertyChangingEvent
        For Each d As [Delegate] In evt.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    target.BeginInvoke(d, New Object() {Me, e})
                End If
            End If
        Next
    End Sub

#End Region

#Region " SYNCHRONOUS NOTIFICATIONS "

    ''' <summary>
    ''' Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes or
    ''' Dynamically Invokes) a change <see cref="PropertyChanging">event</see> in Property value.
    ''' </summary>
    ''' <remarks> David, 12/11/2015. </remarks>
    ''' <param name="e"> Property changing event information. </param>
    Protected Overridable Sub SyncNotifyPropertyChanging(ByVal e As PropertyChangingEventArgs)
        If e IsNot Nothing Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeInvokePropertyChanging(e)
            Else
                SynchronizationContext.Current.Send(New SendOrPostCallback(AddressOf InvokePropertyChanging), e)
            End If
        End If
    End Sub

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanging">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub SyncNotifyPropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SyncNotifyPropertyChanging(New PropertyChangingEventArgs(name))
        End If
    End Sub

#End Region

#Region " ASYNCHRONOUS NOTIFICATIONS "

    ''' <summary>
    ''' Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins Invoke
    ''' or Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property value.
    ''' </summary>
    ''' <remarks> David, 12/11/2015. </remarks>
    ''' <param name="e"> Property changing event information. </param>
    Protected Overridable Sub AsyncNotifyPropertyChanging(ByVal e As PropertyChangingEventArgs)
        If e IsNot Nothing Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeBeginInvokePropertyChanging(e)
            Else
                SynchronizationContext.Current.Post(New SendOrPostCallback(AddressOf InvokePropertyChanging), e)
            End If
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Sub AsyncNotifyPropertyChanging(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.AsyncNotifyPropertyChanging(New PropertyChangingEventArgs(name))
        End If
    End Sub

#End Region

#End Region

#Region " SAFE EVENTS "

    ''' <summary>
    ''' Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled entity</see>)
    ''' or (Dynamically Invokes) an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>.
    ''' </summary>
    ''' <remarks> David, 1/1/2016. </remarks>
    ''' <param name="handler"> The handler. </param>
    ''' <param name="e">       Cancel event information. </param>
    Protected Overridable Sub SafeInvoke(ByVal handler As EventHandler(Of ComponentModel.CancelEventArgs),
                                         ByVal e As ComponentModel.CancelEventArgs)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    target.Invoke(d, New Object() {Me, e})
                End If
            End If
        Next
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Protected Overridable Sub SafeInvoke(ByVal handler As EventHandler(Of System.EventArgs))
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, System.EventArgs.Empty})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, System.EventArgs.Empty})
                Else
                    target.Invoke(d, New Object() {Me, System.EventArgs.Empty})
                End If
            End If
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Protected Overridable Sub SafeBeginInvoke(ByVal handler As EventHandler(Of System.EventArgs))
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, System.EventArgs.Empty})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, System.EventArgs.Empty})
                Else
                    target.BeginInvoke(d, New Object() {Me, System.EventArgs.Empty})
                End If
            End If
        Next
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    Protected Shared Sub SafeInvoke(ByVal handler As EventHandler(Of System.EventArgs), ByVal sender As Object)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, System.EventArgs.Empty})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, System.EventArgs.Empty})
                Else
                    target.Invoke(d, New Object() {sender, System.EventArgs.Empty})
                End If
            End If
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    Protected Shared Sub SafeBeginInvoke(ByVal handler As EventHandler(Of System.EventArgs), ByVal sender As Object)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, System.EventArgs.Empty})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, System.EventArgs.Empty})
                Else
                    target.BeginInvoke(d, New Object() {sender, System.EventArgs.Empty})
                End If
            End If
        Next
    End Sub

    ''' <summary> Synchronously (Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of TEventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    ''' <param name="e">       The arguments for the event. </param>
    ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
    Protected Shared Sub SafeInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs),
                                                                ByVal sender As Object, ByVal e As TEventArgs)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target IsNot Nothing AndAlso target.InvokeRequired Then
                    ' synchronously executes the delegate on the target thread.
                    target.Invoke(handler, New Object() {sender, e})
                Else
                    d.DynamicInvoke(New Object() {sender, e})
                End If
            End If
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of TEventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    ''' <param name="e">       The arguments for the event. </param>
    ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
    Protected Shared Sub SafeBeginInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs),
                                                                     ByVal sender As Object, ByVal e As TEventArgs)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target IsNot Nothing AndAlso target.InvokeRequired Then
                    ' asynchronously executes the delegate on the target thread.
                    target.BeginInvoke(handler, New Object() {sender, e})
                Else
                    d.DynamicInvoke(New Object() {sender, e})
                End If
            End If
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of TEventArgs)">Event</see> and waits for its completion. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    ''' <param name="e">       The arguments for the event. </param>
    ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
    Protected Shared Sub SafeBeginEndInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs),
                                                                        ByVal sender As Object, ByVal e As TEventArgs)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target IsNot Nothing AndAlso target.InvokeRequired Then
                    ' asynchronously executes the delegate on the target thread.
                    Dim result As IAsyncResult = target.BeginInvoke(handler, New Object() {sender, e})
                    If result IsNot Nothing Then
                        ' waits until the process ends.
                        target.EndInvoke(result)
                    End If
                Else
                    d.DynamicInvoke(New Object() {sender, e})
                End If
            End If
        Next
    End Sub

#End Region

End Class
