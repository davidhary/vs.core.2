﻿Imports System.Configuration
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Security.Permissions
''' <summary> A form that persists user settings in the Application Settings file. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/31/2013" by="David" revision="6.1.4779.x"> created. </history>
Public Class UserFormBase
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>

    Protected Sub New()
        MyBase.New()
        Me.InitializeComponent()
        Me.SaveSettingsOnClosing = True
    End Sub

    ''' <summary> Initializes the component. </summary>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(331, 341)
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "UserFormBase"
        Me.ResumeLayout(False)
    End Sub

#Region " CLASS STYLE "

    ''' <summary> The enable drop shadow version. </summary>
    Public Const EnableDropShadowVersion As Integer = 5

    ''' <summary> Gets or sets the class style. </summary>
    ''' <value> The class style. </value>
    Protected Property ClassStyle As ClassStyleConstants = ClassStyleConstants.None

    ''' <summary>
    ''' Adds a drop shadow parameter.
    ''' </summary>
    ''' <remarks>
    ''' From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx
    ''' </remarks>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                                        Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Dim cp As System.Windows.Forms.CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CInt(Me.ClassStyle)
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " SETTING EVENTS "

    ''' <summary> Controls if settings are saved when closing. </summary>
    ''' <remarks> Set this property to false to disable saving form settings on closing. </remarks>
    ''' <value> <c>SaveSettingsOnClosing</c>is a Boolean property. </value>
    Public Property SaveSettingsOnClosing() As Boolean

    ''' <summary> This is called when the form is loaded before it is visible. </summary>
    ''' <remarks> Use this method to set form elements before the form is visible. </remarks>
    <PermissionSetAttribute(SecurityAction.Demand, Unrestricted:=True)>
    Protected Overridable Sub OnLoadSettings()
        If Me.SaveSettingsOnClosing Then
            With My.MyAppSettings.Get()
                Dim startPos As FormStartPosition = .ReadValue(Me.StartPositionKey, Me.StartPosition)
                If Not Me.StartPosition.Equals(startPos) Then
                    Me.StartPosition = startPos
                End If
            End With
        End If
    End Sub

    ''' <summary> This is called when the form is shown after it is visible. </summary>
    ''' <remarks> Use this method to set form elements after the form is visible. </remarks>
    <PermissionSetAttribute(SecurityAction.Demand, Unrestricted:=True)>
    Protected Overridable Sub OnShowSettings()
        If Me.SaveSettingsOnClosing Then
            With My.MyAppSettings.Get()
                If Me.StartPosition = FormStartPosition.Manual Then
                    Me.WindowState = .ReadValue(Me.WindowsStateKey, Me.WindowState)
                    If Me.WindowState = FormWindowState.Normal Then
                        Dim size As Drawing.Size = .ReadValue(Me.SizeKey, Me.Size)
                        Dim loc As Drawing.Point = .ReadValue(Me.LocationKey, Me.Location)
                        If Not Me.Location.Equals(loc) AndAlso
                            loc.X < Screen.PrimaryScreen.WorkingArea.Width AndAlso
                            loc.X + size.Width > 0 AndAlso
                            loc.Y < Screen.PrimaryScreen.WorkingArea.Height AndAlso
                            loc.Y + size.Height > 0 Then
                            Me.Location = loc
                        End If
                        If Not Me.Size.Equals(size) Then
                            Me.Size = size
                        End If
                    End If
                End If
            End With
        End If
    End Sub

    ''' <summary> Is called when the form unloads. </summary>
    ''' <remarks> Use Save settings. </remarks>
    <PermissionSetAttribute(SecurityAction.Demand, Unrestricted:=True)>
    Protected Overridable Sub OnSaveSettings()
        If Me.SaveSettingsOnClosing Then
            With My.MyAppSettings.Get()
                .WriteValue(Me.StartPositionKey, Me.StartPosition)
                If Me.StartPosition = FormStartPosition.Manual Then
                    .WriteValue(Me.WindowsStateKey, Me.WindowState)
                    If Me.WindowState = FormWindowState.Normal Then
                        .WriteValue(Me.LocationKey, Me.Location)
                        .WriteValue(Me.SizeKey, Me.Size)
                    End If
                End If
                .Save()
            End With
        End If
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary> Gets or sets the sentinel indicating that the form loaded without an exception.
    '''           Should be set only if load did not fail. </summary>
    ''' <value> The is loaded. </value>
    Protected Property IsLoaded As Boolean

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event after reading the
    ''' start position from the configuration file. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As System.EventArgs)
        Try
            If Not Me.DesignMode Then
                Me.OnLoadSettings()
            End If
            Me.IsLoaded = True
        Catch
            Throw
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event after positioning
    ''' the form based on the configuration settings. </summary>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnShown(e As System.EventArgs)
        If Not Me.IsLoaded Then Return
        Try
            If Not Me.DesignMode Then
                Me.OnShowSettings()
            End If
        Catch
            Throw
        Finally
            MyBase.OnShown(e)
        End Try
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event after saving the
    ''' form location settings. </summary>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        Try
            If Not Me.DesignMode AndAlso Me.IsLoaded AndAlso e IsNot Nothing AndAlso Not e.Cancel Then
                Me.OnSaveSettings()
            End If
        Catch ex As ConfigurationErrorsException
            ' this error occurs when the system thinks that two managers accessed the configuration file.
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        Catch
            Throw
        Finally
            MyBase.OnClosing(e)
        End Try
    End Sub

#End Region

#Region " CONFIGURATION MEMBERS "

    ''' <summary> Gets the location key. </summary>
    ''' <value> The location key. </value>
    Private ReadOnly Property LocationKey As String
        Get
            Return Me.Name & ".Location"
        End Get
    End Property

    ''' <summary> Gets the size key. </summary>
    ''' <value> The size key. </value>
    Private ReadOnly Property SizeKey As String
        Get
            Return Me.Name & ".Size"
        End Get
    End Property

    ''' <summary> Gets the start position key. </summary>
    ''' <value> The windows state key. </value>
    Private ReadOnly Property StartPositionKey As String
        Get
            Return Me.Name & ".StartPosition"
        End Get
    End Property

    ''' <summary> Gets the windows state key. </summary>
    ''' <value> The windows state key. </value>
    Private ReadOnly Property WindowsStateKey As String
        Get
            Return Me.Name & ".FormWindowState"
        End Get
    End Property

#End Region

End Class
