Imports System.Windows.Forms
''' <summary> Form for displaying a message. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="9/17/2014" by="David" revision=""> Created. </history>
Partial Public NotInheritable Class MessageForm
    Inherits FormBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Prevents a default instance of the <see cref="MessageForm" /> class from being
    ''' created. </summary>
    Private Sub New()

        MyBase.New()

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

    End Sub

    ''' <summary> Gets the locking object to enforce thread safety when creating the singleton
    ''' instance. </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As MessageForm

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As MessageForm
        If Not Instantiated Then
            SyncLock _SyncLocker
                _Instance = New MessageForm
            End SyncLock
        End If
        Return _Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock _SyncLocker
                Return _Instance IsNot Nothing AndAlso Not _Instance.IsDisposed
            End SyncLock
        End Get
    End Property

    ''' <summary> Dispose instance. </summary>
    Public Shared Sub DisposeInstance()
        SyncLock _SyncLocker
            If _Instance IsNot Nothing AndAlso Not _Instance.IsDisposed Then
                _Instance.Dispose()
                _Instance = Nothing
            End If
        End SyncLock
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If components IsNot Nothing Then components.Dispose() : components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENTS "

    Protected Overrides Sub OnShown(e As System.EventArgs)
        MyBase.OnShown(e)
    End Sub

    ''' <summary> Displays this dialog with title 'illegal call'. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2222:DoNotDecreaseInheritedMemberVisibility")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Shadows Function ShowDialog() As DialogResult
        Me.Text = "Illegal Call"
        Return MyBase.ShowDialog()
    End Function

    ''' <summary> Shows the message box with 'illegal call' in the caption. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2222:DoNotDecreaseInheritedMemberVisibility")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Shadows Sub Show()
        Me.Status = ""
        Me.Text = "Illegal Call"
        Me._RichTextBox.Text = "Illegal Call"
        Me.DialogResult = DialogResult.None
        MyBase.Show()
    End Sub

    ''' <summary> Shows the message box with these messages. </summary>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="details"> The details. </param>
    Public Shadows Sub Show(ByVal mdiForm As Form, ByVal caption As String, ByVal details As String)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        Me.Status = ""
        Me.Text = caption
        Me._RichTextBox.Text = details
        Me.DialogResult = DialogResult.None
        Application.DoEvents()
        MyBase.Show()
        Application.DoEvents()
    End Sub

    ''' <summary> Shows the message box with these messages. </summary>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="details"> The details. </param>
    Public Shadows Sub Show(ByVal mdiForm As Form, ByVal caption As String, ByVal details As String, ByVal duration As TimeSpan)
        Me.Show(mdiForm, caption, details)
        Dim endtime As DateTime = DateTime.Now.Add(duration)
        Me._ProgressBar.Value = 100
        Me._ProgressBar.Visible = True
        Do Until endtime < DateTime.Now
            Application.DoEvents()
            Me._ProgressBar.Value = CInt(Math.Max(0, 100 * endtime.Subtract(DateTime.Now).TotalSeconds / duration.TotalSeconds))
            Application.DoEvents()
        Loop
        Me._ProgressBar.Visible = False
        Me.Close()
    End Sub

    ''' <summary> Gets or sets the rich text box. </summary>
    ''' <value> The rich text box. </value>
    <System.ComponentModel.Browsable(False),
        System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property RichTextBox As System.Windows.Forms.RichTextBox
        Get
            Return Me._RichTextBox
        End Get
    End Property

    ''' <summary> Gets or sets the status. </summary>
    ''' <value> The status. </value>
    <System.ComponentModel.Browsable(False),
        System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)>
    Public Property Status As String
        Get
            Return Me._StatusLabel.Text
        End Get
        Set(value As String)
            Me._StatusLabel.Text = value
            Application.DoEvents()
        End Set
    End Property

    ''' <summary> Gets the progress. </summary>
    ''' <value> The progress. </value>
    <System.ComponentModel.Browsable(False),
        System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property Progress As ToolStripProgressBar
        Get
            Return Me._ProgressBar
        End Get
    End Property

    ''' <summary> Gets or sets the custom button text. </summary>
    ''' <value> The custom button text. </value>
    Public Property CustomButtonText As String
        Get
            Return Me._CustomButton.Text
        End Get
        Set(value As String)
            Me._CustomButton.Text = value
            Application.DoEvents()
        End Set
    End Property

    ''' <summary> Custom button click. </summary>
    ''' <remarks> David, 12/28/2015. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _CustomButton_Click(sender As Object, e As System.EventArgs) Handles _CustomButton.Click
        Me.DialogResult = DialogResult.OK
        Application.DoEvents()
        Me.Status = String.Format("{0} requested", Me._CustomButton.Text)
    End Sub

#End Region

End Class
