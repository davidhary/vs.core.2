Imports System.Drawing
Imports System.Windows.Forms
''' <summary> Form with drop shadow. </summary>
''' <license> (c) 2007 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="08/13/2007" by="Nicholas Seward" revision="1.0.2781.x"> http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx. </history>
''' <history date="08/13/2007" by="David" revision="1.0.2781.x">           Convert from C#. </history>
Public Class FormBase
    Inherits Form

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    Protected Sub New()
        MyBase.New()
        Me.InitializeComponent()
    End Sub

    ''' <summary> Initializes the component. </summary>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(331, 341)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FormBase"
        Me.ResumeLayout(False)
    End Sub

#Region " CLASS STYLE "

    ''' <summary> The enable drop shadow version. </summary>
    Public Const EnableDropShadowVersion As Integer = 5

    ''' <summary> Gets or sets the class style. </summary>
    ''' <value> The class style. </value>
    Protected Property ClassStyle As ClassStyleConstants = ClassStyleConstants.None

    ''' <summary> Adds a drop shadow parameter. </summary>
    ''' <remarks> From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx. </remarks>
    ''' <value> Options that control the create. </value>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                                        Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CInt(Me.ClassStyle)
            Return cp
        End Get
    End Property

#End Region

#End Region

End Class

''' <summary> Values that represent class style constants. </summary>
''' <remarks> David, 11/26/2015. </remarks>
<Flags>
Public Enum ClassStyleConstants
    <System.ComponentModel.Description("Not Specified")> None = 0
    <System.ComponentModel.Description("No close button")> HideCloseButton = &H200
    <System.ComponentModel.Description("Drop Shadow")> DropShadow = &H20000 ' 131072
End Enum
