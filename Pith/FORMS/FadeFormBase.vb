Imports System.Drawing
Imports System.Windows.Forms
Imports System.Security.Permissions
''' <summary> Form with fade in and out capabilities. </summary>
''' <remarks> Features: <para>
''' fades in on open; </para><para>
''' fades out on close; </para><para>
''' partially fades out on focus lost;</para><para>
''' fades in on focus; </para><para>
''' fades out on minimize; </para><para>
''' fades in on restore;</para><para>
''' must not annoy the user. </para><para>
''' Usage: To use FadeForm, just extend it instead of Form and you are ready to go. It is
''' defaulted to use the fade-in from nothing on open and out to nothing on close. It will also
''' fade to 85% opacity when not the active window. Fading can be disabled and enabled with two
''' methods setting the default enable and disable modes.</para> </remarks>
''' <license> (c) 2007 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="08/13/2007" by="Nicholas Seward" revision="1.0.2781.x"> http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx. </history>
''' <history date="08/13/2007" by="David" revision="1.0.2781.x">           Convert from C#. </history>
Public Class FadeFormBase
    Inherits Form

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <remarks> David, 12/3/2015. </remarks>
    Protected Sub New()
        MyBase.New()
        Me._fadeTime = 0.35
        Me._activeOpacity = 1
        Me._inactiveOpacity = 0.85
        Me.InitializeComponent()
    End Sub

    ''' <summary> Initializes the component. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Mobility", "CA1601:DoNotUseTimersThatPreventPowerStateChanges")>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(331, 341)
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FadeFormBase"
        Me.timer = New System.Windows.Forms.Timer()
        Me.timer.Interval = 25
        Me.ResumeLayout(False)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If timer IsNot Nothing Then timer.Dispose() : timer = Nothing
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#Region " CLASS STYLE "

    ''' <summary> The enable drop shadow version. </summary>
    Public Const EnableDropShadowVersion As Integer = 5

    ''' <summary> Gets or sets the class style. </summary>
    ''' <value> The class style. </value>
    Protected Property ClassStyle As ClassStyleConstants = ClassStyleConstants.None

    ''' <summary> Adds a drop shadow parameter. </summary>
    ''' <remarks> From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx. </remarks>
    ''' <value> Options that control the create. </value>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                                        Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or Me.ClassStyle
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " FADING PROPERTIES "

    ''' <summary> The active opacity. </summary>
    Private _activeOpacity As Double

    ''' <summary> Gets or sets the opacity that the form will transition to when the form gets focus. </summary>
    ''' <value> The active opacity. </value>
    Public Property ActiveOpacity() As Double
        Get
            Return Me._activeOpacity
        End Get
        Set(ByVal value As Double)
            If value >= 0 AndAlso value <= 1 Then
                Me._activeOpacity = value
            Else
                Throw New ArgumentOutOfRangeException("value", "The Active Opacity must be between 0 and 1.")
            End If
            If Me.ContainsFocus Then
                Me.TargetOpacity = Me._activeOpacity
            End If
        End Set
    End Property

    ''' <summary> The fade time. </summary>
    Private _fadeTime As Double

    ''' <summary> Gets or sets the time it takes to fade from 1 to 0 or the other way around. </summary>
    ''' <value> The fade time. </value>
    Public Property FadeTime() As Double
        Get
            Return Me._fadeTime
        End Get
        Set(ByVal value As Double)
            If value > 0 Then
                Me._fadeTime = value
            Else
                Throw New ArgumentOutOfRangeException("value", "The FadeTime must be a positive value.")
            End If
        End Set
    End Property

    ''' <summary> The inactive opacity. </summary>
    Private _inactiveOpacity As Double

    ''' <summary> Gets or sets the opacity that the form will transition to when the form doesn't have
    ''' focus. </summary>
    ''' <value> The inactive opacity. </value>
    Public Property InactiveOpacity() As Double
        Get
            Return Me._inactiveOpacity
        End Get
        Set(ByVal value As Double)
            If value >= 0 AndAlso value <= 1 Then
                Me._inactiveOpacity = value
            Else
                Throw New ArgumentOutOfRangeException("value", "The InactiveOpacity must be between 0 and 1.")
            End If
            If (Not Me.ContainsFocus) AndAlso Me.WindowState <> FormWindowState.Minimized Then
                Me.TargetOpacity = Me._inactiveOpacity
            End If
        End Set
    End Property

    ''' <summary> The minimized opacity. </summary>
    Private _minimizedOpacity As Double

    ''' <summary> Gets or sets the opacity that the form will transition to when the form is minimized. </summary>
    ''' <value> The minimized opacity. </value>
    Public Property MinimizedOpacity() As Double
        Get
            Return Me._minimizedOpacity
        End Get
        Set(ByVal value As Double)
            If value >= 0 AndAlso value <= 1 Then
                Me._minimizedOpacity = value
            Else
                Throw New ArgumentOutOfRangeException("value", "The MinimizedOpacity must be between 0 and 1.")
            End If
            If (Not Me.ContainsFocus) AndAlso Me.WindowState <> FormWindowState.Minimized Then
                Me.TargetOpacity = Me._inactiveOpacity
            End If
        End Set
    End Property

    ''' <summary> Target opacity. </summary>
    Private _targetOpacity As Double

    ''' <summary> Gets or sets the opacity the form is transitioning to. </summary>
    ''' <remarks> Setting this value amounts also to a one-time fade to any value. The opacity is never
    ''' actually 1. This is hack to keep the window from flashing black. The cause of this is not
    ''' clear. </remarks>
    ''' <value> The target opacity. </value>
    Public Property TargetOpacity() As Double
        Get
            Return Me._targetOpacity
        End Get
        Set(ByVal value As Double)
            Me._targetOpacity = value
            If (Not timer.Enabled) Then
                timer.Start()
            End If
        End Set
    End Property

#End Region

#Region " FADING CONTROL "

    ''' <summary> Turns off opacity fading. </summary>
    Public Sub DisableFade()
        Me._activeOpacity = 1
        Me._inactiveOpacity = 1
        Me._minimizedOpacity = 1
    End Sub

    ''' <summary> Turns on opacity fading. </summary>
    Public Sub EnableFadeDefaults()
        Me._activeOpacity = 1
        Me._inactiveOpacity = 0.85
        Me._minimizedOpacity = 0
        Me._fadeTime = 0.35
    End Sub

#End Region

#Region " WINDOWS MESSAGES "

#Region " WindowsMessageCodes "
    ''' <summary> The Windows Message System Command. </summary>
    Private Const WM_SYSCOMMAND As Integer = &H112
    ''' <summary> The Windows Message command. </summary>
    Private Const WM_COMMAND As Integer = &H111
    ''' <summary> The screen minimize. </summary>
    Private Const SC_MINIMIZE As Integer = &HF020
    ''' <summary> The screen restore. </summary>
    Private Const SC_RESTORE As Integer = &HF120
    ''' <summary> The screen close. </summary>
    Private Const SC_CLOSE As Integer = &HF060
#End Region

    ''' <summary>
    ''' Gets or sets the WindowsMessage that is being held until the end of a transition.
    ''' </summary>
    Private heldMessage As Message = New Message()

    ''' <summary> Intercepts Window Messages before they are processed. </summary>
    ''' <remarks> The minimize and close events require messaging because these actions have to be
    ''' postponed until the fade transition is complete. Overriding WndProc catches the request for
    ''' those actions and postpones the action until the transition is done. </remarks>
    ''' <param name="m"> [in,out] Windows Message. </param>
    <SecurityPermission(SecurityAction.Demand, Flags:=SecurityPermissionFlag.UnmanagedCode)>
    Protected Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = WM_SYSCOMMAND OrElse m.Msg = WM_COMMAND Then
            'Fade to zero on minimize
            If m.WParam = New IntPtr(SC_MINIMIZE) Then
                If heldMessage.WParam <> New IntPtr(SC_MINIMIZE) Then
                    heldMessage = m
                    Me.TargetOpacity = Me._minimizedOpacity
                Else
                    heldMessage = New Message()
                    Me.TargetOpacity = Me._activeOpacity
                End If
                Return

                'Fade in if the window is restored from the task bar
            ElseIf m.WParam = New IntPtr(SC_RESTORE) AndAlso Me.WindowState = FormWindowState.Minimized Then
                MyBase.WndProc(m)
                Me.TargetOpacity = Me._activeOpacity
                Return

                'Fade out if the window is closed.
            ElseIf m.WParam = New IntPtr(SC_CLOSE) Then
                heldMessage = m
                Me.TargetOpacity = Me._minimizedOpacity
                Return
            End If
        End If

        MyBase.WndProc(m)
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Gets or sets the sentinel indicating that the form loaded without an exception.
    ''' Should be set only if load did not fail. </summary>
    ''' <value> The is loaded. </value>
    Protected Property IsLoaded As Boolean

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event after reading the
    ''' start position from the configuration file. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As System.EventArgs)
        Try
            Me.Opacity = Me.MinimizedOpacity
            Me.TargetOpacity = Me.ActiveOpacity
            Me.IsLoaded = True
        Catch
            Throw
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Activated" /> event. Fades in the
    ''' form when it gains focus. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnActivated(e As System.EventArgs)
        Me.TargetOpacity = Me.ActiveOpacity
        MyBase.OnActivated(e)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Deactivate" /> event. Fades out the
    ''' form when it losses focus. </summary>
    ''' <param name="e"> The <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnDeactivate(e As System.EventArgs)
        Me.TargetOpacity = Me.InactiveOpacity
        MyBase.OnDeactivate(e)
    End Sub

#End Region

#Region " OPACITY TIMER "

    ''' <summary> Timer to aid in fade effects. </summary>
    Private WithEvents timer As System.Windows.Forms.Timer

    ''' <summary> Performs fade increment. </summary>
    ''' <remarks> The timer is stopped after reaching the target opacity. The timer gets started
    ''' whenever the <see cref="TargetOpacity">target opacity</see> is set method. The timer is
    ''' stopped to conserve processor power when not needed. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub timer_Tick(ByVal sender As Object, ByVal e As EventArgs) Handles timer.Tick

        Dim fadeChangePerTick As Double = timer.Interval * 1.0 / 1000 / Me._fadeTime
        ' Check to see if it is time to stop the timer
        If Math.Abs(Me._targetOpacity - Me.Opacity) < fadeChangePerTick Then
            'Stop the timer to save processor.
            timer.Stop()
            'There is an ugly black flash if you set the Opacity to 1.0
            If Me._targetOpacity = 1 Then
                Me.Opacity = 0.999
            Else
                Me.Opacity = Me.TargetOpacity
            End If
            'Process held Windows Message.
            MyBase.WndProc(heldMessage)
            heldMessage = New Message()
        ElseIf Me.TargetOpacity > Me.Opacity Then
            Me.Opacity += fadeChangePerTick
        ElseIf Me.TargetOpacity < Me.Opacity Then
            Me.Opacity -= fadeChangePerTick
        End If
    End Sub

#End Region

End Class

