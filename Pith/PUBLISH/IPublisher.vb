﻿''' <summary> Defines the contract that must be implemented by publishers. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/22/13" by="David" revision="1.2.5013"> Created. </history>
Public Interface IPublisher

    ''' <summary> Gets a value indicating whether elements can be published. </summary>
    ''' <value> <c>True</c> if elements are publishable; otherwise, <c>False</c>. </value>
    ReadOnly Property Publishable As Boolean

    ''' <summary> Suspends publishing. </summary>
    Sub SuspendPublishing()

    ''' <summary> Resumes publishing. </summary>
    Sub ResumePublishing()

    ''' <summary> Publishes all values. </summary>
    Sub Publish()

End Interface

