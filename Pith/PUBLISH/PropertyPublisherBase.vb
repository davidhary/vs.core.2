﻿Imports System.Threading
Imports System.ComponentModel
''' <summary>  Defines the contract that must be implemented by property publishers. </summary>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/28/12" by="David" revision="1.2.4654"> Documented. </history>
Public MustInherit Class PropertyPublisherBase
    Inherits PropertyNotifyBase
    Implements IPublisher

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of the <see cref="PropertyPublisherBase" /> class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " PUBLISHER BASE "

    ''' <summary> Gets or sets a value indicating whether elements can be published. </summary>
    ''' <value> <c>True</c> if elements are publishable; otherwise, <c>False</c>. </value>
    Protected ReadOnly Property Publishable As Boolean Implements IPublisher.Publishable

    ''' <summary> Publishes all elements. </summary>
    Public MustOverride Sub Publish() Implements IPublisher.Publish

    ''' <summary> Resumes publishing. </summary>
    Public Overridable Sub ResumePublishing() Implements IPublisher.ResumePublishing
        Me._Publishable = True
        Me.AsyncNotifyPropertyChanged(NameOf(Me.Publishable))
    End Sub

    ''' <summary> Suspends publishing. </summary>
    Public Overridable Sub SuspendPublishing() Implements IPublisher.SuspendPublishing
        Me._Publishable = False
        Me.AsyncNotifyPropertyChanged(NameOf(Me.Publishable))
    End Sub

#End Region

#Region " PROPERTY CHANGED EVENT IMPLEMENTATION "

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub InvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then MyBase.InvokePropertyChanged(e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub SafeInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then MyBase.SafeInvokePropertyChanged(e)
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub SafeBeginInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then MyBase.SafeBeginInvokePropertyChanged(e)
    End Sub

#End Region

#Region " NOTIFICATIONS "

    ''' <summary>
    ''' Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins Invoke
    ''' or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value.
    ''' </summary>
    ''' <remarks> David, 12/11/2015. </remarks>
    ''' <param name="e"> Property changed event information. </param>
    Protected Overrides Sub AsyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then MyBase.AsyncNotifyPropertyChanged(e)
    End Sub

    ''' <summary>
    ''' Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes or
    ''' Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property value.
    ''' </summary>
    ''' <remarks> David, 12/11/2015. </remarks>
    ''' <param name="e"> Property changed event information. </param>
    Protected Overrides Sub SyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then MyBase.SyncNotifyPropertyChanged(e)
    End Sub

#End Region

#End Region

#Region " PROPERTY CHANGING EVENT IMPLEMENTATION "

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub InvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        If Publishable Then MyBase.InvokePropertyChanging(e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub SafeInvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        If Me.Publishable Then MyBase.SafeInvokePropertyChanging(e)
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanging">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangingEventArgs" /> instance containing the event
    ''' data. </param>
    Protected Overrides Sub SafeBeginInvokePropertyChanging(ByVal e As PropertyChangingEventArgs)
        If Me.Publishable Then MyBase.SafeBeginInvokePropertyChanging(e)
    End Sub

#End Region

#Region " SYNCHRONOUS NOTIFICATIONS "

    ''' <summary>
    ''' Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes or
    ''' Dynamically Invokes) a change <see cref="PropertyChanging">event</see> in Property value.
    ''' </summary>
    ''' <remarks> David, 12/11/2015. </remarks>
    ''' <param name="e"> Property Changing event information. </param>
    Protected Overrides Sub SyncNotifyPropertyChanging(ByVal e As PropertyChangingEventArgs)
        If Me.Publishable Then MyBase.SyncNotifyPropertyChanging(e)
    End Sub

#End Region

#Region " ASYNCHRONOUS NOTIFICATIONS "

    ''' <summary>
    ''' Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins Invoke
    ''' or Dynamically Invokes) a change
    ''' <see cref="PropertyChanging">event</see> in Property value.
    ''' </summary>
    ''' <remarks> David, 12/11/2015. </remarks>
    ''' <param name="e"> Property Changing event information. </param>
    Protected Overrides Sub AsyncNotifyPropertyChanging(ByVal e As PropertyChangingEventArgs)
        If Me.Publishable Then MyBase.AsyncNotifyPropertyChanging(e)
    End Sub

#End Region

#End Region

#Region " SAFE EVENTS "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Protected Sub SafePublish(ByVal handler As EventHandler(Of ComponentModel.CancelEventArgs),
                              ByVal e As ComponentModel.CancelEventArgs)
        If Me.Publishable Then MyBase.SafeInvoke(handler, e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Protected Sub SafePublish(ByVal handler As EventHandler(Of System.EventArgs))
        If Me.Publishable Then MyBase.SafeInvoke(handler)
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Protected Sub SafeBeginPublish(ByVal handler As EventHandler(Of System.EventArgs))
        If Me.Publishable Then MyBase.SafeBeginInvoke(handler)
    End Sub

#End Region

End Class

