﻿''' <summary> Interface for trace message listener. </summary>
''' <remarks> David, 12/29/2015. </remarks>
''' <license>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
Public Interface ITraceMessageListener

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <param name="value"> The event message. </param>
    ''' <returns> The trace message details. </returns>
    Function TraceEvent(ByVal value As TraceMessage) As String

    ''' <summary> Gets or sets the trace level. </summary>
    Property TraceLevel As TraceEventType

    ''' <summary> Determine if we should trace. </summary>
    ''' <param name="value"> The event message. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Function ShouldTrace(ByVal value As TraceEventType) As Boolean

End Interface

''' <summary> Collection of trace listeners. </summary>
''' <remarks> David, 12/29/2015. </remarks>
''' <license>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/29/2015" by="David" revision=""> Created. </history>
Public Class TraceListenerCollection
    Inherits Collections.ObjectModel.Collection(Of ITraceMessageListener)

    ''' <summary> Adds items. </summary>
    ''' <remarks> David, 12/29/2015. </remarks>
    ''' <param name="items"> The items to add. </param>
    Public Overloads Sub Add(ByVal items As IEnumerable(Of ITraceMessageListener))
        If items IsNot Nothing Then
            For Each item As ITraceMessageListener In items
                Me.Add(item)
            Next
        End If
    End Sub

    ''' <summary> Updates the trace level described by value. </summary>
    ''' <remarks> David, 12/30/2015. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub UpdateTraceLevel(ByVal value As TraceEventType)
        For Each item As ITraceMessageListener In Me
            item.TraceLevel = value
        Next
    End Sub

End Class
