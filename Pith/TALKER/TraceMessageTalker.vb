﻿''' <summary> Trace Message Talker -- broadcasts trace messages to trace message listeners. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="10/10/2014" by="David" revision=""> Created. </history>
Public Class TraceMessageTalker
    Implements ITraceMessageTalker

#Region " CONSTRUCTORS "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 12/12/2015. </remarks>
    Public Sub New()
        MyBase.New
        Me._Listeners = New TraceListenerCollection
        Me._TraceMessage = TraceMessage.Empty
    End Sub

#End Region

#Region " LISTENERS "

    ''' <summary> Gets the listeners. </summary>
    ''' <value> The listeners. </value>
    Public ReadOnly Property Listeners As TraceListenerCollection Implements ITraceMessageTalker.Listeners

#End Region

#Region " PUBLISH "

    ''' <summary> Publishes and logs the message. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Public Function Publish(ByVal eventType As TraceEventType, ByVal id As Integer,
                            ByVal format As String, ByVal ParamArray args() As Object) As String Implements ITraceMessageTalker.Publish
        Return Me.Publish(New TraceMessage(eventType, id, format, args))
    End Function

    Private _TraceMessage As TraceMessage

    ''' <summary> Gets or sets a message describing the trace. </summary>
    ''' <value> A message describing the trace. </value>
    Public ReadOnly Property TraceMessage As TraceMessage Implements ITraceMessageTalker.TraceMessage
        Get
            Return Me._TraceMessage
        End Get
    End Property

    ''' <summary> Publishes the given value. </summary>
    ''' <remarks> David, 12/29/2015. </remarks>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Private Sub _Publish(ByVal value As TraceMessage)
        If Me.Listeners IsNot Nothing Then
            For Each listener As ITraceMessageListener In Me.Listeners
                listener.TraceEvent(value)
                System.Windows.Forms.Application.DoEvents()
            Next
        End If
    End Sub

    ''' <summary> Publishes and logs the message. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Public Function Publish(ByVal value As TraceMessage) As String Implements ITraceMessageTalker.Publish
        Dim details As String = ""
        If value IsNot Nothing Then
            Me._TraceMessage = New TraceMessage(value)
            details = value.Details
            Me._Publish(value)
        End If
        Return details
    End Function

#End Region

End Class
