﻿Imports System.ComponentModel
Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 11/26/2015. </remarks>
    Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ' ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = Pith.My.ProjectTraceEventId.Pith

        Public Const AssemblyTitle As String = "Core Pith Library"
        Public Const AssemblyDescription As String = "Core Pith Library"
        Public Const AssemblyProduct As String = "Core.Pith.2016"

        ''' <summary> Identifies this talker. </summary>
        ''' <remarks> David, 1/21/2016. </remarks>
        ''' <param name="talker"> The talker. </param>
        Public Shared Sub Identify(ByVal talker As isr.Core.Pith.ITraceMessageTalker)
            talker?.Publish(TraceEventType.Information, MyLibrary.TraceEventId, $"{MyLibrary.AssemblyProduct} ID = {MyLibrary.TraceEventId:X}")
        End Sub

    End Class

    ''' <summary> Values that represent project trace event identifiers. </summary>
    ''' <remarks> David, 11/26/2015. </remarks>
    Public Enum ProjectTraceEventId
        <Description("Not specified")> None
        <Description("Core Library Namespace")> BaseTraceEventId = &H30
        <Description("Pith")> Pith = BaseTraceEventId * TraceEventConstants.BaseScaleFactor +
                                                        TraceEventConstants.LibraryDigitMask + &H1
        <Description("Diagnosis Tester")> DiagnosisTester = BaseTraceEventId *
                                                            TraceEventConstants.BaseScaleFactor +
                                                            TraceEventConstants.FormApplicationDigitMask + &H1
        <Description("My Blue Splash Screen")> MyBlueSplashScreen = ProjectTraceEventId.DiagnosisTester + &H1
        <Description("Exception Message Test")> ExceptionMessageTest = BaseTraceEventId *
                                                                       TraceEventConstants.BaseScaleFactor +
                                                                       TraceEventConstants.UnitTestDigitMask + &H1
        <Description("My Exception Message Box Test")> MyExceptionMessageBoxTest = ProjectTraceEventId.ExceptionMessageTest + &H1
    End Enum

End Namespace
