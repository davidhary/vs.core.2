''' <summary> Implements a generic line class. </summary>
''' <typeparam name="T"> Specifies the type parameter of the generic class. </typeparam>
''' <license> (c) 2006 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="04/10/2006" by="David" revision="1.1.2291"> Created. </history>
Public Class Line(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of the <see cref="Line" /> class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="Line" /> class. </summary>
    ''' <param name="x1"> Specifies the X1 coordinate of the line. </param>
    ''' <param name="y1"> Specifies the Y1 coordinate of the line. </param>
    ''' <param name="x2"> Specifies the X2 coordinate of the line. </param>
    ''' <param name="y2"> Specifies the Y2 coordinate of the line. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    Public Sub New(ByVal x1 As T, ByVal y1 As T, ByVal x2 As T, ByVal y2 As T)
        Me.new()
        Me._SetLine(x1, y1, x2, y2)
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <param name="model"> The  <see cref="Line">Line</see> object from which to Copy. </param>
    Public Sub New(ByVal model As Line(Of T))

        Me.New()
        If model IsNot Nothing Then
            Me._SetLine(model._X1, model._Y1, model._X2, model._Y2)
        End If

    End Sub


#End Region

#Region " EQUALS "

    ''' <summary> Compares two lines. </summary>
    ''' <param name="left">  Specifies the line to compare to. </param>
    ''' <param name="right"> Specifies the line to compare. </param>
    ''' <returns> <c>True</c> if the lines are equal. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
        Return Line(Of T).Equals(TryCast(left, Line(Of T)), TryCast(right, Line(Of T)))
    End Function

    ''' <summary> Compares two lines. </summary>
    ''' <remarks> The two lines are the same if they have the same X and Y coordinates. </remarks>
    ''' <param name="left">  Specifies the line to compare to. </param>
    ''' <param name="right"> Specifies the line to compare. </param>
    ''' <returns> <c>True</c> if the lines are equal. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")>
    Public Overloads Shared Function Equals(ByVal left As Line(Of T), ByVal right As Line(Of T)) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left.X1.Equals(right.X1) AndAlso left.X2.Equals(right.X2) AndAlso
                   left.Y1.Equals(right.Y1) AndAlso left.Y2.Equals(right.Y2)
        End If
    End Function

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse Line(Of T).Equals(Me, CType(obj, Line(Of T))))
    End Function

    ''' <summary> Compares two lines. </summary>
    ''' <remarks> The two lines are the same if they have the same X1 and Y1 coordinates. </remarks>
    ''' <param name="other"> Specifies the other line. </param>
    ''' <returns> <c>True</c> if the lines are equal. </returns>
    Public Overloads Function Equals(ByVal other As Line(Of T)) As Boolean
        If other Is Nothing Then
            Return False
        Else
            Return other.X1.Equals(Me.X1) AndAlso other.X2.Equals(Me.X2) AndAlso
                   other.Y1.Equals(Me.Y1) AndAlso other.Y2.Equals(Me.Y2)
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As Line(Of T), ByVal right As Line(Of T)) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse (left IsNot Nothing) AndAlso left.Equals(right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As Line(Of T), ByVal right As Line(Of T)) As Boolean
        Return ((left Is Nothing) AndAlso (right IsNot Nothing)) OrElse Not ((left IsNot Nothing) AndAlso left.Equals(right))
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <returns> An <see cref="T:System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.X1.GetHashCode Xor Me.Y1.GetHashCode Xor Me.X2.GetHashCode Xor Me.Y2.GetHashCode
    End Function

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary> Sets the line based on the coordinates. </summary>
    ''' <remarks> Use this class to set the line. </remarks>
    ''' <param name="x1"> Specifies the X1 coordinate of the line. </param>
    ''' <param name="y1"> Specifies the Y1 coordinate of the line. </param>
    ''' <param name="x2"> Specifies the X2 coordinate of the line. </param>
    ''' <param name="y2"> Specifies the Y2 coordinate of the line. </param>
    Private Sub _SetLine(ByVal x1 As T, ByVal y1 As T, ByVal x2 As T, ByVal y2 As T)
        Me._X1 = x1
        Me._Y1 = y1
        Me._X2 = x2
        Me._Y2 = y2
    End Sub

    ''' <summary> Sets the line based on the coordinates. </summary>
    ''' <remarks> Use this class to set the line. </remarks>
    ''' <param name="x1"> Specifies the X1 coordinate of the line. </param>
    ''' <param name="y1"> Specifies the Y1 coordinate of the line. </param>
    ''' <param name="x2"> Specifies the X2 coordinate of the line. </param>
    ''' <param name="y2"> Specifies the Y2 coordinate of the line. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    Public Sub SetLine(ByVal x1 As T, ByVal y1 As T, ByVal x2 As T, ByVal y2 As T)
        Me._SetLine(x1, y1, x2, y2)
    End Sub

    ''' <summary> Returns the default string representation of the line. </summary>
    ''' <returns> The formatted string representation of the line, e.g., '[(x1,y1),(x2,y2)]'. </returns>
    Public Overrides Function ToString() As String
        Return Line(Of T).ToString(Me.X1, Me.Y1, Me.X2, Me.Y2)
    End Function

    ''' <summary> Returns the default string representation of the line. </summary>
    ''' <param name="x1"> Specifies the X1 coordinate of the line. </param>
    ''' <param name="y1"> Specifies the Y1 coordinate of the line. </param>
    ''' <param name="x2"> Specifies the X2 coordinate of the line. </param>
    ''' <param name="y2"> Specifies the Y2 coordinate of the line. </param>
    ''' <returns> The formatted string representation of the line, e.g., '[(x1,y1),(x2,y2)]'. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="y")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    Private Overloads Shared Function ToString(ByVal x1 As T, ByVal y1 As T, ByVal x2 As T, ByVal y2 As T) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "[({0},{1})-({2},{3})]", x1, y1, x2, y2)
    End Function

    ''' <summary> Transposes the (x,y) line to a (y,x) line. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="line"> Specifies the <see cref="Line">Line</see> to transpose. </param>
    ''' <returns> The transposed (y,x) line. </returns>
    Private Shared Function Transpose(ByVal line As Line(Of T)) As Line(Of T)

        If line Is Nothing Then
            Throw New ArgumentNullException(NameOf(line))
        End If
        Return New Line(Of T)(line.Y1, line.X1, line.Y2, line.X2)

    End Function

    ''' <summary> Holds the X1 coordinate of the line. </summary>
    ''' <value> The x coordinate 1. </value>
    Public Property X1() As T

    ''' <summary> Holds the X2 coordinate of the line. </summary>
    ''' <value> The x coordinate 2. </value>
    Public Property X2() As T

    ''' <summary> Holds the Y1 coordinate of the line. </summary>
    ''' <value> The y coordinate 1. </value>
    Public Property Y1() As T

    ''' <summary> Holds the Y2 coordinate of the line. </summary>
    ''' <value> The y coordinate 2. </value>
    Public Property Y2() As T

    ''' <summary> Suspends update of changes. </summary>
    ''' <value> The suspend update. </value>
    Protected Property SuspendUpdate() As Boolean

#End Region

End Class
