﻿Imports System.Threading
Imports System.ComponentModel
''' <summary>  Defines the contract that must be implemented by property publishers. </summary>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/28/12" by="David" revision="1.2.4654"> Documented. </history>
Public MustInherit Class PropertyPublisherBase
    Inherits PublisherBase
    Implements IDisposable, System.ComponentModel.INotifyPropertyChanged

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of the <see cref="PropertyPublisherBase" /> class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

#Region " Disposable Support "

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks> Do not make this method Overridable (virtual) because a derived class should not be
    ''' able to override this method. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> Gets or sets the dispose status sentinel of the base class.  This applies to the
    ''' derived class provided proper implementation. </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks> Executes in two distinct scenarios as determined by its disposing parameter.  If True,
    ''' the method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed. </remarks>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged resources;
    ''' False if this method releases only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                For Each d As [Delegate] In Me.PropertyChangedEvent.SafeInvocationList
                    Try
                        RemoveHandler Me.PropertyChanged, CType(d, System.ComponentModel.PropertyChangedEventHandler)
                    Catch ex As Exception
                        Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                    End Try
                Next
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

#End Region

#Region " PROPERTY CHANGED EVENT IMPLEMENTATION "

    ''' <summary> Gets the sentinel indicating that multiple synchronization contexts are expected. </summary>
    ''' <remarks> When having multiple user interfaces or a thread running within the user interface,
    ''' the current synchronization context may not reflect the contexts of the current UI causing a
    ''' cross thread exceptions. In this case the more complex
    ''' <see cref="SafeInvokePropertyChanged">thread safe methods</see> must be used. </remarks>
    ''' <value> <c>True</c> if more than one synchronization contexts should be expected. </value>
    Public Property MultipleSyncContextsExpected As Boolean

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub InvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            evt?.Invoke(Me, e)
        End If
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokePropertyChanged(ByVal obj As Object)
        Me.InvokePropertyChanged(CType(obj, System.ComponentModel.PropertyChangedEventArgs))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    Protected Sub SafeInvokePropertyChanged(ByVal name As String)
        Me.SafeInvokePropertyChanged(New System.ComponentModel.PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            For Each d As [Delegate] In evt.SafeInvocationList
                If d.Target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If target Is Nothing Then
                        d.DynamicInvoke(New Object() {Me, e})
                    Else
                        target.Invoke(d, New Object() {Me, e})
                    End If
                End If
            Next
        End If
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    Protected Sub SafeBeginInvokePropertyChanged(ByVal name As String)
        Me.SafeBeginInvokePropertyChanged(New System.ComponentModel.PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeBeginInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            For Each d As [Delegate] In evt.SafeInvocationList
                If d.Target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If target Is Nothing Then
                        d.DynamicInvoke(New Object() {Me, e})
                    Else
                        target.BeginInvoke(d, New Object() {Me, e})
                    End If
                End If
            Next
        End If
    End Sub

#End Region

#Region " SYNCHRONOUS NOTIFICATIONS "

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    Protected Sub SyncNotifyPropertyChanged(ByVal name As String)
        If Me.Publishable AndAlso Not String.IsNullOrWhiteSpace(name) Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeInvokePropertyChanged(New System.ComponentModel.PropertyChangedEventArgs(name))
            Else
                SynchronizationContext.Current.Send(New SendOrPostCallback(AddressOf InvokePropertyChanged),
                                                    New System.ComponentModel.PropertyChangedEventArgs(name))
            End If
        End If
    End Sub

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Get value. </summary>
    ''' <remarks> Strips the "get_" prefix derived from using reflection to get the current function
    ''' name from a property Get construct. </remarks>
    ''' <param name="name"> The 'Get' property name. </param>
    Protected Sub SyncNotifyPropertyGetChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SyncNotifyPropertyChanged(name.TrimStart("get_".ToCharArray()))
        End If
    End Sub

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Set value. </summary>
    ''' <remarks> Strips the "set_" prefix derived from using reflection to Set the current function
    ''' name from a property Set construct. </remarks>
    ''' <param name="name"> The 'Set' property name. </param>
    Protected Sub SyncNotifyPropertySetChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SyncNotifyPropertyChanged(name.TrimStart("set_".ToCharArray()))
        End If
    End Sub

#End Region

#Region " ASYNCHRONOUS NOTIFICATIONS "

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
    ''' <param name="name"> The property name. </param>
    Protected Sub AsyncNotifyPropertyChanged(ByVal name As String)
        If Me.Publishable AndAlso Not String.IsNullOrWhiteSpace(name) Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeBeginInvokePropertyChanged(New System.ComponentModel.PropertyChangedEventArgs(name))
            Else
                SynchronizationContext.Current.Post(New SendOrPostCallback(AddressOf InvokePropertyChanged),
                                                              New System.ComponentModel.PropertyChangedEventArgs(name))
            End If
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begin
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Get value. </summary>
    ''' <remarks> Strips the "get_" prefix derived from using reflection to get the current function
    ''' name from a property Get construct. </remarks>
    ''' <param name="name"> The 'Get' property name. </param>
    Protected Sub AsyncNotifyPropertyGetChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.AsyncNotifyPropertyChanged(name.TrimStart("get_".ToCharArray()))
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begin
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Set value. </summary>
    ''' <remarks> Strips the "set_" prefix derived from using reflection to Set the current function
    ''' name from a property Set construct. </remarks>
    ''' <param name="name"> The 'Set' property name. </param>
    Protected Sub AsyncNotifyPropertySetChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.AsyncNotifyPropertyChanged(name.TrimStart("set_".ToCharArray()))
        End If
    End Sub

#End Region

#End Region

#Region " SAFE EVENTS "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Protected Sub SafeInvoke(ByVal handler As EventHandler(Of System.EventArgs))
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, System.EventArgs.Empty})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, System.EventArgs.Empty})
                Else
                    target.Invoke(d, New Object() {Me, System.EventArgs.Empty})
                End If
            End If
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Protected Sub SafeBeginInvoke(ByVal handler As EventHandler(Of System.EventArgs))
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, System.EventArgs.Empty})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, System.EventArgs.Empty})
                Else
                    target.BeginInvoke(d, New Object() {Me, System.EventArgs.Empty})
                End If
            End If
        Next
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    Protected Shared Sub SafeInvoke(ByVal handler As EventHandler(Of System.EventArgs), ByVal sender As Object)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, System.EventArgs.Empty})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, System.EventArgs.Empty})
                Else
                    target.Invoke(d, New Object() {sender, System.EventArgs.Empty})
                End If
            End If
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    Protected Shared Sub SafeBeginInvoke(ByVal handler As EventHandler(Of System.EventArgs), ByVal sender As Object)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, System.EventArgs.Empty})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, System.EventArgs.Empty})
                Else
                    target.BeginInvoke(d, New Object() {sender, System.EventArgs.Empty})
                End If
            End If
        Next
    End Sub

    ''' <summary> Synchronously (Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of TEventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    ''' <param name="e">       The arguments for the event. </param>
    ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
    Protected Shared Sub SafeInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs),
                                                                ByVal sender As Object, ByVal e As TEventArgs)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, e})
            Else
                Dim target As System.ComponentModel.ISynchronizeInvoke = TryCast(d.Target, System.ComponentModel.ISynchronizeInvoke)
                If target IsNot Nothing AndAlso target.InvokeRequired Then
                    ' asynchronously executes the delegate on the target thread.
                    target.BeginInvoke(handler, New Object() {sender, e})
                Else
                    d.DynamicInvoke(New Object() {sender, e})
                End If
            End If
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of TEventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    ''' <param name="e">       The arguments for the event. </param>
    ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
    Protected Shared Sub SafeBeginInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs),
                                                                     ByVal sender As Object, ByVal e As TEventArgs)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, e})
            Else
                Dim target As System.ComponentModel.ISynchronizeInvoke = TryCast(d.Target, System.ComponentModel.ISynchronizeInvoke)
                If target IsNot Nothing AndAlso target.InvokeRequired Then
                    ' asynchronously executes the delegate on the target thread.
                    target.BeginInvoke(handler, New Object() {sender, e})
                Else
                    d.DynamicInvoke(New Object() {sender, e})
                End If
            End If
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of TEventArgs)">Event</see> and waits for its completion. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    ''' <param name="e">       The arguments for the event. </param>
    ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
    Protected Shared Sub SafeBeginEndInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs),
                                                                        ByVal sender As Object, ByVal e As TEventArgs)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, e})
            Else
                Dim target As System.ComponentModel.ISynchronizeInvoke = TryCast(d.Target, System.ComponentModel.ISynchronizeInvoke)
                If target IsNot Nothing AndAlso target.InvokeRequired Then
                    ' asynchronously executes the delegate on the target thread.
                    Dim result As IAsyncResult = target.BeginInvoke(handler, New Object() {sender, e})
                    If result IsNot Nothing Then
                        ' waits until the process ends.
                        target.EndInvoke(result)
                    End If
                Else
                    d.DynamicInvoke(New Object() {sender, e})
                End If
            End If
        Next
    End Sub

#End Region

End Class

