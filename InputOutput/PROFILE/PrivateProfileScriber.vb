''' <summary> Reads and writes private INI Settings (INI) files. </summary>
''' <remarks> Use this class to read and write Settings from a private INI file. <para>
''' INI Settings (INI) files typically include information about your application in strings that
''' are called Profile Strings. Such information is used most often to determine how programs are
''' run by setting specific properties in these programs with information from the INI Settings
''' (INI) file.  Thus determine the profile of the programs. </para><para>
''' With the ISR INI settings Scriber Class you can define the INI Settings (INI) file name and
''' write and read from the INI Settings file information of any variable type.  
''' In addition, you can write or read lists of data.</para> </remarks>
''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/12/96" by="David" revision="1.0.0001.x"> Add Boolean type. </history>
''' <history date="04/27/97" by="David" revision="1.0.0002.x"> Add object input and output. </history>
''' <history date="12/31/98" by="David" revision="1.0.0003.x"> Convert from ISR INI class
''' and remove object and collection methods. </history>
''' <history date="06/06/99" by="David" revision="1.1.0000.x"> Update error handling. </history>
''' <history date="09/27/99" by="David" revision="1.2.0000.x"> Remove reference to the
''' application object. </history>
''' <history date="08/18/01" by="David" revision="1.2.0000.x"> New notation. </history>
''' <history date="09/02/03" by="David" revision="2.0.1340.x"> adapt for .Net. </history>
''' <history date="09/11/03" by="David" revision="2.0.1349.x"> rename IniSettings. </history>
''' <history date="03/01/04" by="David" revision="2.0.1521.x"> rename Scriber. </history>
''' <history date="08/23/96" by="David" revision="1.0.0000.x"> Created. </history>
Public Class PrivateProfileScriber
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of the <see cref="PrivateProfileScriber" /> class. </summary>
    Public Sub New()
        Me.New(System.Windows.Forms.Application.ExecutablePath & PrivateProfileScriber.DefaultExtension)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="PrivateProfileScriber" /> class. </summary>
    ''' <param name="filePath"> The file path. This also serves as the instance name. </param>
    Public Sub New(ByVal filePath As String)
        MyBase.New()
        Me._FilePath = filePath
        Me._DefaultFileName = System.Windows.Forms.Application.ExecutablePath & PrivateProfileScriber.DefaultExtension
        Me._SettingType = System.Type.GetType("System.Int32")
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks> Do not make this method Overridable (virtual) because a derived class should not be
    ''' able to override this method. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources when called from the
    '''                          runtime finalize. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me._FilePath = ""
                Me._SectionName = ""
                Me._SettingName = ""
                Me._settingValue = Nothing
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary> This destructor will run only if the Dispose method does not get called. It gives the
    ''' base class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class. </summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " BASE METHODS AND PROPERTIES "

    ''' <summary> Gets or sets the full name of the file. </summary>
    ''' <value> The name of the file. </value>
    Public Property FilePath() As String

    ''' <summary> Validates the name of the file. </summary>
    ''' <param name="filePath"> The file path. </param>
    ''' <returns> <c>True</c> if file name is valid, <c>False</c> otherwise. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Shared Function ValidateFileName(ByVal filePath As String) As Boolean
        Try
            ' Check that the file name is legal.
            System.IO.File.Exists(filePath)
            Return True
        Catch
            Return False
        End Try
    End Function

    ''' <summary> Gets or sets the default name of the file. </summary>
    ''' <value> The default name of the file. </value>
    Public Property DefaultFileName As String

#End Region

#Region " SHARED "

    ''' <summary> Holds the default extension of an INI file. </summary>
    Public Const DefaultExtension As String = ProfileScriber.DefaultExtension

#End Region

#Region " METHODS "

    ''' <summary> Reads a list of INI Settings from a INI Settings file. </summary>
    ''' <remarks> The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
    ''' ....).<para>
    ''' Reads all the INI Settings in the list in sequence until no more INI Settings are
    ''' found.</para> <para>
    ''' The list is dimensioned starting with First Item, which defaults to 1.  For example, the
    ''' first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
    ''' -1 to read the list ordered as -1,0,1,...</para> </remarks>
    ''' <param name="firstItemIndex"> is an Int32 expression that specifies the index of the first list
    ''' item (default is 1) </param>
    ''' <returns> The list. </returns>
    ''' <example> This example writes a list of INI Settings to an INI file and then reads the list
    ''' from the file.
    ''' <code>
    ''' Sub Form_Click
    '''   ' Declare some data storage
    '''   Dim i as Int32
    '''   ' Create an instance of the ISR INI Settings Scriber Class
    '''   Dim ini as isr.Configuration.IniSettings.Scriber
    '''   ini = New isr.Configuration.IniSettings.Scriber
    '''   ' Create data to write to the INI Settings file
    '''   Dim items As System.Collections.Specialized.StringCollection = New System.Collections.Specialized.StringCollection
    '''   For i = 0 to UBound(sngList)
    '''     items.Add(Log(convert.ToSingle(i+1))).ToString)
    '''   Next i
    '''   With ini
    '''     ' Set the INI Settings file name, section name, and
    '''     ' setting name
    '''     .FilePath = .DefaultFilePath()
    '''     .SectionName = "Section1"
    '''     .SettingName = "Setting"
    '''     ' Write the list to the file.
    '''     .WriteList(items)
    '''     ' Read the list from the file
    '''     items = .ReadList()
    '''   End With
    ''' End Sub
    ''' </code>
    ''' To run This example, paste the code fragment into a Windows Form class. Run the program by
    ''' pressing F5, and then click on the form</example>
    ''' <seealso cref="PrivateProfileScriber.FilePath"/>
    Public Function ReadList(ByVal firstItemIndex As Int32) As System.Collections.Specialized.StringCollection

        Dim items As System.Collections.Specialized.StringCollection = New System.Collections.Specialized.StringCollection
        Dim currentItem As Int32 = firstItemIndex
        Dim itemValue As String = ""

        ' Set default to Empty. This ensures
        ' that we end reading when the last item is Empty
        Me.DefaultValue = Nothing

        ' Loop through the list data.
        Do

            ' Read data from the INI Settings file
            itemValue = Convert.ToString(Me.ListSetting(currentItem), Globalization.CultureInfo.CurrentCulture)

            If (itemValue.Length > 0) Then

                ' add item to the list
                items.Add(itemValue)

                ' Increment item count
                currentItem += 1

            End If

        Loop Until itemValue.Length = 0

        ' return the list
        Return items

    End Function

    ''' <summary> Reads a list of INI Settings from a INI Settings file. </summary>
    ''' <remarks> The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
    ''' ....).<para>
    ''' Reads all the INI Settings in the list in sequence until no more INI Settings are
    ''' found.</para> <para>
    ''' The list is dimensioned starting with First Item Index, which defaults to 1.  For example, the
    ''' first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
    ''' -1 to read the list ordered as -1,0,1,...</para> </remarks>
    ''' <returns> The list. </returns>
    ''' <seealso cref="PrivateProfileScriber.ReadList"/>
    Public Function ReadList() As System.Collections.Specialized.StringCollection
        Return Me.ReadList(1)
    End Function

    ''' <summary> Writes a list of INI Settings to a INI Settings file. </summary>
    ''' <remarks> The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
    ''' ....).<para>
    ''' Reads all the INI Settings in the list in sequence until no more INI Settings are
    ''' found.</para> <para>
    ''' The list is dimensioned starting with First Item Index, which defaults to 1.  For example, the
    ''' first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
    ''' -1 to read the list ordered as -1,0,1,...</para> </remarks>
    ''' <param name="valueCollection"> A collection of list values. </param>
    ''' <param name="firstItemIndex">  is an Int32 expression that specifies the index of the first
    ''' list item (default is 1) </param>
    ''' <example cref="PrivateProfileScriber.ReadList"> </example>
    ''' <seealso cref="PrivateProfileScriber.ReadList"/>
    Public Sub WriteList(ByVal valueCollection As System.Collections.Specialized.StringCollection, ByVal firstItemIndex As Int32)

        If valueCollection Is Nothing Then
            Throw New ArgumentNullException("valueCollection")
        End If

        Dim itemIndex As Int32 = firstItemIndex
        Dim itemValue As String

        ' Loop through the list data.
        For Each itemValue In valueCollection
            ' Write the list setting
            Me.ListSetting(itemIndex) = itemValue
            itemIndex += 1
        Next

    End Sub

    ''' <summary> Reads a whole section from the INI file. </summary>
    ''' <remarks> Use This method to read a section from an INI file.  The section is returned in a
    ''' collection of string values where the section values are delimited by the null character
    ''' (ASCII 0) </remarks>
    ''' <returns> A collection of strings. </returns>
    Public Function ReadSection() As System.Collections.Specialized.StringCollection

        Return ProfileScriber.ReadSection(Me.FilePath, Me.SectionName)

    End Function

    ''' <summary> Writes a list of INI Settings to a INI Settings file. </summary>
    ''' <remarks> The format of the list of INI Settings is SettingName# (e.g., Setting1, Setting2,
    ''' ....).<para>
    ''' Reads all the INI Settings in the list in sequence until no more INI Settings are
    ''' found.</para> <para>
    ''' The list is dimensioned starting with First Item, which defaults to 1.  For example, the
    ''' first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
    ''' -1S to read the list ordered as -1,0,1,...</para> </remarks>
    ''' <param name="valueCollection"> A collection of list values. </param>
    ''' <example cref="PrivateProfileScriber.ReadList"> </example>
    ''' <seealso cref="PrivateProfileScriber.ReadList"/>
    Public Sub WriteList(ByVal valueCollection As System.Collections.Specialized.StringCollection)
        WriteList(valueCollection, 1)
    End Sub

    ''' <summary> Writes a section to the INI file. </summary>
    ''' <remarks> Use This method to write a section to an INI file. </remarks>
    ''' <param name="items"> A <see cref="System.String">String</see> Collection expression that
    ''' specifies the collection holding the section data. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function WriteSection(ByVal items As System.Collections.Specialized.StringCollection) As Boolean
        Return ProfileScriber.WriteSection(Me.FilePath, Me.SectionName, items)
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets the default value that is set when reading INI Settings from the INI Settings
    ''' file. The default value is read if the setting is Empty. </summary>
    ''' <remarks> Use This property to set or get the default data type and value of the INI settings. </remarks>
    ''' <value> <c>DefaultValue</c> is an Object property that can be read from or written to (read
    ''' or write) that specifies the default value and type of the setting in the INI Settings file. </value>
    ''' <seealso cref="PrivateProfileScriber.FilePath"/>
    Public Property DefaultValue() As Object

    ''' <summary> Gets or sets the name of the setting. </summary>
    ''' <remarks> Use This property to set or get the name of the INI Settings string. </remarks>
    ''' <value> <c>SettingsName</c> is a String property that can be read from or written to (read or
    ''' write) that specifies the name of the INI Settings string in the INI Settings file. </value>
    ''' <seealso cref="PrivateProfileScriber.FilePath"/>
    ''' <example> This example sets a INI Settings file name, writes INI Settings strings to the
    ''' file, and then reads the information from back.
    ''' <code>
    ''' Sub Form_Click
    '''   ' Declare some variables.
    '''   Dim SettingValue As Variant
    '''   ' instantiate the ISR INI Settings Scriber Class
    '''   Dim ini as isr.Configuration.IniSettings.Scriber
    '''   ini = New isr.Configuration.IniSettings.Scriber
    '''   ' Set to trap any errors
    '''   Try
    '''     ' Set the INI Settings file name
    '''     ini.FilePath = ini.DefaultFilePath()
    '''     ' Set the INI Settings Section
    '''     ini.SectionName = "Section1"
    '''     ' Write a string to the file String setting
    '''     ini.SettingName = "String"
    '''     ini.SettingValue = "A String"
    '''     ' Write an Int32 value
    '''     ini.SettingName = "Int32"
    '''     ini.SettingValue = Convert.ToInt32(0)
    '''     ' Write an Double value
    '''     ini.SettingName = "Double"
    '''     ini.settingFormat = "##0.000"
    '''     ini.SettingValue = convert.ToDouble(1.11)
    '''     ' Write two list items
    '''     ini.SettingName = "List"
    '''     ini.ListSetting(1) = "One"
    '''     ini.ListSetting(2) = "Two"
    '''     ' Read the string from the file
    '''     ini.SettingName = "String"
    '''     Me.Print "String: "; ini.SettingValue
    '''     ' Read an Int32 value
    '''     ini.SettingName = "Int32"
    '''     ini.DefaultValue= Convert.ToInt32(0)
    '''     Me.Print "Int32: "; ini.SettingValue
    '''     ' Read a double
    '''     ini.SettingName = "Double"
    '''     Me.Print "Double: "; ini.SettingValue
    '''     ' Read two list items
    '''     ini.SettingName = "List"
    '''     Me.Print ini.ListSetting(1)
    '''     Me.Print ini.ListSetting(2)
    '''     ' Read the entire section
    '''     Me.Print "Section: "; ini.sectionValue
    '''   Catch e As Exception
    '''     ' respond to any file name errors.
    '''     MsgBox e.ToString
    '''   End Try
    ''' End Sub  </code>
    ''' To run This example, paste the code fragment into a Windows Form class.
    ''' Run the program by pressing F5, and then click on the form. </example>
    Public Property SettingName() As String

    ''' <summary> Gets the name of the section. </summary>
    ''' <remarks> Use This property to set or get the name of the INI Settings section. </remarks>
    ''' <value> <c>SectionName</c> is a String property that can be read from or written to (read or
    ''' write) that specifies the name of the section in the INI Settings file. </value>
    ''' <seealso cref="PrivateProfileScriber.SettingName"/>
    Public Property SectionName() As String

    ''' <summary> Gets the section collection. </summary>
    ''' <remarks> Use This property to read or write an entire section to the INI Settings file.  When
    ''' writing, This property replaces the entire section erasing any information that was present
    ''' in the section and replacing it with new information.  Treat This property tenderly! <para>
    ''' When reading a section, returns a string with all the INI Settings in the section.</para> </remarks>
    ''' <value> <c>SectionCollection</c> is a String Collection property that can be read from or
    ''' written to (read or write) that specifies the section data. </value>
    ''' <seealso cref="PrivateProfileScriber.SettingName"/>
    Public ReadOnly Property SectionCollection() As System.Collections.Specialized.StringCollection
        Get
            ' If we have a file name, get the collection
            Return ProfileScriber.ReadSection(Me.FilePath, Me.SectionName)
        End Get
    End Property

    Private _settingValue As Object

    ''' <summary> Gets or sets the INI Settings value. </summary>
    ''' <remarks> Use This property to read or write a INI Settings value. The setting value is read
    ''' from the INI Settings file specified in the <see cref="FilePath">File Path</see> property.  The value returned depends on the variable
    ''' type of the <see cref="DefaultValue">Default Value</see> property.  If the <see cref="DefaultValue">Default Value</see> property is Empty or Null, the type
    ''' is set to String. The value that is written to the INI Settings file is determined by the
    ''' settingFormat property. </remarks>
    ''' <value> <c>SettingValue</c> is a String property that can be read from or written to (read or
    ''' write) that specifies the INI Settings value. </value>
    ''' <seealso cref="PrivateProfileScriber.SettingName"/>
    Public Property SettingValue() As Object
        Get
            ' Read the setting.
            If Me.DefaultValue Is Nothing Then
                If Me.SettingType Is Nothing Then
                    Me._settingValue = ProfileScriber.Read(Me.FilePath, Me.SectionName, Me.SettingName, String.Empty)
                Else
                    Me._settingValue = ProfileScriber.Read(Me.FilePath, Me.SectionName, Me.SettingName, Me.SettingType)
                End If
            Else
                Me._settingValue = ProfileScriber.Read(Me.FilePath, Me.SectionName, Me.SettingName, Me.DefaultValue)
            End If
            ' return the value
            Return Me._settingValue
        End Get
        Set(ByVal value As Object)
            ProfileScriber.Write(Me.FilePath, Me.SectionName, Me.SettingName, value)
            ' Set the value of the private data member
            Me._settingValue = value
        End Set
    End Property

    ''' <summary> Gets the INI Settings type. </summary>
    ''' <remarks> Use This property to define the INI Settings type. </remarks>
    ''' <value> <c>SettingType</c> is a Type property that can be read from or written to (read or
    ''' write) that specifies the INI Settings type. </value>
    ''' <seealso cref="PrivateProfileScriber.SettingName"/>
    Public Property SettingType() As Type

    ''' <summary> This private property returns the name of the ini settings list. </summary>
    ''' <remarks> Use This property to get the name of the list setting for reading or write a list
    ''' item. </remarks>
    ''' <value> <c>_listSettingName</c> is a read only string property. </value>
    ''' <param name="itemNumber"> Is an Int32 expression that specifies the item number in the
    ''' list. </param>
    Private ReadOnly Property listSettingName(ByVal itemNumber As Int32) As String
        Get
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0}{1}", Me.SettingName, itemNumber)
        End Get
    End Property

    ''' <summary> Gets or sets the INI Settings list value. </summary>
    ''' <remarks> Use This property to read or write a settings to a section that consists of a serial
    ''' list of INI Settings such as Setting1, Setting2, .... <para>
    ''' The value returned depends on the variable type of the <see cref="DefaultValue">Default Value</see> property.  If the
    ''' <see cref="DefaultValue">Default Value</see> property is Empty or Null, the type is set to String. </para> <para>
    ''' If the setting number does not exist, the property value is set to the Default Value.
    ''' </para> <para>
    ''' The format of the setting value, for data types of single, double, date, or currency setting,
    ''' is set by the settingFormat property. </para> </remarks>
    ''' <value> <c>ListSetting</c> is a String property that can be read from or written to (read or
    ''' write) that specifies the INI Settings value in a setting list. </value>
    ''' <seealso cref="PrivateProfileScriber.SettingName"/>
    ''' <param name="itemNumber"> is an Int32 expression that specifies the serial number of the
    ''' setting in the list. </param>
    Public Property ListSetting(ByVal itemNumber As Int32) As Object
        Get
            If DefaultValue Is Nothing Then
                Me._settingValue = ProfileScriber.Read(Me.FilePath, Me.SectionName, Me.listSettingName(itemNumber), Me.DefaultValue)
            Else
                Me._settingValue = ProfileScriber.Read(Me.FilePath, Me.SectionName, Me.listSettingName(itemNumber), Me.SettingType)
            End If
            Return Me._settingValue
        End Get
        Set(ByVal value As Object)
            ' use the private writeSetting to write the setting to the INI Settings file.
            ProfileScriber.Write(Me.FilePath, Me.SectionName, Me.listSettingName(itemNumber), value)
            ' Set the private data member value
            Me._settingValue = value
        End Set
    End Property

#End Region

End Class

