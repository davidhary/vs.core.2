Imports System.Runtime.InteropServices
''' <summary> Gets or sets safe application programming interface calls. This class suppresses
''' stack walks for unmanaged code permission.  This class is for methods that are safe for
''' anyone to call. Callers of these methods are not required to do a full security review to
''' ensure that the usage is secure because the methods are harmless for any caller. </summary>
''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/07/05" by="David" revision="1.0.1832.x"> Created. </history>
<AttributeUsage(AttributeTargets.Class Or AttributeTargets.Method Or AttributeTargets.Interface)>
Public NotInheritable Class SafeNativeMethods

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Prevents construction of this class. </summary>
    Private Sub New()
    End Sub

#End Region

#Region " IMPORTS "

    ''' <summary> Gets profile section. </summary>
    ''' <param name="sectionName">  Name of the section. </param>
    ''' <param name="returnedData"> Information describing the returned. </param>
    ''' <param name="dataLength">   Length of the data. </param>
    ''' <returns> The profile section. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="GetProfileSectionA", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetProfileSection(ByVal sectionName As String, ByVal returnedData As Byte(), ByVal dataLength As Int32) As Int32
    End Function

    ''' <summary> Gets private profile section. </summary>
    ''' <param name="sectionName">  Name of the section. </param>
    ''' <param name="returnedData"> Information describing the returned. </param>
    ''' <param name="dataLength">   Length of the data. </param>
    ''' <param name="filePath">     Full pathname of the file. </param>
    ''' <returns> The private profile section. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="GetPrivateProfileSectionA", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetPrivateProfileSection(ByVal sectionName As String, ByVal returnedData As Byte(),
                                                    ByVal dataLength As Int32, ByVal filePath As String) As Int32
    End Function

    ''' <summary> Gets private profile section names. </summary>
    ''' <param name="returnedData"> Information describing the returned. </param>
    ''' <param name="dataLength">   Length of the data. </param>
    ''' <param name="filePath">     Full pathname of the file. </param>
    ''' <returns> The private profile section names. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="GetPrivateProfileSectionNamesA", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetPrivateProfileSectionNames(ByVal returnedData As Byte(), ByVal dataLength As Int32,
                                                         ByVal filePath As String) As Int32
    End Function

    ''' <summary> Gets profile string. </summary>
    ''' <param name="sectionName">  Name of the section. </param>
    ''' <param name="keyName">      Name of the key. </param>
    ''' <param name="defaultData">  The default data. </param>
    ''' <param name="returnedData"> Information describing the returned. </param>
    ''' <param name="dataLength">   Length of the data. </param>
    ''' <returns> The profile string. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    <DllImport("KERNEL32.DLL", EntryPoint:="GetProfileStringA", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetProfileString(ByVal sectionName As String, ByVal keyName As String,
                                            ByVal defaultData As String, ByVal returnedData As String,
                                            ByVal dataLength As Int32) As Int32
    End Function

    ''' <summary> Gets profile string. </summary>
    ''' <param name="sectionName">  Name of the section. </param>
    ''' <param name="keyName">      Name of the key. </param>
    ''' <param name="defaultData">  The default data. </param>
    ''' <param name="returnedData"> Information describing the returned. </param>
    ''' <param name="dataLength">   Length of the data. </param>
    ''' <returns> The profile string. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="GetProfileStringA", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetProfileString(ByVal sectionName As String, ByVal keyName As String,
                                            ByVal defaultData As String, ByVal returnedData As System.Text.StringBuilder,
                                            ByVal dataLength As Int32) As Int32
    End Function

    ''' <summary> Gets private profile string. </summary>
    ''' <param name="sectionName">  Name of the section. </param>
    ''' <param name="keyName">      Name of the key. </param>
    ''' <param name="defaultData">  The default data. </param>
    ''' <param name="returnedData"> Information describing the returned. </param>
    ''' <param name="dataLength">   Length of the data. </param>
    ''' <param name="filePath">     Full pathname of the file. </param>
    ''' <returns> The private profile string. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    <DllImport("KERNEL32.DLL", EntryPoint:="GetPrivateProfileStringA", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetPrivateProfileString(ByVal sectionName As String, ByVal keyName As String,
                                                   ByVal defaultData As String, ByVal returnedData As String,
                                                   ByVal dataLength As Int32, ByVal filePath As String) As Int32
    End Function

    ''' <summary> Gets private profile string. </summary>
    ''' <param name="sectionName">  Name of the section. </param>
    ''' <param name="keyName">      Name of the key. </param>
    ''' <param name="defaultData">  The default data. </param>
    ''' <param name="returnedData"> Information describing the returned. </param>
    ''' <param name="dataLength">   Length of the data. </param>
    ''' <param name="filePath">     Full pathname of the file. </param>
    ''' <returns> The private profile string. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="GetPrivateProfileStringA", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetPrivateProfileString(ByVal sectionName As String, ByVal keyName As String,
                                                   ByVal defaultData As String, ByVal returnedData As System.Text.StringBuilder,
                                                   ByVal dataLength As Int32, ByVal filePath As String) As Int32
    End Function

    ''' <summary> Gets private profile integer. </summary>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="keyName">     Name of the key. </param>
    ''' <param name="defaultData"> The default data. </param>
    ''' <param name="filePath">    Full pathname of the file. </param>
    ''' <returns> The private profile Integer. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="GetPrivateProfileInt", CharSet:=CharSet.Unicode)>
    Friend Shared Function GetPrivateProfile(ByVal sectionName As String, ByVal keyName As String,
                                             ByVal defaultData As Int32, ByVal filePath As String) As Int32
    End Function

    ''' <summary> Writes a private profile section. 
    '''           Writes a list of all names and values for a section of a private profile (.ini) file. </summary>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="data">        The data. </param>
    ''' <param name="filePath">    Full pathname of the file. </param>
    ''' <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="WritePrivateProfileSectionA", CharSet:=CharSet.Unicode)>
    Friend Shared Function WritePrivateProfileSection(ByVal sectionName As String, ByVal data As String,
                                                      ByVal filePath As String) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    ''' <summary> Writes a private profile section. </summary>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="data">        The data. </param>
    ''' <param name="filePath">    Full pathname of the file. </param>
    ''' <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="WritePrivateProfileSectionA", CharSet:=CharSet.Unicode)>
    Friend Shared Function WritePrivateProfileSection(ByVal sectionName As String, ByVal data As Byte(),
                                                      ByVal filePath As String) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    ''' <summary> Writes a private profile string. </summary>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="keyName">     Name of the key. </param>
    ''' <param name="data">        The data. </param>
    ''' <param name="filePath">    Full pathname of the file. </param>
    ''' <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="WritePrivateProfileStringA", CharSet:=CharSet.Unicode)>
    Friend Shared Function WritePrivateProfileString(ByVal sectionName As String, ByVal keyName As String,
                                                     ByVal data As String, ByVal filePath As String) As <MarshalAs(UnmanagedType.Bool)> Boolean
        ' Leave function empty - Import attribute forwards calls to KERNEL32.DLL.
    End Function

    ''' <summary> Writes a private profile string. </summary>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="data">        The data. </param>
    ''' <param name="filePath">    Full pathname of the file. </param>
    ''' <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    <DllImport("KERNEL32.DLL", EntryPoint:="WritePrivateProfileStringA",
       SetLastError:=True, CharSet:=CharSet.Unicode,
       ExactSpelling:=True,
       CallingConvention:=CallingConvention.StdCall)>
    Friend Shared Function writePrivateProfileString(ByVal sectionName As String, ByVal data As Byte(),
                                                     ByVal filePath As String) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    ''' <summary> Writes a profile section. 
    '''           Writes a list of all names and values for a section of the WIN.INI file. </summary>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="data">        The data. </param>
    ''' <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="WriteProfileSectionA", SetLastError:=True, CharSet:=CharSet.Unicode, ExactSpelling:=True,
        CallingConvention:=CallingConvention.StdCall)>
    Friend Shared Function WriteProfileSection(ByVal sectionName As String, ByVal data As Byte()) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    ''' <summary> Writes a profile section. </summary>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="data">        The data. </param>
    ''' <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="WriteProfileSectionA", CharSet:=CharSet.Unicode)>
    Friend Shared Function WriteProfileSection(ByVal sectionName As String, ByVal data As String) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    ''' <summary> Writes a profile string. 
    '''           Writes a string to the WIN.INI file. </summary>
    ''' <param name="sectionName"> Name of the section. </param>
    ''' <param name="keyName">     Name of the key. </param>
    ''' <param name="data">        The data. </param>
    ''' <returns> <c>True</c>  if okay; Otherwise, <c>False</c>. </returns>
    <DllImport("KERNEL32.DLL", EntryPoint:="WriteProfileStringA", CharSet:=CharSet.Unicode)>
    Friend Shared Function WriteProfileString(ByVal sectionName As String, ByVal keyName As String,
                                              ByVal data As String) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

#End Region

End Class

