﻿Imports System.Runtime.CompilerServices
Namespace BinaryWriterExtensions
    ''' <summary> Includes extensions for <see cref="System.IO.BinaryReader">binary reader</see>. </summary>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="01/23/2014" by="David" revision="2.0.5136.x"> Created based on I/O Library binary reader. </history>
    Public Module Methods

        ''' <summary> Closes the binary Writer and base stream. </summary>
        ''' <remarks> Use this method to close the instance. </remarks>
        <Extension()>
        Public Sub CloseWriter(ByVal writer As System.IO.BinaryWriter)
            If writer IsNot Nothing Then
                writer.Close()
                If writer IsNot Nothing AndAlso writer.BaseStream IsNot Nothing Then
                    writer.BaseStream.Close()
                End If
            End If
        End Sub

        ''' <summary> Opens a stream. </summary>
        ''' <param name="filePathName"> Specifies the name of the binary file which to read. </param>
        ''' <returns> A System.IO.FileStream. </returns>
        Private Function OpenStream(ByVal filePathName As String) As System.IO.FileStream
            Dim fileStream As System.IO.FileStream = Nothing
            Dim tempFileStream As System.IO.FileStream = Nothing
            If Not String.IsNullOrWhiteSpace(filePathName) Then
                Try
                    tempFileStream = New System.IO.FileStream(filePathName, System.IO.FileMode.Open, System.IO.FileAccess.Write)
                    fileStream = tempFileStream
                Finally
                    If tempFileStream IsNot Nothing Then tempFileStream.Dispose()
                End Try
            End If
            Return fileStream
        End Function

        ''' <summary> Opens a binary file for Writing and returns a reference to the Writer. The file is
        ''' <see cref="IO.FileMode.Open">opened</see> in
        ''' <see cref="IO.FileAccess.Write">Write access</see>. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <exception cref="IO.FileNotFoundException"> Thrown when the requested file is not present. </exception>
        ''' <param name="filePathName"> Specifies the file name. </param>
        ''' <returns> A reference to an open <see cref="IO.BinaryWriter">binary Writer</see>. </returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
        Public Function OpenWriter(ByVal filePathName As String) As System.IO.BinaryWriter

            If String.IsNullOrWhiteSpace(filePathName) Then
                Throw New ArgumentNullException("filePathName")
            End If
            Dim tempWriter As System.IO.BinaryWriter = Nothing
            Dim writer As System.IO.BinaryWriter = Nothing
            Try
                tempWriter = New System.IO.BinaryWriter(OpenStream(filePathName))
                writer = tempWriter
            Finally
                If tempWriter IsNot Nothing Then tempWriter.Dispose()
            End Try
            Return writer

        End Function

#Region " DOUBLE "

        ''' <summary> Writes a single-dimension <see cref="Double">double-precision</see>
        ''' array to the values file. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values"> Holds the values to write. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal values() As Double)

            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If
            If values Is Nothing Then
                Throw New ArgumentNullException("values")
            End If

            ' write the values size for verifying size and, indirectly also location, upon read
            writer.Write(Convert.ToInt32(values.Length))

            ' write values to the file
            If values.Count > 0 Then
                For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                    writer.Write(values(i))
                Next i
            End If

        End Sub

        ''' <summary> Writes a double-precision value to the values file. </summary>
        ''' <param name="value">    Specifies the value to write. </param>
        ''' <param name="location"> Specifies the file location. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As Double, ByVal location As Int64)

            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If
            writer.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            writer.Write(value)

        End Sub

#End Region

#Region " INT32 "

        ''' <summary> Writes a single-dimension <see cref="Int32">Integer</see>
        ''' array to the values file. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values"> Holds the values to write. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal values() As Int32)

            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If
            If values Is Nothing Then
                Throw New ArgumentNullException("values")
            End If

            ' write the values size for verifying size and, indirectly also location, upon read
            writer.Write(Convert.ToInt32(values.Length))

            ' write values to the file
            If values.Count > 0 Then
                For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                    writer.Write(values(i))
                Next i
            End If

        End Sub

        ''' <summary> Writes an Int32 value to the values file. </summary>
        ''' <param name="value">    Specifies the value to write. </param>
        ''' <param name="location"> Specifies the file location. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As Int32, ByVal location As Int64)

            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If
            writer.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            writer.Write(value)

        End Sub

#End Region

#Region " INT64 "

        ''' <summary> Writes a single-dimension <see cref="Int64">Long Integer</see>
        ''' array to the values file. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values"> Holds the values to write. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal values() As Int64)

            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If
            If values Is Nothing Then
                Throw New ArgumentNullException("values")
            End If

            ' write the values size for verifying size and, indirectly also location, upon read
            writer.Write(Convert.ToInt32(values.Length))

            ' write values to the file
            If values.Count > 0 Then
                For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                    writer.Write(values(i))
                Next i
            End If

        End Sub

        ''' <summary> Writes a Int64 value to the values file. </summary>
        ''' <param name="value">    Specifies the value to write. </param>
        ''' <param name="location"> Specifies the file location. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As Int64, ByVal location As Int64)

            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If
            writer.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            writer.Write(value)

        End Sub

#End Region

#Region " SINGLE "

        ''' <summary> Writes a single-dimension <see cref="Single">single-precision</see>
        ''' array to the values file. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values"> Holds the values to write. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal values() As Single)

            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If
            If values Is Nothing Then
                Throw New ArgumentNullException("values")
            End If

            ' write the values size for verifying size and, indirectly also location, upon read
            writer.Write(Convert.ToInt32(values.Length))

            ' write values to the file
            If values.Count > 0 Then
                For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                    writer.Write(values(i))
                Next i
            End If

        End Sub

        ''' <summary> Writes a single-precision value to the values file. </summary>
        ''' <param name="value">    Specifies the value to write. </param>
        ''' <param name="location"> Specifies the file location. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As Single, ByVal location As Int64)

            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If
            writer.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            writer.Write(value)

        End Sub

#End Region

#Region " STRING "

        ''' <summary> Writes a string to the binary file padding it with spaces as necessary to fill the
        ''' length. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">   Holds the value to write. </param>
        ''' <param name="length"> The length to write. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As String, ByVal length As Int32)

            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If
            If value Is Nothing Then
                Throw New ArgumentNullException("value")
            End If

            ' pad but make sure not to exceed length.
            writer.Write(value.PadRight(length).Substring(0, length))

        End Sub

        ''' <summary> Writes a string value to the values file. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="value">    Specifies the value to write. </param>
        ''' <param name="location"> Specifies the file location. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As String, ByVal location As Int64)

            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If
            If value Is Nothing Then
                Throw New ArgumentNullException("value")
            End If

            writer.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            writer.Write(value)

        End Sub

#End Region

#Region " DATE TIME "

        ''' <summary>
        ''' Writes a <see cref="Datetime">date time</see> value the file.
        ''' </summary>
        ''' <remarks> David, 12/19/2015. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer"> The writer. </param>
        ''' <param name="value">  Specifies the value to write. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As DateTime)
            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If
            writer.Write(BitConverter.GetBytes(value.ToOADate))
        End Sub

        ''' <summary> Writes a <see cref="Datetime">date time</see> value the file. </summary>
        ''' <remarks> David, 12/19/2015. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer">   The writer. </param>
        ''' <param name="value">    Specifies the value to write. </param>
        ''' <param name="location"> Specifies the file location. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As DateTime, ByVal location As Int64)
            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If
            writer.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            writer.Write(value)
        End Sub


#End Region

#Region " TIME SPAN "

        ''' <summary>
        ''' Writes a <see cref="TimeSpan">time span</see> value the file.
        ''' </summary>
        ''' <remarks> David, 12/19/2015. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer"> The writer. </param>
        ''' <param name="value">  Specifies the value to write. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As TimeSpan)
            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If
            writer.Write(value.TotalMilliseconds)
        End Sub

        ''' <summary> Writes a a <see cref="TimeSpan">time span</see> value the file </summary>
        ''' <remarks> David, 12/19/2015. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="writer">   The writer. </param>
        ''' <param name="value">    Specifies the value to write. </param>
        ''' <param name="location"> Specifies the file location. </param>
        <Extension()>
        Public Sub Write(ByVal writer As System.IO.BinaryWriter, ByVal value As TimeSpan, ByVal location As Int64)
            If writer Is Nothing Then
                Throw New ArgumentNullException("writer")
            End If
            writer.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
            writer.Write(value)
        End Sub

#End Region


    End Module
End Namespace

