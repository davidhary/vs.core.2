﻿Namespace My

    Partial Friend Class MyLibrary

        Private Sub New()
            MyBase.New
        End Sub

        Public Const AssemblyTitle As String = "Core Input Output Library"
        Public Const AssemblyDescription As String = "Core Input Output Library"
        Public Const AssemblyProduct As String = "Core.InputOutput.2016"

    End Class

End Namespace
