﻿Imports System.ComponentModel
''' <summary> Information about the resistance. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="5/19/2014" by="David" revision=""> Created. </history>
Public Class ResistanceInfo

#Region " CONSTRUCTORS "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me._ResistanceCode = ""
        Me._ParseDetails = ""
        Me._ParseFailed = False
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="resistanceCode"> The resistance code. </param>
    Public Sub New(ByVal resistanceCode As String)
        Me.New()
        Me._ResistanceCode = resistanceCode
        Me._Parse()
    End Sub

#End Region

    ''' <summary> Gets or sets the parse details. </summary>
    ''' <value> The parse details. </value>
    Public Property ParseDetails As String

    ''' <summary> Gets or sets a value indicating whether the parse failed. </summary>
    ''' <value> <c>true</c> if parse failed; otherwise <c>false</c>. </value>
    Public Property ParseFailed As Boolean

    ''' <summary> Gets or sets the nominal resistance. </summary>
    ''' <value> The nominal resistance. </value>
    Public Property NominalResistance As Double

    ''' <summary> Gets or sets the resistance code. </summary>
    ''' <value> The resistance code. </value>
    Public Property ResistanceCode As String

    Public Shared Function IsValid(ByVal resistanceCode As String) As Boolean
        Dim details As String = ""
        Dim resistance As Double = 0
        Return TryParse(resistanceCode, resistance, details)
    End Function

    ''' <summary> Parses the <see cref="ResistanceCode">resistance code</see>. </summary>
    Private Sub _Parse()
        If String.IsNullOrWhiteSpace(Me.ResistanceCode) Then
            Me._ParseDetails = "Resistance code is empty."
            Me._ParseFailed = True
        Else
            Me._ParseFailed = Not ResistanceInfo.TryParse(Me._ResistanceCode, Me._NominalResistance, Me._ParseDetails)
        End If
    End Sub

    ''' <summary> Parses the <see cref="ResistanceCode">resistance code</see>. </summary>
    Public Sub Parse()
        Me._Parse()
    End Sub

    ''' <summary> Parses the <see cref="ResistanceCode">resistance code</see>. </summary>
    ''' <param name="resistanceCode"> The resistance code. </param>
    ''' <returns> A Double. </returns>
    Public Shared Function Parse(ByVal resistanceCode As String) As Double
        Dim resistance As Double = 0
        Dim details As String = ""
        TryParse(resistanceCode, resistance, details)
        Return resistance
    End Function

    ''' <summary> Tries to parse the resistance code. </summary>
    ''' <param name="resistanceCode"> The resistance code. </param>
    ''' <param name="resistance">     [in,out] The resistance. </param>
    ''' <param name="details">        [in,out] The details. </param>
    ''' <returns> <c>true</c> if resistance code can be parsed. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#")>
    Public Shared Function TryParse(ByVal resistanceCode As String, ByRef resistance As Double, ByRef details As String) As Boolean
        Dim affirmative As Boolean
        If String.IsNullOrWhiteSpace(resistanceCode) Then
            details = "Resistance code is empty."
        Else
            Dim scaleFactors As Double() = New Double() {1, 1, 1000, 1000000, 1000000000}
            Dim scaleCodes As String() = New String() {".", "R", "K", "M", "G"}
            Dim scaleCode As String = ""
            Dim scaleFactor As Double = 0
            Dim unscaledValue As String = ""

            ' lookup the scale code:
            For i As Integer = 0 To scaleCodes.Length - 1
                scaleCode = scaleCodes(i)
                If resistanceCode.IndexOf(scaleCode, StringComparison.OrdinalIgnoreCase) >= 0 Then
                    scaleFactor = scaleFactors(i)
                    Exit For
                End If
            Next

            If scaleFactor > 0 Then
                ' found scale code in resistance code, get the un-scaled value
                unscaledValue = resistanceCode.Replace(scaleCode, ".")
                ' remove trailing periods to use cases such as 100.0R
                If unscaledValue.EndsWith(".", StringComparison.OrdinalIgnoreCase) Then unscaledValue = unscaledValue.Substring(0, unscaledValue.Length - 1)
            ElseIf resistanceCode.Length >= 3 Then
                ' no scale code in resistance code; check if the resistance code has at least 4 elements.
                ' then the scale power is the last digit.
                Dim scaler As Integer = 0
                If Integer.TryParse(resistanceCode.Substring(resistanceCode.Length - 1), scaler) Then
                    scaleFactor = Math.Pow(10, scaler)
                    unscaledValue = resistanceCode.Substring(0, resistanceCode.Length - 1)
                End If
            End If

            If String.IsNullOrWhiteSpace(unscaledValue) Then
                details = String.Format("Unknown resistance code '{0}'", resistanceCode)
            ElseIf Double.TryParse(unscaledValue, resistance) Then
                resistance = resistance * scaleFactor
                affirmative = True
                details = ""
            Else
                details = String.Format("Failed converting '{0}' created from '{1}' to nominal resistance", unscaledValue, resistanceCode)
            End If
        End If
        Return affirmative
    End Function

End Class
