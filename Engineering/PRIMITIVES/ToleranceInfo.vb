﻿''' <summary> A tolerance acceptance interval. </summary>
''' <remarks> This class is sealed to ensure that the hash value of its elements is not used
'''           by two instances with different hash value set. </remarks>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="5/19/2014" by="David" revision=""> Created. </history>
Public NotInheritable Class ToleranceInfo
    Inherits AcceptanceInterval

    ''' <summary> Constructor. </summary>
    ''' <param name="code"> The tolerance code. </param>
    Public Sub New(ByVal code As String)
        MyBase.New()
        If String.IsNullOrWhiteSpace(code) Then
            Me.Parsed = False
        Else
            Dim info As ToleranceInfo = ToleranceInfo.Parse(code)
            Me.Parsed = info IsNot Nothing
            If Me.Parsed Then
                Me.Code = code
                Me.RelativeInterval = New ToleranceInterval(info.RelativeInterval)
                Me.Caption = info.Caption
                Me.CompoundCaption = AcceptanceInterval.BuildCompoundCaptionFormat(code, Caption)
            End If
        End If
    End Sub

    ''' <summary> Constructor for symmetric interval. </summary>
    ''' <param name="code">      The tolerance code. </param>
    ''' <param name="tolerance"> The tolerance interval. </param>
    ''' <param name="caption">   The caption. </param>
    Public Sub New(ByVal code As String, ByVal tolerance As Interval, ByVal caption As String)
        MyBase.New(code, tolerance, caption)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <param name="value"> The value. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="0")>
    Public Sub New(ByVal value As AcceptanceInterval)
        Me.New(value.Code, value.RelativeInterval, value.Caption)
    End Sub

    ''' <summary> Constructor for symmetric interval and standard caption. </summary>
    ''' <param name="code">      The tolerance code. </param>
    ''' <param name="tolerance"> The tolerance. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="1")>
    Public Sub New(ByVal code As String, ByVal tolerance As Interval)
        Me.New(code, tolerance, ToleranceInfo.BuildCaption(tolerance.HighEndPoint))
    End Sub

    ''' <summary> Constructor for symmetric interval and standard caption. </summary>
    ''' <param name="code">      The tolerance code. </param>
    ''' <param name="tolerance"> The tolerance. </param>
    Public Sub New(ByVal code As String, ByVal tolerance As Double)
        Me.New(code, tolerance, ToleranceInfo.BuildCaption(tolerance))
    End Sub

    ''' <summary> Constructor for symmetric interval. </summary>
    ''' <param name="code">      The tolerance code. </param>
    ''' <param name="tolerance"> The tolerance. </param>
    ''' <param name="caption">   The caption. </param>
    Public Sub New(ByVal code As String, ByVal tolerance As Double, ByVal caption As String)
        Me.New(code, New ToleranceInterval(tolerance), caption)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="lowerTolerance"> The lower tolerance. </param>
    ''' <param name="upperTolerance"> The upper tolerance. </param>
    ''' <param name="caption">        The caption. </param>
    Public Sub New(ByVal lowerTolerance As Double, ByVal upperTolerance As Double, ByVal caption As String)
        Me.New(ToleranceInfo.UserCode, New ToleranceInterval(lowerTolerance, upperTolerance), caption)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="code">  The tolerance code. </param>
    ''' <param name="lowerTolerance"> The lower tolerance. </param>
    ''' <param name="upperTolerance"> The upper tolerance. </param>
    ''' <param name="caption">        The caption. </param>
    Public Sub New(ByVal code As String, ByVal lowerTolerance As Double, ByVal upperTolerance As Double, ByVal caption As String)
        Me.New(code, New ToleranceInterval(lowerTolerance, upperTolerance), caption)
    End Sub

    ''' <summary> The clone Constructor. </summary>
    ''' <param name="value"> The value. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="0")>
    Public Sub New(ByVal value As ToleranceInfo)
        Me.New(value.Code, value.RelativeInterval, value.Caption)
    End Sub

    ''' <summary> Gets the empty value. </summary>
    ''' <value> The empty. </value>
    Public Shared ReadOnly Property Empty As ToleranceInfo
        Get
            Return New ToleranceInfo(ToleranceInfo.EmptyCode, 0)
        End Get
    End Property

    ''' <summary> Gets the caption format. </summary>
    ''' <value> The caption format. </value>
    Public Shared Property CaptionFormat As String = "±{0}%"

    ''' <summary> Builds a caption. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String value using the 
    ''' <see cref="BsTcrInfo.CaptionFormat">caption format</see>. </returns>
    Public Shared Function BuildCaption(ByVal value As Double) As String
        Dim scaleFactor As Double = 1
        If ToleranceInfo.CaptionFormat.Contains("%}") Then
            scaleFactor = 1
        ElseIf ToleranceInfo.CaptionFormat.Contains("%") Then
            scaleFactor = 100
        End If
        Return BSTcrInfo.BuildCaption(value, scaleFactor, CaptionFormat)
    End Function

    ''' <summary> Builds a caption. </summary>
    ''' <param name="value">       The value. </param>
    ''' <param name="scaleFactor"> The scale factor. </param>
    ''' <param name="format">      Describes the format to use. </param>
    ''' <returns> A String value using the
    ''' <see cref="BsTcrInfo.CaptionFormat">caption format</see>. </returns>
    Public Shared Function BuildCaption(ByVal value As Double, ByVal scaleFactor As Double, ByVal format As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, format, scaleFactor * value)
    End Function

    ''' <summary> The user tolerance value. </summary>
    Public Shared Property UserValue As ToleranceInfo = New ToleranceInfo("@", 0.99)

    ''' <summary> The unknown tolerance value. </summary>
    Public Shared Property UnknownValue As ToleranceInfo = New ToleranceInfo("Z", 0.99)

    ''' <summary> Gets the unknown code. </summary>
    ''' <value> The unknown code. </value>
    Public Overloads Shared Property UnknownCode As String
        Get
            Return UnknownValue.Code
        End Get
        Set(value As String)
            ToleranceInfo.UnknownValue = New ToleranceInfo(value, ToleranceInfo.UnknownValue.RelativeInterval)
        End Set
    End Property

    ''' <summary> Gets the user code. </summary>
    ''' <value> The user code. </value>
    Public Overloads Shared Property UserCode As String
        Get
            Return UserValue.Code
        End Get
        Set(value As String)
            ToleranceInfo.UserValue = New ToleranceInfo(value, ToleranceInfo.UserValue.RelativeInterval)
        End Set
    End Property

    Private Shared _Dictionary As AcceptanceIntervalCollection
    ''' <summary> Gets the <see cref="AcceptanceIntervalCollection">dictionary</see>. </summary>
    ''' <returns> A Dictionary(Of String, CodedIntervalBase). </returns>
    Public Shared Function Dictionary() As AcceptanceIntervalCollection
        If ToleranceInfo._Dictionary Is Nothing OrElse ToleranceInfo._Dictionary.Count = 0 Then
            ToleranceInfo.BuildDictionary()
        End If
        Return ToleranceInfo._Dictionary
    End Function

    ''' <summary> Builds coded interval base dictionary. </summary>
    Public Shared Sub BuildDictionary(ByVal values As AcceptanceIntervalCollection)
        ToleranceInfo._Dictionary = New AcceptanceIntervalCollection
        ToleranceInfo._Dictionary.Populate(values)
    End Sub

    ''' <summary> Builds tolerance information dictionary. </summary>
    Private Shared Sub BuildDictionary()
        Dim dix As New AcceptanceIntervalCollection
        dix.Add(New ToleranceInfo("A", 0.0005))
        dix.Add(New ToleranceInfo("B", 0.001))
        dix.Add(New ToleranceInfo("C", 0.0025))
        dix.Add(New ToleranceInfo("D", 0.005))
        dix.Add(New ToleranceInfo("F", 0.01))
        dix.Add(New ToleranceInfo("G", 0.02))
        dix.Add(New ToleranceInfo("H", 0.03))
        dix.Add(New ToleranceInfo("J", 0.05))
        dix.Add(New ToleranceInfo("K", 0.1))
        dix.Add(New ToleranceInfo("M", 0.2))
        dix.Add(New ToleranceInfo("P", 0.0015))
        dix.Add(New ToleranceInfo("Q", 0.0002))
        dix.Add(New ToleranceInfo("R", 0.002))
        dix.Add(New ToleranceInfo("S", 0.00025))
        dix.Add(New ToleranceInfo("T", 0.0001))
        dix.Add(New ToleranceInfo("U", 0.0003))
        dix.Add(New ToleranceInfo("V", 0.00005))
        dix.Add(New ToleranceInfo("Y", 0.00015))
        dix.Add(ToleranceInfo.UnknownValue)
        dix.Add(ToleranceInfo.UserValue)
        ToleranceInfo.BuildDictionary(dix)
        dix = Nothing
    End Sub

    ''' <summary> Parses. </summary>
    ''' <param name="code"> The code. </param>
    ''' <returns> A ToleranceInfo. </returns>
    Public Shared Function Parse(ByVal code As String) As ToleranceInfo
        With ToleranceInfo.Dictionary
            If .Contains(code) Then
                Return New ToleranceInfo(.Item(code))
            Else
                Return Nothing
            End If
        End With
    End Function

    ''' <summary> Tries to parse a coded value. </summary>
    ''' <param name="code">    The code. </param>
    ''' <param name="value">   [in,out] The Margin Factor. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if Margin Factor code can be parsed. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#")>
    Public Shared Function TryParse(ByVal code As String, ByRef value As ToleranceInfo, ByRef details As String) As Boolean
        Dim affirmative As Boolean = False
        With ToleranceInfo.Dictionary
            If .Contains(code) Then
                value = New ToleranceInfo(.Item(code))
                affirmative = True
            Else
                details = String.Format("MarginFactor code '{0}' is unknown", code)
            End If
        End With
        Return affirmative
    End Function

    ''' <summary> Displays tolerances in the specified 
    '''           <see cref="System.Windows.Forms.ListControl">control</see>. </summary>
    ''' <param name="control"> The list control to bind. </param>
    ''' <returns> The item count. </returns>
    Public Shared Shadows Function ListValues(ByVal control As System.Windows.Forms.ListControl) As Integer
        Return AcceptanceInterval.ListValues(control, ToleranceInfo.Dictionary.CompoundCaptions)
    End Function

    ''' <summary> Gets the sentinel indicating if the entered value is Empty defined. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsEmptyCode As Boolean
        Get
            Return String.Equals(Me.Code, ToleranceInfo.EmptyCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating if the entered code is unknown. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsUnknownCode As Boolean
        Get
            Return String.Equals(Me.Code, ToleranceInfo.UnknownCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating if the entered value is user defined. </summary>
    ''' <value> <c>True</c> if unknown code. </value>
    Public Overrides ReadOnly Property IsUserCode As Boolean
        Get
            Return String.Equals(Me.Code, ToleranceInfo.UserCode, StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

End Class
