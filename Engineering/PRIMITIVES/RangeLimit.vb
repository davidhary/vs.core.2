﻿''' <summary> Range limit. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="11/25/2014" by="David" revision=""> Created. </history>
Public Class RangeLimit

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me._LowerLimit = 0
        Me._UpperLimit = 0
        Me._Epsilon = Single.Epsilon
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="limit"> The limit. </param>
    Public Sub New(ByVal limit As Double)
        Me.New()
        Me.UpperLimit = limit
        Me.LowerLimit = -limit
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="LowerLimit"> The lower limit. </param>
    ''' <param name="upperLimit"> The upper limit. </param>
    Public Sub New(ByVal lowerLimit As Double, ByVal upperLimit As Double)
        Me.New()
        Me._UpperLimit = upperLimit
        Me._LowerLimit = LowerLimit
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As RangeLimit)
        Me.New()
        If value IsNot Nothing Then
            Me._UpperLimit = value.UpperLimit
            Me._LowerLimit = value.LowerLimit
            Me._Epsilon = value.Epsilon
        End If
    End Sub

    ''' <summary> Gets the range. </summary>
    ''' <value> The range. </value>
    Public ReadOnly Property Range As Double
        Get
            Return Me.UpperLimit - Me.LowerLimit
        End Get
    End Property

    ''' <summary> Gets the epsilon. </summary>
    ''' <value> The epsilon. </value>
    Public Property Epsilon As Double

    ''' <summary> Gets a value indicating whether the limit defined. </summary>
    ''' <value> true if this object is limit defined, false if not. </value>
    Public ReadOnly Property IsDefined As Boolean
        Get
            Return Me.Range > Me.Epsilon
        End Get
    End Property

    ''' <summary> Gets or sets the lower limit. </summary>
    ''' <value> The lower limit. </value>
    Public Property LowerLimit As Double

    ''' <summary> Gets or sets the upper limit. </summary>
    ''' <value> The upper limit. </value>
    Public Property UpperLimit As Double

    ''' <summary> In range. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function InRange(ByVal value As Double) As Boolean
        Return Me.IsDefined AndAlso value >= LowerLimit AndAlso value <= UpperLimit
    End Function

    ''' <summary> Check if the value is inside the range not touching the limits. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function Inside(ByVal value As Double) As Boolean
        Return Me.IsDefined AndAlso value > LowerLimit AndAlso value < UpperLimit
    End Function


#Region " EQUALS "

    ''' <summary> Determines whether the specified <see cref="T:System.Object" /> is equal to the
    ''' current <see cref="T:System.Object" />. </summary>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    ''' <see cref="T:System.Object" />. </param>
    ''' <returns> <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>. </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse RangeLimit.Equals(Me, CType(obj, RangeLimit)))
    End Function

    ''' <summary> Compares two lines. The lines are compared using their LowerLimits and UpperLimits. </summary>
    ''' <remarks> The two lines are the same if the have the same minimum and maximum values. </remarks>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal value As Double) As Boolean
            Return Math.Abs(Me.UpperLimit - value) < Me.Epsilon
    End Function

    ''' <summary> Compares two lines. The lines are compared using their LowerLimits and UpperLimits. </summary>
    ''' <remarks> The two lines are the same if the have the same minimum and maximum values. </remarks>
    ''' <param name="other"> Specifies the other <see cref="RangeLimit">Line</see>
    ''' to compare for equality with this instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As RangeLimit) As Boolean
        Return Me.Equals(other, Me.Epsilon)
    End Function

    ''' <summary> Compares two lines. The lines are compared using their LowerLimits and UpperLimits. </summary>
    ''' <remarks> The two lines are the same if the have the same minimum and maximum values. </remarks>
    ''' <param name="other"> Specifies the other <see cref="RangeLimit">Line</see>
    ''' to compare for equality with this instance. </param>
    ''' <param name="tolerance">     Specifies the relative tolerance for comparing the two lines.
    ''' The lines are compared based on their LowerLimit and UpperLimit.  The UpperLimit tolerance is based on it
    ''' relative change from the Y range.  The tolerance if based on the reference line. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As RangeLimit, ByVal tolerance As Double) As Boolean
        If other Is Nothing Then
            Return False
        Else
            Return Math.Abs(other.UpperLimit - Me.UpperLimit) < tolerance AndAlso
                   Math.Abs(other.LowerLimit - Me.LowerLimit) < tolerance
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As RangeLimit, ByVal right As RangeLimit) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse (left IsNot Nothing) AndAlso left.Equals(right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As RangeLimit, ByVal right As RangeLimit) As Boolean
        Return ((left Is Nothing) AndAlso (right IsNot Nothing)) OrElse Not ((left IsNot Nothing) AndAlso left.Equals(right))
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <returns> An <see cref="System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.UpperLimit.GetHashCode Xor Me.LowerLimit.GetHashCode
    End Function

#End Region

End Class
