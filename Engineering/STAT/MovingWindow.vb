﻿Imports System.ComponentModel
''' <summary> Moving Window filter. </summary>
''' <remarks> David, 1/27/2016. </remarks>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="1/27/2016" by="David" revision=""> Created. </history>
Public Class MovingWindow
    Inherits MovingAverage
    Implements ICloneable

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me._ResetKnownState()
    End Sub

    ''' <summary> The cloning constructor. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As MovingWindow)
        MyBase.New(value)
        If value IsNot Nothing Then
            Me._Window = value.Window
            Me._Status = value.Status
            Me._UpdateRule = value.UpdateRule
            Me._TimeoutInterval = value.TimeoutInterval
            Me._ReadingsQueue = New Queue(Of Double)(value.ReadingsQueue)
        End If
    End Sub

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Overrides Function Clone() As Object Implements System.ICloneable.Clone
        Return New MovingWindow(Me)
    End Function

#End Region

#Region " RESET AND CLEAR "

    ''' <summary> Clears values to their known (initial) state. </summary>
    ''' <remarks> David, 1/30/2016. </remarks>
    Private Sub _ClearKnownState()
        Me._FirstReadingTime = DateTime.MinValue
        Me._LastReadingTime = DateTime.MinValue
        Me._ReadingsQueue = New Queue(Of Double)
        Me._Status = MovingWindowStatus.None
    End Sub

    ''' <summary> Clears values to their known (initial) state. </summary>
    ''' <remarks> David, 1/30/2016. </remarks>
    Public Overrides Sub ClearKnownState()
        MyBase.ClearKnownState()
        Me._ClearKnownState()
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    Private Sub _ResetKnownState()
        Me._ClearKnownState()
        Me._TimeoutInterval = TimeSpan.MaxValue
        Me._UpdateRule = MovingWindowUpdateRule.None
        Me._Window = 0
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    Public Overrides Sub ResetKnownState()
        MyBase.ResetKnownState()
        Me._ResetKnownState()
    End Sub

#End Region

#Region " MOVING WINDOW "

    ''' <summary> Gets the first reading time. </summary>
    ''' <value> The first reading time. </value>
    Public ReadOnly Property FirstReadingTime As DateTime

    ''' <summary> Gets the last reading time. </summary>
    ''' <value> The last reading time. </value>
    Public ReadOnly Property LastReadingTime As DateTime

    ''' <summary> Gets the elapsed time. </summary>
    ''' <value> The elapsed time. </value>
    Public ReadOnly Property ElapsedTime As TimeSpan
        Get
            Return Me.LastReadingTime.Subtract(Me.FirstReadingTime)
        End Get
    End Property

    ''' <summary> Gets the elapsed milliseconds. </summary>
    ''' <value> The elapsed milliseconds. </value>
    Public ReadOnly Property ElapsedMilliseconds As Double
        Get
            Return Me.ElapsedTime.Ticks / TimeSpan.TicksPerMillisecond
        End Get
    End Property


    ''' <summary> Gets the timeout interval. </summary>
    ''' <value> The timeout interval. </value>
    Public Property TimeoutInterval As TimeSpan

    ''' <summary> Gets or sets the update rule. </summary>
    ''' <value> The update rule. </value>
    Public Property UpdateRule As MovingWindowUpdateRule

    ''' <summary> Gets or sets the window. </summary>
    ''' <value> The window. </value>
    Public Property Window As Double

    ''' <summary> Gets or sets the status. </summary>
    ''' <value> The status. </value>
    Public ReadOnly Property Status As MovingWindowStatus

    ''' <summary> Gets the is completed. </summary>
    ''' <value> The is completed. </value>
    Public ReadOnly Property IsCompleted() As Boolean
        Get
            Return Me.Status = Core.Engineering.MovingWindowStatus.WithinWindow
        End Get
    End Property

    ''' <summary> Query if 'timeoutInterval' is timeout. </summary>
    ''' <remarks> David, 2/5/2016. </remarks>
    ''' <returns> <c>true</c> if timeout; otherwise <c>false</c> </returns>
    Public Function IsTimeout() As Boolean
        Return Me.ElapsedTime > Me.TimeoutInterval
    End Function

    ''' <summary> Tests value. </summary>
    ''' <remarks> David, 1/27/2016. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if the test passes, <c>false</c> if the test fails. </returns>
    Public Function CheckStatus(ByVal value As Double) As MovingWindowStatus
        Dim result As MovingWindowStatus = MovingWindowStatus.None
        If Me.Window > 0 Then
            If Me.Mean > 0 Then
                value = (value - Me.Mean) / Me.Mean
            End If
            value += value
            If value > Me.Window Then
                result = MovingWindowStatus.AboveWindow
            ElseIf value < -Me.Window Then
                result = MovingWindowStatus.BelowWindow
            Else
                result = MovingWindowStatus.WithinWindow
            End If
        End If
        Return result
    End Function

    ''' <summary> Gets or sets a queue of readings. </summary>
    ''' <value> A Queue of readings. </value>
    Public ReadOnly Property ReadingsQueue As Queue(Of Double)

    ''' <summary> Gets the number of readings. </summary>
    ''' <value> The number of readings. </value>
    Public ReadOnly Property ReadingsCount As Integer
        Get
            Return Me.ReadingsQueue.Count
        End Get
    End Property

    ''' <summary> Gets the last reading. </summary>
    ''' <value> The last reading. </value>
    Public ReadOnly Property LastReading As Double?
        Get
            If Me.ReadingsQueue?.Count > 0 Then
                Return Me.ReadingsQueue.Last
            Else
                Return New Double?
            End If
        End Get
    End Property

    ''' <summary> Adds a value. </summary>
    ''' <remarks> David, 1/27/2016. </remarks>
    ''' <param name="value"> The value. </param>
    Public Overrides Sub AddValue(ByVal value As Double)
        If Me.ReadingsCount = 0 Then Me._FirstReadingTime = DateTime.Now
        Me._LastReadingTime = DateTime.Now
        Me.ReadingsQueue.Enqueue(value)
        If Me.Count < Me.Length OrElse Me.UpdateRule = MovingWindowUpdateRule.None Then
            Me._Status = MovingWindowStatus.Filling
            MyBase.AddValue(value)
        Else
            Me.EvaluateMean()
            Me._Status = Me.CheckStatus(value)
            ' if value is within window, we are done.
            If Not Me.Status = MovingWindowStatus.WithinWindow Then
                ' if not in range, drop first value and add this value
                MyBase.AddValue(value)
            End If
        End If
    End Sub

#End Region

End Class

''' <summary> Values that represent moving window update rules. </summary>
''' <remarks> David, 1/27/2016. </remarks>
Public Enum MovingWindowUpdateRule
    <Description("Not stopping")> None
    <Description("Stop On Within Window")> StopOnWithinWindow
End Enum

''' <summary> Values that represent moving window status. </summary>
''' <remarks> David, 1/30/2016. </remarks>
Public Enum MovingWindowStatus
    <Description("Not stopping")> None
    <Description("Filling")> Filling
    <Description("Within Window")> WithinWindow
    <Description("Above Window")> AboveWindow
    <Description("Below window")> BelowWindow
End Enum
