﻿''' <summary> Moving average. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="11/19/2014" by="David" revision=""> Created. </history>
Public Class MovingAverage
    Inherits SampleStatistics
    Implements ICloneable

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me._ResetKnownState()
    End Sub

    ''' <summary> The cloning constructor. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As MovingAverage)
        MyBase.New(value)
        If value IsNot Nothing Then
            Me._Length = value.Length
        End If
    End Sub

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Overrides Function Clone() As Object Implements System.ICloneable.Clone
        Return New MovingAverage(Me)
    End Function

#End Region

#Region " RESET AND CLEAR "

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    Private Sub _ResetKnownState()
        Me._Length = 0
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    Public Overrides Sub ResetKnownState()
        MyBase.ResetKnownState()
        Me._ResetKnownState()
    End Sub

#End Region

#Region " MOVING AVERAGE "

    ''' <summary> Gets the length of the moving average. </summary>
    ''' <value> The length. </value>
    Public Property Length As Integer

    ''' <summary> Adds a value. </summary>
    ''' <param name="value"> The value. </param>
    Public Overrides Sub AddValue(ByVal value As Double)
        Dim updateRangeRequired As Boolean = Me.Count >= Me.Length
        Do While Me.Count >= Me.Length
            Me.ValueList.RemoveAt(0)
        Loop
        If updateRangeRequired Then Me.UpdateRange()
        MyBase.AddValue(value)
    End Sub

#End Region

End Class
