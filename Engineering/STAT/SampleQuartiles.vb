﻿
''' <summary> Sample quartile calculations. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="5/29/2014" by="David" revision=""> Created. </history>
Public Class SampleQuartiles

#Region " CONSTRUCTOR "

    ''' <summary> Constructor. </summary>
    ''' <param name="values"> The values. </param>
    Public Sub New(ByVal values As Double())
        MyBase.New()
        Me._ResetKnownState()
        If values IsNot Nothing Then
            Me._Sample.AddValues(values)
        End If
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="sample"> The sample. </param>
    Public Sub New(ByVal sample As SampleStatistics)
        MyBase.New()
        Me._ResetKnownState()
        If sample IsNot Nothing Then
            Me._Sample = New SampleStatistics(sample)
        End If
    End Sub

#End Region

#Region " RESET AND CLEAR "

    ''' <summary> Clears values to their known (initial) state. </summary>
    Private Sub _ClearKnownState()
        Me._Quartiles = New Quartiles
        Me._Fence = Interval.Empty
        Me._FilteredSample = New SampleStatistics
    End Sub

    ''' <summary> Clears values to their known (initial) state. </summary>
    Public Overridable Sub ClearKnownState()
        Me._ClearKnownState()
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    Private Sub _ResetKnownState()
        Me._Sample = New SampleStatistics()
        Me._ClearKnownState()
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    Public Overridable Sub ResetKnownState()
        Me._ResetKnownState()
    End Sub

#End Region

#Region " SAMPLE STATISTICS "

    ''' <summary> Gets or sets the sample. </summary>
    ''' <value> The sample. </value>
    Public Property Sample As SampleStatistics

    ''' <summary> Gets the filtered sample with outliers removed. </summary>
    ''' <value> The filtered sample. </value>
    Public Property FilteredSample As SampleStatistics

    ''' <summary> Gets the number of outliers. </summary>
    ''' <value> The number of outliers. </value>
    Public ReadOnly Property OutlierCount As Integer
        Get
            Return Me.Sample.Count - Me.FilteredSample.Count
        End Get
    End Property

#End Region

#Region " QUARTILES "

    Public Property Quartiles As Quartiles

    ''' <summary> Gets or sets the fence. </summary>
    ''' <value> The fence. </value>
    Public Property Fence As Interval

    ''' <summary> Evaluates the <see cref="Quartiles">quartiles</see> and the 
    ''' <see cref="FilteredSample">filtered sample</see>. </summary> 
    Public Sub Evaluate()
        Me.ClearKnownState()
        Me._Quartiles.Evaluate(Me._Sample.Values.ToArray)
        Me._Fence = Engineering.Interval.CreateInstance(Me.Quartiles.First - 1.5 * Me.Quartiles.Range,
                                                        Me.Quartiles.Third + 1.5 * Me.Quartiles.Range,
                                                        0.000001 * Me.Quartiles.Range)
        Me.FilterSample()
    End Sub

    ''' <summary> Filter sample. </summary>
    ''' <remarks> This function is set outside the evaluation to allow recalculation is the fence is updated. </remarks>
    Public Sub FilterSample()
        Me._FilteredSample = New SampleStatistics
        For Each value As Double In Me._Sample.Values
            If Me.Fence.Contains(value) Then
                Me._FilteredSample.AddValue(value)
            End If
        Next
    End Sub

#End Region

End Class
