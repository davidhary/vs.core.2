﻿''' <summary> Process control statistics. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="4/29/2015" by="David" revision=""> Created. </history>
Public Class ProcessControlStatistics

#Region " PROCESS CONTROL "

    Private _CapabilityRatio As Double

    ''' <summary> Gets or sets the capability ratio (Cp). </summary>
    ''' <value> The capability ratio (Cp). </value>
    Public Property CapabilityRatio As Double
        Get
            Return Me._CapabilityRatio
        End Get
        Protected Set(value As Double)
            Me._CapabilityRatio = value
        End Set
    End Property

    Private _CapabilityRatioMinimal As Double

    ''' <summary> Gets or sets the capability ratio minimal (Cpk). </summary>
    ''' <value> The capability ratio minimal (Cpk). </value>
    Public Property CapabilityRatioMinimal As Double
        Get
            Return Me._CapabilityRatioMinimal
        End Get
        Protected Set(value As Double)
            Me._CapabilityRatioMinimal = value
        End Set
    End Property

    Private _CapabilityRatioNominal As Double

    ''' <summary> Gets or sets the capability ratio nominal (Cpm). </summary>
    ''' <value> The capability ratio nominal  (Cpm). </value>
    Public Property CapabilityRatioNominal As Double
        Get
            Return Me._CapabilityRatioNominal
        End Get
        Protected Set(value As Double)
            Me._CapabilityRatioNominal = value
        End Set
    End Property

    ''' <summary> Calculates the process control capability ratios. </summary>
    ''' <param name="targetValue">        Target value. </param>
    ''' <param name="specificationLevel"> The specification level. </param>
    ''' <param name="mean">               The sample mean. </param>
    ''' <param name="sigma">              The sample sigma. </param>
    Public Sub CalculateProcessControl(ByVal targetValue As Double, ByVal specificationLevel As Interval,
                                       ByVal mean As Double, ByVal sigma As Double)
        If specificationLevel IsNot Nothing AndAlso Not specificationLevel.IsEmpty AndAlso sigma > 0 Then
            Me.CapabilityRatio = (specificationLevel.HighEndPoint - specificationLevel.LowEndPoint) / (6 * sigma)
            Me.CapabilityRatioMinimal = Math.Min(specificationLevel.HighEndPoint - mean,
                                                 mean - specificationLevel.LowEndPoint) / (3 * sigma)
            Dim factor As Double = Math.Sqrt(1 + ((mean - targetValue) / sigma) ^ 2)
            Me.CapabilityRatioNominal = Me.CapabilityRatio / factor
        End If
    End Sub

#End Region


End Class
