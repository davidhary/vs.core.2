﻿
''' <summary> Form for viewing the engineering up down. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="4/5/2014" by="David" revision=""> Created. </history>
Public Class EngineeringUpDownForm

#Region " FORM EVENTS "

    Private Sub _Load(sender As Object, e As System.EventArgs) Handles Me.Load

        With Me._EngineeringScaleComboBox
            .DataSource = Nothing
            .Items.Clear()
            .DataSource = Extensions.ValueDescriptionPairs(GetType(EngineeringScale))
            .DisplayMember = "Value"
            .ValueMember = "Key"
        End With

    End Sub

#End Region

#Region " CONTROL EVENTS "

    Private Sub _EnterValueNumeric_ValueChanged(sender As System.Object, e As System.EventArgs) Handles _EnterValueNumeric.ValueChanged, NumericUpDown1.ValueChanged
        _EditedEngineeringUpDown.ScaledValue = Me._EnterValueNumeric.Value
        Me.NumericUpDown1.Value = Me._EnterValueNumeric.Value
    End Sub

    Private Sub _EngineeringScaleComboBox_SelectionChangeCommitted(sender As Object, e As System.EventArgs) Handles _EngineeringScaleComboBox.SelectionChangeCommitted
        Me._EditedEngineeringUpDown.EngineeringScale = CType(CType(Me._EngineeringScaleComboBox.SelectedItem, KeyValuePair(Of [Enum], String)).Key, EngineeringScale)
    End Sub

    Private Sub _EditedEngineeringUpDown_ValueChanged(sender As Object, e As System.EventArgs) Handles _EditedEngineeringUpDown.ValueChanged
        Me._ScaledValueTextBox.Text = Me._EditedEngineeringUpDown.ScaledValue.ToString("0.#########E-00 " & Me._UnitTextBox.Text)
    End Sub

    Private Sub _DecimalPlacesNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _DecimalPlacesNumeric.ValueChanged
        Me._EditedEngineeringUpDown.DecimalPlaces = CInt(Me._DecimalPlacesNumeric.Value)
    End Sub

    Private Sub _UnitTextBox_TextChanged(sender As Object, e As System.EventArgs) Handles _UnitTextBox.TextChanged
        Me._EditedEngineeringUpDown.Unit = Me._UnitTextBox.Text
    End Sub

    Private Sub _ReadOnlyCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles _ReadOnlyCheckBox.CheckedChanged
        Me._EditedEngineeringUpDown.ReadOnly = Me._ReadOnlyCheckBox.Checked
    End Sub

#End Region

End Class
