﻿''' <summary> Form for viewing the selector. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="3/6/2015" by="David" revision=""> Created. </history>
Public Class SelectorForm

#Region " FORM EVENTS "

#End Region

#Region " CONTROL EVENTS "

    Private Sub _ReadOnlyCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles _ReadOnlyCheckBox.CheckedChanged
        Me._SelectorComboBox.ReadOnly = Me._ReadOnlyCheckBox.Checked
        Me._SelectorNumeric.ReadOnly = Me._ReadOnlyCheckBox.Checked
        Me._ToolStripSelectorComboBox.ReadOnly = Me._ReadOnlyCheckBox.Checked
        Me._ToolStripSelectorNumeric.ReadOnly = Me._ReadOnlyCheckBox.Checked
    End Sub

    Private Sub _ApplyButton_Click(sender As System.Object, e As System.EventArgs) Handles _ApplyButton.Click
        Me._SelectorComboBox.Text = Me._EnterNumeric.Text
        Me._SelectorNumeric.Value = Me._EnterNumeric.Value
        Me._ToolStripSelectorComboBox.Text = Me._EnterNumeric.Text
        Me._ToolStripSelectorNumeric.Value = Me._EnterNumeric.Value
    End Sub

    Private Sub _SelectButton_Click(sender As System.Object, e As System.EventArgs) Handles _SelectButton.Click
        Me._SelectorComboBox.SelectValue()
        Me._SelectorNumeric.SelectValue()
        Me._ToolStripSelectorComboBox.SelectValue()
        Me._ToolStripSelectorNumeric.SelectValue()
    End Sub

    Private Sub _SelectorComboBox_ValueSelected(sender As Object, e As System.EventArgs) Handles _SelectorComboBox.ValueSelected
        Me._SelectedTextBox.Text = Me._SelectorComboBox.SelectedText
    End Sub

    Private Sub _SelectorNumeric_ValueSelected(sender As Object, e As System.EventArgs) Handles _SelectorNumeric.ValueSelected
        Me._SelectedTextBox.Text = Me._SelectorNumeric.SelectedText
    End Sub

    Private Sub _ToolStripSelectorComboBox_ValueSelected(sender As System.Object, e As System.EventArgs) Handles _ToolStripSelectorComboBox.ValueSelected
        Me._SelectedTextBox.Text = Me._ToolStripSelectorComboBox.SelectedText
    End Sub

    Private Sub _ToolStripSelectorNumeric_ValueSelected(sender As System.Object, e As System.EventArgs) Handles _ToolStripSelectorNumeric.ValueSelected
        Me._SelectedTextBox.Text = Me._ToolStripSelectorNumeric.SelectedText
    End Sub

#End Region

End Class
