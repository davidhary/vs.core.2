﻿Public Class NullableNumericUpDownForm

    Private Sub NullableNumericUpDownForm_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        Me._Timer.Interval = 1000
        Me._Timer.Enabled = True
    End Sub

    Private Sub _NullableNumericUpDown_Validated(sender As Object, e As System.EventArgs) Handles _NullableNumericUpDown.Validated
        Me._ValidatedNullableValueTextBox.Text = Me._NullableNumericUpDown.NullableValue.ToString
        Me._ValidatedValueTextBox.Text = Me._NullableNumericUpDown.Value.ToString
        Me._NullableTextTextBox.Text = Me._NullableNumericUpDown.Text
    End Sub

    Private Sub _NullableNumericUpDown_ValueChanged(sender As Object, e As System.EventArgs) Handles _NullableNumericUpDown.ValueChanged
        Me._ChangedNullableValueTextBox.Text = Me._NullableNumericUpDown.NullableValue.ToString
        Me._ChangedValueTextBox.Text = Me._NullableNumericUpDown.Value.ToString
        Me._NullableTextTextBox.Text = Me._NullableNumericUpDown.Text
    End Sub

    Private Sub _NullableNumericUpDown_ValueDecrementing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles _NullableNumericUpDown.ValueDecrementing
        Me._SpinningNullableValueTextBox.Text = Me._NullableNumericUpDown.NullableValue.ToString
        Me._SpinningValueTextBox.Text = Me._NullableNumericUpDown.Value.ToString
        Me._NullableTextTextBox.Text = Me._NullableNumericUpDown.Text
    End Sub

    Private Sub _NullableNumericUpDown_ValueIncrementing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles _NullableNumericUpDown.ValueIncrementing
        Me._SpinningNullableValueTextBox.Text = Me._NullableNumericUpDown.NullableValue.ToString
        Me._SpinningValueTextBox.Text = Me._NullableNumericUpDown.Value.ToString
        Me._NullableTextTextBox.Text = Me._NullableNumericUpDown.Text
    End Sub

    Private Sub _NilifyButton_Click(sender As System.Object, e As System.EventArgs) Handles _NilifyButton.Click
        Me._NullableNumericUpDown.NullableValue = New Decimal?
        Me._NullableNumericUpDown.Validate()
        My.Application.DoEvents()
    End Sub

    Private Sub _Timer_Tick(sender As System.Object, e As System.EventArgs) Handles _Timer.Tick
        Me._NullableValueTextBox.Text = Me._NullableNumericUpDown.NullableValue.ToString
    End Sub

End Class