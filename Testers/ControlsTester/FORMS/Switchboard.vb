Imports System.Runtime.CompilerServices
Imports System.ComponentModel

''' <summary> Switches between test panels. </summary>
''' <license> (c) 2003 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="4/5/2014" by="David" revision=""> Created based on legacy code. </history>
Public Class Switchboard

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of this class.
    ''' </summary>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Event handler. Called by form for load events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try


            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me._MessagesTextBox.AddMessage("Loading...")

            ' instantiate form objects
            populateTestPanelSelector()

            ' set the form caption
            Me.Text = Extensions.BuildDefaultCaption("TESTER")

            ' center the form
            Me.CenterToScreen()

        Catch

            Me._MessagesTextBox.AddMessage("Exception...")

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me._MessagesTextBox.AddMessage("Loaded.")

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>Closes the form and exits the application.</summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _ExitButton_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles _ExitButton.Click

        Try
            Me.Close()
        Catch ex As Exception
            ex.Data.Add("@isr", "Unhandled Exception.")
            MessageBox.Show(ex.ToString, "Unhandled Exception",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try

    End Sub

    Private Sub _CancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.Close()
    End Sub

    Private Sub _TestButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TestButton.Click

        If True Then
            Dim fi As New System.IO.FileInfo(System.Windows.Forms.Application.ExecutablePath)
            Debug.Print(fi.FullName)
            Stop
        End If

    End Sub

#End Region

#Region " Logon "

    Private WithEvents _machineLogOn As isr.Core.Controls.MachineLogOn
    Private Sub _machineLogOn_PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Handles _machineLogOn.PropertyChanged
        Dim logon As isr.Core.Controls.MachineLogOn = CType(sender, isr.Core.Controls.MachineLogOn)
        If logon IsNot Nothing Then
            Select Case e.PropertyName
                Case "UserName"
                    Me._MessagesTextBox.AddMessage(String.Format("User name: {0}", logon.UserName))
                Case "UserRoles"
                    Dim role As String = ""
                    For Each s As String In logon.UserRoles
                        role = role & "," & s
                    Next
                    Me._MessagesTextBox.AddMessage(String.Format("User role: {0}", role))
                Case "IsAuthenticated"
                    If logon.IsAuthenticated Then
                        Me._MessagesTextBox.AddMessage(String.Format("User {0} authenticated", logon.UserName))
                    Else
                        Me._MessagesTextBox.AddMessage(String.Format("User not authenticated"))
                    End If
                    If _popup IsNot Nothing Then
                        ' _popup.Hide()
                    End If
            End Select
        End If
    End Sub

    Private _popup As ToolStripDropDown

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Private Sub _LogOnButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LogOnButton.Click

        Dim roles As New List(Of String)
        roles.Add("Administrators")
        _machineLogOn = New isr.Core.Controls.MachineLogOn
        If True Then
            Dim content As New isr.Core.Controls.LogOnControl
            content.UserLogOn = _machineLogOn
            content.ShowPopup(Me, System.Drawing.Point.Add(Me._LogOnButton.Parent.Location,
                                                     New System.Drawing.Size(Me._LogOnButton.Location.X, Me._LogOnButton.Location.Y)))
        Else
            ' logon.Validate("admin", New System.Collections.ObjectModel.ReadOnlyCollection(Of String)(roles))
            _popup = New ToolStripDropDown()
            _popup.Margin = Padding.Empty
            _popup.Padding = Padding.Empty
            Dim content As New isr.Core.Controls.LogOnControl
            Dim host As New ToolStripControlHost(content)
            host.Enabled = True
            host.Margin = Padding.Empty
            host.Padding = Padding.Empty
            _popup.Items.Add(host)
            _popup.Show(Me, System.Drawing.Point.Add(Me._LogOnButton.Parent.Location,
                                                     New System.Drawing.Size(Me._LogOnButton.Location.X, Me._LogOnButton.Location.Y)))
            content.UserLogOn = _machineLogOn
            content.Enabled = True
        End If

    End Sub

#End Region

#Region " SWITCH BOARD "

    ''' <summary> Descriptive Enumeration for test forms. </summary>
    Private Enum TestPanel
        <System.ComponentModel.Description("Engineering Up Down Control Form")> EngineeringUpDownControlForm
        <System.ComponentModel.Description("Nullable Up Down Control Form")> NullableUpDownControlForm
        <System.ComponentModel.Description("Drop Down text Box Control Form")> DropDownTextBoxForm
        <System.ComponentModel.Description("Rich Text Box Form")> RichTextBoxForm
        <System.ComponentModel.Description("Selector Form")> SelectorForm
    End Enum

    ''' <summary>Open selected items.</summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub openButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenButton.Click

        Try

            Select Case CType(CType(Me._ActionsComboBox.SelectedItem, KeyValuePair(Of [Enum], String)).Key, TestPanel)

                Case TestPanel.EngineeringUpDownControlForm

                    Using myForm As New EngineeringUpDownForm
                        myForm.ShowDialog()
                    End Using

                Case TestPanel.NullableUpDownControlForm

                    Using myForm As New NullableNumericUpDownForm
                        myForm.ShowDialog()
                    End Using

                Case TestPanel.DropDownTextBoxForm

                    Using myForm As New DropDownTextBoxForm
                        myForm.ShowDialog()
                    End Using

                Case TestPanel.RichTextBoxForm

                    Using myform As New isr.Core.Controls.RichTextBoxForm
                        myform.ShowDialog(Nothing)
                    End Using

                Case TestPanel.SelectorForm

                    Using myform As New SelectorForm
                        myform.ShowDialog(Nothing)
                    End Using

            End Select

        Catch ex As Exception

            ex.Data.Add("@isr", "Unhandled Exception.")
            MessageBox.Show(ex.ToString, "Unhandled Exception",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)

        Finally
        End Try

    End Sub

    ''' <summary>
    ''' Populates the list of test panels.
    ''' </summary>
    Private Sub populateTestPanelSelector()

        With _ActionsComboBox
            .DataSource = Nothing
            .Items.Clear()
            .DataSource = Extensions.ValueDescriptionPairs(GetType(TestPanel))
            .DisplayMember = "Value"
            .ValueMember = "Key"
        End With

    End Sub

#End Region

End Class

Public Module Extensions

    ''' <summary> Adds a message to 'message'. </summary>
    ''' <param name="box">     The box control. </param>
    ''' <param name="message"> The message. </param>
    <Extension()>
    Public Sub AddMessage(ByVal box As TextBox, ByVal message As String)
        If box IsNot Nothing Then
            box.SelectionStart = box.Text.Length
            box.SelectionLength = 0
            box.SelectedText = message & Environment.NewLine
        End If
    End Sub

    ''' <summary> Gets the <see cref="DescriptionAttribute"/> of an <see cref="System.Enum"/> type
    ''' value. </summary>
    ''' <param name="value"> The <see cref="System.Enum"/> type value. </param>
    ''' <returns> A string containing the text of the <see cref="DescriptionAttribute"/>. </returns>
    <Extension()>
    Public Function Description(ByVal value As System.Enum) As String

        If value Is Nothing Then Return ""

        Dim candidate As String = value.ToString()
        Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(candidate)
        Dim attributes As DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())

        If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
            candidate = attributes(0).Description
        End If
        Return candidate

    End Function

    ''' <summary> Gets a Key Value Pair description item. </summary>
    ''' <param name="value"> The <see cref="System.Enum"/> type value. </param>
    ''' <returns> A list of. </returns>
    <Extension()>
    Public Function ValueDescriptionPair(ByVal value As System.Enum) As System.Collections.Generic.KeyValuePair(Of System.Enum, String)
        Return New System.Collections.Generic.KeyValuePair(Of System.Enum, String)(value, value.Description)
    End Function


    <Extension()>
    Public Function ValueDescriptionPairs(ByVal type As Type) As IList

        Dim keyValuePairs As ArrayList = New ArrayList()
        For Each value As System.Enum In System.Enum.GetValues(type)
            keyValuePairs.Add(value.ValueDescriptionPair())
        Next value
        Return keyValuePairs

    End Function

    ''' <summary> Builds the default caption. </summary>
    ''' <param name="subtitle"> The subtitle. </param>
    ''' <returns> System.String. </returns>
    Public Function BuildDefaultCaption(ByVal subtitle As String) As String

        Dim builder As New System.Text.StringBuilder
        builder.Append(My.Application.Info.Title)
        builder.Append(" ")
        builder.Append(My.Application.Info.Version.ToString)
        If My.Application.Info.Version.Major < 1 Then
            builder.Append(".")
            Select Case My.Application.Info.Version.Minor
                Case 0
                    builder.Append("Alpha")
                Case 1
                    builder.Append("Beta")
                Case 2 To 8
                    builder.Append(String.Format(Globalization.CultureInfo.CurrentCulture, "RC{0}", My.Application.Info.Version.Minor - 1))
                Case Else
                    builder.Append("Gold")
            End Select
        End If
        If Not String.IsNullOrWhiteSpace(subtitle) Then
            builder.Append(": ")
            builder.Append(subtitle)
        End If
        Return builder.ToString

    End Function

End Module
