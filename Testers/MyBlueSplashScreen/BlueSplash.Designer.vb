﻿Partial Public Class BlueSplash
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.appMini = New System.Windows.Forms.Label()
        Me.bigApp = New System.Windows.Forms.Label()
        Me.tasks = New System.Windows.Forms.Label()
        Me.close_Renamed = New System.Windows.Forms.Label()
        Me.minimize = New System.Windows.Forms.Label()
        Me.progressbar1 = New isr.Core.Controls.MetroProgressBar()
        Me.splashtime = New System.Windows.Forms.Timer(Me.components)
        Me.label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        ' 
        ' appMini
        ' 
        Me.appMini.AutoSize = True
        Me.appMini.BackColor = System.Drawing.Color.Transparent
        Me.appMini.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(238)))
        Me.appMini.ForeColor = System.Drawing.Color.White
        Me.appMini.Location = New System.Drawing.Point(30, 9)
        Me.appMini.Name = "appMini"
        Me.appMini.Size = New System.Drawing.Size(130, 18)
        Me.appMini.TabIndex = 0
        Me.appMini.Text = "Your Application"
        ' 
        ' bigApp
        ' 
        Me.bigApp.AutoSize = True
        Me.bigApp.BackColor = System.Drawing.Color.Transparent
        Me.bigApp.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0F, (CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle)), System.Drawing.GraphicsUnit.Point, (CByte(238)))
        Me.bigApp.ForeColor = System.Drawing.Color.White
        Me.bigApp.Location = New System.Drawing.Point(23, 86)
        Me.bigApp.Name = "bigApp"
        Me.bigApp.Size = New System.Drawing.Size(389, 55)
        Me.bigApp.TabIndex = 1
        Me.bigApp.Text = "Your Application"
        ' 
        ' tasks
        ' 
        Me.tasks.AutoSize = True
        Me.tasks.BackColor = System.Drawing.Color.Transparent
        Me.tasks.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, (CByte(238)))
        Me.tasks.ForeColor = System.Drawing.Color.White
        Me.tasks.Location = New System.Drawing.Point(12, 208)
        Me.tasks.Name = "tasks"
        Me.tasks.Size = New System.Drawing.Size(136, 29)
        Me.tasks.TabIndex = 2
        Me.tasks.Text = "current task"
        ' 
        ' close
        ' 
        Me.close_Renamed.AutoSize = True
        Me.close_Renamed.BackColor = System.Drawing.Color.Transparent
        Me.close_Renamed.Font = New System.Drawing.Font("Webdings", 18.0F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(2)))
        Me.close_Renamed.ForeColor = System.Drawing.Color.White
        Me.close_Renamed.Location = New System.Drawing.Point(397, 1)
        Me.close_Renamed.Name = "close"
        Me.close_Renamed.Size = New System.Drawing.Size(36, 26)
        Me.close_Renamed.TabIndex = 3
        Me.close_Renamed.Text = "r"
        '			Me.close.Click += New System.EventHandler(Me.close_Click)
        '			Me.close.MouseLeave += New System.EventHandler(Me.close_MouseLeave)
        '			Me.close.MouseHover += New System.EventHandler(Me.close_MouseHover)
        ' 
        ' minimize
        ' 
        Me.minimize.AutoSize = True
        Me.minimize.BackColor = System.Drawing.Color.Transparent
        Me.minimize.Font = New System.Drawing.Font("Webdings", 18.0F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(2)))
        Me.minimize.ForeColor = System.Drawing.Color.White
        Me.minimize.Location = New System.Drawing.Point(362, 1)
        Me.minimize.Name = "minimize"
        Me.minimize.Size = New System.Drawing.Size(36, 26)
        Me.minimize.TabIndex = 4
        Me.minimize.Text = "0"
        '			Me.minimize.Click += New System.EventHandler(Me.minimize_Click)
        '			Me.minimize.MouseLeave += New System.EventHandler(Me.minimize_MouseLeave)
        '			Me.minimize.MouseHover += New System.EventHandler(Me.minimize_MouseHover)
        ' 
        ' progressbar1
        ' 
        Me.progressbar1.BackColor = System.Drawing.Color.Transparent
        Me.progressbar1.Location = New System.Drawing.Point(64, 162)
        Me.progressbar1.Name = "progressbar1"
        Me.progressbar1.Size = New System.Drawing.Size(308, 14)
        Me.progressbar1.TabIndex = 5
        ' 
        ' splashtime
        ' 
        Me.splashtime.Interval = 1600
        '			Me.splashtime.Tick += New System.EventHandler(Me.splashtime_Tick)
        ' 
        ' label1
        ' 
        Me.label1.AutoSize = True
        Me.label1.BackColor = System.Drawing.Color.Transparent
        Me.label1.Font = New System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(2)))
        Me.label1.ForeColor = System.Drawing.Color.White
        Me.label1.Location = New System.Drawing.Point(5, 11)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(28, 16)
        Me.label1.TabIndex = 6
        Me.label1.Text = "1"
        ' 
        ' Splash
        ' 
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0F, 13.0F)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = My.Resources.Splash
        Me.ClientSize = New System.Drawing.Size(439, 248)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.progressbar1)
        Me.Controls.Add(Me.minimize)
        Me.Controls.Add(Me.close_Renamed)
        Me.Controls.Add(Me.tasks)
        Me.Controls.Add(Me.bigApp)
        Me.Controls.Add(Me.appMini)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximumSize = New System.Drawing.Size(439, 248)
        Me.MinimumSize = New System.Drawing.Size(439, 248)
        Me.Name = "Splash"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Splash"
        '			Me.FormClosed += New System.Windows.Forms.FormClosedEventHandler(Me.Splash_FormClosed)
        '			Me.MouseDown += New System.Windows.Forms.MouseEventHandler(Me.Splash_MouseDown)
        '			Me.MouseMove += New System.Windows.Forms.MouseEventHandler(Me.Splash_MouseMove)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private appMini As System.Windows.Forms.Label
    Private bigApp As System.Windows.Forms.Label
    Private tasks As System.Windows.Forms.Label
    Private WithEvents close_Renamed As System.Windows.Forms.Label
    Private WithEvents minimize As System.Windows.Forms.Label
    Private progressbar1 As isr.Core.Controls.MetroProgressBar
    Private WithEvents splashtime As System.Windows.Forms.Timer
    Private label1 As System.Windows.Forms.Label
End Class
