﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.Pith.My.ProjectTraceEventId.MyBlueSplashScreen

        Public Const AssemblyTitle As String = "Blue Splash Screen Tester"
        Public Const AssemblyDescription As String = "Blue Splash Screen Tester"
        Public Const AssemblyProduct As String = "Blue.Splash.Screen.Tester.2016"

    End Class

End Namespace

