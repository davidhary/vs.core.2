﻿Imports System.ComponentModel
Imports System.Text
Imports System.Threading.Tasks
Imports System.Threading

Partial Public Class BlueSplash
    Inherits Form

    Public Sub New()
        InitializeComponent()

        'Tasks
        'Starting
        tasks.Text = "Starting..."
        ' Thread.Sleep(1000)



        'start timer
        ' splashtime.Start()
    End Sub

    Public Isminimized As Boolean = False

    'Close Application
    Private Sub close_Click(ByVal sender As Object, ByVal e As EventArgs) Handles close_Renamed.Click
        Application.Exit()
    End Sub

    'Minimize Application
    Private Sub minimize_Click(ByVal sender As Object, ByVal e As EventArgs) Handles minimize.Click
        Me.WindowState = FormWindowState.Minimized
        Isminimized = True
    End Sub

    'Mouse hover and leave effects
    Private Sub close_MouseHover(ByVal sender As Object, ByVal e As EventArgs) Handles close_Renamed.MouseHover
        close_Renamed.ForeColor = Color.Silver
    End Sub

    Private Sub close_MouseLeave(ByVal sender As Object, ByVal e As EventArgs) Handles close_Renamed.MouseLeave
        close_Renamed.ForeColor = Color.White
    End Sub

    Private Sub minimize_MouseHover(ByVal sender As Object, ByVal e As EventArgs) Handles minimize.MouseHover
        minimize.ForeColor = Color.Silver
    End Sub

    Private Sub minimize_MouseLeave(ByVal sender As Object, ByVal e As EventArgs) Handles minimize.MouseLeave
        minimize.ForeColor = Color.White
    End Sub

    'Show MainForm(Form1)
    Public Sub frmNewFormThread()
        Dim frmNewForm As Form = New Form1()
        If Isminimized = True Then
            frmNewForm.WindowState = FormWindowState.Minimized
        Else
            frmNewForm.WindowState = FormWindowState.Maximized
        End If
        Application.Run(frmNewForm)
    End Sub

    Private Sub splashtime_Tick(ByVal sender As Object, ByVal e As EventArgs) Handles splashtime.Tick
        splashtime.Stop()

        Dim newThread As Thread = New System.Threading.Thread(AddressOf frmNewFormThread)

        newThread.SetApartmentState(System.Threading.ApartmentState.STA)
        newThread.Start()
        Me.Close()

    End Sub

    Private Sub Splash_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles MyBase.FormClosed

    End Sub


    Private Sub Splash_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles MyBase.MouseMove

    End Sub

    Private Sub Splash_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles MyBase.MouseDown

    End Sub
End Class
