﻿'
' *	Created/modified in 2011 by Simon Baer
' *	
' *  Licensed under the Code Project Open License (CPOL).
' 
Partial Public Class Form1
    Inherits Form

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub _ShowButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _ShowButton.Click
        _PopupNotifier.TitleText = _NotificationTitleTextBox.Text
        _PopupNotifier.ContentText = _NotificationTextTextBox.Text
        _PopupNotifier.ShowCloseButton = _ShowNotificationCloseButtonCheckBox.Checked
        _PopupNotifier.ShowOptionsButton = _ShowNotificationOptionMenuCheckBox.Checked
        _PopupNotifier.ShowGrip = _ShowNotificationGripCheckBox.Checked
        _PopupNotifier.Delay = Integer.Parse(_NotificationDelayTextBox.Text)
        _PopupNotifier.AnimationInterval = Integer.Parse(_NotificationAnimationIntervalTextBox.Text)
        _PopupNotifier.AnimationDuration = Integer.Parse(_AnimationDurationTextBox.Text)
        _PopupNotifier.TitlePadding = New Padding(Integer.Parse(_NotificationTitlePaddingTextBox.Text))
        _PopupNotifier.ContentPadding = New Padding(Integer.Parse(_NotificationContentPaddingTextBox.Text))
        _PopupNotifier.ImagePadding = New Padding(Integer.Parse(_NotificationIconPaddingTextBox.Text))
        _PopupNotifier.Scroll = _NotificationScrollCheckBox.Checked

        If _ShowNotificationIconCheckBox.Checked Then
            _PopupNotifier.Image = My.Resources._157_GetPermission_48x48_72
        Else
            _PopupNotifier.Image = Nothing
        End If

        _PopupNotifier.Popup()
    End Sub

    Private Sub _ExitButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _ExitButton.Click
        Me.Close()
        Application.Exit()
    End Sub
End Class
