﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.Pith.My.ProjectTraceEventId.DiagnosisTester

        Public Const AssemblyTitle As String = "Core Diagnosis Tester"
        Public Const AssemblyDescription As String = "Core Diagnosis Tester"
        Public Const AssemblyProduct As String = "Core.Diagnosis.Tester.2016"

    End Class

End Namespace

