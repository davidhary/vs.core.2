''' <summary> Permits counting down using long integer. </summary>
''' <license> (c) 2006 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="06/13/2006" by="David" revision="1.0.2355.x"> Created. </history>
Public Class DownCounter

#Region " COUNTER "

    ''' <summary> Decrement the count. </summary>
    Public Sub Countdown()
        Me._currentValue -= Me._DecrementValue
    End Sub

    ''' <summary> The current value. </summary>
    Private _currentValue As Integer

    ''' <summary> Gets the current value of the counter. </summary>
    ''' <value> The current value. </value>
    Public ReadOnly Property CurrentValue() As Integer
        Get
            Return Me._currentValue
        End Get
    End Property

    ''' <summary> Gets or set the amount by which the count is decremented by on each countdown. </summary>
    ''' <value> The decrement value. </value>
    Public Property DecrementValue() As Integer

    ''' <summary> Gets the initial count down value. </summary>
    ''' <value> The initial value. </value>
    Public Property InitialValue() As Integer

    ''' <summary> Returns true if countdown is completed. </summary>
    ''' <returns> The is countdown done. </returns>
    Public Function IsCountdownDone() As Boolean
        Return Me._currentValue < 0
    End Function

    ''' <summary> Restart from the initial value. </summary>
    Public Sub Restart()
        Me._currentValue = Me._InitialValue
    End Sub

#End Region

End Class

