﻿''' <summary> Time span keeper. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="11/19/2014" by="David" revision="2.1.5436"> Created. </history>
Public Class TimespanKeeper

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.new()
        Me._ResetKnownState()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As TimespanKeeper)
        Me.new()
        If value IsNot Nothing Then
            Me._initializeKnownState(value.StartTime, value.EndTime)
        End If
    End Sub

#End Region

#Region " RESET AND CLEAR "

    ''' <summary> Clears to known (clear) state; Clears select values to their initial state. </summary>
    Private Sub _ClearKnownState()
        Me._StartTime = DateTime.MinValue
        Me._EndTime = DateTime.MinValue
    End Sub

    ''' <summary> Clears to known (clear) state; Clears select values to their initial state. </summary>
    Public Sub ClearKnownState()
        Me._ClearKnownState()
    End Sub

    ''' <summary> Initializes to known (Initialize) state; Initializes select values to their initial
    ''' state. </summary>
    ''' <param name="startTime"> The start time. </param>
    ''' <param name="endTime">   The end time. </param>
    Private Sub _initializeKnownState(ByVal startTime As DateTime, ByVal endTime As DateTime)
        Me._StartTime = startTime
        Me._EndTime = endTime
    End Sub

    ''' <summary> Initializes to known (Initialize) state; Initializes select values to their initial state. </summary>
    Private Sub _initializeKnownState()
        Dim value As DateTime = DateTime.Now
        Me._initializeKnownState(value, value)
    End Sub

    ''' <summary> Initializes to known (Initialize) state; Initializes select values to their initial state. </summary>
    Public Sub InitializeKnownState()
        Me._initializeKnownState()
    End Sub

    ''' <summary> Initializes to known (Initialize) state; Initializes select values to their initial
    ''' state. </summary>
    ''' <param name="startTime"> The start time. </param>
    ''' <param name="endTime">   The end time. </param>
    Public Sub InitializeKnownState(ByVal startTime As DateTime, ByVal endTime As DateTime)
        Me._initializeKnownState(startTime, endTime)
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    Private Sub _ResetKnownState()
        Me._ClearKnownState()
        Me._ApplyTimestampFormat(DefaultTimestampFormat)
        Me._ApplyElapsedTimeFormat(DefaultElapsedTimeFormat)
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    Public Sub ResetKnownState()
        Me._ResetKnownState()
    End Sub


#End Region

#Region " COMMANDS "

    ''' <summary> Starts the time keeper. </summary>
    Public Sub [Start]()
        Me.InitializeKnownState()
    End Sub

    ''' <summary> Ends the time keeper. </summary>
    Public Sub [End]()
        Me._EndTime = DateTime.Now
    End Sub

    ''' <summary> Resume the time keeper. </summary>
    Public Sub [Resume]()
        Me._EndTime = Me.StartTime
    End Sub

#End Region

#Region " PROPERTIES "

    Private _StartTime As DateTime
    ''' <summary> Gets or sets the start time. </summary>
    ''' <value> The start time. </value>
    Public ReadOnly Property StartTime As DateTime
        Get
            Return Me._StartTime
        End Get
    End Property

    Private _EndTime As DateTime
    ''' <summary> Gets or sets the end time. </summary>
    ''' <value> The end time. </value>
    Public ReadOnly Property EndTime As DateTime
        Get
            Return Me._EndTime
        End Get
    End Property

    ''' <summary> Query if this object is active. </summary>
    ''' <returns> <c>true</c> if active; otherwise <c>false</c>. </returns>
    Public Function IsActive() As Boolean
        Return Me.HasStarted AndAlso Not Me.HasEnded
    End Function

    ''' <summary> Query if time span has ended. </summary>
    ''' <returns> <c>true</c> if time span has ended; otherwise <c>false</c> </returns>
    Public Function HasEnded() As Boolean
        Return Me.EndTime > Me.StartTime AndAlso DateTime.Now < Me.EndTime
    End Function

    ''' <summary> Determines if time keeper time was started. </summary>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function HasStarted() As Boolean
        Return Me.StartTime > DateTime.MinValue AndAlso Me.EndTime > DateTime.MinValue
    End Function

    ''' <summary> Gets the elapsed time span. </summary>
    ''' <value> The elapsed time span. </value>
    Public ReadOnly Property ElapsedTimespan As TimeSpan
        Get
            If Me.HasEnded Then
                Return Me.EndTime.Subtract(Me.StartTime)
            ElseIf Me.IsActive Then
                Return DateTime.Now.Subtract(Me.StartTime)
            Else
                Return TimeSpan.Zero
            End If
        End Get
    End Property

#End Region

#Region " TIMESTAMP "

    Public Const DefaultTimestampFormat As String = "yyyy/MM/dd HH:mm:ss"
    Public Const DefaultTimestampStringFormat As String = "{0:" & TimespanKeeper.DefaultTimestampFormat & "}"

    ''' <summary> Applies the timestamp format described by value. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub _ApplyTimestampFormat(ByVal value As String)
        Me._TimestampFormat = value
        Me._TimestampStringFormat = "{0:" & Me.TimestampFormat & "}"
    End Sub

    Private _TimestampFormat As String
    ''' <summary> Gets or sets the timestamp format. </summary>
    ''' <value> The timestamp format. </value>
    Public Property TimestampFormat As String
        Get
            Return Me._TimestampFormat
        End Get
        Set(value As String)
            Me._ApplyTimestampFormat(value)
        End Set
    End Property

    ''' <summary> Gets or sets the timestamp string format. </summary>
    ''' <value> The timestamp string format. </value>
    Public Property TimestampStringFormat As String

    ''' <summary> Gets the start time caption. </summary>
    ''' <value> The start time caption. </value>
    Public ReadOnly Property StartTimeCaption As String
        Get
            Return Me.ToTimestampCaption(Me.StartTime)
        End Get
    End Property

    ''' <summary> Gets the End time caption. </summary>
    ''' <value> The End time caption. </value>
    Public ReadOnly Property EndTimeCaption As String
        Get
            Return Me.ToTimestampCaption(Me.EndTime)
        End Get
    End Property

    ''' <summary> Converts a timestamp to a caption. </summary>
    ''' <param name="timestamp"> The timestamp Date/Time. </param>
    ''' <returns> timestamp as a String. </returns>
    Public Function ToTimestampCaption(ByVal timestamp As Date) As String
        Return TimespanKeeper.ToTimestampCaption(Me.TimestampStringFormat, timestamp)
    End Function

    ''' <summary> Converts timestamp to date. </summary>
    ''' <param name="timestamp"> The timestamp Date/Time. </param>
    ''' <returns> A Date. </returns>
    Public Function FromTimestampCaption(ByVal timestamp As String) As Date
        Return TimespanKeeper.FromTimestampCaption(Me.TimestampFormat, timestamp)
    End Function

    ''' <summary> Converts a timestamp to a caption. </summary>
    ''' <param name="timestamp"> The timestamp Date/Time. </param>
    ''' <returns> timestamp as a String. </returns>
    Public Shared Function ToDefaultTimestampCaption(ByVal timestamp As Date) As String
        Return TimespanKeeper.ToTimestampCaption(TimespanKeeper.DefaultTimestampStringFormat, timestamp)
    End Function

    ''' <summary> Converts timestamp to date. </summary>
    ''' <param name="timestamp"> The timestamp Date/Time. </param>
    ''' <returns> A Date. </returns>
    Public Shared Function FromDefaultTimestampCaption(ByVal timestamp As String) As Date
        Return TimespanKeeper.FromTimestampCaption(TimespanKeeper.DefaultTimestampFormat, timestamp)
    End Function

    ''' <summary> Converts a timestamp to a caption. </summary>
    ''' <param name="format">    The ElapsedTime Date/Time. </param>
    ''' <param name="timestamp"> The timestamp Date/Time. </param>
    ''' <returns> timestamp as a String. </returns>
    Public Shared Function ToTimestampCaption(ByVal format As String, ByVal timestamp As Date) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, format, timestamp)
    End Function

    ''' <summary> Converts timestamp to date. </summary>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="timestamp"> The timestamp Date/Time. </param>
    ''' <returns> A Date. </returns>
    Public Shared Function FromTimestampCaption(ByVal format As String, ByVal timestamp As String) As Date
        Dim value As Date = Date.MinValue
        If Date.TryParseExact(timestamp, format,
                              Globalization.CultureInfo.CurrentCulture,
                              Globalization.DateTimeStyles.AssumeLocal, value) Then
        End If
        Return value
    End Function

#End Region

#Region " ELAPSED TIME "

    Public Const DefaultElapsedTimeFormat As String = "HH:mm:ss.fff"
    Public Const DefaultElapsedTimeStringFormat As String = "{0:" & TimespanKeeper.DefaultElapsedTimeFormat & "}"

    ''' <summary> Applies the elapsed time format described by value. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub _ApplyElapsedTimeFormat(ByVal value As String)
        Me._ElapsedTimeFormat = value
        Me._ElapsedTimeStringFormat = "{0:" & Me.ElapsedTimeFormat & "}"
    End Sub

    Private _ElapsedTimeFormat As String
    ''' <summary> Gets or sets the ElapsedTime format. </summary>
    ''' <value> The ElapsedTime format. </value>
    Public Property ElapsedTimeFormat As String
        Get
            Return Me._ElapsedTimeFormat
        End Get
        Set(value As String)
            Me._ApplyElapsedTimeFormat(value)
        End Set
    End Property

    ''' <summary> Gets or sets the ElapsedTime string format. </summary>
    ''' <value> The ElapsedTime string format. </value>
    Public Property ElapsedTimeStringFormat As String

    ''' <summary> Gets the start time caption. </summary>
    ''' <value> The start time caption. </value>
    Public ReadOnly Property ElapsedTimeCaption As String
        Get
            Return Me.ToElapsedTimeCaption(Me.ElapsedTimespan)
        End Get
    End Property

    ''' <summary> Converts a ElapsedTime to a caption. </summary>
    ''' <param name="ElapsedTime"> The ElapsedTime Date/Time. </param>
    ''' <returns> ElapsedTime as a String. </returns>
    Public Function ToElapsedTimeCaption(ByVal elapsedTime As TimeSpan) As String
        Return TimespanKeeper.ToElapsedTimeCaption(Me.ElapsedTimeStringFormat, elapsedTime)
    End Function

    ''' <summary> Converts ElapsedTime to date. </summary>
    ''' <param name="ElapsedTime"> The ElapsedTime Date/Time. </param>
    ''' <returns> A Date. </returns>
    Public Function FromElapsedTimeCaption(ByVal elapsedTime As String) As TimeSpan
        Return TimespanKeeper.FromElapsedTimeCaption(Me.ElapsedTimeFormat, elapsedTime)
    End Function

    ''' <summary> Converts a ElapsedTime to a caption. </summary>
    ''' <param name="ElapsedTime"> The ElapsedTime Date/Time. </param>
    ''' <returns> ElapsedTime as a String. </returns>
    Public Shared Function ToDefaultElapsedTimeCaption(ByVal elapsedTime As TimeSpan) As String
        Return TimespanKeeper.ToElapsedTimeCaption(TimespanKeeper.DefaultElapsedTimeStringFormat, elapsedTime)
    End Function

    ''' <summary> Converts ElapsedTime to date. </summary>
    ''' <param name="ElapsedTime"> The ElapsedTime Date/Time. </param>
    ''' <returns> A Date. </returns>
    Public Shared Function FromDefaultElapsedTimeCaption(ByVal elapsedTime As String) As TimeSpan
        Return TimespanKeeper.FromElapsedTimeCaption(TimespanKeeper.DefaultElapsedTimeFormat, elapsedTime)
    End Function

    ''' <summary> Converts a ElapsedTime to a caption. </summary>
    ''' <param name="format">      The ElapsedTime Date/Time. </param>
    ''' <param name="elapsedTime"> The elapsed time. </param>
    ''' <returns> ElapsedTime as a String. </returns>
    Public Shared Function ToElapsedTimeCaption(ByVal format As String, ByVal elapsedTime As TimeSpan) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, format, elapsedTime)
    End Function

    ''' <summary> Converts ElapsedTime to date. </summary>
    ''' <param name="ElapsedTime"> The ElapsedTime Date/Time. </param>
    ''' <returns> A Date. </returns>
    Public Shared Function FromElapsedTimeCaption(ByVal format As String, ByVal elapsedTime As String) As TimeSpan
        Dim value As TimeSpan = TimeSpan.Zero
        If TimeSpan.TryParseExact(elapsedTime, format,
                                  Globalization.CultureInfo.CurrentCulture, value) Then
        End If
        Return value
    End Function


#End Region
End Class
