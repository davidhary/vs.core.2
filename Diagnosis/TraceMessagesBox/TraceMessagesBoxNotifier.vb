﻿Imports System.ComponentModel
Imports isr.Core.Pith
Partial Public Class TraceMessagesBox

    Private WithEvents _TraceMessageLogNotifier As Pith.ITraceMessageNotify

    ''' <summary> Registers the notifier described by notifier. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    ''' <param name="notifier"> The notifier. </param>
    Public Sub RegisterNotifier(ByVal notifier As Pith.ITraceMessageNotify)
        Me._TraceMessageLogNotifier = notifier
    End Sub

    ''' <summary> Raises the property changed event. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Private Sub OnTraceMessageChanged(sender As ITraceMessageNotify, e As PropertyChangedEventArgs)
        If sender IsNot Nothing AndAlso String.Equals(e?.PropertyName, NameOf(sender.TraceMessage)) Then
            Me.AddMessage(sender.TraceMessage)
        End If
    End Sub

    Private Sub _TraceMessageLogNotifier_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _TraceMessageLogNotifier.PropertyChanged
        Try
            Me.OnTraceMessageChanged(TryCast(sender, Pith.ITraceMessageNotify), e)
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Failed adding message",
                         New StackTrace(True).ToString.Split(Environment.NewLine.ToCharArray())(0))
        End Try
    End Sub

End Class
