Imports System.ComponentModel
Imports isr.Core.Pith
''' <summary> A message logging text box. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/04/2013" by="David" revision="1.2.4955"> created based on the legacy
''' messages box. </history>
<System.ComponentModel.Description("Trace messages Text Box")>
Public Class TraceMessagesBox
    Inherits MessagesBox

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Constructor for this class. </summary>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        Me._TraceMessageFormat = "{0}, {1:X}, {2:HH:mm:ss.fff}, {3}"
        Me._AlertLevel = TraceEventType.Warning

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the
    ''' <see cref="T:System.Windows.Forms.TextBox" /> and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId:="components")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then Me.components?.Dispose() : Me.components = Nothing
                ' release the notifier
                Me._TraceMessageLogNotifier = Nothing
            End If
        Finally
            ' Invoke the base class dispose method        
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DISPLAY AND MESSAGE SENTINELS "

    Private _AlertLevel As TraceEventType

    ''' <summary> Gets or sets the alert level. Message <see cref="TraceEventType">levels</see> equal
    ''' or lower than this are tagged as alerts and set the <see cref="AlertsAdded">alert
    ''' sentinel</see>. </summary>
    ''' <value> The alert level. </value>
    <Category("Appearance"), Description("Level for notifying of alerts"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("TraceEventType.Warning")>
    Public Property AlertLevel As TraceEventType
        Get
            Return Me._AlertLevel
        End Get
        Set(value As TraceEventType)
            If value <> Me.AlertLevel Then
                Me._AlertLevel = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.AlertLevel))
            End If
        End Set
    End Property

    Private _AlertsAdded As Boolean

    ''' <summary> Gets or sets the sentinel indicating that new messages were added. </summary>
    ''' <value> The new messages available. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property AlertsAdded As Boolean
        Get
            Return Me._AlertsAdded
        End Get
        Set(value As Boolean)
            If value <> Me.AlertsAdded Then
                Me._AlertsAdded = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.AlertsAdded))
            End If
        End Set
    End Property


    ''' <summary> Displays the available lines and clears the alerts and message sentinels. </summary>
    Public Overrides Sub Display()
        If Me.Visible Then
            Me.AlertsAdded = False
        End If
        MyBase.Display()
    End Sub

    ''' <summary> Executes the clear action. </summary>
    Protected Overrides Sub OnClear()
        Me.AlertsAdded = False
        MyBase.OnClear()
    End Sub

#End Region

#Region " ADD MESSAGE "

    ''' <summary> Determines if the trace event type should be alerted. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> if the trace event type should be alerted. </returns>
    Protected Function ShouldAlert(ByVal eventType As TraceEventType) As Boolean
        Return eventType <= Me.AlertLevel
    End Function

    ''' <summary> Adds a message to the display. </summary>
    ''' <param name="value">  The value. </param>
    Public Overloads Sub AddMessage(ByVal value As TraceMessage)
        If value Is Nothing OrElse String.IsNullOrWhiteSpace(value.Details) Then
            Debug.Assert(Not Debugger.IsAttached, "Empty trace message @",
                         New StackTrace(True).ToString.Split(Environment.NewLine.ToCharArray())(0))
        Else
            ' sustains the alerts flag until cleared.
            Me.AlertsAdded = Not Me.UserVisible AndAlso (Me.AlertsAdded OrElse Me.ShouldAlert(value.EventType))
            Me.AddMessage(value.ToString(Me.TraceMessageFormat))
        End If
    End Sub

    ''' <summary> Adds a message to the display. </summary>
    ''' <param name="condition">  True to add the message or false to ignore. </param>
    ''' <param name="value"> Specifies the <see cref="TraceMessage">message</see>. </param>
    Public Overloads Sub AddMessage(ByVal condition As Boolean, ByVal value As TraceMessage)
        If condition Then
            Me.AddMessage(value)
        End If
    End Sub

    ''' <summary> Gets or sets the default format for displaying the message. </summary>
    ''' <remarks> The format must include 4 elements to display the first two characters of
    ''' the trace event type, the id, timestamp and message details. For example,<code>
    ''' "{0}, {1:X}, {2:HH:mm:ss.fff}, {3}"</code>. </remarks>
    ''' <value> The trace message format. </value>
    <Category("Appearance"), Description("Formats the test message"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("{0}, {1:X}, {2:HH:mm:ss.fff}, {3}")>
    Public Property TraceMessageFormat As String

    ''' <summary> Adds an error message to the message box. </summary>
    ''' <param name="value"> The <see cref="System.Exception">error</see> to add. </param>
    Public Overloads Sub AddMessage(ByVal value As Exception)

        If Not (value Is Nothing OrElse String.IsNullOrWhiteSpace(value.Message)) Then

            If value.InnerException IsNot Nothing Then
                Me.AddMessage(value.InnerException)
            End If

            Dim messageBuilder As New System.Text.StringBuilder
            messageBuilder.Append(value.Message)
            If value IsNot Nothing AndAlso value.StackTrace IsNot Nothing Then
                messageBuilder.AppendLine()
                messageBuilder.Append("Stack Trace ")
                messageBuilder.AppendLine()
                messageBuilder.Append(value.StackTrace)
            End If
            If value.Data IsNot Nothing AndAlso value.Data.Count > 0 Then
                messageBuilder.AppendLine()
                messageBuilder.Append("Data: ")
                For Each keyValuePair As System.Collections.DictionaryEntry In value.Data
                    messageBuilder.AppendLine()
                    messageBuilder.Append(keyValuePair.Key.ToString & "=" & keyValuePair.Value.ToString)
                Next
            End If

            Me.AddMessage(messageBuilder.ToString)

        End If

    End Sub

#End Region

End Class


