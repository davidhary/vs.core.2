﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 11/26/2015. </remarks>
    Partial Friend NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ' ''' <summary> Gets the identifier of the trace source. </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")>
        Public Const TraceEventId As Integer = Pith.My.ProjectTraceEventId.Pith + 1

        Public Const AssemblyTitle As String = "Core Diagnosis Library"
        Public Const AssemblyDescription As String = "Core Diagnosis Library"
        Public Const AssemblyProduct As String = "Core.Diagnosis.2016"

    End Class

End Namespace

