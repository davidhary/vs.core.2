﻿Imports isr.Core.Pith
''' <summary> The contract implemented by the class publishing messages. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/04/2013" by="David" revision="1.2.4955"> created based on the legacy
''' extended message. </history>
Public Interface ITraceMessagePublisher

    ''' <summary> Occurs when a message is available. </summary>
    Event TraceMessageAvailable As EventHandler(Of TraceMessageEventArgs)

    ''' <summary> Raises the <see cref="E:TraceMessageAvailable" /> event. </summary>
    ''' <param name="e"> The <see cref="TraceMessageEventArgs" /> instance containing the event data. </param>
    Sub OnTraceMessageAvailable(ByVal e As TraceMessageEventArgs)

    ''' <summary> Publishes trace message. </summary>
    ''' <param name="value"> Trace message to publish. </param>
    Sub OnTraceMessageAvailable(ByVal value As TraceMessage)

    ''' <summary> Builds and publishes a trace message. </summary>
    ''' <param name="eventType"> The trace event type. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    The format. </param>
    ''' <param name="args">      The arguments. </param>
    ''' <returns> The built <see cref="TraceMessage">trace message</see>. </returns>
    Function OnTraceMessageAvailable(ByVal eventType As TraceEventType, ByVal id As Integer,
                                     ByVal format As String, ByVal ParamArray args() As Object) As TraceMessage

End Interface

