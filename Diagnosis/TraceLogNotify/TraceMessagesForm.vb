﻿Imports System.ComponentModel
Imports isr.Core.Pith
''' <summary> Form for viewing the messages. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="10/14/2014" by="David" revision=""> Created. </history>
Public Class TraceMessagesForm
    Inherits FormBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 12/12/2015. </remarks>
    ''' <param name="publisher"> The publisher. </param>
    Public Sub New(ByVal publisher As TraceMessageLogNotifier)

        MyBase.New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me._TraceMessageNotifier = publisher

        If Version.Parse(My.Computer.Info.OSVersion).Major <= EnableDropShadowVersion Then
            Me.ClassStyle = Core.Pith.ClassStyleConstants.DropShadow
        End If
        Me.ClassStyle = Me.ClassStyle Or Core.Pith.ClassStyleConstants.HideCloseButton

    End Sub

    ''' <summary> Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />. </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    ''' release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId:="components")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then Me.components?.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary> Displays this dialog with title 'illegal call'. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2222:DoNotDecreaseInheritedMemberVisibility")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Shadows Function ShowDialog() As Windows.Forms.DialogResult
        Me.Text = "Illegal Call"
        Return MyBase.ShowDialog()
    End Function

    ''' <summary> Displays this dialog with title 'illegal call'. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2222:DoNotDecreaseInheritedMemberVisibility")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Shadows Sub Show()
        Me.Text = "Illegal Call"
        MyBase.Show()
    End Sub

    ''' <summary> Shows the message box with these messages. </summary>
    ''' <param name="owner">   The owner. </param>
    ''' <param name="caption"> The caption. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="details")>
    Public Shadows Sub Show(ByVal mdiForm As Windows.Forms.Form, ByVal owner As System.Windows.Forms.IWin32Window, ByVal caption As String)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        Me.Text = caption
        MyBase.Show(owner)
    End Sub

    ''' <summary> Shows the message box with these messages. </summary>
    ''' <param name="caption"> The caption. </param>
    Public Shadows Sub Show(ByVal mdiForm As Windows.Forms.Form, ByVal caption As String)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        Me.Text = caption
        MyBase.Show()
    End Sub

#End Region

#Region " TRACE MESSAGE NOTIFICATIONS "

    Private WithEvents _TraceMessageNotifier As TraceMessageLogNotifier

    Private Sub _TraceMessageNotifier_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _TraceMessageNotifier.PropertyChanged
        Me.OnPropertyChanged(TryCast(sender, TraceMessageLogNotifier), e)
    End Sub

    ''' <summary> Raises the system. component model. property changed event. </summary>
    ''' <remarks> David, 12/12/2015. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Private Sub OnPropertyChanged(sender As TraceMessageLogNotifier, e As PropertyChangedEventArgs)
        If sender IsNot Nothing AndAlso e IsNot Nothing Then
            Select Case e.PropertyName
                Case NameOf(sender.TraceMessage)
                    If Me._TraceMessagesBox IsNot Nothing Then
                        Me._TraceMessagesBox.AddMessage(sender.TraceMessage)
                    End If
            End Select
        End If
    End Sub

#End Region

End Class