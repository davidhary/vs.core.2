﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class AboutProgram

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._GroupBox = New System.Windows.Forms.GroupBox()
        Me._TextBox = New System.Windows.Forms.RichTextBox()
        Me._Layout.SuspendLayout()
        Me._GroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6.0!))
        Me._Layout.Controls.Add(Me._GroupBox, 1, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(0, 0)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6.0!))
        Me._Layout.Size = New System.Drawing.Size(799, 624)
        Me._Layout.TabIndex = 0
        '
        '_GroupBox
        '
        Me._GroupBox.Controls.Add(Me._TextBox)
        Me._GroupBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._GroupBox.Location = New System.Drawing.Point(9, 9)
        Me._GroupBox.Name = "_GroupBox"
        Me._GroupBox.Size = New System.Drawing.Size(781, 606)
        Me._GroupBox.TabIndex = 1
        Me._GroupBox.TabStop = False
        Me._GroupBox.Text = "ABOUT"
        '
        '_TextBox
        '
        Me._TextBox.BackColor = System.Drawing.SystemColors.Info
        Me._TextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._TextBox.Location = New System.Drawing.Point(3, 21)
        Me._TextBox.Name = "_TextBox"
        Me._TextBox.Size = New System.Drawing.Size(775, 582)
        Me._TextBox.TabIndex = 0
        Me._TextBox.Text = ""
        '
        'AboutProgram
        '
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me._Layout)
        Me.Name = "AboutProgram"
        Me.Size = New System.Drawing.Size(799, 624)
        Me._Layout.ResumeLayout(False)
        Me._GroupBox.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Layout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _TextBox As System.Windows.Forms.RichTextBox
    Private WithEvents _GroupBox As System.Windows.Forms.GroupBox

End Class
