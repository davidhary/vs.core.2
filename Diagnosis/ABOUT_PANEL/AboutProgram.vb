﻿Imports System.Drawing
''' <summary> Displays program information. </summary>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
<System.ComponentModel.Description("About Program"), System.ComponentModel.DefaultEvent("Validated")>
Public Class AboutProgram
    Inherits Pith.MyUserControlBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> A constructor for this panel. </summary>
    Public Sub New()

        ' instantiate the base class
        MyBase.New()

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' this is required to update the fonts for any items that did not exit when the base class is created.
        Me.Font = MyBase.Font

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Diagnosis.AboutProgram and optionally
    ''' releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId:="_ContentsFont")>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId:="_HeaderFont")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    Try
                        Me._ContentsFont?.Dispose() : Me._ContentsFont = Nothing
                    Catch ex As Exception
                        Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                    End Try
                    Try
                        Me._HeaderFont?.Dispose() : Me._HeaderFont = Nothing
                    Catch ex As Exception
                        Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                    End Try
                    Me.DisposeEventHandler(Me.DisplayingCopyrightsEvent)
                    Me.components?.Dispose() : Me.components = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DISPLAY "

    ''' <summary> Gets or sets the font of the text displayed by the control. </summary>
    ''' <value> The <see cref="T:System.Drawing.Font" /> to apply to the text displayed by the
    ''' control. The default is the value of the
    ''' <see cref="P:System.Windows.Forms.Control.DefaultFont" /> property. </value>
    Public Overrides Property Font As System.Drawing.Font
        Get
            Return MyBase.Font
        End Get
        Set(ByVal value As System.Drawing.Font)
            MyBase.Font = value
            If Me._GroupBox IsNot Nothing Then
                Me._GroupBox.Font = Font
            End If
        End Set
    End Property

    <System.ComponentModel.Description("The group box title of the control."), System.ComponentModel.Category("Appearance"),
    System.ComponentModel.DefaultValue("ABOUT")>
    Public Property Title As String
        Get
            If Me._GroupBox Is Nothing Then
                Return ""
            Else
                Return Me._GroupBox.Text
            End If
        End Get
        Set(ByVal value As String)
            If Me._GroupBox IsNot Nothing Then
                Me._GroupBox.Text = value
            End If
        End Set
    End Property

    Private _HeaderFont As Font
    ''' <summary> Gets or sets the header font. </summary>
    ''' <value> The header font. </value>
    <System.ComponentModel.Description("The font of the header lines."), System.ComponentModel.Category("Appearance"),
    System.ComponentModel.DefaultValue(GetType(Font), "Me.Font")>
    Public Property HeaderFont As Font
        Get
            If Me._HeaderFont Is Nothing Then
                Me._HeaderFont = New Font(Me.Font, FontStyle.Regular)
            End If
            Return Me._HeaderFont
        End Get
        Set(ByVal value As Font)
            Me._HeaderFont = value
        End Set
    End Property

    Private _ContentsFont As Font

    ''' <summary> Gets or sets the Contents font. </summary>
    ''' <value> The Contents font. </value>
    <System.ComponentModel.Description("The font of the contents lines."), System.ComponentModel.Category("Appearance")>
    Public Property ContentsFont As Font
        Get
            If Me._ContentsFont Is Nothing Then
                Me._ContentsFont = New Font(Me.Font, FontStyle.Bold)
            End If
            Return Me._ContentsFont
        End Get
        Set(ByVal value As Font)
            Me._ContentsFont = value
        End Set
    End Property

    ''' <summary> Appends a header line. </summary>
    ''' <param name="text"> The text. </param>
    Public Sub AppendHeaderLine(ByVal text As String)
        Me.AppendLine(text, Me.HeaderFont)
    End Sub

    ''' <summary> Appends a content line. </summary>
    ''' <param name="text"> The text. </param>
    ''' <param name="font"> The font. </param>
    Public Sub AppendLine(ByVal text As String, ByVal font As Font)
        Me._TextBox.SelectionStart = Me._TextBox.TextLength
        Me._TextBox.SelectionFont = font
        Me._TextBox.AppendText(text)
        Me._TextBox.AppendText(Environment.NewLine)
    End Sub

    ''' <summary> Appends a content line. </summary>
    ''' <param name="text"> The text. </param>
    Public Sub AppendContentLine(ByVal text As String)
        Me.AppendLine(text, Me.ContentsFont)
    End Sub

    ''' <summary> Updates the display. </summary>
    Public Sub Display()

        Me._TextBox.Clear()

        Me.AppendHeaderLine("Product Name:")
        Me.AppendContentLine(My.Application.Info.ProductName)
        Me.AppendContentLine("")

        Me.AppendHeaderLine("Program Title:")
        Me.AppendContentLine(My.Application.Info.Title)
        Me.AppendContentLine("")

        Me.AppendHeaderLine("Program Name:")
        Me.AppendContentLine(My.Application.Info.AssemblyName)
        Me.AppendContentLine("")

        Me.AppendHeaderLine("Program Version:")
        Me.AppendContentLine(My.Application.Info.Version.ToString)
        Me.AppendContentLine("")

        Me.AppendHeaderLine("Program Folder:")
        Me.AppendContentLine(My.Application.Info.DirectoryPath)
        Me.AppendContentLine("")

        Me.AppendHeaderLine("Program Data Folder:")
        Me.AppendContentLine(My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData)
        Me.AppendContentLine("")

        Me.AppendHeaderLine("User Program Data Folder:")
        Me.AppendContentLine(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData)
        Me.AppendContentLine("")

        Dim configuration As System.Configuration.Configuration = Global.System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None)
        Me.AppendHeaderLine("All Users Configuration File:")
        Me.AppendContentLine(configuration.FilePath())
        If configuration.Locations.Count > 0 Then
            Me.AppendHeaderLine("Configuration Locations:")
            For Each s As System.Configuration.ConfigurationLocation In configuration.Locations
                Me.AppendContentLine(s.Path)
            Next
        End If
        Me.AppendContentLine("")

        configuration = Global.System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.PerUserRoaming)
        Me.AppendHeaderLine("User Roaming Configuration File:")
        Me.AppendContentLine(configuration.FilePath())
        If configuration.Locations.Count > 0 Then
            Me.AppendHeaderLine("Configuration Locations:")
            For Each s As System.Configuration.ConfigurationLocation In configuration.Locations
                Me.AppendContentLine(s.Path)
            Next
        End If
        Me.AppendContentLine("")

        configuration = Global.System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.PerUserRoamingAndLocal)
        Me.AppendHeaderLine("User Local Configuration File:")
        Me.AppendContentLine(configuration.FilePath())
        If configuration.Locations.Count > 0 Then
            Me.AppendHeaderLine("Configuration Locations:")
            For Each s As System.Configuration.ConfigurationLocation In configuration.Locations
                Me.AppendContentLine(s.Path)
            Next
        End If
        Me.AppendContentLine("")

        ' allows the calling application to add data.
        Me.OnDisplayingCopyrights(System.EventArgs.Empty)

        Me.AppendHeaderLine("Product Copyrights:")
        Me.AppendContentLine(My.Application.Info.Copyright)
        Me.AppendContentLine("")

    End Sub

#End Region

#Region " EVENTS "

    ''' <summary> Raised before displaying the copyrights. </summary>
    '''   <param name="e">The reading event arguments.</param>
    Public Event DisplayingCopyrights As EventHandler(Of EventArgs)

    ''' <summary> Raises the displaying Copyrights event. </summary>
    ''' <param name="e"> The reading event arguments. </param>
    Friend Sub OnDisplayingCopyrights(ByVal e As EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.DisplayingCopyrightsEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Handler, called when the dispose event. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    ''' <param name="value"> The handler. </param>
    Private Sub DisposeEventHandler(ByVal value As EventHandler(Of EventArgs))
        Try
            For Each d As [Delegate] In value.SafeInvocationList
                Try
                    RemoveHandler Me.DisplayingCopyrights, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                Catch
                End Try
            Next
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        End Try
    End Sub
#End Region

End Class
