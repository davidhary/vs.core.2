Imports System.ComponentModel
Imports System.Windows.Forms
''' <summary> Messages display text box. </summary>
''' <license> (c) 2002 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/21/2002" by="David" revision="1.0.839.x"> created. </history>
<Description("Messages Test Box")>
Public Class MessagesBox
    Inherits TextBox
    Implements INotifyPropertyChanged

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Constructor for this class. </summary>
    Public Sub New()
        MyBase.New()

        ' set defaults
        Me.BackColor = Drawing.SystemColors.Info
        Me.Multiline = True
        Me.ReadOnly = True
        Me.CausesValidation = False
        Me.ScrollBars = ScrollBars.Both
        Me.Size = New System.Drawing.Size(150, 150)

        Me.syncLocker = New Object
        Me._lines = New List(Of String)
        Me.ResetCount = 200
        Me.PresetCount = 100
        Me.Appending = False

        Me.TabCaption = "Log"
        Me.CaptionFormat = "{0} " & System.Text.Encoding.GetEncoding(437).GetString(New Byte() {240})

        MyBase.ContextMenuStrip = createContextMenuStrip()

        Me.ContentsQueue = New Queue(Of String)
        Me.InitializeWorker()

    End Sub

    ''' <summary> Releases the unmanaged resources used by the
    ''' <see cref="T: TextBox" /> and optionally releases the managed resources. </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    ''' release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    Me.DisposeWorker()
                    Me._ContentsQueue?.Clear() : Me._ContentsQueue = Nothing
                    Me._lines?.Clear() : Me._lines = Nothing

                    Me._MyContextMenuStrip?.Dispose() : Me._MyContextMenuStrip = Nothing

                    Me.DisposeEventHandler(Me.PropertyChangedEvent)
                End If
            End If
        Finally
            ' Invoke the base class dispose method        
            MyBase.Dispose(disposing)
        End Try
    End Sub


#End Region

#Region " LIST MANAGER "

    Private _Appending As Boolean
    ''' <summary> Gets the appending sentinel. Items are appended to an appending list. 
    '''           Otherwise, items are added to the top of the list. </summary>
    ''' <value> The ascending sentinel; True if the list is ascending or descending. </value>
    <Category("Appearance"), Description("True to add items to the bottom of the list"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(False)>
    Public Property Appending As Boolean
        Get
            Return Me._Appending
        End Get
        Set(value As Boolean)
            If value <> Me.Appending Then
                Me._Appending = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.Appending))
            End If
        End Set
    End Property

    ''' <summary> The synchronization locker. </summary>
    Private syncLocker As Object

    ''' <summary> Holds the list of lines to display. </summary>
    Private _lines As List(Of String)

    ''' <summary> Adds the message to the message list. A message may include one or more lines. </summary>
    ''' <param name="message"> The message to add. </param>
    Private Sub AddMessageLines(ByVal message As String)
        If String.IsNullOrWhiteSpace(message) Then
            message = ""
        End If
        Dim values As String() = message.Split(CChar(Environment.NewLine))
        If values.Count = 1 Then
            If Me.Appending Then
                Me._lines.Add(message)
            Else
                Me._lines.Insert(0, message)
            End If
        Else
            If Me.Appending Then
                Me._lines.AddRange(values)
            Else
                Me._lines.InsertRange(0, values)
            End If
        End If
    End Sub

    ''' <summary> Executes the clear action. </summary>
    Protected Overridable Sub OnClear()
        Me._lines.Clear()
        MyBase.Clear()
        Me.NewMessagesAdded = False
    End Sub

    ''' <summary> Clears all text from the text box control. </summary>
    Public Shadows Sub Clear()
        If Me.InvokeRequired Then
            Me.Invoke(New Action(AddressOf Me.Clear), New Object() {})
        Else
            If Not Me.IsDisposed AndAlso Me._lines IsNot Nothing Then
                SyncLock syncLocker
                    Me.OnClear()
                End SyncLock
            End If
        End If
    End Sub

    Private _resetCount As Integer

    ''' <summary> Gets or sets the reset count. </summary>
    ''' <remarks> The message list gets reset to the preset count when the message count exceeds the
    ''' reset count. </remarks>
    ''' <value> <c>ResetSize</c>is an integer property. </value>
    <Category("Appearance"), Description("Number of lines at which to reset"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("100")>
    Public Property ResetCount() As Integer
        Get
            Return Me._resetCount
        End Get
        Set(ByVal value As Integer)
            If Me.ResetCount <> value Then
                Me._resetCount = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.ResetCount))
            End If
        End Set
    End Property

    Private _presetCount As Integer

    ''' <summary> Gets or sets the preset count. </summary>
    ''' <remarks> The message list gets reset to the preset count when the message count exceeds the
    ''' reset count. </remarks>
    ''' <value> <c>PresetSize</c>is an integer property. </value>
    <Category("Appearance"), Description("Number of lines to reset to"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("50")>
    Public Property PresetCount() As Integer
        Get
            Return Me._presetCount
        End Get
        Set(ByVal value As Integer)
            If Me.PresetCount <> value Then
                Me._presetCount = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.PresetCount))
            End If
        End Set
    End Property

    ''' <summary> Gets the sentinel indicating if the control is visible to the user. </summary>
    ''' <value> The showing. </value>
    Protected ReadOnly Property UserVisible As Boolean
        Get
            Return Me.Visible AndAlso (Me.GetContainerControl IsNot Nothing AndAlso
                                       Me.GetContainerControl.ActiveControl IsNot Nothing AndAlso Me.GetContainerControl.ActiveControl.Visible)
        End Get
    End Property

    ''' <summary> Displays the available lines and clear the <see cref="NewMessagesAdded">message
    ''' sentinel</see>. </summary>
    Public Overridable Sub Display()
        If Me.UserVisible Then
            Me.NewMessagesAdded = False
            Me.Lines = Me._lines.ToArray
        End If
    End Sub

    ''' <summary> Raises the <see cref="E: Control.VisibleChanged" /> event. 
    '''           Updates the display. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnVisibleChanged(e As System.EventArgs)
        Me.Display()
        MyBase.OnVisibleChanged(e)
    End Sub

#End Region

#Region " DISPLAY WORKER "

    ''' <summary> Gets a queue of contents. </summary>
    ''' <value> A Queue of contents. </value>
    Private Property ContentsQueue As Queue(Of String)

    ''' <summary> This event handler sets the Text property of the TextBox control. It is called on the
    ''' thread that created the TextBox control, so the call is thread-safe. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Run worker completed event information. </param>
    Private Sub UpdateDisplay(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles worker.RunWorkerCompleted

        If Not (Me.IsDisposed OrElse e.Cancelled OrElse e.Error IsNot Nothing) Then
            SyncLock syncLocker
                Do While Me.ContentsQueue.Count > 0
                    Me.AddMessageLines(Me.ContentsQueue.Dequeue)
                Loop
                If Me._lines.Count > Me.ResetCount Then
                    If Me.Appending Then
                        Me._lines.RemoveRange(0, Me._lines.Count - Me.PresetCount)
                    Else
                        Me._lines.RemoveRange(Me.PresetCount, Me._lines.Count - Me.PresetCount)
                    End If
                End If
                Me.NewMessagesAdded = Not Me.UserVisible
            End SyncLock
            Me.Display()
            Me.Invalidate()
            Application.DoEvents()
        End If
    End Sub

    ''' <summary> Uses the message worker to flush any queued messages. </summary>
    ''' <remarks> If the calling thread is different from the thread that created the TextBox control,
    ''' this method creates a SetTextCallback and calls itself asynchronously using the Invoke
    ''' method. </remarks>
    Public Sub FlushMessages()
        If Me.InvokeRequired Then
            Me.Invoke(New Action(AddressOf Me.FlushMessages), New Object() {})
        Else
            If Not (Me.IsDisposed OrElse Me.worker.IsBusy) Then
                Me.worker.RunWorkerAsync()
                Application.DoEvents()
            End If
        End If
    End Sub

#End Region

#Region " ADD MESSAGE "

    ''' <summary> Prepends or appends a new value to the messages box. </summary>
    ''' <remarks> Starts a Background Worker by calling RunWorkerAsync. The Text property of
    ''' the TextBox control is set when the Background Worker raises the RunWorkerCompleted event. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub AddMessage(ByVal value As String)
        Me.ContentsQueue.Enqueue(value)
        Me.FlushMessages()
    End Sub

    ''' <summary> Prepends or appends a new message to the messages list. </summary>
    ''' <remarks> Starts a Background Worker by calling RunWorkerAsync. The Text property of
    ''' the TextBox control is set when the Background Worker raises the RunWorkerCompleted event. </remarks>
    ''' <param name="condition">      True to add the message or false to ignore. </param>
    ''' <param name="value">          The value. </param>
    Public Sub AddMessage(ByVal condition As Boolean, ByVal value As String)
        If condition Then
            Me.AddMessage(value)
        End If
    End Sub

#End Region

#Region " CAPTION "

    Private _NewMessagesAdded As Boolean

    ''' <summary> Gets or sets the sentinel indicating that new messages were added. </summary>
    ''' <value> The new messages available. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property NewMessagesAdded As Boolean
        Get
            Return Me._NewMessagesAdded
        End Get
        Set(value As Boolean)
            If value <> Me.NewMessagesAdded Then
                Me._NewMessagesAdded = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.NewMessagesAdded))
                Me.UpdateCaption()
            End If
        End Set
    End Property

    Private _TabCaption As String

    ''' <summary> Gets or sets the tab caption. </summary>
    ''' <value> The tab caption. </value>
    <Category("Appearance"), Description("Default title for the parent tab"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("Messages")>
    Public Property TabCaption() As String
        Get
            Return Me._TabCaption
        End Get
        Set(ByVal Value As String)
            If Me.TabCaption <> Value Then
                Me._TabCaption = Value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.TabCaption))
                Me.UpdateCaption()
            End If
        End Set
    End Property

    Private _Captionformat As String

    ''' <summary> Gets or sets the caption format indicating that messages were added. </summary>
    ''' <value> The tab caption format. </value>
    <Category("Appearance"), Description("Formats the tab caption with number of new messages"), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue("{0} ≡")>
    Public Property CaptionFormat() As String
        Get
            Return Me._Captionformat
        End Get
        Set(ByVal Value As String)
            If String.IsNullOrEmpty(Value) Then Value = ""
            If Not Value.Equals(Me.CaptionFormat) Then
                Me._Captionformat = Value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.CaptionFormat))
                Me.UpdateCaption()
            End If
        End Set
    End Property

    Private Property _Caption As String

    ''' <summary> Gets or sets the caption. </summary>
    ''' <value> The caption. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property Caption As String
        Get
            Return Me._Caption
        End Get
        Set(value As String)
            If String.IsNullOrEmpty(value) Then value = ""
            If Not value.Equals(Me.Caption) Then
                Me._Caption = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.Caption))
            End If
        End Set
    End Property

    ''' <summary> Updates the caption. </summary>
    Private Sub UpdateCaption()
        If True = Me.NewMessagesAdded Then
            Me.Caption = String.Format(Globalization.CultureInfo.CurrentCulture, Me.CaptionFormat, Me.TabCaption)
        Else
            Me.Caption = Me.TabCaption
        End If
    End Sub

#End Region

#Region " CONTEXT MENU STRIP "

    Dim _MyContextMenuStrip As Windows.Forms.ContextMenuStrip

    ''' <summary> Creates a context menu strip. </summary>
    ''' <returns> The new context menu strip. </returns>
    Private Function createContextMenuStrip() As Windows.Forms.ContextMenuStrip

        ' Create a new ContextMenuStrip control.
        Me._MyContextMenuStrip = New Windows.Forms.ContextMenuStrip()

        ' Attach an event handler for the 
        ' ContextMenuStrip control's Opening event.
        AddHandler _MyContextMenuStrip.Opening, AddressOf Me.contectMenuOpeningHandler

        Return Me._MyContextMenuStrip

    End Function

    ''' <summary> Adds menu items. </summary>
    ''' <remarks> This event handler is invoked when the <see cref="ContextMenuStrip"/> control's
    ''' Opening event is raised. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub contectMenuOpeningHandler(ByVal sender As Object, ByVal e As CancelEventArgs)

        Me._MyContextMenuStrip = CType(sender, Windows.Forms.ContextMenuStrip)

        ' Clear the ContextMenuStrip control's Items collection.
        Me._MyContextMenuStrip.Items.Clear()

        ' Populate the ContextMenuStrip control with its default items.
        ' myContextMenuStrip.Items.Add("-")
        Me._MyContextMenuStrip.Items.Add(New Windows.Forms.ToolStripMenuItem("Clear &All", Nothing, AddressOf Me.clearAllHandler, "Clear"))
        Me._MyContextMenuStrip.Items.Add(New Windows.Forms.ToolStripMenuItem("Flush &Queue", Nothing, AddressOf Me.flushQueuesHandler, "Flush"))

        ' Set Cancel to false. 
        ' It is optimized to true based on empty entry.
        e.Cancel = False

    End Sub

    ''' <summary> Applies the high point Output. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub clearAllHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Clear()
    End Sub

    ''' <summary> Flush messages and content. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub flushQueuesHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Me.FlushContent()
        Me.FlushMessages()
    End Sub

#End Region

End Class
