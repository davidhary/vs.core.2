﻿Imports System.ComponentModel
Imports System.Windows.Forms
Partial Public Class MessagesBox

    Private Sub InitializeWorker()
        Me.worker = New BackgroundWorker()
        Me.worker.WorkerSupportsCancellation = True
    End Sub

    ''' <summary> The background worker is used for setting the messages text box
    ''' in a thread safe way. </summary>
    Private WithEvents worker As BackgroundWorker

    ''' <summary> Dispose worker. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    Private Sub DisposeWorker()
        Try
            If Me.worker IsNot Nothing Then
                Me.worker.CancelAsync()
                Application.DoEvents()
                If Not (Me.worker.IsBusy OrElse Me.worker.CancellationPending) Then
                    Me.worker.Dispose()
                End If
            End If
        Catch
        End Try
    End Sub

End Class
