Imports System.Windows.Forms
''' <summary> A tab style default provider. </summary>
''' <license>
''' (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="09/26/2015" by="David" revision=""> Created.
''' http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </history>
<System.ComponentModel.ToolboxItem(False)>
Public Class TabStyleDefaultProvider
    Inherits TabStyleProvider

    Public Sub New(tabControl As CustomTabControl)
        MyBase.New(tabControl, 2, True)
    End Sub

    Public Overrides Sub AddTabBorder(path As System.Drawing.Drawing2D.GraphicsPath, tabBounds As System.Drawing.Rectangle)
        If path Is Nothing Then Throw New ArgumentNullException("path")
        Select Case Me.TabControl.Alignment
            Case TabAlignment.Top
                path.AddLine(tabBounds.X, tabBounds.Bottom, tabBounds.X, tabBounds.Y)
                path.AddLine(tabBounds.X, tabBounds.Y, tabBounds.Right, tabBounds.Y)
                path.AddLine(tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom)
                Exit Select
            Case TabAlignment.Bottom
                path.AddLine(tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom)
                path.AddLine(tabBounds.Right, tabBounds.Bottom, tabBounds.X, tabBounds.Bottom)
                path.AddLine(tabBounds.X, tabBounds.Bottom, tabBounds.X, tabBounds.Y)
                Exit Select
            Case TabAlignment.Left
                path.AddLine(tabBounds.Right, tabBounds.Bottom, tabBounds.X, tabBounds.Bottom)
                path.AddLine(tabBounds.X, tabBounds.Bottom, tabBounds.X, tabBounds.Y)
                path.AddLine(tabBounds.X, tabBounds.Y, tabBounds.Right, tabBounds.Y)
                Exit Select
            Case TabAlignment.Right
                path.AddLine(tabBounds.X, tabBounds.Y, tabBounds.Right, tabBounds.Y)
                path.AddLine(tabBounds.Right, tabBounds.Y, tabBounds.Right, tabBounds.Bottom)
                path.AddLine(tabBounds.Right, tabBounds.Bottom, tabBounds.X, tabBounds.Bottom)
                Exit Select
        End Select
    End Sub

    Public Overrides Function GetTabRect(index As Integer) As Rectangle
        If index < 0 Then
            Return New Rectangle()
        End If

        Dim tabBounds As Rectangle = MyBase.GetTabRect(index)
        Dim firstTabinRow As Boolean = Me.TabControl.IsFirstTabInRow(index)

        '	Make non-SelectedTabs smaller and selected tab bigger
        If index <> Me.TabControl.SelectedIndex Then
            Select Case Me.TabControl.Alignment
                Case TabAlignment.Top
                    tabBounds.Y += 1
                    tabBounds.Height -= 1
                    Exit Select
                Case TabAlignment.Bottom
                    tabBounds.Height -= 1
                    Exit Select
                Case TabAlignment.Left
                    tabBounds.X += 1
                    tabBounds.Width -= 1
                    Exit Select
                Case TabAlignment.Right
                    tabBounds.Width -= 1
                    Exit Select
            End Select
        Else
            Select Case Me.TabControl.Alignment
                Case TabAlignment.Top
                    If tabBounds.Y > 0 Then
                        tabBounds.Y -= 1
                        tabBounds.Height += 1
                    End If

                    If firstTabinRow Then
                        tabBounds.Width += 1
                    Else
                        tabBounds.X -= 1
                        tabBounds.Width += 2
                    End If
                    Exit Select
                Case TabAlignment.Bottom
                    If tabBounds.Bottom < Me.TabControl.Bottom Then
                        tabBounds.Height += 1
                    End If
                    If firstTabinRow Then
                        tabBounds.Width += 1
                    Else
                        tabBounds.X -= 1
                        tabBounds.Width += 2
                    End If
                    Exit Select
                Case TabAlignment.Left
                    If tabBounds.X > 0 Then
                        tabBounds.X -= 1
                        tabBounds.Width += 1
                    End If

                    If firstTabinRow Then
                        tabBounds.Height += 1
                    Else
                        tabBounds.Y -= 1
                        tabBounds.Height += 2
                    End If
                    Exit Select
                Case TabAlignment.Right
                    If tabBounds.Right < Me.TabControl.Right Then
                        tabBounds.Width += 1
                    End If
                    If firstTabinRow Then
                        tabBounds.Height += 1
                    Else
                        tabBounds.Y -= 1
                        tabBounds.Height += 2
                    End If
                    Exit Select
            End Select
        End If

        '	Adjust first tab in the row to align with tab page
        Me.EnsureFirstTabIsInView(tabBounds, index)

        Return tabBounds
    End Function
End Class
