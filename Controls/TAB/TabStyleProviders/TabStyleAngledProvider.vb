Imports System.Windows.Forms
''' <summary> A tab style angled provider. </summary>
''' <license>
''' (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="09/26/2015" by="David" revision=""> Created.
''' http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </history>
<System.ComponentModel.ToolboxItem(False)>
Public Class TabStyleAngledProvider
    Inherits TabStyleProvider

    Public Sub New(tabControl As CustomTabControl)
        MyBase.New(tabControl, ContentAlignment.MiddleRight, 10, 7, New Point(10, 3))
    End Sub

    Public Overrides Sub AddTabBorder(path As System.Drawing.Drawing2D.GraphicsPath, tabBounds As System.Drawing.Rectangle)
        If path Is Nothing Then Throw New ArgumentNullException("path")
        Select Case Me.TabControl.Alignment
            Case TabAlignment.Top
                path.AddLine(tabBounds.X, tabBounds.Bottom, tabBounds.X + Me.Radius - 2, tabBounds.Y + 2)
                path.AddLine(tabBounds.X + Me.Radius, tabBounds.Y, tabBounds.Right - Me.Radius, tabBounds.Y)
                path.AddLine(tabBounds.Right - Me.Radius + 2, tabBounds.Y + 2, tabBounds.Right, tabBounds.Bottom)
                Exit Select
            Case TabAlignment.Bottom
                path.AddLine(tabBounds.Right, tabBounds.Y, tabBounds.Right - Me.Radius + 2, tabBounds.Bottom - 2)
                path.AddLine(tabBounds.Right - Me.Radius, tabBounds.Bottom, tabBounds.X + Me.Radius, tabBounds.Bottom)
                path.AddLine(tabBounds.X + Me.Radius - 2, tabBounds.Bottom - 2, tabBounds.X, tabBounds.Y)
                Exit Select
            Case TabAlignment.Left
                path.AddLine(tabBounds.Right, tabBounds.Bottom, tabBounds.X + 2, tabBounds.Bottom - Me.Radius + 2)
                path.AddLine(tabBounds.X, tabBounds.Bottom - Me.Radius, tabBounds.X, tabBounds.Y + Me.Radius)
                path.AddLine(tabBounds.X + 2, tabBounds.Y + Me.Radius - 2, tabBounds.Right, tabBounds.Y)
                Exit Select
            Case TabAlignment.Right
                path.AddLine(tabBounds.X, tabBounds.Y, tabBounds.Right - 2, tabBounds.Y + Me.Radius - 2)
                path.AddLine(tabBounds.Right, tabBounds.Y + Me.Radius, tabBounds.Right, tabBounds.Bottom - Me.Radius)
                path.AddLine(tabBounds.Right - 2, tabBounds.Bottom - Me.Radius + 2, tabBounds.X, tabBounds.Bottom)
                Exit Select
        End Select
    End Sub

End Class
