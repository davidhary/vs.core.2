Imports System.ComponentModel
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms
''' <summary> A tab style provider. </summary>
''' <remarks> David, 9/26/2015. </remarks>
''' <license>
''' (c) 2010 The Man from U.N.C.L.E. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="09/26/2015" by="David" revision=""> Created.
''' http://www.codeproject.com/Articles/91387/Painting-Your-Own-Tabs-Second-Edition </history>
<System.ComponentModel.ToolboxItem(False)>
Public MustInherit Class TabStyleProvider
    Inherits Component

#Region "Constructor"

    Protected Sub New(tabControl As CustomTabControl, ByVal imageAlignment As ContentAlignment,
                      ByVal borderColor As Color, ByVal borderColorSelected As Color, ByVal borderColorHot As Color,
                      ByVal focusColor As Color, ByVal textColor As Color, ByVal textColorDisabled As Color,
                      ByVal hotTrack As Boolean, ByVal padding As Point, ByVal radius As Integer,
                      ByVal showTabCloser As Boolean, ByVal closerColorActive As Color, ByVal closerColor As Color)
        MyBase.New()
        Me._TabControl = tabControl
        Me._BorderColor = borderColor
        Me._BorderColorSelected = borderColorSelected
        Me._BorderColorHot = borderColorHot

        Me._FocusColor = focusColor
        Me._ImageAlign = imageAlignment
        Me._HotTrack = hotTrack

        Me._Radius = radius
        Me._ShowTabCloser = showTabCloser
        Me._CloserColorActive = closerColorActive
        Me._CloserColor = closerColor
        Me._TextColor = textColor
        Me._TextColorDisabled = textColorDisabled
        Me._Opacity = 1

        Me._Padding = padding
        Me._AdjustPadding(padding)
    End Sub

    Protected Sub New(tabControl As CustomTabControl,
                      ByVal borderColor As Color, ByVal borderColorSelected As Color, ByVal borderColorHot As Color,
                      ByVal focusColor As Color, ByVal textColor As Color, ByVal textColorDisabled As Color,
                      ByVal hotTrack As Boolean, ByVal padding As Point, ByVal radius As Integer,
                      ByVal showTabCloser As Boolean, ByVal closerColorActive As Color, ByVal closerColor As Color)
        Me.New(tabControl, If(tabControl?.RightToLeftLayout, ContentAlignment.MiddleRight, ContentAlignment.MiddleLeft),
                   borderColor, borderColorSelected, borderColorHot,
                   focusColor, textColor, textColorDisabled,
                   hotTrack, padding, radius,
               showTabCloser, closerColorActive, closerColor)
    End Sub

    Protected Sub New(tabControl As CustomTabControl,
                      ByVal borderColor As Color, ByVal borderColorHot As Color,
                      ByVal textColor As Color, ByVal textColorDisabled As Color,
                      ByVal padding As Point, ByVal radius As Integer,
                      ByVal showTabCloser As Boolean, ByVal closerColorActive As Color, ByVal closerColor As Color)
        Me.New(tabControl, If(tabControl?.RightToLeftLayout, ContentAlignment.MiddleRight, ContentAlignment.MiddleLeft),
                   borderColor, Color.Empty, borderColorHot,
                   Color.Orange, textColor, textColorDisabled,
                   True, padding, radius,
               showTabCloser, closerColorActive, closerColor)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 9/26/2015. </remarks>
    ''' <param name="tabControl">     The tab control. </param>
    ''' <param name="imageAlignment"> The image alignment. </param>
    Protected Sub New(tabControl As CustomTabControl, ByVal imageAlignment As ContentAlignment)
        MyBase.New()
        Me._TabControl = tabControl

        Me._BorderColor = Color.Empty
        Me._BorderColorSelected = Color.Empty
        Me._BorderColorHot = Color.Empty

        Me._FocusColor = Color.Orange
        Me._ImageAlign = imageAlignment
        Me._HotTrack = True

        Me._Radius = 1
        Me._CloserColorActive = Color.Black
        Me._CloserColor = Color.DarkGray

        Me._TextColor = Color.Empty
        Me._TextColorDisabled = Color.Empty
        Me._Opacity = 1

        Me._Padding = New Point(6, 3)
        Me._AdjustPadding(Padding)
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 9/26/2015. </remarks>
    ''' <param name="tabControl"> The tab control. </param>
    Protected Sub New(tabControl As CustomTabControl)
        Me.New(tabControl, If(tabControl?.RightToLeftLayout, ContentAlignment.MiddleRight, ContentAlignment.MiddleLeft))
    End Sub

    Protected Sub New(tabControl As CustomTabControl, ByVal radius As Integer, ByVal focusTrack As Boolean)
        Me.New(tabControl, If(tabControl?.RightToLeftLayout, ContentAlignment.MiddleRight, ContentAlignment.MiddleLeft))
        Me._Radius = radius
        Me._FocusTrack = focusTrack
        Me._Padding = New Point(6, 3)
        Me._AdjustPadding(Padding)
    End Sub

    Protected Sub New(tabControl As CustomTabControl, ByVal radius As Integer)
        Me.New(tabControl, If(tabControl?.RightToLeftLayout, ContentAlignment.MiddleRight, ContentAlignment.MiddleLeft))
        Me._Radius = radius
        Me._Padding = New Point(6, 3)
        Me._AdjustPadding(Padding)
    End Sub

    Protected Sub New(tabControl As CustomTabControl, ByVal radius As Integer,
                      ByVal showTabCloser As Boolean, ByVal closerColorActive As Color,
                      ByVal padding As Point)
        Me.New(tabControl, If(tabControl?.RightToLeftLayout, ContentAlignment.MiddleRight, ContentAlignment.MiddleLeft))
        Me._ShowTabCloser = showTabCloser
        Me._CloserColorActive = closerColorActive
        Me._Radius = radius
        Me._Padding = padding
        Me._AdjustPadding(padding)
    End Sub

    Protected Sub New(tabControl As CustomTabControl,
                      ByVal radius As Integer, ByVal overlap As Integer,
                      ByVal showTabCloser As Boolean, ByVal closerColorActive As Color,
                      ByVal padding As Point)
        Me.New(tabControl, If(tabControl?.RightToLeftLayout, ContentAlignment.MiddleRight, ContentAlignment.MiddleLeft))
        Me._ShowTabCloser = showTabCloser
        Me._CloserColorActive = closerColorActive
        Me._Overlap = overlap
        Me._Radius = radius
        Me._Padding = padding
        Me._AdjustPadding(padding)
    End Sub

    Protected Sub New(tabControl As CustomTabControl, ByVal imageAlignment As ContentAlignment,
                      ByVal radius As Integer, ByVal overlap As Integer,
                      ByVal padding As Point)
        Me.New(tabControl, imageAlignment)
        Me._Overlap = overlap
        Me._Radius = radius
        Me._Padding = padding
        Me._AdjustPadding(padding)
    End Sub

#End Region

#Region "Factory Methods"

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Shared Function CreateProvider(tabControl As CustomTabControl) As TabStyleProvider
        If tabControl Is Nothing Then Throw New ArgumentNullException("tabControl")
        Dim provider As TabStyleProvider

        '	Depending on the display style of the tabControl generate an appropriate provider.
        Select Case tabControl.DisplayStyle
            Case TabStyle.None
                provider = New TabStyleNoneProvider(tabControl)
                Exit Select

            Case TabStyle.[Default]
                provider = New TabStyleDefaultProvider(tabControl)
                Exit Select

            Case TabStyle.Angled
                provider = New TabStyleAngledProvider(tabControl)
                Exit Select

            Case TabStyle.Rounded
                provider = New TabStyleRoundedProvider(tabControl)
                Exit Select

            Case TabStyle.VisualStudio
                provider = New TabStyleVisualStudioProvider(tabControl)
                Exit Select

            Case TabStyle.Chrome
                provider = New TabStyleChromeProvider(tabControl)
                Exit Select

            Case TabStyle.IE8
                provider = New TabStyleIE8Provider(tabControl)
                Exit Select

            Case TabStyle.VS2010
                provider = New TabStyleVS2010Provider(tabControl)
                Exit Select
            Case Else

                provider = New TabStyleDefaultProvider(tabControl)
                Exit Select
        End Select

        provider.DisplayStyle = tabControl.DisplayStyle
        Return provider
    End Function

#End Region

#Region "overridable Methods"

    Public MustOverride Sub AddTabBorder(path As GraphicsPath, tabBounds As Rectangle)

    Public Overridable Function GetTabRect(index As Integer) As Rectangle

        If index < 0 Then
            Return New Rectangle()
        End If
        Dim tabBounds As Rectangle = Me.TabControl.GetTabRect(index)
        If Me.TabControl.RightToLeftLayout Then
            tabBounds.X = Me.TabControl.Width - tabBounds.Right
        End If
        Dim firstTabinRow As Boolean = Me.TabControl.IsFirstTabInRow(index)

        '	Expand to overlap the tab page
        Select Case Me.TabControl.Alignment
            Case TabAlignment.Top
                tabBounds.Height += 2
                Exit Select
            Case TabAlignment.Bottom
                tabBounds.Height += 2
                tabBounds.Y -= 2
                Exit Select
            Case TabAlignment.Left
                tabBounds.Width += 2
                Exit Select
            Case TabAlignment.Right
                tabBounds.X -= 2
                tabBounds.Width += 2
                Exit Select
        End Select


        '	Create Overlap unless first tab in the row to align with tab page
        If (Not firstTabinRow OrElse Me.TabControl.RightToLeftLayout) AndAlso Me.Overlap > 0 Then
            If Me.TabControl.Alignment <= TabAlignment.Bottom Then
                tabBounds.X -= Me.Overlap
                tabBounds.Width += Me.Overlap
            Else
                tabBounds.Y -= Me.Overlap
                tabBounds.Height += Me.Overlap
            End If
        End If

        '	Adjust first tab in the row to align with tab page
        Me.EnsureFirstTabIsInView(tabBounds, index)

        Return tabBounds
    End Function


    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="0#")>
    Protected Overridable Sub EnsureFirstTabIsInView(ByRef tabBounds As Rectangle, index As Integer)
        '	Adjust first tab in the row to align with tab page
        '	Make sure we only reposition visible tabs, as we may have scrolled out of view.

        Dim firstTabinRow As Boolean = Me.TabControl.IsFirstTabInRow(index)

        If firstTabinRow Then
            If Me.TabControl.Alignment <= TabAlignment.Bottom Then
                If Me.TabControl.RightToLeftLayout Then
                    If tabBounds.Left < Me.TabControl.Right Then
                        Dim tabPageRight As Integer = Me.TabControl.GetPageBounds(index).Right
                        If tabBounds.Right > tabPageRight Then
                            tabBounds.Width -= (tabBounds.Right - tabPageRight)
                        End If
                    End If
                Else
                    If tabBounds.Right > 0 Then
                        Dim tabPageX As Integer = Me.TabControl.GetPageBounds(index).X
                        If tabBounds.X < tabPageX Then
                            tabBounds.Width -= (tabPageX - tabBounds.X)
                            tabBounds.X = tabPageX
                        End If
                    End If
                End If
            Else
                If Me.TabControl.RightToLeftLayout Then
                    If tabBounds.Top < Me.TabControl.Bottom Then
                        Dim tabPageBottom As Integer = Me.TabControl.GetPageBounds(index).Bottom
                        If tabBounds.Bottom > tabPageBottom Then
                            tabBounds.Height -= (tabBounds.Bottom - tabPageBottom)
                        End If
                    End If
                Else
                    If tabBounds.Bottom > 0 Then
                        Dim tabPageY As Integer = Me.TabControl.GetPageBounds(index).Location.Y
                        If tabBounds.Y < tabPageY Then
                            tabBounds.Height -= (tabPageY - tabBounds.Y)
                            tabBounds.Y = tabPageY
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Protected Overridable Function GetTabBackgroundBrush(index As Integer) As Brush
        Dim fillBrush As LinearGradientBrush = Nothing

        '	Capture the colors dependent on selection state of the tab
        Dim dark As Color = Color.FromArgb(207, 207, 207)
        Dim light As Color = Color.FromArgb(242, 242, 242)

        If Me.TabControl.SelectedIndex = index Then
            dark = SystemColors.ControlLight
            light = SystemColors.Window
        ElseIf Not Me.TabControl.TabPages(index).Enabled Then
            light = dark
        ElseIf Me.HotTrack AndAlso index = Me.TabControl.ActiveIndex Then
            '	Enable hot tracking
            light = Color.FromArgb(234, 246, 253)
            dark = Color.FromArgb(167, 217, 245)
        End If

        '	Get the correctly aligned gradient
        Dim tabBounds As Rectangle = Me.GetTabRect(index)
        tabBounds.Inflate(3, 3)
        tabBounds.X -= 1
        tabBounds.Y -= 1
        Select Case Me.TabControl.Alignment
            Case TabAlignment.Top
                If Me.TabControl.SelectedIndex = index Then
                    dark = light
                End If
                fillBrush = New LinearGradientBrush(tabBounds, light, dark, LinearGradientMode.Vertical)
                Exit Select
            Case TabAlignment.Bottom
                fillBrush = New LinearGradientBrush(tabBounds, light, dark, LinearGradientMode.Vertical)
                Exit Select
            Case TabAlignment.Left
                fillBrush = New LinearGradientBrush(tabBounds, dark, light, LinearGradientMode.Horizontal)
                Exit Select
            Case TabAlignment.Right
                fillBrush = New LinearGradientBrush(tabBounds, light, dark, LinearGradientMode.Horizontal)
                Exit Select
        End Select

        '	Add the blend
        fillBrush.Blend = Me.GetBackgroundBlend()

        Return fillBrush
    End Function

#End Region

#Region "Base Properties"

    ''' <summary> Gets or sets the tab control. </summary>
    ''' <value> The tab control. </value>
    Protected Property TabControl As CustomTabControl

    Private _Style As TabStyle = TabStyle.[Default]

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property DisplayStyle() As TabStyle
        Get
            Return Me._Style
        End Get
        Set
            Me._Style = Value
        End Set
    End Property

    Private _ImageAlign As ContentAlignment

    <Category("Appearance")>
    Public Property ImageAlign() As ContentAlignment
        Get
            Return Me._ImageAlign
        End Get
        Set
            Me._ImageAlign = Value
            Me.TabControl.Invalidate()
        End Set
    End Property

    Private _Padding As Point

    Private Sub _AdjustPadding(ByVal value As Point)
        '	This line will trigger the handle to recreate, therefore invalidating the control
        If Me.ShowTabCloser Then
            If value.X + CInt(Me.Radius \ 2) < -6 Then
                DirectCast(Me.TabControl, TabControl).Padding = New Point(0, value.Y)
            Else
                DirectCast(Me.TabControl, TabControl).Padding = New Point(value.X + CInt(Me.Radius \ 2) + 6, value.Y)
            End If
        Else
            If value.X + CInt(Me.Radius \ 2) < 1 Then
                DirectCast(Me.TabControl, TabControl).Padding = New Point(0, value.Y)
            Else
                DirectCast(Me.TabControl, TabControl).Padding = New Point(value.X + CInt(Me.Radius \ 2) - 1, value.Y)
            End If
        End If
    End Sub

    <Category("Appearance")>
    Public Property Padding() As Point
        Get
            Return Me._Padding
        End Get
        Set
            Me._Padding = Value
            Me._AdjustPadding(Value)
        End Set
    End Property


    Private _Radius As Integer

    <Category("Appearance"), DefaultValue(1), Browsable(True)>
    Public Property Radius() As Integer
        Get
            Return Me._Radius
        End Get
        Set
            If Value < 1 Then Value = 1
            Me._Radius = Value
            Me._AdjustPadding(Me._Padding)
        End Set
    End Property

    Private _Overlap As Integer

    <Category("Appearance")>
    Public Property Overlap() As Integer
        Get
            Return Me._Overlap
        End Get
        Set
            If Value < 0 Then Value = 0
            Me._Overlap = Value
        End Set
    End Property

    Private _FocusTrack As Boolean

    <Category("Appearance")>
    Public Property FocusTrack() As Boolean
        Get
            Return Me._FocusTrack
        End Get
        Set
            Me._FocusTrack = Value
            Me.TabControl.Invalidate()
        End Set
    End Property

    Private _HotTrack As Boolean

    <Category("Appearance")>
    Public Property HotTrack() As Boolean
        Get
            Return Me._HotTrack
        End Get
        Set
            Me._HotTrack = Value
            DirectCast(Me.TabControl, TabControl).HotTrack = Value
        End Set
    End Property

    Private _ShowTabCloser As Boolean
    <Category("Appearance")>
    Public Property ShowTabCloser() As Boolean
        Get
            Return Me._ShowTabCloser
        End Get
        Set
            Me._ShowTabCloser = Value
            Me._AdjustPadding(Me._Padding)
        End Set
    End Property

    Private _Opacity As Single = 1
    <Category("Appearance")>
    Public Property Opacity() As Single
        Get
            Return Me._Opacity
        End Get
        Set
            If Value < 0 Then
                Value = 0
            ElseIf Value > 1 Then
                Value = 1
            End If
            Me._Opacity = Value
            Me.TabControl.Invalidate()
        End Set
    End Property

    Private _BorderColorSelected As Color
    <Category("Appearance"), DefaultValue(GetType(Color), "")>
    Public Property BorderColorSelected() As Color
        Get
            If Me._BorderColorSelected.IsEmpty Then
                Return ThemedColors.ToolBorder
            Else
                Return Me._BorderColorSelected
            End If
        End Get
        Set
            If Value.Equals(ThemedColors.ToolBorder) Then
                Me._BorderColorSelected = Color.Empty
            Else
                Me._BorderColorSelected = Value
            End If
            Me.TabControl.Invalidate()
        End Set
    End Property

    Private _BorderColorHot As Color
    <Category("Appearance"), DefaultValue(GetType(Color), "")>
    Public Property BorderColorHot() As Color
        Get
            If Me._BorderColorHot.IsEmpty Then
                Return SystemColors.ControlDark
            Else
                Return Me._BorderColorHot
            End If
        End Get
        Set
            If Value.Equals(SystemColors.ControlDark) Then
                Me._BorderColorHot = Color.Empty
            Else
                Me._BorderColorHot = Value
            End If
            Me.TabControl.Invalidate()
        End Set
    End Property

    Private _BorderColor As Color
    <Category("Appearance"), DefaultValue(GetType(Color), "")>
    Public Property BorderColor() As Color
        Get
            If Me._BorderColor.IsEmpty Then
                Return SystemColors.ControlDark
            Else
                Return Me._BorderColor
            End If
        End Get
        Set
            If Value.Equals(SystemColors.ControlDark) Then
                Me._BorderColor = Color.Empty
            Else
                Me._BorderColor = Value
            End If
            Me.TabControl.Invalidate()
        End Set
    End Property

    Private _TextColor As Color
    <Category("Appearance"), DefaultValue(GetType(Color), "")>
    Public Property TextColor() As Color
        Get
            If Me._TextColor.IsEmpty Then
                Return SystemColors.ControlText
            Else
                Return Me._TextColor
            End If
        End Get
        Set
            If Value.Equals(SystemColors.ControlText) Then
                Me._TextColor = Color.Empty
            Else
                Me._TextColor = Value
            End If
            Me.TabControl.Invalidate()
        End Set
    End Property

    Private _TextColorSelected As Color = Color.Empty
    <Category("Appearance"), DefaultValue(GetType(Color), "")>
    Public Property TextColorSelected() As Color
        Get
            If Me._TextColorSelected.IsEmpty Then
                Return SystemColors.ControlText
            Else
                Return Me._TextColorSelected
            End If
        End Get
        Set
            If Value.Equals(SystemColors.ControlText) Then
                Me._TextColorSelected = Color.Empty
            Else
                Me._TextColorSelected = Value
            End If
            Me.TabControl.Invalidate()
        End Set
    End Property

    Private _TextColorDisabled As Color
    <Category("Appearance"), DefaultValue(GetType(Color), "")>
    Public Property TextColorDisabled() As Color
        Get
            If Me._TextColorDisabled.IsEmpty Then
                Return SystemColors.ControlDark
            Else
                Return Me._TextColorDisabled
            End If
        End Get
        Set
            If Value.Equals(SystemColors.ControlDark) Then
                Me._TextColorDisabled = Color.Empty
            Else
                Me._TextColorDisabled = Value
            End If
            Me.TabControl.Invalidate()
        End Set
    End Property

    Private _FocusColor As Color
    <Category("Appearance"), DefaultValue(GetType(Color), "Orange")>
    Public Property FocusColor() As Color
        Get
            Return Me._FocusColor
        End Get
        Set
            Me._FocusColor = Value
            Me.TabControl.Invalidate()
        End Set
    End Property

    Private _CloserColorActive As Color
    <Category("Appearance"), DefaultValue(GetType(Color), "Black")>
    Public Property CloserColorActive() As Color
        Get
            Return Me._CloserColorActive
        End Get
        Set
            Me._CloserColorActive = Value
            Me.TabControl.Invalidate()
        End Set
    End Property

    Private _CloserColor As Color

    <Category("Appearance"), DefaultValue(GetType(Color), "DarkGrey")>
    Public Property CloserColor() As Color
        Get
            Return Me._CloserColor
        End Get
        Set
            Me._CloserColor = Value
            Me.TabControl.Invalidate()
        End Set
    End Property

#End Region

#Region "Painting"

    Public Sub PaintTab(index As Integer, graphics As Graphics)
        If graphics Is Nothing Then Throw New ArgumentNullException("graphics")
        Using tabpath As GraphicsPath = Me.GetTabBorder(index)
            Using fillBrush As Brush = Me.GetTabBackgroundBrush(index)
                '	Paint the background
                graphics.FillPath(fillBrush, tabpath)
                '	Paint a focus indication
                If Me.TabControl.Focused Then
                    Me.DrawTabFocusIndicator(tabpath, index, graphics)
                End If
                '	Paint the closer
                Me.DrawTabCloser(index, graphics)
            End Using
        End Using
    End Sub

    Protected Overridable Sub DrawTabCloser(index As Integer, graphics As Graphics)
        If graphics Is Nothing Then Throw New ArgumentNullException("graphics")
        If Me.ShowTabCloser Then
            Dim closerRect As Rectangle = Me.TabControl.GetTabCloserRect(index)
            graphics.SmoothingMode = SmoothingMode.AntiAlias
            Using closerPath As GraphicsPath = TabStyleProvider.GetCloserPath(closerRect)
                If closerRect.Contains(Me.TabControl.MousePosition) Then
                    Using closerPen As New Pen(Me.CloserColorActive)
                        graphics.DrawPath(closerPen, closerPath)
                    End Using
                Else
                    Using closerPen As New Pen(Me.CloserColor)
                        graphics.DrawPath(closerPen, closerPath)
                    End Using

                End If
            End Using
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Protected Shared Function GetCloserPath(closerRect As Rectangle) As GraphicsPath
        Dim closerPath As New GraphicsPath()
        closerPath.AddLine(closerRect.X, closerRect.Y, closerRect.Right, closerRect.Bottom)
        closerPath.CloseFigure()
        closerPath.AddLine(closerRect.Right, closerRect.Y, closerRect.X, closerRect.Bottom)
        closerPath.CloseFigure()

        Return closerPath
    End Function

    Private Sub DrawTabFocusIndicator(tabpath As GraphicsPath, index As Integer, graphics As Graphics)
        If Me.FocusTrack AndAlso Me.TabControl.Focused AndAlso index = Me.TabControl.SelectedIndex Then
            Dim pathRect As RectangleF = tabpath.GetBounds()
            Dim focusRect As Rectangle = Rectangle.Empty
            Select Case Me.TabControl.Alignment
                Case TabAlignment.Top
                    focusRect = New Rectangle(CInt(Math.Truncate(pathRect.X)), CInt(Math.Truncate(pathRect.Y)), CInt(Math.Truncate(pathRect.Width)), 4)
                    Using focusBrush As Brush = New LinearGradientBrush(focusRect, Me.FocusColor, SystemColors.Window, LinearGradientMode.Vertical)
                        Using focusRegion As New Region(focusRect)
                            focusRegion.Intersect(tabpath)
                            graphics.FillRegion(focusBrush, focusRegion)
                        End Using
                    End Using
                Case TabAlignment.Bottom
                    focusRect = New Rectangle(CInt(Math.Truncate(pathRect.X)), CInt(Math.Truncate(pathRect.Bottom)) - 4, CInt(Math.Truncate(pathRect.Width)), 4)
                    Using focusBrush As Brush = New LinearGradientBrush(focusRect, SystemColors.ControlLight, Me.FocusColor, LinearGradientMode.Vertical)
                        Using focusRegion As New Region(focusRect)
                            focusRegion.Intersect(tabpath)
                            graphics.FillRegion(focusBrush, focusRegion)
                        End Using
                    End Using
                Case TabAlignment.Left
                    focusRect = New Rectangle(CInt(Math.Truncate(pathRect.X)), CInt(Math.Truncate(pathRect.Y)), 4, CInt(Math.Truncate(pathRect.Height)))
                    Using focusBrush As Brush = New LinearGradientBrush(focusRect, Me.FocusColor, SystemColors.ControlLight, LinearGradientMode.Horizontal)
                        Using focusRegion As New Region(focusRect)
                            focusRegion.Intersect(tabpath)
                            graphics.FillRegion(focusBrush, focusRegion)
                        End Using
                    End Using
                Case TabAlignment.Right
                    focusRect = New Rectangle(CInt(Math.Truncate(pathRect.Right)) - 4, CInt(Math.Truncate(pathRect.Y)), 4, CInt(Math.Truncate(pathRect.Height)))
                    Using focusBrush As Brush = New LinearGradientBrush(focusRect, SystemColors.ControlLight, Me.FocusColor, LinearGradientMode.Horizontal)
                        Using focusRegion As New Region(focusRect)
                            focusRegion.Intersect(tabpath)
                            graphics.FillRegion(focusBrush, focusRegion)
                        End Using
                    End Using
            End Select

        End If
    End Sub

#End Region

#Region "Background brushes"

    Private Function GetBackgroundBlend() As Blend
        Dim relativeIntensities As Single() = New Single() {0F, 0.7F, 1.0F}
        Dim relativePositions As Single() = New Single() {0F, 0.6F, 1.0F}

        '	Glass look to top aligned tabs
        If Me.TabControl.Alignment = TabAlignment.Top Then
            relativeIntensities = New Single() {0F, 0.5F, 1.0F, 1.0F}
            relativePositions = New Single() {0F, 0.5F, 0.51F, 1.0F}
        End If

        Dim blend As New Blend()
        blend.Factors = relativeIntensities
        blend.Positions = relativePositions

        Return blend
    End Function

    Public Overridable Function GetPageBackgroundBrush(index As Integer) As Brush

        '	Capture the colors dependent on selection state of the tab
        Dim light As Color = Color.FromArgb(242, 242, 242)
        If Me.TabControl.Alignment = TabAlignment.Top Then
            light = Color.FromArgb(207, 207, 207)
        End If

        If Me.TabControl.SelectedIndex = index Then
            light = SystemColors.Window
        ElseIf Not Me.TabControl.TabPages(index).Enabled Then
            light = Color.FromArgb(207, 207, 207)
        ElseIf Me.HotTrack AndAlso index = Me.TabControl.ActiveIndex Then
            '	Enable hot tracking
            light = Color.FromArgb(234, 246, 253)
        End If

        Return New SolidBrush(light)
    End Function

#End Region

#Region "Tab border and rectangle"

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Function GetTabBorder(index As Integer) As GraphicsPath

        Dim path As New GraphicsPath()
        Dim tabBounds As Rectangle = Me.GetTabRect(index)

        Me.AddTabBorder(path, tabBounds)

        path.CloseFigure()
        Return path
    End Function

#End Region

End Class

''' <summary> Values that represent tab styles. </summary>
''' <remarks> David, 9/26/2015. </remarks>
Public Enum TabStyle
    None
    [Default]
    VisualStudio
    Rounded
    Angled
    Chrome
    IE8
    VS2010
End Enum
