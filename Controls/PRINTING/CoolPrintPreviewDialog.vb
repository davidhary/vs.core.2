﻿Imports System.Drawing.Printing
Imports System.ComponentModel
Imports System.Windows.Forms
''' <summary> Dialog for setting the cool print preview. </summary>
''' <license> (c) 2009 Bernardo Castillo. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="8/5/2009" by="Bernardo Castilho" revision=""> 
''' http://www.codeproject.com/Articles/38758/An-Enhanced-PrintPreviewDialog. </history>
Public Class CoolPrintPreviewDialog
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTORS "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        Me.New(Nothing)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="parentForm"> The parent form. </param>
    Public Sub New(ByVal parentForm As Control)
        MyBase.New()
        Me.components = Nothing
        Me.InitializeComponent()
        If parentForm IsNot Nothing Then
            Me.Size = parentForm.Size
        End If
    End Sub

    ''' <summary> Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />. </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    ''' release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM OVERLOADS "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.FormClosing" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.FormClosingEventArgs" /> that contains
    ''' the event data. </param>
    Protected Overrides Sub OnFormClosing(ByVal e As FormClosingEventArgs)
        If e Is Nothing Then Return
        MyBase.OnFormClosing(e)
        If Not (Not Me._Preview.IsRendering OrElse e.Cancel) Then
            Me._Preview.Cancel()
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnShown(ByVal e As EventArgs)
        If e Is Nothing Then Return
        MyBase.OnShown(e)
        Me._Preview.Document = Me.Document
    End Sub

#End Region

#Region " PROPERTIES "

    Private _doc As PrintDocument
    ''' <summary> Gets or sets the document. </summary>
    ''' <value> The document. </value>
    Public Property Document() As PrintDocument
        Get
            Return Me._doc
        End Get
        Set(ByVal value As PrintDocument)
            If (Not Me._doc Is Nothing) Then
                RemoveHandler Me._doc.BeginPrint, New PrintEventHandler(AddressOf Me._doc_BeginPrint)
                RemoveHandler Me._doc.EndPrint, New PrintEventHandler(AddressOf Me._doc_EndPrint)
            End If
            Me._doc = value
            If (Not Me._doc Is Nothing) Then
                AddHandler Me._doc.BeginPrint, New PrintEventHandler(AddressOf Me._doc_BeginPrint)
                AddHandler Me._doc.EndPrint, New PrintEventHandler(AddressOf Me._doc_EndPrint)
            End If
            If Me.Visible Then
                Me._Preview.Document = Me.Document
            End If
        End Set
    End Property

#End Region

#Region " EVENT HANDLERS "

    Private Sub _CancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _CancelButton.Click
        If Me._Preview.IsRendering Then
            Me._Preview.Cancel()
        Else
            Me.Close()
        End If
    End Sub

    Private Sub _FirstButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _FirstButton.Click
        Me._Preview.StartPage = 0
    End Sub

    Private Sub _LastButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _LastButton.Click
        Me._Preview.StartPage = (Me._Preview.PageCount - 1)
    End Sub

    Private Sub _NextButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _NextButton.Click
        Me._Preview.StartPage += 1
    End Sub

    Private Sub _PreviousButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _PreviousButton.Click
        Me._Preview.StartPage -= 1
    End Sub

    Private Sub _PageSetupButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _PageSetupButton.Click
        Using dlg As PageSetupDialog = New PageSetupDialog
            dlg.Document = Me.Document
            If dlg.ShowDialog(Me) = DialogResult.OK Then
                Me._Preview.RefreshPreview()
            End If
        End Using
    End Sub

    Private Sub _PrintButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _PrintButton.Click
        Using dlg As PrintDialog = New PrintDialog
            dlg.AllowSomePages = True
            dlg.AllowSelection = True
            dlg.UseEXDialog = True
            dlg.Document = Me.Document
            Dim ps As PrinterSettings = dlg.PrinterSettings
            ps.MinimumPage = 1
            ps.MaximumPage = Me._Preview.PageCount
            ps.FromPage = 1
            ps.ToPage = Me._Preview.PageCount
            If (dlg.ShowDialog(Me) = DialogResult.OK) Then
                Me._Preview.Print()
            End If
        End Using
    End Sub

    Private Sub _ZoomButton_ButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles _ZoomButton.ButtonClick
        Me._Preview.ZoomMode = CType(IIf((Me._Preview.ZoomMode = ZoomMode.ActualSize), ZoomMode.FullPage, ZoomMode.ActualSize), ZoomMode)
    End Sub

    Private Sub _ZoomButton_DropDownItemClicked(ByVal sender As Object, ByVal e As ToolStripItemClickedEventArgs) Handles _ZoomButton.DropDownItemClicked
        If e Is Nothing Then Return
        If e.ClickedItem Is Me._ActualSizeMenuItem Then
            Me._Preview.ZoomMode = ZoomMode.ActualSize
        ElseIf e.ClickedItem Is Me._FullPageMenuItem Then
            Me._Preview.ZoomMode = ZoomMode.FullPage
        ElseIf e.ClickedItem Is Me._PageWidthMenuItem Then
            Me._Preview.ZoomMode = ZoomMode.PageWidth
        ElseIf e.ClickedItem Is Me._TwoPagesMenuItem Then
            Me._Preview.ZoomMode = ZoomMode.TwoPages
        End If
        If e.ClickedItem Is Me._Zoom10MenuItem Then
            Me._Preview.Zoom = 0.1
        ElseIf e.ClickedItem Is Me._Zoom100MenuItem Then
            Me._Preview.Zoom = 1
        ElseIf e.ClickedItem Is Me._Zoom150MenuItem Then
            Me._Preview.Zoom = 1.5
        ElseIf e.ClickedItem Is Me._Zomm200MenuItem Then
            Me._Preview.Zoom = 2
        ElseIf e.ClickedItem Is Me._Zoom25MenuItem Then
            Me._Preview.Zoom = 0.25
        ElseIf e.ClickedItem Is Me._Zoom50MenuItem Then
            Me._Preview.Zoom = 0.5
        ElseIf e.ClickedItem Is Me._Zoom500MenuItem Then
            Me._Preview.Zoom = 5
        ElseIf e.ClickedItem Is Me._Zoom75MenuItem Then
            Me._Preview.Zoom = 0.75
        End If
    End Sub

    Private Sub _doc_BeginPrint(ByVal sender As Object, ByVal e As PrintEventArgs)
        Me._CancelButton.Text = "&Cancel"
        Me._PrintButton.Enabled = Me._PageSetupButton.Enabled = False
    End Sub

    Private Sub _doc_EndPrint(ByVal sender As Object, ByVal e As PrintEventArgs)
        Me._CancelButton.Text = "&Close"
        Me._PrintButton.Enabled = Me._PageSetupButton.Enabled = True
    End Sub

    Private Sub _Preview_PageCountChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _Preview.PageCountChanged
        Me.Update()
        Application.DoEvents()
        Me._PageCountLabel.Text = String.Format("of {0}", Me._Preview.PageCount)
    End Sub

    Private Sub _Preview_StartPageChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _Preview.StartPageChanged
        Me._StartPageTextBox.Text = (Me._Preview.StartPage + 1).ToString
    End Sub

    Private Sub _StartPageTextBox_Enter(ByVal sender As Object, ByVal e As EventArgs) Handles _StartPageTextBox.Enter
        Me._StartPageTextBox.SelectAll()
    End Sub

    Private Sub _StartPageTextBox_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles _StartPageTextBox.KeyPress
        If e Is Nothing Then Return
        Dim c As Char = e.KeyChar
        If c = ChrW(13) Then
            Me.CommitPageNumber()
            e.Handled = True
        ElseIf Not ((c <= " "c) OrElse Char.IsDigit(c)) Then
            e.Handled = True
        End If
    End Sub

    Private Sub _StartPageTextBox_Validating(ByVal sender As Object, ByVal e As CancelEventArgs) Handles _StartPageTextBox.Validating
        Me.CommitPageNumber()
    End Sub

    ''' <summary> Commits page number. </summary>
    Private Sub CommitPageNumber()
        Dim page As Integer
        If Integer.TryParse(Me._StartPageTextBox.Text, page) Then
            Me._Preview.StartPage = page - 1
        End If
    End Sub

#End Region

End Class