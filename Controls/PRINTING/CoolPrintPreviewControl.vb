﻿Imports System.ComponentModel
Imports System.Drawing.Imaging
Imports System.Drawing.Printing
Imports System.Windows.Forms
Imports isr.Core.Pith.EventHandlerExtensions
''' <summary> Cool print preview control. </summary>
''' <license> (c) 2009 Bernardo Castillo. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="8/5/2009" by="Bernardo Castilho" revision=""> 
''' http://www.codeproject.com/Articles/38758/An-Enhanced-PrintPreviewDialog. </history>
<System.ComponentModel.Description("Print Preview Control")>
Public Class CoolPrintPreviewControl
    Inherits UserControl

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me._himm2pix = New PointF(-1.0!, -1.0!)
        Me._PageImages = New List(Of Image)
        Me.BackColor = SystemColors.AppWorkspace
        Me.ZoomMode = ZoomMode.FullPage
        Me.StartPage = 0
        Me.SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
    End Sub

    ''' <summary> Releases the unmanaged resources used by the
    ''' CoolPrintPreviewVB.CoolPrintPreviewControl and optionally releases the managed resources. </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    ''' release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' unable to use null conditional because it is not seen by code analysis
                If Me._backBrush IsNot Nothing Then Me._backBrush.Dispose() : Me._backBrush = Nothing
                Me.RemovePageCountChangedEventHandler(Me.PageCountChangedEvent)
                Me.RemoveStartPageChangedEventHandler(Me.StartPageChangedEvent)
                Me.RemoveZoomModeChangedEventHandler(Me.ZoomModeChangedEvent)
                Me._PageImages?.Clear() : Me._PageImages = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " EVENT HANDLERS "

    Private Sub _doc_EndPrint(ByVal sender As Object, ByVal e As PrintEventArgs)
        Me.SyncPageImages(True)
    End Sub

    Private Sub _doc_PrintPage(ByVal sender As Object, ByVal e As PrintPageEventArgs)
        If e Is Nothing Then Return
        Me.SyncPageImages(False)
        If Me._cancel Then
            e.Cancel = True
        End If
    End Sub

#End Region

#Region " FUNCTIONS "

    Private _cancel As Boolean

    ''' <summary> Cancels printing. </summary>
    Public Sub Cancel()
        Me._cancel = True
    End Sub

    ''' <summary> Gets an image. </summary>
    ''' <param name="page"> The page. </param>
    ''' <returns> The image. </returns>
    Private Function GetImage(ByVal page As Integer) As Image
        If (page > -1) AndAlso (page < Me.PageCount) Then
            Return Me._PageImages.Item(page)
        Else
            Return Nothing
        End If
    End Function

    ''' <summary> Gets image rectangle. </summary>
    ''' <param name="img"> The image. </param>
    ''' <returns> The image rectangle. </returns>
    Private Function GetImageRectangle(ByVal img As Image) As Rectangle

        Dim sz As Size = Me.GetImageSizeInPixels(img)
        Dim rc As New Rectangle(0, 0, sz.Width, sz.Height)

        Dim rcCli As Rectangle = Me.ClientRectangle
        Select Case Me._zoomMode
            Case ZoomMode.ActualSize
                Me._zoom = 1
            Case ZoomMode.FullPage
                Me._zoom = Math.Min(CDbl(IIf((rc.Width > 0), (CDbl(rcCli.Width) / CDbl(rc.Width)), 0)),
                                    CDbl(IIf((rc.Height > 0), (CDbl(rcCli.Height) / CDbl(rc.Height)), 0)))
            Case ZoomMode.PageWidth
                Me._zoom = CDbl(IIf((rc.Width > 0), (CDbl(rcCli.Width) / CDbl(rc.Width)), 0))
            Case ZoomMode.TwoPages
                rc.Width = (rc.Width * 2)
                Me._zoom = Math.Min(CDbl(IIf((rc.Width > 0), (CDbl(rcCli.Width) / CDbl(rc.Width)), 0)),
                                    CDbl(IIf((rc.Height > 0), (CDbl(rcCli.Height) / CDbl(rc.Height)), 0)))
            Case Else
        End Select
#If NotUsed Then
        Dim zoomX As Double = CDbl(IIf((rc.Width > 0), (CDbl(rcCli.Width) / CDbl(rc.Width)), 0))
        Dim zoomY As Double = CDbl(IIf((rc.Height > 0), (CDbl(rcCli.Height) / CDbl(rc.Height)), 0))
#End If
        rc.Width = CInt((rc.Width * Me._zoom))
        rc.Height = CInt((rc.Height * Me._zoom))
        Dim dx As Integer = CInt(((rcCli.Width - rc.Width) / 2))
        If (dx > 0) Then
            rc.X = (rc.X + dx)
        End If
        Dim dy As Integer = CInt(((rcCli.Height - rc.Height) / 2))
        If (dy > 0) Then
            rc.Y = (rc.Y + dy)
        End If
        rc.Inflate(-4, -4)
        If (Me._zoomMode = ZoomMode.TwoPages) Then
            rc.Inflate(-2, 0)
        End If
        Return rc
    End Function

    Private _himm2pix As PointF

    ''' <summary> Gets image size in pixels. </summary>
    ''' <param name="img"> The image. </param>
    ''' <returns> The image size in pixels. </returns>
    Private Function GetImageSizeInPixels(ByVal img As Image) As Size
        Dim szf As SizeF = img.PhysicalDimension
        If TypeOf img Is Metafile Then
            If (Me._himm2pix.X < 0.0!) Then
                Using g As Graphics = Me.CreateGraphics
                    Me._himm2pix.X = (g.DpiX / 2540.0!)
                    Me._himm2pix.Y = (g.DpiY / 2540.0!)
                End Using
            End If
            szf.Width = (szf.Width * Me._himm2pix.X)
            szf.Height = (szf.Height * Me._himm2pix.Y)
        End If
        Return Size.Truncate(szf)
    End Function

    ''' <summary> Determines whether the specified key is a regular input key or a special key that
    ''' requires preprocessing. </summary>
    ''' <param name="keyData"> One of the <see cref="T:System.Windows.Forms.Keys" /> values. </param>
    ''' <returns> true if the specified key is a regular input key; otherwise, false. </returns>
    Protected Overrides Function IsInputKey(ByVal keyData As Keys) As Boolean
        Select Case keyData
            Case Keys.Prior, Keys.Next, Keys.End, Keys.Home, Keys.Left, Keys.Up, Keys.Right, Keys.Down
                Return True
        End Select
        Return MyBase.IsInputKey(keyData)
    End Function

#End Region

#Region " OVERRIDES "

    Protected Overrides Sub OnKeyDown(ByVal e As KeyEventArgs)
        If e Is Nothing Then Return
        MyBase.OnKeyDown(e)
        If e.Handled Then Return

        Select Case e.KeyCode

            ' arrow keys scroll or browse, depending on ZoomMode
            Case Keys.Left, Keys.Up, Keys.Right, Keys.Down

                ' browse
                If Me.ZoomMode = ZoomMode.FullPage OrElse Me.ZoomMode = ZoomMode.TwoPages Then
                    Select Case e.KeyCode
                        Case Keys.Left, Keys.Up
                            StartPage -= 1

                        Case Keys.Right, Keys.Down
                            StartPage += 1
                    End Select
                End If

                ' scroll
                Dim pt As Point = AutoScrollPosition
                Select Case e.KeyCode
                    Case Keys.Left
                        pt.X += 20
                    Case Keys.Right
                        pt.X -= 20
                    Case Keys.Up
                        pt.Y += 20
                    Case Keys.Down
                        pt.Y -= 20
                End Select
                AutoScrollPosition = New Point(-pt.X, -pt.Y)

                ' page up/down browse pages
            Case Keys.PageUp
                StartPage -= 1
            Case Keys.PageDown
                StartPage += 1

                ' home/end 
            Case Keys.Home
                AutoScrollPosition = Point.Empty
                StartPage = 0
            Case Keys.End
                AutoScrollPosition = Point.Empty
                StartPage = PageCount - 1

            Case Else
                Return
        End Select

        ' if we got here, the event was handled
        e.Handled = True
    End Sub

    Private _ptLast As Point

    ''' <summary> Raises the mouse event. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnMouseDown(ByVal e As MouseEventArgs)
        If e Is Nothing Then Return
        MyBase.OnMouseDown(e)
        If ((e.Button = MouseButtons.Left) AndAlso (Me.AutoScrollMinSize <> Size.Empty)) Then
            Me.Cursor = Cursors.NoMove2D
            Me._ptLast = New Point(e.X, e.Y)
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseMove" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnMouseMove(ByVal e As MouseEventArgs)
        If e Is Nothing Then Return
        MyBase.OnMouseMove(e)
        If (Me.Cursor Is Cursors.NoMove2D) Then
            Dim dx As Integer = (e.X - Me._ptLast.X)
            Dim dy As Integer = (e.Y - Me._ptLast.Y)
            If ((dx <> 0) OrElse (dy <> 0)) Then
                Dim pt As Point = Me.AutoScrollPosition
                Me.AutoScrollPosition = New Point(-(pt.X + dx), -(pt.Y + dy))
                Me._ptLast = New Point(e.X, e.Y)
            End If
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.MouseUp" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnMouseUp(ByVal e As MouseEventArgs)
        If e Is Nothing Then Return
        MyBase.OnMouseUp(e)
        If ((e.Button = MouseButtons.Left) AndAlso (Me.Cursor Is Cursors.NoMove2D)) Then
            Me.Cursor = Cursors.Default
        End If
    End Sub

    ''' <summary> Event queue for all listeners interested in PageCountChanged events. </summary>
    Public Event PageCountChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemovePageCountChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.PageCountChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the page count changed event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnPageCountChanged(ByVal e As EventArgs)
        RaiseEvent PageCountChanged(Me, e)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If e Is Nothing Then Return
        Dim img As Image = Me.GetImage(Me.StartPage)
        If (Not img Is Nothing) Then
            Dim rc As Rectangle = Me.GetImageRectangle(img)
            If ((rc.Width > 2) AndAlso (rc.Height > 2)) Then
                rc.Offset(Me.AutoScrollPosition)
                If (Me._zoomMode <> ZoomMode.TwoPages) Then
                    CoolPrintPreviewControl.RenderPage(e.Graphics, img, rc)
                Else
                    rc.Width = CInt(((rc.Width - 4) / 2))
                    CoolPrintPreviewControl.RenderPage(e.Graphics, img, rc)
                    img = Me.GetImage((Me.StartPage + 1))
                    If (Not img Is Nothing) Then
                        rc = Me.GetImageRectangle(img)
                        rc.Width = CInt(((rc.Width - 4) / 2))
                        rc.Offset((rc.Width + 4), 0)
                        CoolPrintPreviewControl.RenderPage(e.Graphics, img, rc)
                    End If
                End If
            End If
        End If
        e.Graphics.FillRectangle(Me._backBrush, Me.ClientRectangle)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.SizeChanged" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnSizeChanged(ByVal e As EventArgs)
        Me.UpdateScrollBars()
        MyBase.OnSizeChanged(e)
    End Sub

    ''' <summary> Event queue for all listeners interested in StartPageChanged events. </summary>
    Public Event StartPageChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveStartPageChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.StartPageChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the start page changed event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnStartPageChanged(ByVal e As EventArgs)
        RaiseEvent StartPageChanged(Me, e)
    End Sub

    ''' <summary> Event queue for all listeners interested in ZoomModeChanged events. </summary>
    Public Event ZoomModeChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveZoomModeChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.ZoomModeChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the zoom mode changed event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnZoomModeChanged(ByVal e As EventArgs)
        RaiseEvent ZoomModeChanged(Me, e)
    End Sub

#End Region

#Region " PRINT FUNCTIONS "

    ''' <summary> Prints this object. </summary>
    Public Sub Print()
        Dim ps As PrinterSettings = Me._PrintDocument.PrinterSettings
        Dim first As Integer = (ps.MinimumPage - 1)
        Dim last As Integer = (ps.MaximumPage - 1)
        Select Case ps.PrintRange
            Case PrintRange.AllPages
                Me.Document.Print()
                Return
            Case PrintRange.Selection
                first = Me.StartPage
                last = Me.StartPage
                If (Me.ZoomMode = ZoomMode.TwoPages) Then
                    last = Math.Min(CInt((first + 1)), CInt((Me.PageCount - 1)))
                End If
                Exit Select
            Case PrintRange.SomePages
                first = (ps.FromPage - 1)
                last = (ps.ToPage - 1)
                Exit Select
            Case PrintRange.CurrentPage
                first = Me.StartPage
                last = Me.StartPage
                Exit Select
        End Select
        Using dp As DocumentPrinter = New DocumentPrinter(Me, first, last)
            dp.Print()
        End Using
    End Sub

    ''' <summary> Refresh preview. </summary>
    Public Sub RefreshPreview()
        If (Not Me._PrintDocument Is Nothing) Then
            Me._PageImages.Clear()
            Dim savePC As PrintController = Me._PrintDocument.PrintController
            Try
                Me._cancel = False
                Me._IsRendering = True
                Me._PrintDocument.PrintController = New PreviewPrintController
                AddHandler Me._PrintDocument.PrintPage, New PrintPageEventHandler(AddressOf Me._doc_PrintPage)
                AddHandler Me._PrintDocument.EndPrint, New PrintEventHandler(AddressOf Me._doc_EndPrint)
                Me._PrintDocument.Print()
            Finally
                Me._cancel = False
                Me._IsRendering = False
                RemoveHandler Me._PrintDocument.PrintPage, New PrintPageEventHandler(AddressOf Me._doc_PrintPage)
                RemoveHandler Me._PrintDocument.EndPrint, New PrintEventHandler(AddressOf Me._doc_EndPrint)
                Me._PrintDocument.PrintController = savePC
            End Try
        End If
        Me.OnPageCountChanged(EventArgs.Empty)
        Me.UpdatePreview()
        Me.UpdateScrollBars()
    End Sub

    ''' <summary> Renders the page. </summary>
    ''' <param name="g">   The Graphics to process. </param>
    ''' <param name="img"> The image. </param>
    ''' <param name="rc">  The rectangle. </param>
    Private Shared Sub RenderPage(ByVal g As Graphics, ByVal img As Image, ByVal rc As Rectangle)
        If g Is Nothing Then Return
        rc.Offset(1, 1)
        g.DrawRectangle(Pens.Black, rc)
        rc.Offset(-1, -1)
        g.FillRectangle(Brushes.White, rc)
        g.DrawImage(img, rc)
        g.DrawRectangle(Pens.Black, rc)
        rc.Width += 1
        rc.Height += 1
        g.ExcludeClip(rc)
        rc.Offset(1, 1)
        g.ExcludeClip(rc)
    End Sub

    ''' <summary> Synchronization page images. </summary>
    ''' <param name="lastPageReady"> true to last page ready. </param>
    Private Sub SyncPageImages(ByVal lastPageReady As Boolean)
        Dim pv As PreviewPrintController = DirectCast(Me._PrintDocument.PrintController, PreviewPrintController)
        If (Not pv Is Nothing) Then
            Dim pageInfo As PreviewPageInfo() = pv.GetPreviewPageInfo
            Dim count As Integer = CInt(IIf(lastPageReady, pageInfo.Length, (pageInfo.Length - 1)))
            Dim i As Integer
            For i = Me._PageImages.Count To count - 1
                Dim img As Image = pageInfo(i).Image
                Me._PageImages.Add(img)
                Me.OnPageCountChanged(EventArgs.Empty)
                If (Me.StartPage < 0) Then
                    Me.StartPage = 0
                End If
                If ((i = Me.StartPage) OrElse (i = (Me.StartPage + 1))) Then
                    Me.Refresh()
                End If
                Application.DoEvents()
            Next i
        End If
    End Sub

    ''' <summary> Updates the preview. </summary>
    Private Sub UpdatePreview()
        If (Me._startPage < 0) Then
            Me._startPage = 0
        End If
        If (Me._startPage > (Me.PageCount - 1)) Then
            Me._startPage = (Me.PageCount - 1)
        End If
        Me.Invalidate()
    End Sub

    ''' <summary> Updates the scroll bars. </summary>
    Private Sub UpdateScrollBars()
        Dim rc As Rectangle = Rectangle.Empty
        Dim img As Image = Me.GetImage(Me.StartPage)
        If (Not img Is Nothing) Then
            rc = Me.GetImageRectangle(img)
        End If
        Dim scrollSize As New Size(0, 0)
        Select Case Me._zoomMode
            Case ZoomMode.ActualSize, ZoomMode.Custom
                scrollSize = New Size((rc.Width + 8), (rc.Height + 8))
                Exit Select
            Case ZoomMode.PageWidth
                scrollSize = New Size(0, (rc.Height + 8))
                Exit Select
        End Select
        If (scrollSize <> Me.AutoScrollMinSize) Then
            Me.AutoScrollMinSize = scrollSize
        End If
        Me.UpdatePreview()
    End Sub

#End Region

#Region " PROPERTIES "

    Private _backBrush As Brush

    ''' <summary> Gets or sets the background color for the control. </summary>
    ''' <value> A <see cref="T:System.Drawing.Color" /> that represents the background color of the
    ''' control. The default is the value of the
    ''' <see cref="P:System.Windows.Forms.Control.DefaultBackColor" /> property. </value>
    <DefaultValue(GetType(Color), "AppWorkspace")> _
    Public Overrides Property BackColor() As Color
        Get
            Return MyBase.BackColor
        End Get
        Set(ByVal value As Color)
            MyBase.BackColor = value
            Me._backBrush = New SolidBrush(value)
        End Set
    End Property

    Private _PrintDocument As PrintDocument

    ''' <summary> Gets or sets the document. </summary>
    ''' <value> The document. </value>
    Public Property Document() As PrintDocument
        Get
            Return Me._PrintDocument
        End Get
        Set(ByVal value As PrintDocument)
            If (Not value Is Me._PrintDocument) Then
                Me._PrintDocument = value
                Me.RefreshPreview()
            End If
        End Set
    End Property

    Private _IsRendering As Boolean

    ''' <summary> Gets or sets a value indicating whether this object is rendering. </summary>
    ''' <value> <c>true</c> if this object is rendering; otherwise <c>false</c> </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public ReadOnly Property IsRendering() As Boolean
        Get
            Return Me._IsRendering
        End Get
    End Property

    ''' <summary> Gets or sets the number of pages. </summary>
    ''' <value> The number of pages. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public ReadOnly Property PageCount() As Integer
        Get
            Return Me._PageImages.Count
        End Get
    End Property

    Private _PageImages As List(Of Image)

    ''' <summary> Gets or sets the page images. </summary>
    ''' <value> The page images. </value>
    <Browsable(False)> _
    Public ReadOnly Property PageImages() As PageImageCollection
        Get
            Return New PageImageCollection(Me._PageImages.ToArray)
        End Get
    End Property

    Private _startPage As Integer

    ''' <summary> Gets or sets the start page number. </summary>
    ''' <value> The Zero-Based start page number. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property StartPage() As Integer
        Get
            Return Me._startPage
        End Get
        Set(ByVal value As Integer)
            If (value > (Me.PageCount - 1)) Then
                value = (Me.PageCount - 1)
            End If
            If (value < 0) Then
                value = 0
            End If
            If (value <> Me._startPage) Then
                Me._startPage = value
                Me.UpdateScrollBars()
                Me.OnStartPageChanged(EventArgs.Empty)
            End If
        End Set
    End Property

    Private _zoom As Double
    ''' <summary> Gets or sets the zoom. </summary>
    ''' <value> The zoom. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property Zoom() As Double
        Get
            Return Me._zoom
        End Get
        Set(ByVal value As Double)
            If ((value <> Me._zoom) OrElse (Me.ZoomMode <> ZoomMode.Custom)) Then
                Me.ZoomMode = ZoomMode.Custom
                Me._zoom = value
                Me.UpdateScrollBars()
                Me.OnZoomModeChanged(EventArgs.Empty)
            End If
        End Set
    End Property

    Private _zoomMode As ZoomMode

    ''' <summary> Gets or sets the zoom mode. </summary>
    ''' <value> The zoom mode. </value>
    <DefaultValue(1)> _
    Public Property ZoomMode() As ZoomMode
        Get
            Return Me._zoomMode
        End Get
        Set(ByVal value As ZoomMode)
            If (value <> Me._zoomMode) Then
                Me._zoomMode = value
                Me.UpdateScrollBars()
                Me.OnZoomModeChanged(EventArgs.Empty)
            End If
        End Set
    End Property

#End Region

#Region " DOCUMENT PRINTER CLASS "

    ''' <summary> Document printer. </summary>
    ''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    ''' SOFTWARE.</para> </license>
    ''' <history date="5/2/2015" by="David" revision=""> Created. </history>
    Friend Class DocumentPrinter
        Inherits PrintDocument
        ' Methods
        Public Sub New(ByVal preview As CoolPrintPreviewControl, ByVal first As Integer, ByVal last As Integer)
            Me._first = first
            Me._last = last
            Me._pageImages = preview.PageImages
            Me.DefaultPageSettings = preview.Document.DefaultPageSettings
            Me.PrinterSettings = preview.Document.PrinterSettings
        End Sub

        ''' <summary> Raises the <see cref="E:System.Drawing.Printing.PrintDocument.BeginPrint" /> event.
        ''' It is called after the <see cref="M:System.Drawing.Printing.PrintDocument.Print" /> method is
        ''' called and before the first page of the document prints. </summary>
        ''' <param name="e"> A <see cref="T:System.Drawing.Printing.PrintEventArgs" /> that contains the
        ''' event data. </param>
        Protected Overrides Sub OnBeginPrint(ByVal e As PrintEventArgs)
            Me._index = Me._first
        End Sub

        ''' <summary> Raises the <see cref="E:System.Drawing.Printing.PrintDocument.PrintPage" /> event. It
        ''' is called before a page prints. </summary>
        ''' <param name="e"> A <see cref="T:System.Drawing.Printing.PrintPageEventArgs" /> that contains
        ''' the event data. </param>
        Protected Overrides Sub OnPrintPage(ByVal e As PrintPageEventArgs)
            If e Is Nothing Then Return
            e.Graphics.PageUnit = GraphicsUnit.Display
            e.Graphics.DrawImage(Me._pageImages.Item(Me._index), e.PageBounds)
            Me._index = Me._index + 1
            e.HasMorePages = (Me._index <= Me._last)
        End Sub

        ' Fields
        Private _first As Integer
        Private _pageImages As PageImageCollection
        Private _index As Integer
        Private _last As Integer
    End Class
#End Region

End Class

''' <summary> Collection of page images. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="5/2/2015" by="David" revision=""> Created. </history>
Public Class PageImageCollection
    Inherits Collections.ObjectModel.ReadOnlyCollection(Of Image)

    ''' <summary> Constructor. </summary>
    ''' <param name="images"> The images. </param>
    Public Sub New(ByVal images As Image())
        MyBase.New(images)
    End Sub
End Class

''' <summary> Values that represent zoom modes. </summary>
Public Enum ZoomMode
    ActualSize = 0
    FullPage = 1
    PageWidth = 2
    TwoPages = 3
    Custom = 4
End Enum
