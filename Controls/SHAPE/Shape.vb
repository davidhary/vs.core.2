Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Drawing.Drawing2D
''' <summary> Shape. </summary>
''' <remarks> Supports VB6 Shape controls. </remarks>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="10/13/2014" by="David" revision="">         Created. </history>
''' <history date="10/13/2014" by="David" revision="2.1.5399"> Created. </history>
Partial Public Class Shape
    Inherits Pith.MyUserControlBase
    Implements IDisposable

#Region " CONSTRUCTORS "

    ''' <summary> Constructor. </summary>
    Public Sub New()
        MyBase.new()
        InitializeComponent()

        Me.SetStyle(ControlStyles.UserPaint, True)
        Me.BackColor = Color.Transparent

        Me._FillBrush = New HatchBrush(HatchStyle.Horizontal, Color.Transparent, Color.Transparent)
        Me._BorderPen = New Pen(SystemColors.WindowText)
        Me._borderColor = SystemColors.WindowText
        Me._shape = ShapeStyle.Rectangle
        Me._backStyle = ShapeBackStyle.Transparent
        Me._fillStyle = ShapeFillStyle.Solid
        Me._borderStyle = ShapeBorderStyle.Solid
        Me._borderWidth = 1
        Me._fillColor = Color.Black
        Me._roundPercent = 0.15
        Me.SelectBrush()
        Me.selectPen()
    End Sub

    ''' <summary> Cleans up any resources being used. </summary>
    ''' <param name="disposing"> <c>true</c> if managed resources should be disposed; otherwise, false. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
                If Me._BorderPen IsNot Nothing Then Me._BorderPen.Dispose() : Me._BorderPen = Nothing
                If Me._FillBrush IsNot Nothing Then Me._FillBrush.Dispose() : Me._FillBrush = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " SHAPE PROPERTIES "

    ''' <summary>
    ''' Brush used to paint the Shape control.
    ''' </summary>
    Private _FillBrush As HatchBrush

    ' Private _backColor As Color

    ''' <summary> Background Color to display text and graphics. </summary>
    ''' <value> The color of the back. </value>
    <Description("Returns/sets the background color used to display text and graphics in an object."), Category("Appearance")> _
    Public Overrides Property BackColor() As Color
        Get
            Return MyBase.BackColor
        End Get
        Set(ByVal value As Color)
            MyBase.BackColor = value
            'Me.SelectBrush()
            'Me.Refresh()
        End Set
    End Property

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.BackColorChanged" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnBackColorChanged(e As System.EventArgs)
        MyBase.OnBackColorChanged(e)
        Me.SelectBrush()
        Me.Refresh()
    End Sub

    ''' <summary>
    ''' Stores the Back Style property.
    ''' </summary>
    Private _backStyle As ShapeBackStyle

    ''' <summary> Indicates whether a Label or the background of a Shape is transparent or opaque. </summary>
    ''' <value> The back style. </value>
    <Description("Indicates whether a Label or the background of a Shape is transparent or opaque."), Category("Appearance")> _
    Public Property BackStyle() As ShapeBackStyle
        Get
            Return Me._backStyle
        End Get
        Set(ByVal value As ShapeBackStyle)
            Me._backStyle = value
            Me.SelectBrush()
            Me.Refresh()
        End Set
    End Property

    ''' <summary>
    ''' Pen used to paint the border of the Shape Control.
    ''' </summary>
    Private _BorderPen As Pen

    ''' <summary> Stores the BorderColor property. </summary>
    Private _borderColor As Color

    ''' <summary> Color of the Shape border. </summary>
    ''' <value> The color of the border. </value>
    <Description("Returns/sets the color of an object's border."), Category("Appearance")> _
    Public Property BorderColor() As Color
        Get
            Return _borderColor
        End Get
        Set(ByVal value As Color)
            Me._borderColor = value
            Me.selectPen()
            Me.Refresh()
        End Set
    End Property

    ''' <summary> Select pen. </summary>
    Private Sub selectPen()
        Me._BorderPen.Width = Me.BorderWidth
        Me._BorderPen.DashOffset = 1500
        Select Case Me.BorderStyle
            Case ShapeBorderStyle.None
                Me._BorderPen.Color = Color.Transparent
            Case ShapeBorderStyle.Solid
                Me._BorderPen.Color = Me.BorderColor
                Me._BorderPen.DashStyle = DashStyle.Solid
            Case ShapeBorderStyle.Dash
                Me._BorderPen.Color = Me.BorderColor
                Me._BorderPen.DashStyle = DashStyle.Dash
            Case ShapeBorderStyle.Dot
                Me._BorderPen.Color = Me.BorderColor
                Me._BorderPen.DashStyle = DashStyle.Dot
            Case ShapeBorderStyle.DashDot
                Me._BorderPen.Color = Me.BorderColor
                Me._BorderPen.DashStyle = DashStyle.DashDot
            Case ShapeBorderStyle.DashDotDot
                Me._BorderPen.Color = Me.BorderColor
                Me._BorderPen.DashStyle = DashStyle.DashDotDot
            Case Else
                Me._borderStyle = ShapeBorderStyle.Solid
                Me._BorderPen.Color = Me.BorderColor
                Me._BorderPen.DashStyle = DashStyle.Solid
        End Select
    End Sub

    ''' <summary> Stores the Border Style property. </summary>
    Private _borderStyle As ShapeBorderStyle

    ''' <summary> Border style of the Shape control. </summary>
    ''' <value> The border style. </value>
    <Description("Returns/sets the border style for an object."), Category("Appearance")> _
    Public Shadows Property BorderStyle() As ShapeBorderStyle
        Get
            Return Me._borderStyle
        End Get
        Set(ByVal value As ShapeBorderStyle)
            Me._borderStyle = value
            Me.selectPen()
            Me.Refresh()
        End Set
    End Property


    ''' <summary> Stores the BorderWidth property. </summary>
    Private _borderWidth As Integer

    ''' <summary> Width of the Shape border. </summary>
    ''' <value> The width of the border. </value>
    <Description("Returns or sets the width of a control's border."), Category("Appearance")> _
    Public Property BorderWidth() As Integer
        Get
            Return _borderWidth
        End Get
        Set(ByVal value As Integer)
            Me._borderWidth = value
            Me._BorderPen.Width = Me.BorderWidth
            Me.Refresh()
        End Set
    End Property

    ''' <summary>
    ''' Stores FillColor property.
    ''' </summary>
    Private _fillColor As Color

    ''' <summary> Color to fill in Shape control. </summary>
    ''' <value> The color of the fill. </value>
    <Description("Returns/sets the color used to fill in shapes, circles, and boxes"), Category("Appearance")> _
    Public Property FillColor() As Color
        Get
            Return _fillColor
        End Get
        Set(ByVal value As Color)
            Me._fillColor = value
            Me.SelectBrush()
            Me.Refresh()
        End Set
    End Property

    ''' <summary> Select brush. </summary>
    Private Sub SelectBrush()
        Select Case Me.FillStyle
            Case ShapeFillStyle.Solid
                Me._FillBrush = New HatchBrush(HatchStyle.Horizontal, Me.FillColor, Me.FillColor)
            Case ShapeFillStyle.HorizontalLine
                Me._FillBrush = If(Me.BackStyle = ShapeBackStyle.Opaque, New HatchBrush(HatchStyle.Horizontal, Me.FillColor, Me.BackColor),
                                   New HatchBrush(HatchStyle.Horizontal, Me.FillColor, Color.Transparent))
            Case ShapeFillStyle.VerticalLine
                Me._FillBrush = If(Me.BackStyle = ShapeBackStyle.Opaque, New HatchBrush(HatchStyle.Vertical, Me.FillColor, Me.BackColor),
                                   New HatchBrush(HatchStyle.Vertical, Me.FillColor, Color.Transparent))
            Case ShapeFillStyle.DownwardDiagonal
                Me._FillBrush = If(Me.BackStyle = ShapeBackStyle.Opaque, New HatchBrush(HatchStyle.WideDownwardDiagonal, Me.FillColor, Me.BackColor),
                                   New HatchBrush(HatchStyle.WideDownwardDiagonal, Me.FillColor, Color.Transparent))
            Case ShapeFillStyle.UpwardDiagonal
                Me._FillBrush = If(Me.BackStyle = ShapeBackStyle.Opaque, New HatchBrush(HatchStyle.WideUpwardDiagonal, Me.FillColor, Me.BackColor),
                                   New HatchBrush(HatchStyle.WideUpwardDiagonal, Me.FillColor, Color.Transparent))
            Case ShapeFillStyle.Cross
                Me._FillBrush = If(Me.BackStyle = ShapeBackStyle.Opaque, New HatchBrush(HatchStyle.Cross, Me.FillColor, Me.BackColor),
                                   New HatchBrush(HatchStyle.Cross, Me.FillColor, Color.Transparent))
            Case ShapeFillStyle.DiagonalCross
                Me._FillBrush = If(Me.BackStyle = ShapeBackStyle.Opaque, New HatchBrush(HatchStyle.DiagonalCross, Me.FillColor, Me.BackColor),
                                   New HatchBrush(HatchStyle.DiagonalCross, Me.FillColor, Color.Transparent))
            Case ShapeFillStyle.Transparent
                Me._FillBrush = If(Me.BackStyle = ShapeBackStyle.Transparent,
                                   New HatchBrush(HatchStyle.Horizontal, Color.Transparent, Color.Transparent),
                                   New HatchBrush(HatchStyle.Horizontal, Me.BackColor, Me.BackColor))
            Case Else
                Me._FillBrush = If(Me.BackStyle = ShapeBackStyle.Opaque, New HatchBrush(HatchStyle.Horizontal, Me.FillColor, Me.BackColor),
                                   New HatchBrush(HatchStyle.DiagonalCross, Me.FillColor, Color.Transparent))
        End Select
    End Sub

    ''' <summary>
    ''' Stores FillStyle property.
    ''' </summary>
    Private _fillStyle As ShapeFillStyle

    ''' <summary> FillStyle in Shape control. </summary>
    ''' <value> The fill style. </value>
    <Description("Returns/sets the fill style of a shape"), Category("Appearance")> _
    Public Property FillStyle() As ShapeFillStyle
        Get
            Return Me._fillStyle
        End Get
        Set(ByVal value As ShapeFillStyle)
            Me._fillStyle = value
            Me.SelectBrush()
            Me.Refresh()
        End Set
    End Property

    ''' <summary>
    ''' Stores the Shape property.
    ''' </summary>
    Private _shape As ShapeStyle

    ''' <summary> The kind of Shape. </summary>
    ''' <value> The shape. </value>
    <Description("Returns/sets a value indicating the appearance of a control"), Category("Appearance")> _
    Public Property Shape() As ShapeStyle
        Get
            Return Me._shape
        End Get
        Set(ByVal value As ShapeStyle)
            Me._shape = value
            Me.Refresh()
        End Set
    End Property

    ''' <summary>
    ''' Stores the RoundPercent property.
    ''' </summary>
    Private _roundPercent As Double

    ''' <summary> Adds a property to specify the percent used to round the corners in round rectangles
    ''' and round squares. </summary>
    ''' <value> The round percent. </value>
    <Description("Allows to specify the percent used to round the corners of round rectangles and round squares")> _
    Public Property RoundPercent() As Integer
        Get
            Return CInt(Math.Floor(_roundPercent * 100))
        End Get
        Set(ByVal value As Integer)
            value = Math.Max(1, Math.Min(50, value))
            Me._roundPercent = CDbl(value) / 100
            Me.Refresh()
        End Set
    End Property

#End Region

#Region " PAINT "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnPaint(e As System.Windows.Forms.PaintEventArgs)
        MyBase.OnPaint(e)
        If e IsNot Nothing Then
            Dim _clientRectangle As New Rectangle(0, 0, ClientRectangle.Width - 1, ClientRectangle.Height - 1)
            Select Case Shape
                Case ShapeStyle.Rectangle
                    Me.DrawRectangle(_clientRectangle, e.Graphics)
                Case ShapeStyle.Square
                    Me.DrawSquare(_clientRectangle, e.Graphics)
                Case ShapeStyle.Oval
                    Me.DrawOval(_clientRectangle, e.Graphics)
                Case ShapeStyle.Circle
                    Me.DrawCircle(_clientRectangle, e.Graphics)
                Case ShapeStyle.RoundRectangle
                    Me.DrawRoundRectangle(_clientRectangle, e.Graphics)
                Case ShapeStyle.RoundSquare
                    Me.DrawRoundSquare(_clientRectangle, e.Graphics)
            End Select
        End If
    End Sub

    ''' <summary> Raises the system. event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnResize(e As System.EventArgs)
        MyBase.OnResize(e)
        ' Me.Refresh()
    End Sub

    ''' <summary> Draws a round square. </summary>
    ''' <param name="clientRectangle"> . </param>
    ''' <param name="g">               . </param>
    Private Sub DrawRoundSquare(ByVal clientRectangle As Rectangle, ByVal g As Graphics)
        Dim maxDiameter As Integer = Math.Min(clientRectangle.Height, clientRectangle.Width)
        Dim newClientRectangle As New Rectangle(clientRectangle.Location.X + (clientRectangle.Width - maxDiameter) \ 2,
                                                clientRectangle.Location.Y + (clientRectangle.Height - maxDiameter) \ 2, maxDiameter, maxDiameter)
        Me.DrawRoundRectangle(newClientRectangle, g)
    End Sub

    ''' <summary> Draws a round rectangle. </summary>
    ''' <param name="clientRectangle"> The region where to draw. </param>
    ''' <param name="g">               The GDI used to draw the rectangle. </param>
    Private Sub DrawRoundRectangle(ByVal clientRectangle As Rectangle, ByVal g As Graphics)
        Dim percentX As Double = clientRectangle.Width * Me.RoundPercent
        Dim percentY As Double = clientRectangle.Height * Me.RoundPercent
        Dim minPercent As Double = Math.Min(percentX, percentY)
        Dim halfPercentX As Double = percentX / 2
        Dim halfPercentY As Double = percentY / 2
        Dim minHalfPercent As Double = Math.Min(halfPercentX, halfPercentY)

        Dim pUp1 As New PointF(CSng(clientRectangle.X + minPercent), clientRectangle.Y)
        Dim pUp2 As New PointF(CSng(clientRectangle.X + clientRectangle.Width - minPercent), clientRectangle.Y)

        Dim pDown1 As New PointF(CSng(clientRectangle.X + clientRectangle.Width - minPercent), clientRectangle.Y + clientRectangle.Height)
        Dim pDown2 As New PointF(CSng(clientRectangle.X + minPercent), clientRectangle.Y + clientRectangle.Height)

        Dim pLeft1 As New PointF(clientRectangle.X, CSng(clientRectangle.Y + clientRectangle.Height - minPercent))
        Dim pLeft2 As New PointF(clientRectangle.X, CSng(clientRectangle.Y + minPercent))

        Dim pRight1 As New PointF(clientRectangle.X + clientRectangle.Width, CSng(clientRectangle.Y + minPercent))
        Dim pRight2 As New PointF(clientRectangle.X + clientRectangle.Width, CSng(clientRectangle.Y + clientRectangle.Height - minPercent))

        Dim pCornerA1 As New PointF(clientRectangle.X, CSng(clientRectangle.Y + minHalfPercent))
        Dim pCornerA2 As New PointF(CSng(clientRectangle.X + minHalfPercent), clientRectangle.Y)

        Dim pCornerB1 As New PointF(CSng(clientRectangle.X + clientRectangle.Width - minHalfPercent), clientRectangle.Y)
        Dim pCornerB2 As New PointF(clientRectangle.X + clientRectangle.Width, CSng(clientRectangle.Y + minHalfPercent))

        Dim pCornerC1 As New PointF(clientRectangle.X + clientRectangle.Width, CSng(clientRectangle.Y + clientRectangle.Height - minHalfPercent))
        Dim pCornerC2 As New PointF(CSng(clientRectangle.X + clientRectangle.Width - minHalfPercent), clientRectangle.Y + clientRectangle.Height)

        Dim pCornerD1 As New PointF(CSng(clientRectangle.X + minHalfPercent), clientRectangle.Y + clientRectangle.Height)
        Dim pCornerD2 As New PointF(clientRectangle.X, CSng(clientRectangle.Y + clientRectangle.Height - minHalfPercent))

        If (_backStyle <> ShapeBackStyle.Transparent) OrElse (_fillStyle <> ShapeFillStyle.Transparent) Then
            Using gPath As New GraphicsPath()
                gPath.AddLine(pUp1, pUp2)
                gPath.AddBezier(pUp2, pCornerB1, pCornerB2, pRight1)
                gPath.AddLine(pRight1, pRight2)
                gPath.AddBezier(pRight2, pCornerC1, pCornerC2, pDown1)
                gPath.AddLine(pDown1, pDown2)
                gPath.AddBezier(pDown2, pCornerD1, pCornerD2, pLeft1)
                gPath.AddLine(pLeft1, pLeft2)
                gPath.AddBezier(pLeft2, pCornerA1, pCornerA2, pUp1)
                Using region As New Region(gPath)
                    g.FillRegion(_FillBrush, region)
                End Using
            End Using
        End If

        g.DrawLine(Me._BorderPen, pUp1, pUp2)
        g.DrawLine(Me._BorderPen, pDown1, pDown2)
        g.DrawLine(Me._BorderPen, pLeft1, pLeft2)
        g.DrawLine(Me._BorderPen, pRight1, pRight2)

        g.DrawBezier(Me._BorderPen, pLeft2, pCornerA1, pCornerA2, pUp1)
        g.DrawBezier(Me._BorderPen, pUp2, pCornerB1, pCornerB2, pRight1)
        g.DrawBezier(Me._BorderPen, pRight2, pCornerC1, pCornerC2, pDown1)
        g.DrawBezier(Me._BorderPen, pDown2, pCornerD1, pCornerD2, pLeft1)
    End Sub

    ''' <summary> Draws a circle. </summary>
    ''' <param name="clientRectangle"> The region where to draw. </param>
    ''' <param name="g">               The GDI used to draw the rectangle. </param>
    Private Sub DrawCircle(ByVal clientRectangle As Rectangle, ByVal g As Graphics)

        Dim maxDiameter As Integer = Math.Min(clientRectangle.Height, clientRectangle.Width)
        Dim newClientRectangle As New Rectangle(clientRectangle.Location.X + (clientRectangle.Width - maxDiameter) \ 2,
                                                clientRectangle.Location.Y + (clientRectangle.Height - maxDiameter) \ 2, maxDiameter, maxDiameter)
        If (Me._backStyle <> ShapeBackStyle.Transparent) OrElse (Me._fillStyle <> ShapeFillStyle.Transparent) Then
            g.FillEllipse(_FillBrush, newClientRectangle)
        End If

        g.DrawEllipse(Me._BorderPen, newClientRectangle)
    End Sub

    ''' <summary> Draws an oval. </summary>
    ''' <param name="clientRectangle"> The region where to draw. </param>
    ''' <param name="g">               The GDI used to draw the rectangle. </param>
    Private Sub DrawOval(ByVal clientRectangle As Rectangle, ByVal g As Graphics)
        If (Me.BackStyle <> ShapeBackStyle.Transparent) OrElse (Me.FillStyle <> ShapeFillStyle.Transparent) Then
            g.FillEllipse(_FillBrush, clientRectangle)
        End If

        g.DrawEllipse(Me._BorderPen, clientRectangle)
    End Sub

    ''' <summary> Draws a square. </summary>
    ''' <param name="clientRectangle"> The region where to draw. </param>
    ''' <param name="g">               The GDI used to draw the rectangle. </param>
    Private Sub DrawSquare(ByVal clientRectangle As Rectangle, ByVal g As Graphics)
        Dim maxDiameter As Integer = Math.Min(clientRectangle.Height, clientRectangle.Width)
        Dim newClientRectangle As New Rectangle(clientRectangle.Location.X + (clientRectangle.Width - maxDiameter) \ 2, clientRectangle.Location.Y + (clientRectangle.Height - maxDiameter) \ 2, maxDiameter, maxDiameter)

        If (_backStyle <> ShapeBackStyle.Transparent) OrElse (_fillStyle <> ShapeFillStyle.Transparent) Then
            g.FillRectangle(_FillBrush, newClientRectangle)
        End If
        g.DrawRectangle(Me._BorderPen, newClientRectangle)
    End Sub

    ''' <summary> Draws a rectangle. </summary>
    ''' <param name="clientRectangle"> The region where to draw. </param>
    ''' <param name="g">               The GDI used to draw the rectangle. </param>
    Private Sub DrawRectangle(ByVal clientRectangle As Rectangle, ByVal g As Graphics)
        If (Me.BackStyle <> ShapeBackStyle.Transparent) OrElse (Me.FillStyle <> ShapeFillStyle.Transparent) Then
            g.FillRectangle(_FillBrush, clientRectangle)
        End If

        g.DrawRectangle(Me._BorderPen, clientRectangle)
    End Sub

    ''' <summary> Overriding <see cref="CreateParams"/> method from UserControl. </summary>
    ''' <value> Options that control the create. </value>
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ExStyle = cp.ExStyle Or &H20 'WS_EX_TRANSPARENT
            Return cp
        End Get
    End Property

    ''' <summary> Overriding OnPaintBackground method from UserControl. </summary>
    ''' <param name="pevent"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnPaintBackground(ByVal pevent As PaintEventArgs)
        'do not allow the background to be painted  
    End Sub
#End Region

End Class

''' <summary> Values that represent Shape Style. </summary>
Public Enum ShapeStyle
    ''' <summary> An enum constant representing the rectangle option. </summary>
    Rectangle = 0
    ''' <summary> An enum constant representing the square option. </summary>
    Square = 1
    ''' <summary> An enum constant representing the oval option. </summary>
    Oval = 2
    ''' <summary> An enum constant representing the circle option. </summary>
    Circle = 3
    ''' <summary> An enum constant representing the round rectangle option. </summary>
    RoundRectangle = 4
    ''' <summary> An enum constant representing the round square option. </summary>
    RoundSquare = 5
End Enum

''' <summary> Values that represent BackStyle. </summary>
Public Enum ShapeBackStyle
    ''' <summary> An enum constant representing the transparent option. </summary>
    Transparent = 0
    ''' <summary> An enum constant representing the opaque option. </summary>
    Opaque = 1
End Enum

''' <summary> Values that represent ShapeBorderStyle. </summary>
Public Enum ShapeBorderStyle
    ''' <summary> An enum constant representing the none option. </summary>
    None
    ''' <summary> An enum constant representing the solid option. </summary>
    Solid
    ''' <summary> An enum constant representing the dash option. </summary>
    Dash
    ''' <summary> An enum constant representing the dot option. </summary>
    Dot
    ''' <summary> An enum constant representing the dash dot option. </summary>
    DashDot
    ''' <summary> An enum constant representing the dash dot option. </summary>
    DashDotDot
End Enum

''' <summary> Values that represent ShapeFillStyle. </summary>
Public Enum ShapeFillStyle
    ''' <summary> An enum constant representing the solid option. </summary>
    Solid = 0
    ''' <summary> An enum constant representing the transparent option. </summary>
    Transparent = 1
    ''' <summary> An enum constant representing the horizontal line option. </summary>
    HorizontalLine = 2
    ''' <summary> An enum constant representing the vertical line option. </summary>
    VerticalLine = 3
    ''' <summary> An enum constant representing the downward diagonal option. </summary>
    DownwardDiagonal = 4
    ''' <summary> An enum constant representing the upward diagonal option. </summary>
    UpwardDiagonal = 5
    ''' <summary> An enum constant representing the cross option. </summary>
    Cross = 6
    ''' <summary> An enum constant representing the diagonal cross option. </summary>
    DiagonalCross = 7
End Enum

