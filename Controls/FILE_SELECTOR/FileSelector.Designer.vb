<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FileSelector

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Friend _toolTip As System.Windows.Forms.ToolTip
    Private WithEvents _FilePathTextBox As System.Windows.Forms.TextBox
    Private WithEvents _browseButton As System.Windows.Forms.Button
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._browseButton = New System.Windows.Forms.Button()
        Me._FilePathTextBox = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        '_browseButton
        '
        Me._browseButton.BackColor = System.Drawing.SystemColors.Control
        Me._browseButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._browseButton.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me._browseButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._browseButton.Location = New System.Drawing.Point(516, 1)
        Me._browseButton.Name = "_browseButton"
        Me._browseButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._browseButton.Size = New System.Drawing.Size(29, 22)
        Me._browseButton.TabIndex = 0
        Me._browseButton.Text = "..."
        Me._browseButton.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me._toolTip.SetToolTip(Me._browseButton, "Browses for a file")
        Me._browseButton.UseMnemonic = False
        Me._browseButton.UseVisualStyleBackColor = True
        '
        '_FilePathTextBox
        '
        Me._FilePathTextBox.AcceptsReturn = True
        Me._FilePathTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._FilePathTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._FilePathTextBox.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me._FilePathTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._FilePathTextBox.Location = New System.Drawing.Point(0, 0)
        Me._FilePathTextBox.MaxLength = 0
        Me._FilePathTextBox.Name = "_FilePathTextBox"
        Me._FilePathTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FilePathTextBox.Size = New System.Drawing.Size(510, 25)
        Me._FilePathTextBox.TabIndex = 1
        '
        'FileSelector
        '
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.Controls.Add(Me._FilePathTextBox)
        Me.Controls.Add(Me._browseButton)
        Me.Name = "FileSelector"
        Me.Size = New System.Drawing.Size(552, 28)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

End Class
