﻿Imports System.Drawing.Drawing2D
Imports isr.Core.Controls.ColorExtensions
''' <summary> A toggle switch brushed metal renderer. </summary>
''' <remarks> David, 12/4/2015. </remarks>
''' <license>
''' (c) 2015 Johnny J.. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/4/2015" by="David" revision=""> Created. </history>
Public Class ToggleSwitchBrushedMetalRenderer
    Inherits ToggleSwitchRendererBase

#Region "Constructor"

    Public Sub New()
        BorderColor1 = Color.FromArgb(255, 145, 146, 149)
        BorderColor2 = Color.FromArgb(255, 227, 229, 232)
        BackColor1 = Color.FromArgb(255, 125, 126, 128)
        BackColor2 = Color.FromArgb(255, 224, 226, 228)
        UpperShadowColor1 = Color.FromArgb(150, 0, 0, 0)
        UpperShadowColor2 = Color.FromArgb(5, 0, 0, 0)
        ButtonNormalBorderColor = Color.FromArgb(255, 144, 144, 145)
        ButtonNormalSurfaceColor = Color.FromArgb(255, 251, 251, 251)
        ButtonHoverBorderColor = Color.FromArgb(255, 166, 167, 168)
        ButtonHoverSurfaceColor = Color.FromArgb(255, 238, 238, 238)
        ButtonPressedBorderColor = Color.FromArgb(255, 123, 123, 123)
        ButtonPressedSurfaceColor = Color.FromArgb(255, 184, 184, 184)

        UpperShadowHeight = 8
    End Sub

#End Region ' Constructor

#Region "Public Properties"

    Public Property BorderColor1() As Color
    Public Property BorderColor2() As Color
    Public Property BackColor1() As Color
    Public Property BackColor2() As Color
    Public Property UpperShadowColor1() As Color
    Public Property UpperShadowColor2() As Color
    Public Property ButtonNormalBorderColor() As Color
    Public Property ButtonNormalSurfaceColor() As Color
    Public Property ButtonHoverBorderColor() As Color
    Public Property ButtonHoverSurfaceColor() As Color
    Public Property ButtonPressedBorderColor() As Color
    Public Property ButtonPressedSurfaceColor() As Color

    Public Property UpperShadowHeight() As Integer

#End Region ' Public Properties

#Region "Render Method Implementations"

    Public Overrides Sub RenderBorder(ByVal g As Graphics, ByVal borderRectangle As Rectangle)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        'Draw outer border
        Using outerControlPath As GraphicsPath = GetRectangleClipPath(borderRectangle)
            g.SetClip(outerControlPath)

            'INSTANT VB NOTE: The variable borderColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
            Dim borderColor1_Renamed As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, BorderColor1.ToGrayscale(), BorderColor1)
            'INSTANT VB NOTE: The variable borderColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
            Dim borderColor2_Renamed As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, BorderColor2.ToGrayscale(), BorderColor2)

            Using borderBrush As Brush = New LinearGradientBrush(borderRectangle, borderColor1_Renamed, borderColor2_Renamed, LinearGradientMode.Vertical)
                g.FillPath(borderBrush, outerControlPath)
            End Using

            g.ResetClip()
        End Using

        'Draw inner background
        Dim innercontrolRectangle As New Rectangle(borderRectangle.X + 1, borderRectangle.Y + 1, borderRectangle.Width - 1, borderRectangle.Height - 2)

        Using innerControlPath As GraphicsPath = GetRectangleClipPath(innercontrolRectangle)
            g.SetClip(innerControlPath)

            'INSTANT VB NOTE: The variable backColor1 was renamed since Visual Basic does not handle local variables named the same as class members well:
            Dim backColor1_Renamed As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, BackColor1.ToGrayscale(), BackColor1)
            'INSTANT VB NOTE: The variable backColor2 was renamed since Visual Basic does not handle local variables named the same as class members well:
            Dim backColor2_Renamed As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, BackColor2.ToGrayscale(), BackColor2)

            Using backgroundBrush As Brush = New LinearGradientBrush(borderRectangle, backColor1_Renamed, backColor2_Renamed, LinearGradientMode.Horizontal)
                g.FillPath(backgroundBrush, innerControlPath)
            End Using

            'Draw inner top shadow
            Dim upperShadowRectangle As New Rectangle(innercontrolRectangle.X, innercontrolRectangle.Y, innercontrolRectangle.Width, UpperShadowHeight)

            g.IntersectClip(upperShadowRectangle)

            Using shadowBrush As Brush = New LinearGradientBrush(upperShadowRectangle, UpperShadowColor1, UpperShadowColor2, LinearGradientMode.Vertical)
                g.FillRectangle(shadowBrush, upperShadowRectangle)
            End Using

            g.ResetClip()
        End Using
    End Sub

    Public Overrides Sub RenderLeftToggleField(ByVal g As Graphics, ByVal leftRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        Dim innercontrolRectangle As New Rectangle(1, 1, ToggleSwitch.Width - 1, ToggleSwitch.Height - 2)

        Using innerControlPath As GraphicsPath = GetRectangleClipPath(innercontrolRectangle)
            g.SetClip(innerControlPath)

            'Draw image or text
            If ToggleSwitch.OnSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(ToggleSwitch.OnText)) Then
                Dim fullRectangle As New RectangleF(leftRectangle.X + 2 - (totalToggleFieldWidth - leftRectangle.Width), 2, totalToggleFieldWidth - 2, ToggleSwitch.Height - 4)

                g.IntersectClip(fullRectangle)

                If ToggleSwitch.OnSideImage IsNot Nothing Then
                    Dim imageSize As Size = ToggleSwitch.OnSideImage.Size
                    Dim imageRectangle As Rectangle

                    Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                    If ToggleSwitch.OnSideScaleImageToFit Then
                        Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                        Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                        If ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                        ElseIf ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle)
                        End If
                    Else
                        If ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                        ElseIf ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImageUnscaled(ToggleSwitch.OnSideImage, imageRectangle)
                        End If
                    End If
                ElseIf Not String.IsNullOrEmpty(ToggleSwitch.OnText) Then
                    Dim textSize As SizeF = g.MeasureString(ToggleSwitch.OnText, ToggleSwitch.OnFont)

                    Dim textXPos As Single = fullRectangle.X

                    If ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Center Then
                        textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                    ElseIf ToggleSwitch.OnSideAlignment = ToggleSwitchAlignment.Near Then
                        textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                    End If

                    Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                    Dim textForeColor As Color = ToggleSwitch.OnForeColor

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        textForeColor = textForeColor.ToGrayscale()
                    End If

                    Using textBrush As Brush = New SolidBrush(textForeColor)
                        g.DrawString(ToggleSwitch.OnText, ToggleSwitch.OnFont, textBrush, textRectangle)
                    End Using
                End If
            End If

            g.ResetClip()
        End Using
    End Sub

    Public Overrides Sub RenderRightToggleField(ByVal g As Graphics, ByVal rightRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        Dim innercontrolRectangle As New Rectangle(1, 1, ToggleSwitch.Width - 1, ToggleSwitch.Height - 2)

        Using innerControlPath As GraphicsPath = GetRectangleClipPath(innercontrolRectangle)
            g.SetClip(innerControlPath)

            'Draw image or text
            If ToggleSwitch.OffSideImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(ToggleSwitch.OffText)) Then
                Dim fullRectangle As New RectangleF(rightRectangle.X, 2, totalToggleFieldWidth - 2, ToggleSwitch.Height - 4)

                g.IntersectClip(fullRectangle)

                If ToggleSwitch.OffSideImage IsNot Nothing Then
                    Dim imageSize As Size = ToggleSwitch.OffSideImage.Size
                    Dim imageRectangle As Rectangle

                    Dim imageXPos As Integer = CInt(Fix(fullRectangle.X))

                    If ToggleSwitch.OffSideScaleImageToFit Then
                        Dim canvasSize As New Size(CInt(fullRectangle.Width), CInt(fullRectangle.Height))
                        Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                        If ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)) / 2)))
                        ElseIf ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(resizedImageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle)
                        End If
                    Else
                        If ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(imageSize.Width)) / 2)))
                        ElseIf ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                            imageXPos = CInt(Fix(CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(imageSize.Width)))
                        End If

                        imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                            g.DrawImage(ToggleSwitch.OnSideImage, imageRectangle, 0, 0, ToggleSwitch.OnSideImage.Width, ToggleSwitch.OnSideImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                        Else
                            g.DrawImageUnscaled(ToggleSwitch.OffSideImage, imageRectangle)
                        End If
                    End If
                ElseIf Not String.IsNullOrEmpty(ToggleSwitch.OffText) Then
                    Dim textSize As SizeF = g.MeasureString(ToggleSwitch.OffText, ToggleSwitch.OffFont)

                    Dim textXPos As Single = fullRectangle.X

                    If ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Center Then
                        textXPos = CSng(fullRectangle.X) + ((CSng(fullRectangle.Width) - CSng(textSize.Width)) / 2)
                    ElseIf ToggleSwitch.OffSideAlignment = ToggleSwitchAlignment.Far Then
                        textXPos = CSng(fullRectangle.X) + CSng(fullRectangle.Width) - CSng(textSize.Width)
                    End If

                    Dim textRectangle As New RectangleF(textXPos, CSng(fullRectangle.Y) + ((CSng(fullRectangle.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                    Dim textForeColor As Color = ToggleSwitch.OffForeColor

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        textForeColor = textForeColor.ToGrayscale()
                    End If

                    Using textBrush As Brush = New SolidBrush(textForeColor)
                        g.DrawString(ToggleSwitch.OffText, ToggleSwitch.OffFont, textBrush, textRectangle)
                    End Using
                End If
            End If

            g.ResetClip()
        End Using
    End Sub

    Public Overrides Sub RenderButton(ByVal g As Graphics, ByVal buttonRectangle As Rectangle)
        If g Is Nothing Then Return
        g.SmoothingMode = SmoothingMode.HighQuality
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
        g.InterpolationMode = InterpolationMode.HighQualityBilinear

        g.SetClip(buttonRectangle)

        'Draw button surface
        Dim buttonSurfaceColor As Color = ButtonNormalSurfaceColor

        If ToggleSwitch.IsButtonPressed Then
            buttonSurfaceColor = ButtonPressedSurfaceColor
        ElseIf ToggleSwitch.IsButtonHovered Then
            buttonSurfaceColor = ButtonHoverSurfaceColor
        End If

        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
            buttonSurfaceColor = buttonSurfaceColor.ToGrayscale()
        End If

        Using buttonSurfaceBrush As Brush = New SolidBrush(buttonSurfaceColor)
            g.FillEllipse(buttonSurfaceBrush, buttonRectangle)
        End Using

        'Draw "metal" surface
        Dim centerPoint1 As New PointF(buttonRectangle.X + (buttonRectangle.Width / 2.0F), buttonRectangle.Y + 1.2F * (buttonRectangle.Height / 2.0F))

        Using firstMetalBrush As PathGradientBrush = GetBrush(New Color() {Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.FromArgb(255, 110, 110, 110), Color.Transparent, Color.Transparent, Color.Transparent}, buttonRectangle, centerPoint1)
            g.FillEllipse(firstMetalBrush, buttonRectangle)
        End Using

        Dim centerPoint2 As New PointF(buttonRectangle.X + 0.8F * (buttonRectangle.Width / 2.0F), buttonRectangle.Y + (buttonRectangle.Height / 2.0F))

        Using secondMetalBrush As PathGradientBrush = GetBrush(New Color() {Color.FromArgb(255, 110, 110, 110), Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.FromArgb(255, 110, 110, 110)}, buttonRectangle, centerPoint2)
            g.FillEllipse(secondMetalBrush, buttonRectangle)
        End Using

        Dim centerPoint3 As New PointF(buttonRectangle.X + 1.2F * (buttonRectangle.Width / 2.0F), buttonRectangle.Y + (buttonRectangle.Height / 2.0F))

        Using thirdMetalBrush As PathGradientBrush = GetBrush(New Color() {Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.FromArgb(255, 98, 98, 98), Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent}, buttonRectangle, centerPoint3)
            g.FillEllipse(thirdMetalBrush, buttonRectangle)
        End Using

        Dim centerPoint4 As New PointF(buttonRectangle.X + 0.9F * (buttonRectangle.Width / 2.0F), buttonRectangle.Y + 0.9F * (buttonRectangle.Height / 2.0F))

        Using fourthMetalBrush As PathGradientBrush = GetBrush(New Color() {Color.Transparent, Color.FromArgb(255, 188, 188, 188), Color.FromArgb(255, 110, 110, 110), Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent, Color.Transparent}, buttonRectangle, centerPoint4)
            g.FillEllipse(fourthMetalBrush, buttonRectangle)
        End Using

        'Draw button border
        Dim buttonBorderColor As Color = ButtonNormalBorderColor

        If ToggleSwitch.IsButtonPressed Then
            buttonBorderColor = ButtonPressedBorderColor
        ElseIf ToggleSwitch.IsButtonHovered Then
            buttonBorderColor = ButtonHoverBorderColor
        End If

        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
            buttonBorderColor = buttonBorderColor.ToGrayscale()
        End If

        Using buttonBorderPen As New Pen(buttonBorderColor)
            g.DrawEllipse(buttonBorderPen, buttonRectangle)
        End Using

        g.ResetClip()
    End Sub

#End Region ' Render Method Implementations

#Region "Helper Method Implementations"

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Shared Function GetRectangleClipPath(ByVal rect As Rectangle) As GraphicsPath
        Dim borderPath As New GraphicsPath()
        borderPath.AddArc(rect.X, rect.Y, rect.Height, rect.Height, 90, 180)
        borderPath.AddArc(rect.Width - rect.Height, rect.Y, rect.Height, rect.Height, 270, 180)
        borderPath.CloseFigure()
        Return borderPath
    End Function

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")>
    Public Function GetButtonClipPath() As GraphicsPath
        Dim buttonRectangle As Rectangle = GetButtonRectangle()

        Dim buttonPath As New GraphicsPath()

        buttonPath.AddArc(buttonRectangle.X, buttonRectangle.Y, buttonRectangle.Height, buttonRectangle.Height, 0, 360)

        Return buttonPath
    End Function

    Public Overrides Function GetButtonWidth() As Integer
        Return ToggleSwitch.Height - 2
    End Function

    Public Overrides Function GetButtonRectangle() As Rectangle
        Dim buttonWidth As Integer = GetButtonWidth()
        Return GetButtonRectangle(buttonWidth)
    End Function

    Public Overrides Function GetButtonRectangle(ByVal buttonWidth As Integer) As Rectangle
        Dim buttonRect As New Rectangle(ToggleSwitch.ButtonValue, 1, buttonWidth, buttonWidth)
        Return buttonRect
    End Function

    <CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Private Shared Function GetBrush(ByVal Colors() As Color, ByVal r As RectangleF, ByVal centerPoint As PointF) As PathGradientBrush
        Dim i As Integer = Colors.Length - 1
        Dim points(i) As PointF

        Dim a As Single = 0F
        Dim n As Integer = 0
        Dim cx As Single = r.Width / 2.0F
        Dim cy As Single = r.Height / 2.0F

        Dim w As Integer = CInt(Fix(Math.Floor((180.0 * (i - 2.0) / i) / 2.0)))
        Dim wi As Double = w * Math.PI / 180.0
        Dim faktor As Double = 1.0 / Math.Sin(wi)

        Dim radx As Single = CSng(cx * faktor)
        Dim rady As Single = CSng(cy * faktor)

        Do While a <= Math.PI * 2
            points(n) = New PointF(CSng((cx + (Math.Cos(a) * radx))) + r.Left, CSng((cy + (Math.Sin(a) * rady))) + r.Top)
            n += 1
            a += CSng(Math.PI * 2 / i)
            ' a += CSng(Math.PI * 2 \ i)
        Loop

        points(i) = points(0)
        Dim graphicsPath As New GraphicsPath()
        graphicsPath.AddLines(points)

        Dim fBrush As New PathGradientBrush(graphicsPath)
        fBrush.CenterPoint = centerPoint
        fBrush.CenterColor = Color.Transparent
        fBrush.SurroundColors = New Color() {Color.White}

        Try
            fBrush.SurroundColors = Colors
        Catch ex As Exception
            Throw New InvalidOperationException("Too may colors!", ex)
        End Try

        Return fBrush
    End Function

#End Region ' Helper Method Implementations
End Class
