﻿Imports isr.Core.Controls.ColorExtensions
''' <summary> A toggle switch android renderer. </summary>
''' <license>
''' (c) 2015 Johnny J.. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="12/4/2015" by="David" revision=""> Created. </history>
Public Class ToggleSwitchAndroidRenderer
    Inherits ToggleSwitchRendererBase

#Region "Constructor"

    Public Sub New()
        BorderColor = Color.FromArgb(255, 166, 166, 166)
        BackColor = Color.FromArgb(255, 32, 32, 32)
        LeftSideColor = Color.FromArgb(255, 32, 32, 32)
        RightSideColor = Color.FromArgb(255, 32, 32, 32)
        OffButtonColor = Color.FromArgb(255, 70, 70, 70)
        OnButtonColor = Color.FromArgb(255, 27, 161, 226)
        OffButtonBorderColor = Color.FromArgb(255, 70, 70, 70)
        OnButtonBorderColor = Color.FromArgb(255, 27, 161, 226)
        SlantAngle = 8
    End Sub

#End Region ' Constructor

#Region "Public Properties"

    Public Property BorderColor() As Color
    Public Property BackColor() As Color
    Public Property LeftSideColor() As Color
    Public Property RightSideColor() As Color
    Public Property OffButtonColor() As Color
    Public Property OnButtonColor() As Color
    Public Property OffButtonBorderColor() As Color
    Public Property OnButtonBorderColor() As Color
    Public Property SlantAngle() As Integer

#End Region ' Public Properties

#Region "Render Method Implementations"

    Public Overrides Sub RenderBorder(ByVal g As Graphics, ByVal borderRectangle As Rectangle)
        If g Is Nothing Then Return
        Dim renderColor As Color = If((Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled, BorderColor.ToGrayscale(), BorderColor)
        g.SetClip(borderRectangle)
        Using borderPen As New Pen(renderColor)
            g.DrawRectangle(borderPen, borderRectangle.X, borderRectangle.Y, borderRectangle.Width - 1, borderRectangle.Height - 1)
        End Using
    End Sub

    Public Overrides Sub RenderLeftToggleField(ByVal g As Graphics, ByVal leftRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        Dim leftColor As Color = LeftSideColor

        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
            leftColor = leftColor.ToGrayscale()
        End If

        Dim controlRectangle As Rectangle = GetInnerControlRectangle()

        g.SetClip(controlRectangle)

        Dim halfCathetusLength As Integer = GetHalfCathetusLengthBasedOnAngle()

        Dim adjustedLeftRect As New Rectangle(leftRectangle.X, leftRectangle.Y, leftRectangle.Width + halfCathetusLength, leftRectangle.Height)

        g.IntersectClip(adjustedLeftRect)

        Using leftBrush As Brush = New SolidBrush(leftColor)
            g.FillRectangle(leftBrush, adjustedLeftRect)
        End Using

        g.ResetClip()
    End Sub

    Public Overrides Sub RenderRightToggleField(ByVal g As Graphics, ByVal rightRectangle As Rectangle, ByVal totalToggleFieldWidth As Integer)
        If g Is Nothing Then Return
        Dim rightColor As Color = RightSideColor

        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
            rightColor = rightColor.ToGrayscale()
        End If

        Dim controlRectangle As Rectangle = GetInnerControlRectangle()

        g.SetClip(controlRectangle)

        Dim halfCathetusLength As Integer = GetHalfCathetusLengthBasedOnAngle()

        Dim adjustedRightRect As New Rectangle(rightRectangle.X - halfCathetusLength, rightRectangle.Y, rightRectangle.Width + halfCathetusLength, rightRectangle.Height)

        g.IntersectClip(adjustedRightRect)

        Using rightBrush As Brush = New SolidBrush(rightColor)
            g.FillRectangle(rightBrush, adjustedRightRect)
        End Using

        g.ResetClip()
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    Public Overrides Sub RenderButton(ByVal g As Graphics, ByVal buttonRectangle As Rectangle)
        If g Is Nothing Then Return
        Dim controlRectangle As Rectangle = GetInnerControlRectangle()

        g.SetClip(controlRectangle)

        Dim fullCathetusLength As Integer = GetCathetusLengthBasedOnAngle()
        Dim dblFullCathetusLength As Integer = 2 * fullCathetusLength

        Dim polygonPoints(3) As Point

        Dim adjustedButtonRect As New Rectangle(buttonRectangle.X - fullCathetusLength, controlRectangle.Y, buttonRectangle.Width + dblFullCathetusLength, controlRectangle.Height)

        If SlantAngle > 0 Then
            polygonPoints(0) = New Point(adjustedButtonRect.X + fullCathetusLength, adjustedButtonRect.Y)
            polygonPoints(1) = New Point(adjustedButtonRect.X + adjustedButtonRect.Width - 1, adjustedButtonRect.Y)
            polygonPoints(2) = New Point(adjustedButtonRect.X + adjustedButtonRect.Width - fullCathetusLength - 1, adjustedButtonRect.Y + adjustedButtonRect.Height - 1)
            polygonPoints(3) = New Point(adjustedButtonRect.X, adjustedButtonRect.Y + adjustedButtonRect.Height - 1)
        Else
            polygonPoints(0) = New Point(adjustedButtonRect.X, adjustedButtonRect.Y)
            polygonPoints(1) = New Point(adjustedButtonRect.X + adjustedButtonRect.Width - fullCathetusLength - 1, adjustedButtonRect.Y)
            polygonPoints(2) = New Point(adjustedButtonRect.X + adjustedButtonRect.Width - 1, adjustedButtonRect.Y + adjustedButtonRect.Height - 1)
            polygonPoints(3) = New Point(adjustedButtonRect.X + fullCathetusLength, adjustedButtonRect.Y + adjustedButtonRect.Height - 1)
        End If

        g.IntersectClip(adjustedButtonRect)

        Dim buttonColor As Color = If(ToggleSwitch.Checked, OnButtonColor, OffButtonColor)
        Dim buttonBorderColor As Color = If(ToggleSwitch.Checked, OnButtonBorderColor, OffButtonBorderColor)

        If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
            buttonColor = buttonColor.ToGrayscale()
            buttonBorderColor = buttonBorderColor.ToGrayscale()
        End If

        Using buttonPen As New Pen(buttonBorderColor)
            g.DrawPolygon(buttonPen, polygonPoints)
        End Using

        Using buttonBrush As Brush = New SolidBrush(buttonColor)
            g.FillPolygon(buttonBrush, polygonPoints)
        End Using

        Dim buttonImage As Image = If(ToggleSwitch.ButtonImage, (If(ToggleSwitch.Checked, ToggleSwitch.OnButtonImage, ToggleSwitch.OffButtonImage)))
        Dim buttonText As String = If(ToggleSwitch.Checked, ToggleSwitch.OnText, ToggleSwitch.OffText)

        If buttonImage IsNot Nothing OrElse (Not String.IsNullOrEmpty(buttonText)) Then
            Dim alignment As ToggleSwitchButtonAlignment = If(ToggleSwitch.ButtonImage IsNot Nothing, ToggleSwitch.ButtonAlignment, (If(ToggleSwitch.Checked, ToggleSwitch.OnButtonAlignment, ToggleSwitch.OffButtonAlignment)))

            If buttonImage IsNot Nothing Then
                Dim imageSize As Size = buttonImage.Size
                Dim imageRectangle As Rectangle

                Dim imageXPos As Integer = CInt(adjustedButtonRect.X)

                Dim scaleImage As Boolean = If(ToggleSwitch.ButtonImage IsNot Nothing, ToggleSwitch.ButtonScaleImageToFit, (If(ToggleSwitch.Checked, ToggleSwitch.OnButtonScaleImageToFit, ToggleSwitch.OffButtonScaleImageToFit)))

                If scaleImage Then
                    Dim canvasSize As Size = adjustedButtonRect.Size
                    Dim resizedImageSize As Size = ImageHelper.RescaleImageToFit(imageSize, canvasSize)

                    If alignment = ToggleSwitchButtonAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(adjustedButtonRect.X) + ((CSng(adjustedButtonRect.Width) - CSng(resizedImageSize.Width)) / 2)))
                    ElseIf alignment = ToggleSwitchButtonAlignment.Right Then
                        imageXPos = CInt(Fix(CSng(adjustedButtonRect.X) + CSng(adjustedButtonRect.Width) - CSng(resizedImageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(adjustedButtonRect.Y) + ((CSng(adjustedButtonRect.Height) - CSng(resizedImageSize.Height)) / 2))), resizedImageSize.Width, resizedImageSize.Height)

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImage(buttonImage, imageRectangle)
                    End If
                Else
                    If alignment = ToggleSwitchButtonAlignment.Center Then
                        imageXPos = CInt(Fix(CSng(adjustedButtonRect.X) + ((CSng(adjustedButtonRect.Width) - CSng(imageSize.Width)) / 2)))
                    ElseIf alignment = ToggleSwitchButtonAlignment.Right Then
                        imageXPos = CInt(Fix(CSng(adjustedButtonRect.X) + CSng(adjustedButtonRect.Width) - CSng(imageSize.Width)))
                    End If

                    imageRectangle = New Rectangle(imageXPos, CInt(Fix(CSng(adjustedButtonRect.Y) + ((CSng(adjustedButtonRect.Height) - CSng(imageSize.Height)) / 2))), imageSize.Width, imageSize.Height)

                    If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                        g.DrawImage(buttonImage, imageRectangle, 0, 0, buttonImage.Width, buttonImage.Height, GraphicsUnit.Pixel, ImageHelper.GetGrayscaleAttributes())
                    Else
                        g.DrawImageUnscaled(buttonImage, imageRectangle)
                    End If
                End If
            ElseIf Not String.IsNullOrEmpty(buttonText) Then
                Dim buttonFont As Font = If(ToggleSwitch.Checked, ToggleSwitch.OnFont, ToggleSwitch.OffFont)
                Dim buttonForeColor As Color = If(ToggleSwitch.Checked, ToggleSwitch.OnForeColor, ToggleSwitch.OffForeColor)

                If (Not ToggleSwitch.Enabled) AndAlso ToggleSwitch.GrayWhenDisabled Then
                    buttonForeColor = buttonForeColor.ToGrayscale()
                End If

                Dim textSize As SizeF = g.MeasureString(buttonText, buttonFont)

                Dim textXPos As Single = adjustedButtonRect.X

                If alignment = ToggleSwitchButtonAlignment.Center Then
                    textXPos = CSng(adjustedButtonRect.X) + ((CSng(adjustedButtonRect.Width) - CSng(textSize.Width)) / 2)
                ElseIf alignment = ToggleSwitchButtonAlignment.Right Then
                    textXPos = CSng(adjustedButtonRect.X) + CSng(adjustedButtonRect.Width) - CSng(textSize.Width)
                End If

                Dim textRectangle As New RectangleF(textXPos, CSng(adjustedButtonRect.Y) + ((CSng(adjustedButtonRect.Height) - CSng(textSize.Height)) / 2), textSize.Width, textSize.Height)

                Using textBrush As Brush = New SolidBrush(buttonForeColor)
                    g.DrawString(buttonText, buttonFont, textBrush, textRectangle)
                End Using
            End If
        End If

        g.ResetClip()
    End Sub

#End Region ' Render Method Implementations

#Region "Helper Method Implementations"

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")>
    Public Function GetInnerControlRectangle() As Rectangle
        Return New Rectangle(1, 1, ToggleSwitch.Width - 2, ToggleSwitch.Height - 2)
    End Function

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")>
    Public Function GetCathetusLengthBasedOnAngle() As Integer
        If SlantAngle = 0 Then
            Return 0
        End If

        Dim degrees As Double = Math.Abs(SlantAngle)
        Dim radians As Double = degrees * (Math.PI / 180)
        Dim length As Double = Math.Tan(radians) * ToggleSwitch.Height

        Return CInt(Fix(length))
    End Function

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")>
    Public Function GetHalfCathetusLengthBasedOnAngle() As Integer
        If SlantAngle = 0 Then
            Return 0
        End If

        Dim degrees As Double = Math.Abs(SlantAngle)
        Dim radians As Double = degrees * (Math.PI / 180)
        Dim length As Double = (Math.Tan(radians) * ToggleSwitch.Height) / 2

        Return CInt(Fix(length))
    End Function

    Public Overrides Function GetButtonWidth() As Integer
        Dim buttonWidth As Double = CDbl(ToggleSwitch.Width) / 2
        Return CInt(Fix(buttonWidth))
    End Function

    Public Overrides Function GetButtonRectangle() As Rectangle
        Dim buttonWidth As Integer = GetButtonWidth()
        Return GetButtonRectangle(buttonWidth)
    End Function

    Public Overrides Function GetButtonRectangle(ByVal buttonWidth As Integer) As Rectangle
        Dim buttonRect As New Rectangle(ToggleSwitch.ButtonValue, 0, buttonWidth, ToggleSwitch.Height)
        Return buttonRect
    End Function

#End Region ' Helper Method Implementations
End Class
