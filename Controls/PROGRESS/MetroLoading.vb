﻿Imports System.Windows.Forms
Partial Public Class MetroLoading
    Inherits UserControl

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 12/19/2015. </remarks>
    Public Sub New()
        InitializeComponent()
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Controls.MetroLoading and optionally
    ''' releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 12/19/2015. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

End Class
