﻿Partial Public Class MetroProgressBar
    ''' <summary> 
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

#Region "Component Designer generated code"

    ''' <summary> 
    ''' Required method for Designer support - do not modify 
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MetroProgressBar))
        Me._pictureBox = New System.Windows.Forms.PictureBox()
        CType(Me._pictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_pictureBox
        '
        Me._pictureBox.BackColor = System.Drawing.Color.Transparent
        Me._pictureBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._pictureBox.Image = CType(resources.GetObject("_pictureBox.Image"), System.Drawing.Image)
        Me._pictureBox.Location = New System.Drawing.Point(0, 0)
        Me._pictureBox.Name = "_pictureBox"
        Me._pictureBox.Size = New System.Drawing.Size(308, 14)
        Me._pictureBox.TabIndex = 0
        Me._pictureBox.TabStop = False
        '
        'MetroProgressBar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me._pictureBox)
        Me.Name = "MetroProgressBar"
        Me.Size = New System.Drawing.Size(308, 14)
        CType(Me._pictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private _pictureBox As System.Windows.Forms.PictureBox
End Class
