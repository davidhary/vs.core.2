﻿Imports System.ComponentModel
Imports System.Windows.Forms
''' <summary> User log on/off interface. </summary>
''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="10/30/08" by="David" revision="1.00.3225.x"> created. </history>
<Description("User Log On Control"), System.Drawing.ToolboxBitmap(GetType(LogOnControl))>
<System.Runtime.InteropServices.ComVisible(False)>
Public Class LogOnControl
    Inherits Pith.MyUserControlBase

#Region " CONSTRUCTORS "

    ''' <summary> Default constructor. </summary>
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._ErrorProvider.ContainerControl = Me
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Controls.LogOnControl and optionally
    ''' releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 12/19/2015. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._popup IsNot Nothing Then Me._popup.Dispose() : Me._popup = Nothing
                Me._UserLogOn = Nothing
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " USER ACTIONS "

    Private _isAuthenticated As Boolean
    ''' <summary> Gets or sets a value indicating whether this object is authenticated. </summary>
    ''' <value> <c>true</c> if this object is Authenticated; otherwise <c>false</c>. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property IsAuthenticated() As Boolean
        Get
            Return Me._isAuthenticated
        End Get
        Set(ByVal value As Boolean)
            Me._isAuthenticated = value
            Me.updateCaptions()
        End Set
    End Property

    Private _UserLogOn As LogOnBase
    ''' <summary>
    ''' Gets or sets reference to the user log on implementation object.
    ''' </summary>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property UserLogOn() As LogOnBase
        Get
            Return Me._UserLogOn
        End Get
        Set(value As LogOnBase)
            Me._UserLogOn = value
            Me.IsAuthenticated = False
        End Set
    End Property

#End Region

#Region " ALLOWED USER ROLES "

    Private _AllowedUserRoles As ArrayList

    ''' <summary> Gets the allowed user roles. </summary>
    ''' <value> The allowed user roles. </value>
    Public ReadOnly Property AllowedUserRoles As ArrayList
        Get
            Return Me._AllowedUserRoles
        End Get
    End Property

    ''' <summary>
    ''' Adds a user role to the list of allowed user roles. 
    ''' </summary>
    ''' <param name="value">Specifies a user role to add to the list of allowed user roles.</param>
    Public Sub AddAllowedUserRole(ByVal value As String)
        If Me._AllowedUserRoles Is Nothing Then
            Me._AllowedUserRoles = New ArrayList
        End If
        If Not Me._AllowedUserRoles.Contains(value) Then
            Me._AllowedUserRoles.Add(value)
        End If
    End Sub

    ''' <summary> Adds a user roles. </summary>
    ''' <param name="values"> The values. </param>
    Public Sub AddAllowedUserRoles(ByVal values() As String)
        If Me._AllowedUserRoles Is Nothing Then
            Me._AllowedUserRoles = New ArrayList
        End If
        Me._AllowedUserRoles.AddRange(values)
    End Sub

    ''' <summary>
    ''' Clears the allowed user roles.
    ''' </summary>
    Public Sub ClearAllowedUserRoles()
        Me._AllowedUserRoles = New ArrayList
    End Sub

    ''' <summary> Authenticated roles. </summary>
    ''' <returns> An ArrayList. </returns>
    Public Function AuthenticatedRoles() As ArrayList
        Return isr.Core.Controls.DomainLogOn.EnumerateUserRoles(Me.UserLogOn.UserRoles, Me.AllowedUserRoles)
    End Function

    ''' <summary> Returns true if the specified role is allowed. </summary>
    ''' <param name="value"> Specifies a user role to check against the list of allowed user roles. </param>
    ''' <returns> <c>true</c> if user role allowed; otherwise <c>false</c>. </returns>
    Public Function IsUserRoleAllowed(ByVal value As String) As Boolean
        Return Me._AllowedUserRoles IsNot Nothing AndAlso Me._AllowedUserRoles.Contains(value)
    End Function

#End Region

#Region " GUI "

    ''' <summary> Logs the user on. </summary>
    Private Sub _logOn()
        Try
            Me.Cursor = Cursors.WaitCursor
            If Me.AllowedUserRoles Is Nothing OrElse Me.AllowedUserRoles.Count = 0 Then
                Me.IsAuthenticated = Me.UserLogOn.Authenticate(Me._userNameTextBox.Text, Me._passwordTextBox.Text.Trim)
            Else
                Me.IsAuthenticated = Me.UserLogOn.Authenticate(Me._userNameTextBox.Text, Me._passwordTextBox.Text.Trim, Me._AllowedUserRoles)
            End If
            If Not Me.IsAuthenticated Then
                Me._ErrorProvider.SetError(Me._LogOnLinkLabel, Me.UserLogOn.ValidationMessage)
            End If
        Catch
            Throw
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub _LogOnLinkLabel_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles _LogOnLinkLabel.LinkClicked
        Me._ErrorProvider.SetError(Me._LogOnLinkLabel, "")
        If Me.IsAuthenticated Then
            Me.IsAuthenticated = False
            Me.UserLogOn.Invalidate()
        Else
            Me._logOn()
        End If
        If Me._popup IsNot Nothing Then
            _popup.Hide()
        End If
    End Sub

    Private _logOnCaption As String = "Log In"

    ''' <summary> Gets or sets the log on caption. </summary>
    ''' <value> The log on caption. </value>
    <Category("Appearance"), Description("Caption for log on"),
      Browsable(True),
      DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
      DefaultValue("Log In")>
    Public Property LogOnCaption() As String
        Get

            Return Me._logOnCaption
        End Get
        Set(ByVal value As String)
            Me._logOnCaption = value
            Me.updateCaptions()
        End Set
    End Property

    Private _logOffCaption As String = "Log Out"

    ''' <summary> Gets or sets the log off caption. </summary>
    ''' <value> The log off caption. </value>
    <Category("Appearance"), Description("Caption for log off"),
      Browsable(True),
      DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
      DefaultValue("Log Out")>
    Public Property LogOffCaption() As String
        Get
            Return Me._logOffCaption
        End Get
        Set(ByVal value As String)
            Me._logOffCaption = value
            Me.updateCaptions()
        End Set
    End Property

    ''' <summary>Initializes the user interface and tool tips.</summary>
    Private Sub userLogOnControl_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' disable when loaded.
        Me._userNameTextBox.Enabled = False
        Me._userNameTextBox.Text = ""
        Me._passwordTextBox.Enabled = False
        Me._userNameTextBox.Text = ""
        Me._LogOnLinkLabel.Enabled = False
        Me._LogOnLinkLabel.Text = Me.LogOnCaption

    End Sub

    ''' <summary> Logs on control visible changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    ''' <remarks> This in fact enables the control when in a pop-up container. </remarks>
    Private Sub LogOnControl_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        Me.updateCaptions()
    End Sub

    ''' <summary> Updates the captions. </summary>
    Private Sub updateCaptions()
        If Me._isAuthenticated Then
            Me._LogOnLinkLabel.Text = Me.LogOffCaption
        Else
            Me._LogOnLinkLabel.Text = Me.LogOnCaption
        End If
        If Me._UserLogOn IsNot Nothing Then
            ' enable the user name and password boxes
            Me._userNameTextBox.Enabled = True
            Me._passwordTextBox.Enabled = True
            Me._LogOnLinkLabel.Enabled = True
        End If
    End Sub

#End Region

#Region " POPUP "

    Private _popup As PopupContainer

    ''' <summary> Shows the pop-up. </summary>
    ''' <param name="control">  The control. </param>
    ''' <param name="location"> The location. </param>
    Public Sub ShowPopup(ByVal control As Control, ByVal location As Drawing.Point)
        Me._popup = New PopupContainer(Me)
        Me._popup.Show(control, location)
    End Sub

#End Region

End Class

