﻿Imports System.DirectoryServices
Imports System.DirectoryServices.AccountManagement
Imports System.DirectoryServices.ActiveDirectory
''' <summary> Domain log on. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="10/10/2014" by="David" revision="2.1.5396"> Created. </history>
Public Class DomainLogOn
    Inherits LogOnBase

#Region " SHARED "

    ''' <summary> Enumerate user roles. </summary>
    ''' <param name="user"> The user. </param>
    ''' <returns> <c>true</c> if in role; otherwise <c>false</c>. </returns>
    Public Shared Function EnumerateUserRoles(ByVal user As UserPrincipal) As ArrayList
        Dim list As New ArrayList
        If user IsNot Nothing Then
            For Each group As Principal In user.GetAuthorizationGroups
                list.Add(group.Name)
            Next
        End If
        Return list
    End Function


    ''' <summary> Enumerate user roles. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="user">             The user. </param>
    ''' <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    ''' the name of enumeration flags. </param>
    ''' <returns> <c>true</c> if in role; otherwise <c>false</c>. </returns>
    Public Shared Function EnumerateUserRoles(ByVal user As UserPrincipal, ByVal allowedUserRoles As ArrayList) As ArrayList
        If user Is Nothing Then
            Throw New ArgumentNullException("user")
        ElseIf allowedUserRoles Is Nothing Then
            Throw New ArgumentNullException("allowedUserRoles")
        End If
        Dim roles As New ArrayList
        For Each group As Principal In user.GetAuthorizationGroups
            If allowedUserRoles.Contains(group.Name) Then
                roles.Add(group.Name)
            End If
        Next
        Return roles
    End Function

    ''' <summary> Enumerate user roles. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="userRoles">        The user roles. </param>
    ''' <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    ''' the name of enumeration flags. </param>
    ''' <returns> <c>true</c> if in role; otherwise <c>false</c>. </returns>
    Public Shared Function EnumerateUserRoles(ByVal userRoles As ArrayList, allowedUserRoles As ArrayList) As ArrayList
        If userRoles Is Nothing Then
            Throw New ArgumentNullException("userRoles")
        ElseIf allowedUserRoles Is Nothing Then
            Throw New ArgumentNullException("allowedUserRoles")
        End If
        Dim roles As New ArrayList
        For Each role As String In userRoles
            If allowedUserRoles.Contains(role) Then
                roles.Add(role)
            End If
        Next
        Return roles
    End Function

    ''' <summary> Enumerate domains. </summary>
    ''' <returns> An ArrayList. </returns>
    Public Shared Function EnumerateDomains() As ArrayList
        Dim domainNames As New ArrayList()
        Try
            Dim currentForest As Forest = Forest.GetCurrentForest()
            Dim myDomains As DomainCollection = currentForest.Domains
            For Each domain As Domain In myDomains
                domainNames.Add(domain.Name)
            Next domain
        Catch ex As System.DirectoryServices.ActiveDirectory.ActiveDirectoryOperationException
            ' this exception will occur if the Current security context is not associated with an Active Directory domain or forest.
            ' so an empty list is returned.
        End Try
        Return domainNames
    End Function

    ''' <summary> Attempts to find user on the machine. </summary>
    ''' <param name="userName"> Specifies a user name. </param>
    ''' <param name="details">  [in,out] The details. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overloads Shared Function FindUser(ByVal userName As String, ByRef details As String) As UserPrincipal
        Dim user As UserPrincipal = Nothing
        If String.IsNullOrWhiteSpace(userName) Then
            details = String.Format("User name is empty")
        Else
            Dim ctx As PrincipalContext = Nothing
            Try
                ctx = New PrincipalContext(ContextType.Machine)
                user = UserPrincipal.FindByIdentity(ctx, userName)
                If user Is Nothing Then
                    details = String.Format("User '{0}' not found @'{1}'", userName, My.Computer.Name)
                End If
            Catch ex As Exception
                details = String.Format("Exception occurred.; details: {0}", ex)
            Finally
                If user Is Nothing Then ctx?.Dispose() : ctx = Nothing
            End Try
        End If
        Return user
    End Function

    ''' <summary> Attempts to find user with the allowed roles on the machine. </summary>
    ''' <param name="userName">     Specifies a user name. </param>
    ''' <param name="allowedUserRoles"> The allowed roles. </param>
    ''' <param name="details">      [in,out] The details. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overloads Shared Function FindUser(ByVal userName As String, ByVal allowedUserRoles As ArrayList, ByRef details As String) As UserPrincipal
        Dim user As UserPrincipal = Nothing
        If String.IsNullOrWhiteSpace(userName) Then
            details = String.Format("User name is empty")
        Else
            Try
                user = DomainLogOn.FindUser(userName, details)
                If user Is Nothing Then
                ElseIf DomainLogOn.EnumerateUserRoles(user, allowedUserRoles).Count = 0 Then
                    details = String.Format("User '{0}' found @'{1}' is not a member in any of the allowed groups. ", userName, My.Computer.Name)
                    user.Dispose()
                    user = Nothing
                End If
            Catch ex As Exception
                details = String.Format("Exception occurred.; details: {0}", ex)
            End Try
        End If
        Return user
    End Function

    ''' <summary> Attempts to find user on the machine. </summary>
    ''' <param name="userName"> Specifies a user name. </param>
    ''' <param name="details">  [in,out] The details. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overloads Shared Function TryFindUser(ByVal userName As String, ByRef details As String) As Boolean
        Dim result As Boolean = False
        If String.IsNullOrWhiteSpace(userName) Then
            details = String.Format("User name is empty")
        Else
            Try
                Using user As UserPrincipal = DomainLogOn.FindUser(userName, details)
                    If user Is Nothing Then
                        details = String.Format("User '{0}' not found @'{1}'", userName, My.Computer.Name)
                    Else
                        result = True
                    End If
                End Using
            Catch ex As Exception
                details = String.Format("Exception occurred.; details: {0}", ex)
            End Try
        End If
        Return result
    End Function

    ''' <summary> Attempts to find user on the domain. </summary>
    ''' <param name="userName">   Specifies a user name. </param>
    ''' <param name="domainName"> Name of the domain. </param>
    ''' <param name="details">    [in,out] The details. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overloads Shared Function FindUser(ByVal userName As String, ByVal domainName As String, ByRef details As String) As UserPrincipal
        Dim user As UserPrincipal = Nothing
        If String.IsNullOrWhiteSpace(userName) Then
            details = String.Format("User name is empty")
        ElseIf String.IsNullOrWhiteSpace(domainName) Then
            details = String.Format("Domain name is empty")
        Else
            Dim ctx As PrincipalContext = Nothing
            Try
                ctx = New PrincipalContext(ContextType.Domain, domainName)
                user = UserPrincipal.FindByIdentity(ctx, userName)
                If user Is Nothing Then
                    details = String.Format("User '{0}' not identified @'{1}'", userName, domainName)
                End If
            Catch ex As Exception
                details = String.Format("Exception occurred.; details: {0}", ex)
            Finally
                If user Is Nothing Then ctx?.Dispose() : ctx = Nothing
            End Try
        End If
        Return user
    End Function

    ''' <summary> Attempts to find user on the domain with an allowed role. </summary>
    ''' <param name="userName">     Specifies a user name. </param>
    ''' <param name="domainName">   Name of the domain. </param>
    ''' <param name="allowedUserRoles"> The allowed roles. </param>
    ''' <param name="details">      [in,out] The details. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="3#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overloads Shared Function FindUser(ByVal userName As String, ByVal domainName As String,
                                              ByVal allowedUserRoles As ArrayList, ByRef details As String) As UserPrincipal
        Dim user As UserPrincipal = Nothing
        If String.IsNullOrWhiteSpace(userName) Then
            details = String.Format("User name is empty")
        ElseIf String.IsNullOrWhiteSpace(domainName) Then
            details = String.Format("Domain name is empty")
        Else
            Dim ctx As PrincipalContext = Nothing
            Try
                user = DomainLogOn.FindUser(userName, domainName, details)
                If user Is Nothing Then
                ElseIf DomainLogOn.EnumerateUserRoles(user, allowedUserRoles).Count = 0 Then
                    details = String.Format("User '{0}' found @'{1}' is not a member in any of the allowed groups.", userName, domainName)
                    user.Dispose()
                    user = Nothing
                End If

                ctx = New PrincipalContext(ContextType.Domain, domainName)
                user = UserPrincipal.FindByIdentity(ctx, userName)
                If user Is Nothing Then
                    details = String.Format("User '{0}' not identified @'{1}'", userName, domainName)
                End If
            Catch ex As Exception
                details = String.Format("Exception occurred.; details: {0}", ex)
            Finally
                If user Is Nothing Then ctx?.Dispose() : ctx = Nothing
            End Try
        End If
        Return user
    End Function


    ''' <summary> Attempts to find user on the machine. </summary>
    ''' <param name="userName">   Specifies a user name. </param>
    ''' <param name="domainName"> Name of the domain. </param>
    ''' <param name="details">    [in,out] The details. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overloads Shared Function FindNextUser(ByVal userName As String, ByRef domainName As String, ByRef details As String) As UserPrincipal
        Dim user As UserPrincipal = Nothing
        If String.IsNullOrWhiteSpace(userName) Then
            details = String.Format("User name is empty")
        Else
            Try
                Dim domainNames As ArrayList = DomainLogOn.EnumerateDomains
                If domainNames Is Nothing OrElse domainNames.Count = 0 Then
                    details = "No domains found; Most likely, the current security context is not associated with an Active Directory domain or forest."
                Else
                    Dim foundDomain As Boolean = String.IsNullOrEmpty(domainName)
                    For Each name As String In domainNames
                        If foundDomain Then
                            ' skip until the domain is found or empty.
                            user = FindUser(userName, name, details)
                            If user IsNot Nothing Then
                                domainName = name
                                Exit For
                            End If
                        Else
                            foundDomain = String.Equals(name, domainName, StringComparison.OrdinalIgnoreCase)
                        End If
                    Next
                End If
            Catch ex As Exception
                details = String.Format("Exception occurred.; details: {0}", ex)
            End Try
        End If
        Return user
    End Function

    ''' <summary> Attempts to find user on the machine. </summary>
    ''' <param name="userName">         Specifies a user name. </param>
    ''' <param name="allowedUserRoles"> The allowed roles. </param>
    ''' <param name="domainName">       [in,out] Name of the domain. </param>
    ''' <param name="details">          [in,out] The details. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="3#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overloads Shared Function FindNextUser(ByVal userName As String, ByVal allowedUserRoles As ArrayList,
                                                  ByRef domainName As String, ByRef details As String) As UserPrincipal
        Dim user As UserPrincipal = Nothing
        If String.IsNullOrWhiteSpace(userName) Then
            details = String.Format("User name is empty")
        Else
            Try
                Dim domainNames As ArrayList = DomainLogOn.EnumerateDomains
                If domainNames Is Nothing OrElse domainNames.Count = 0 Then
                    details = "No domains found; Most likely, the current security context is not associated with an Active Directory domain or forest."
                Else
                    Dim foundDomain As Boolean = String.IsNullOrEmpty(domainName)
                    For Each name As String In domainNames
                        If foundDomain Then
                            ' skip until the domain is found or empty.
                            user = FindUser(userName, name, allowedUserRoles, details)
                            If user IsNot Nothing Then
                                domainName = name
                                Exit For
                            End If
                        Else
                            foundDomain = String.Equals(name, domainName, StringComparison.OrdinalIgnoreCase)
                        End If
                    Next
                End If
            Catch ex As Exception
                details = String.Format("Exception occurred.; details: {0}", ex)
            End Try
        End If
        Return user
    End Function


    ''' <summary> Attempts to find user on the machine. </summary>
    ''' <param name="userName">   Specifies a user name. </param>
    ''' <param name="domainName"> Name of the domain. </param>
    ''' <param name="details">    [in,out] The details. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overloads Shared Function TryFindUser(ByVal userName As String, ByVal domainName As String, ByRef details As String) As Boolean
        Dim result As Boolean = False
        If String.IsNullOrWhiteSpace(userName) Then
            details = String.Format("User name is empty")
        Else
            Try
                Using ctx As PrincipalContext = New PrincipalContext(ContextType.Domain, domainName)
                    Using user As UserPrincipal = UserPrincipal.FindByIdentity(ctx, userName)
                        If user Is Nothing Then
                            details = String.Format("User '{0}' not found @{1}", userName, domainName)
                        Else
                            result = True
                        End If
                    End Using
                End Using
            Catch ex As Exception
                details = String.Format("Exception occurred.; details: {0}", ex)
            End Try
        End If
        Return result
    End Function

    ''' <summary> Attempts to find user on the machine. </summary>
    ''' <param name="userName">    Specifies a user name. </param>
    ''' <param name="contextType"> [in,out] Type of the context. </param>
    ''' <param name="details">     [in,out] The details. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#")>
    Public Overloads Shared Function TryFindUser(ByVal userName As String, ByRef contextType As ContextType, ByRef details As String) As Boolean

        ' first try the machine.
        Dim result As Boolean = DomainLogOn.TryFindUser(userName, details)
        If result Then
            contextType = AccountManagement.ContextType.Machine
        Else
            Dim domainNames As ArrayList = DomainLogOn.EnumerateDomains
            If domainNames Is Nothing OrElse domainNames.Count = 0 Then
                details = "No domains found; Most likely, the current security context is not associated with an Active Directory domain or forest."
            Else
                contextType = AccountManagement.ContextType.Domain
                For Each name As String In domainNames
                    result = TryFindUser(userName, name, details)
                    If result Then Exit For
                Next
            End If
        End If
        Return result
    End Function

#End Region

#Region " AUTHENTICATE "

    ''' <summary> Authenticates a user name and password on the machine and then on the domain. </summary>
    ''' <param name="userName"> Specifies a user name. </param>
    ''' <param name="password"> Specifies a password. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overloads Overrides Function Authenticate(ByVal userName As String, ByVal password As String) As Boolean
        Dim result As Boolean = False
        If String.IsNullOrWhiteSpace(userName) Then
            Me.ValidationMessage = String.Format("User name is empty")
        ElseIf String.IsNullOrWhiteSpace(password) Then
            Me.ValidationMessage = String.Format("User password is empty")
        Else
            Try
                Using user As UserPrincipal = DomainLogOn.FindUser(userName, Me.ValidationMessage)
                    result = user IsNot Nothing AndAlso user.Context.ValidateCredentials(userName, password)
                    If result Then

                    End If
                    If Not result AndAlso user IsNot Nothing Then
                        Me.ValidationMessage = String.Format("User '{0}' not authenticated @'{1}", userName, My.Computer.Name)
                    End If
                End Using
                If Not result Then
                    Dim user As UserPrincipal = Nothing
                    Try
                        Dim domainName As String = ""
                        Do
                            user = DomainLogOn.FindNextUser(userName, domainName, Me.ValidationMessage)
                            result = user IsNot Nothing AndAlso user.Context.ValidateCredentials(userName, password)
                            If Not result AndAlso user IsNot Nothing Then
                                Me.ValidationMessage = String.Format("User '{0}' not authenticated @'{1}", userName, domainName)
                            End If
                        Loop Until result OrElse user Is Nothing
                        If result Then
                            Me.UserRoles = DomainLogOn.EnumerateUserRoles(user)
                            MyBase.UserName = userName
                        End If
                    Catch
                        Throw
                    Finally
                        user?.Dispose() : user = Nothing
                    End Try
                End If
            Catch ex As Exception
                Me.ValidationMessage = ex.ToString
            End Try
        End If
        Me.IsAuthenticated = result
        Me.Failed = Not result
        Return result
    End Function

    ''' <summary> Authenticates a user name and password. </summary>
    ''' <param name="userName">         Specifies a user name. </param>
    ''' <param name="password">         Specifies a password. </param>
    ''' <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    ''' the name of enumeration flags. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overloads Overrides Function Authenticate(ByVal userName As String, ByVal password As String, ByVal allowedUserRoles As ArrayList) As Boolean
        Dim result As Boolean = False
        If String.IsNullOrWhiteSpace(userName) Then
            Me.ValidationMessage = String.Format("User name is empty")
        ElseIf String.IsNullOrWhiteSpace(password) Then
            Me.ValidationMessage = String.Format("User password is empty")
        Else
            Try
                Using user As UserPrincipal = DomainLogOn.FindUser(userName, allowedUserRoles, Me.ValidationMessage)
                    result = user IsNot Nothing AndAlso user.Context.ValidateCredentials(userName, password)
                    If Not result AndAlso user IsNot Nothing Then
                        Me.ValidationMessage = String.Format("User '{0}' not authenticated @'{1}", userName, My.Computer.Name)
                    End If
                End Using
                If Not result Then
                    Dim user As UserPrincipal = Nothing
                    Try
                        Dim domainName As String = ""
                        Do
                            user = DomainLogOn.FindNextUser(userName, allowedUserRoles, domainName, Me.ValidationMessage)
                            result = user IsNot Nothing AndAlso user.Context.ValidateCredentials(userName, password)
                            If Not result AndAlso user IsNot Nothing Then
                                Me.ValidationMessage = String.Format("User '{0}' not authenticated @'{1}", userName, domainName)
                            End If
                        Loop Until result OrElse user Is Nothing
                        If result Then
                            Me.UserRoles = DomainLogOn.EnumerateUserRoles(user)
                            MyBase.UserName = userName
                        End If
                    Catch
                        Throw
                    Finally
                        user?.Dispose() : user = Nothing
                    End Try
                End If
            Catch ex As Exception
                Me.ValidationMessage = ex.ToString
            End Try
        End If
        Me.IsAuthenticated = result
        Me.Failed = Not result
        Return result
    End Function

    ''' <summary> Tries to find a user in a group role. </summary>
    ''' <param name="userName">         Specifies a user name. </param>
    ''' <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    ''' the name of enumeration flags. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overloads Overrides Function TryFindUser(userName As String, ByVal allowedUserRoles As ArrayList) As Boolean
        Dim result As Boolean = False
        Me.Failed = False
        If String.IsNullOrWhiteSpace(userName) Then
            Me.ValidationMessage = String.Format("User name is empty")
        ElseIf allowedUserRoles Is Nothing Then
            Me.ValidationMessage = String.Format("User roles not specified")
        ElseIf allowedUserRoles.Count = 0 Then
            Me.ValidationMessage = String.Format("Expected user roles not set")
        Else
            Try
                Using user As UserPrincipal = DomainLogOn.FindUser(userName, Me.ValidationMessage)
                    result = user IsNot Nothing AndAlso EnumerateUserRoles(user, allowedUserRoles).Count > 0
                    If Not result AndAlso user IsNot Nothing Then
                        Me.ValidationMessage = String.Format("User '{0}' is not a member in any of the allowed groups @'{1}",
                                                                 userName, My.Computer.Name)
                    End If
                End Using
                If Not result Then
                    Dim user As UserPrincipal = Nothing
                    Try
                        Dim domainName As String = ""
                        Do
                            user = DomainLogOn.FindNextUser(userName, domainName, Me.ValidationMessage)
                            result = user IsNot Nothing AndAlso DomainLogOn.EnumerateUserRoles(user, allowedUserRoles).Count > 0
                            If Not result AndAlso user IsNot Nothing Then
                                Me.ValidationMessage = String.Format("User '{0}' is not a member in any of the allowed groups @'{1}",
                                                                         userName, domainName)
                            End If
                        Loop Until result OrElse user Is Nothing
                        If result Then
                            Me.UserRoles = DomainLogOn.EnumerateUserRoles(user)
                            If DomainLogOn.EnumerateUserRoles(user, allowedUserRoles).Count > 0 Then
                                MyBase.UserName = userName
                            End If
                        End If
                    Catch
                        Throw
                    Finally
                        user?.Dispose() : user = Nothing
                    End Try
                End If
            Catch ex As Exception
                Me.ValidationMessage = ex.ToString
            End Try
        End If
        Me.Failed = Not result
        Return result
    End Function
#End Region

End Class
