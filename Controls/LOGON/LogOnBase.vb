﻿Imports System.ComponentModel
Imports isr.Core.Pith.EventHandlerExtensions
''' <summary> Interfaces user log on/off functionality.</summary>
''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
'''           Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="10/30/08" by="David" revision="1.00.3225.x"> Created </history>
<System.Runtime.InteropServices.ComVisible(False)>
Public MustInherit Class LogOnBase
    Implements IDisposable, INotifyPropertyChanged

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of the <see cref="LogOnBase" /> class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

#Region " Disposable Support "

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks> Do not make this method Overridable (virtual) because a derived class should not be
    ''' able to override this method. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> Gets or sets the dispose status sentinel of the base class.  This applies to the
    ''' derived class provided proper implementation. </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter:<para>
    ''' If True, the method has been called directly or indirectly by a user's code--managed and
    ''' unmanaged resources can be disposed.</para><para>
    ''' If False, the method has been called by the runtime from inside the finalizer and you should
    ''' not reference other objects--only unmanaged resources can be disposed.</para>
    ''' </remarks>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged
    '''                          resources;
    '''                          False if this method releases only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveEventHandler(Me.PropertyChangedEvent)
            End If
        Finally
            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

#End Region

#Region " EVENTS "

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Removes the event handler. </summary>
    ''' <remarks> David, 12/21/2015. </remarks>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As PropertyChangedEventHandler)
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanged, CType(d, PropertyChangedEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

#End Region

#Region " VALIDATION METHODS "

    ''' <summary> Authenticates a user name and password. </summary>
    ''' <param name="userName"> Specifies a user name. </param>
    ''' <param name="password"> Specifies a password. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    Public MustOverride Function Authenticate(ByVal userName As String, ByVal password As String) As Boolean

    ''' <summary> Authenticates a user name and password. </summary>
    ''' <param name="userName">         Specifies a user name. </param>
    ''' <param name="password">         Specifies a password. </param>
    ''' <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    ''' the name of enumeration flags. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    Public Overridable Function Authenticate(userName As String, password As String, ByVal allowedUserRoles As ArrayList) As Boolean
        If Me.TryFindUser(userName, allowedUserRoles) Then
            Me.IsAuthenticated = Authenticate(userName, password)
        Else
            Me.IsAuthenticated = False
        End If
        Return Me.IsAuthenticated
    End Function

    ''' <summary> Tries to find a user in a group role. </summary>
    ''' <param name="userName">         Specifies a user name. </param>
    ''' <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    ''' the name of enumeration flags. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    Public MustOverride Function TryFindUser(ByVal userName As String, ByVal allowedUserRoles As ArrayList) As Boolean

    ''' <summary> Invalidates the last login user. </summary>
    Public Sub Invalidate()
        Me.IsAuthenticated = False
        Me.Failed = False
    End Sub

#End Region

#Region " VALIDATION OUTCOMES "

    Private _userName As String

    ''' <summary> Gets or sets (protected) the name of the user. </summary>
    ''' <value> The name of the user. </value>
    Public Property UserName As String
        Get
            Return Me._userName
        End Get
        Protected Set(ByVal value As String)
            Me._userName = value
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            evt?.Invoke(Me, New PropertyChangedEventArgs("UserName"))
        End Set
    End Property

    Private _userRoles As ArrayList

    ''' <summary> Gets or sets (protected) the roles of the user. </summary>
    ''' <value> The Role of the user. </value>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")>
    Public Property UserRoles As ArrayList
        Get
            Return Me._userRoles
        End Get
        Protected Set(ByVal value As ArrayList)
            Me._userRoles = value
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            evt?.Invoke(Me, New PropertyChangedEventArgs("UserRoles"))
        End Set
    End Property

    Private _isAuthenticated As Boolean

    ''' <summary> Gets or sets (protected) a value indicating whether the user is authenticated. </summary>
    ''' <value> <c>true</c> if the user authenticated is valid; otherwise <c>false</c>. </value>
    Public Property IsAuthenticated As Boolean
        Get
            Return Me._isAuthenticated
        End Get
        Protected Set(ByVal value As Boolean)
            Me._isAuthenticated = value
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            evt?.Invoke(Me, New PropertyChangedEventArgs("IsAuthenticated"))
        End Set
    End Property

    Private _validationDetails As String
    ''' <summary> Gets or sets (protected) the validation message. </summary>
    Public Property ValidationMessage() As String
        Get
            Return Me._validationDetails
        End Get
        Protected Set(ByVal value As String)
            Me._validationDetails = value
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            evt?.Invoke(Me, New PropertyChangedEventArgs("ValidationMessage"))
        End Set
    End Property

    Private _Failed As Boolean

    ''' <summary> Gets or sets a value indicating whether the login failed. </summary>
    ''' <value> <c>true</c> if failed; otherwise <c>false</c>. </value>
    Public Property Failed As Boolean
        Get
            Return Me._Failed
        End Get
        Protected Set(value As Boolean)
            Me._Failed = value
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            evt?.Invoke(Me, New PropertyChangedEventArgs("Failed"))
        End Set
    End Property

#End Region

End Class
