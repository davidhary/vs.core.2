﻿Imports System.Windows.Forms
Imports System.Windows.Forms.Design
Imports System.ComponentModel
''' <summary> Tool strip selector combo box. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="3/2/2015" by="David" revision=""> Created. </history>
<ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)>
Public Class ToolStripSelectorComboBox
    Inherits ToolStripSelectorBase

    ''' <summary> Default constructor. </summary>
    ''' <remarks> Call the base constructor passing in a SelectorComboBox instance. </remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub New()
        MyBase.New(New SelectorComboBox())
    End Sub

#Region " UNDERLYING CONTROL PROPERTIES "

    ''' <summary> Gets the numeric up down control. </summary>
    ''' <value> The numeric up down control. </value>
    Public ReadOnly Property SelectorComboBoxControl() As SelectorComboBox
        Get
            Return TryCast(Control, SelectorComboBox)
        End Get
    End Property

    ''' <summary> Gets the combo box. </summary>
    ''' <value> The combo box. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ComboBox As isr.Core.Controls.ComboBox
        Get
            Return Me.SelectorComboBoxControl.ComboBox
        End Get
    End Property

    ''' <summary> Gets the button. </summary>
    ''' <value> The button. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Button As Button
        Get
            Return Me.SelectorComboBoxControl.Button
        End Get
    End Property

#End Region

#Region " UNDERLYING CONTROL METHODS "

    ''' <summary> Select value. </summary>
    Public Overloads Sub SelectValue(ByVal value As String)
        Me.SelectorComboBoxControl.SelectValue(value)
    End Sub

#End Region

End Class

