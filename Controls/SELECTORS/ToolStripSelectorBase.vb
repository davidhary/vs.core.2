﻿Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Windows.Forms.Design
Imports isr.Core.Pith.EventHandlerExtensions
''' <summary> Tool strip selector base. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="3/2/2015" by="David" revision=""> Created. </history>
<ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)>
Public Class ToolStripSelectorBase
    Inherits ToolStripControlHost

    ''' <summary> Default constructor. </summary>
    ''' <remarks> Call the base constructor passing in a base selector instance. </remarks>
    ''' <param name="selectorControl"> The selector control. </param>
    Public Sub New(ByVal selectorControl As SelectorControlBase)
        MyBase.New(selectorControl)
        Me.AutoSize = False
    End Sub

    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveDirtyChangedEventHandler(Me.DirtyChangedEvent)
                Me.RemoveValueSelectedEventHandler(Me.ValueSelectedEvent)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try

    End Sub

#Region " COLORS "

    ''' <summary> Gets or sets the color of the read only back. </summary>
    ''' <value> The color of the read only back. </value>
    <DefaultValue(GetType(Drawing.Color), "SystemColors.orange")>
    <Description("Back color when dirty"), Category("Appearance")>
    Public Property DirtyBackColor() As Color
        Get
            Return Me.SelectorControlBase.DirtyBackColor
        End Get
        Set(value As Color)
            Me.SelectorControlBase.DirtyBackColor = value
        End Set
    End Property

    ''' <summary> Gets or sets the color of the read only foreground. </summary>
    ''' <value> The color of the read only foreground. </value>
    <DefaultValue(GetType(Drawing.Color), "SystemColors.InactiveCaption")>
    <Description("Fore color when dirty"), Category("Appearance")>
    Public Property DirtyForeColor() As Color
        Get
            Return Me.SelectorControlBase.DirtyForeColor
        End Get
        Set(value As Color)
            Me.SelectorControlBase.DirtyForeColor = value
        End Set
    End Property

#End Region

#Region " UNDERLYING CONTROL PROPERTIES "

    ''' <summary> Gets the numeric up down control. </summary>
    ''' <value> The numeric up down control. </value>
    Public ReadOnly Property SelectorControlBase() As SelectorControlBase
        Get
            Return TryCast(Control, SelectorControlBase)
        End Get
    End Property

    ''' <summary> Gets a value indicating whether this object has value. </summary>
    ''' <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property HasValue As Boolean
        Get
            Return Me.SelectorControlBase.HasValue
        End Get
    End Property

    ''' <summary> The watermark text. </summary>
    ''' <value> The water mark text with this control. </value>
    <DefaultValue(GetType(System.String), "Watermark")>
    <Description("Watermark Text"), Category("Appearance")>
    Public Property Watermark As String
        Get
            Return Me.SelectorControlBase.Watermark
        End Get
        Set(value As String)
            Me.SelectorControlBase.Watermark = value
        End Set
    End Property

    ''' <summary> Gets or sets the selected text. </summary>
    ''' <value> The selected text. </value>
    <DefaultValue("")>
    <Description("Selected text"), Category("Appearance")>
    Public Property SelectedText() As String
        Get
            Return Me.SelectorControlBase.SelectedText
        End Get
        Set(ByVal value As String)
            Me.SelectorControlBase.SelectedText = value
        End Set
    End Property

    ''' <summary> Gets or sets the selected text. </summary>
    ''' <value> The selected text. </value>
    <DefaultValue("")>
    <Description("text"), Category("Appearance")>
    Public Overrides Property Text() As String
        Get
            Return Me.SelectorControlBase.Text
        End Get
        Set(ByVal value As String)
            Me.SelectorControlBase.Text = value
        End Set
    End Property

    ''' <summary> Selector icon image. </summary>
    ''' <value> The selector icon. </value>
    <Description("Selector icon image"), Category("Appearance")>
    Public Property SelectorIcon As Drawing.Image
        Get
            Return Me.SelectorControlBase.SelectorIcon
        End Get
        Set(value As Drawing.Image)
            Me.SelectorControlBase.SelectorIcon = value
        End Set
    End Property

    ''' <summary> Gets or sets the read only property. </summary>
    ''' <value> The read only. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether the check box is read only.")>
    Public Property [ReadOnly]() As Boolean
        Get
            Return Me.SelectorControlBase.ReadOnly
        End Get
        Set(value As Boolean)
            Me.SelectorControlBase.ReadOnly = value
        End Set
    End Property

#End Region

#Region " UNDERLYING CONTROL METHODS "

    ''' <summary> Select value. </summary>
    Public Sub SelectValue()
        Me.SelectorControlBase.SelectValue()
    End Sub

#End Region

#Region " EVENT HANDLERS "

    ''' <summary> Subscribes events from the hosted control. </summary>
    ''' <remarks> Subscribe the control events to expose. </remarks>
    ''' <param name="control"> The control from which to subscribe events. </param>
    Protected Overrides Sub OnSubscribeControlEvents(ByVal control As Control)

        If control IsNot Nothing Then

            ' Call the base so the base events are connected.
            MyBase.OnSubscribeControlEvents(control)

            ' Cast the control to a Selector control base.
            Dim ctrl As SelectorControlBase = TryCast(control, SelectorControlBase)

            If ctrl IsNot Nothing Then
                ' Add the event.
                AddHandler ctrl.DirtyChanged, AddressOf Me.OnDirtyChanged
                AddHandler ctrl.ValueSelected, AddressOf Me.OnValueSelected
            End If

        End If
    End Sub

    ''' <summary> Unsubscribes events from the hosted control. </summary>
    ''' <param name="control"> The control from which to unsubscribe events. </param>
    Protected Overrides Sub OnUnsubscribeControlEvents(ByVal control As Control)

        ' Call the base method so the basic events are unsubscribed.
        MyBase.OnUnsubscribeControlEvents(control)

        ' Cast the control to a Selector control base.
        Dim ctrl As SelectorControlBase = TryCast(control, SelectorControlBase)

        If ctrl IsNot Nothing Then
            ' Remove the events.
            RemoveHandler ctrl.DirtyChanged, AddressOf OnDirtyChanged
            RemoveHandler ctrl.ValueSelected, AddressOf OnValueSelected
        End If


    End Sub

    ''' <summary> Event queue for all listeners interested in DirtyChanged events. </summary>
    Public Event DirtyChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveDirtyChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.DirtyChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the value selected event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Private Sub OnDirtyChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.DirtyChangedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Event queue for all listeners interested in ValueSelected events. </summary>
    Public Event ValueSelected As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveValueSelectedEventHandler(ByVal value As EventHandler(Of EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.ValueSelected, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the value selected event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Private Sub OnValueSelected(ByVal sender As Object, ByVal e As EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.ValueSelectedEvent
        evt?.Invoke(Me, e)
    End Sub

#End Region

End Class

