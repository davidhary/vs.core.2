﻿
''' <summary> Form for viewing and printing a <see cref="RichTextBox">rich text box</see>. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="1/8/2015" by="David" revision=""> Created. </history>
Public Class RichTextBoxForm
    Inherits Pith.FormBase

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 12/21/2015. </remarks>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me._FormatFontMessageBoxMenuItem.Text = Drawing.SystemFonts.MessageBoxFont.Name

    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 12/19/2015. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " SHOW DIALOGS "

    ''' <summary> Shows the <see cref="RichTextBox">rich text box</see> form with these messages. </summary>
    ''' <param name="owner">   The owner. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="details"> The details. </param>
    Public Shadows Sub Show(ByVal mdiForm As System.Windows.Forms.Form, ByVal owner As System.Windows.Forms.IWin32Window,
                            ByVal caption As String, ByVal details As String)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        Me.Text = caption
        Me._RichTextBox.Text = details
        MyBase.Show(owner)
    End Sub

    ''' <summary> Shows the <see cref="RichTextBox">rich text box</see> form with these messages. </summary>
    ''' <param name="owner">   The owner. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="details"> The details. </param>
    Public Shadows Sub Show(ByVal mdiForm As System.Windows.Forms.Form, ByVal owner As System.Windows.Forms.IWin32Window,
                            ByVal font As Drawing.Font, ByVal caption As String, ByVal details As String)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        Me.Text = caption
        Me._RichTextBox.Font = font
        Me._RichTextBox.Text = details
        MyBase.Show(owner)
    End Sub

    ''' <summary> Shows the <see cref="RichTextBox">rich text box</see> form with these messages. </summary>
    Public Shadows Sub ShowDialog(ByVal mdiForm As System.Windows.Forms.Form)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        MyBase.ShowDialog()
    End Sub

    ''' <summary> Shows the <see cref="RichTextBox">rich text box</see> form with these messages. </summary>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="details"> The details. </param>
    Public Shadows Sub ShowDialog(ByVal mdiForm As System.Windows.Forms.Form, ByVal font As Drawing.Font, ByVal caption As String, ByVal details As String)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        Me.Text = caption
        Me._RichTextBox.Font = font
        Me._RichTextBox.Text = details
        MyBase.ShowDialog()
    End Sub

    ''' <summary> Shows the <see cref="RichTextBox">rich text box</see> form with these messages. </summary>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="details"> The details. </param>
    Public Shadows Sub Show(ByVal mdiForm As System.Windows.Forms.Form, ByVal font As Drawing.Font, ByVal caption As String, ByVal details As String)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        Me.Text = caption
        Me._RichTextBox.Font = font
        Me._RichTextBox.Text = details
        MyBase.Show()
    End Sub


    ''' <summary> Shows the <see cref="RichTextBox">rich text box</see> form with these messages. </summary>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="details"> The details. </param>
    Public Shadows Sub Show(ByVal mdiForm As System.Windows.Forms.Form, ByVal caption As String, ByVal details As String)
        If mdiForm IsNot Nothing AndAlso mdiForm.IsMdiContainer Then
            Me.MdiParent = mdiForm
            mdiForm.Show()
        End If
        Me.Text = caption
        Me._RichTextBox.Text = details
        MyBase.Show()
    End Sub

#End Region

#Region " PRINT SETTINGS "

    ''' <summary> Gets a reference to the print document. </summary>
    ''' <value> The print document. </value>
    Public ReadOnly Property PrintDocument As System.Drawing.Printing.PrintDocument
        Get
            Return _PrintDocument
        End Get
    End Property

    ''' <summary> Gets the reference to the rich text box. </summary>
    ''' <value> The rich text box. </value>
    Public ReadOnly Property RichTextBox As RichTextBox
        Get
            Return Me._RichTextBox
        End Get
    End Property

#End Region

#Region " PRINT DIALOG "

    Private _FirstCharOnPage As Integer

    Private Sub _PrintDocument_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles _PrintDocument.BeginPrint
        ' Start at the beginning of the text
        _FirstCharOnPage = 0
    End Sub

    Private Sub _PrintDocument_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles _PrintDocument.PrintPage
        ' To print the boundaries of the current page margins
        ' uncomment the next line:
        e.Graphics.DrawRectangle(System.Drawing.Pens.Blue, e.MarginBounds)

        ' make the RichTextBoxEx calculate and render as much text as will
        ' fit on the page and remember the last character printed for the
        ' beginning of the next page
        Me._FirstCharOnPage = _RichTextBox.FormatRange(False, e, _FirstCharOnPage, _RichTextBox.TextLength)

        ' check if there are more pages to print
        If (_FirstCharOnPage < _RichTextBox.TextLength) Then
            e.HasMorePages = True
        Else
            e.HasMorePages = False
        End If
    End Sub

    Private Sub _PrintDocument_EndPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles _PrintDocument.EndPrint
        ' Clean up cached information
        _RichTextBox.FormatRangeDone()
    End Sub

#End Region

#Region " FILE MENU "

    Private Sub _FilePageSetupMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FilePageSetupMenu.Click
        _PageSetupDialog.ShowDialog()
    End Sub

    Private Sub _FilePrintPreviewMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FilePrintPreviewMenu.Click
        If (_PrintPreviewDialog.ShowDialog = System.Windows.Forms.DialogResult.OK) Then
            _PrintDocument.Print()
        End If
    End Sub

    Private Sub _FilePrintMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FilePrintMenu.Click
        _PrintDocument.Print()
    End Sub

    Private Sub _FileExitMenu_Click(sender As System.Object, e As System.EventArgs) Handles _FileExitMenu.Click
        Me.Close()
    End Sub

#End Region

#Region " FORMAT MENU "

    Private Sub _FormatBoldMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatBoldMenu.Click
        _RichTextBox.SetSelectionBold(True)
    End Sub

    Private Sub _FormatItalicMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatItalicMenu.Click
        _RichTextBox.SetSelectionItalic(True)
    End Sub

    Private Sub _FormatUnderlinedMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatUnderlinedMenu.Click
        _RichTextBox.SetSelectionUnderlined(True)
    End Sub

    Private Sub _FormatFontSize8Menu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatFontSize8Menu.Click
        _RichTextBox.SetSelectionSize(8)
    End Sub

    Private Sub _FormatFontSize10Menu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatFontSize10Menu.Click
        _RichTextBox.SetSelectionSize(10)
    End Sub

    Private Sub _FontSize12Menu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FontSize12Menu.Click
        _RichTextBox.SetSelectionSize(12)
    End Sub

    Private Sub _FormatFontSize18Menu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatFontSize18Menu.Click
        _RichTextBox.SetSelectionSize(18)
    End Sub

    Private Sub _FormatFontSize24Menu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatFontSize24Menu.Click
        _RichTextBox.SetSelectionSize(24)
    End Sub

    Private Sub _FormatFontArialMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatFontArialMenu.Click
        _RichTextBox.SetSelectionFont("Arial")
    End Sub

    Private Sub _FormatFontCourierMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatFontCourierMenu.Click
        _RichTextBox.SetSelectionFont("Courier New")
    End Sub

    Private Sub _FormatFontLucidaMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles _FormatFontLucidaMenuItem.Click
        _RichTextBox.SetSelectionFont("Lucida Console")
    End Sub

    Private Sub _FormatFontSegoeMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles _FormatFontMessageBoxMenuItem.Click
        _RichTextBox.SetSelectionFont(Drawing.SystemFonts.MessageBoxFont.Name)
    End Sub

    Private Sub _FormatFontTimesMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FormatFontTimesMenu.Click
        _RichTextBox.SetSelectionFont("Times New Roman")
    End Sub

#End Region


End Class