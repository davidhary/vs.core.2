﻿Imports isr.Core.Pith.EventHandlerExtensions
''' <summary> A timer with state management. </summary>
''' <license> (c) 2008 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="11/08/2013" by="David" revision="">           Added to the Core Controls. </history>
''' <history date="05/07/2009" by="David" revision="1.1.3414.x"> Sync locks timer and dispose methods. </history>
''' <history date="01/30/2008" by="David" revision="1.0.2951.x"> created. </history>
Public Class StateAwareTimer
    Inherits System.Timers.Timer

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Constructor specifying a <paramref name="synchronizer">synchronizing object.</paramref> </summary>
    ''' <param name="synchronizer"> The form where this class resides. </param>
    Public Sub New(ByVal synchronizer As System.ComponentModel.ISynchronizeInvoke)
        Me.New()
        SynchronizingObject = synchronizer
        ' prefixing Synchronizing object with MyBase causes warning code AD0001
    End Sub

    ''' <summary> Constructs this class. </summary>
    Public Sub New()
        MyBase.New()
        Me._timerState = TimerStates.None
        Me.onTickLocker = New Object
    End Sub

    ''' <summary> Gets the dispose status sentinel of the base class.  This applies to the derived
    ''' class provided proper implementation. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose",
        Justification:="There seems to be a problem with the base class, which keeps calling this method causing a stack overflow.")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        SyncLock onTickLocker
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                    Me.RemoveTickEventHandler(Me.TickEvent)
                    If SynchronizingObject IsNot Nothing Then SynchronizingObject = Nothing
                End If
            Finally
                Me.IsDisposed = True
                MyBase.Dispose(disposing)
            End Try
        End SyncLock

    End Sub

#End Region

#Region " PROPERTIES "

    Private _timerState As TimerStates

    ''' <summary> Gets the timer state. </summary>
    ''' <value> The timer state. </value>
    Public ReadOnly Property TimerState() As TimerStates
        Get
            Return Me._timerState
        End Get
    End Property

    ''' <summary> Gets a True if the timer is processing an event. </summary>
    ''' <value> The is in tick. </value>
    Public ReadOnly Property IsInTick() As Boolean
        Get
            Return (Me.TimerState And TimerStates.InTick) <> 0
        End Get
    End Property

    ''' <summary> Gets a True if timer is in Running state. </summary>
    ''' <value> The is running. </value>
    Public ReadOnly Property IsRunning() As Boolean
        Get
            Return (Me.TimerState And TimerStates.Running) <> 0
        End Get
    End Property

    ''' <summary>
    ''' Gets a True if timer is in Stopping state.
    ''' </summary>
    Public ReadOnly Property IsStopping() As Boolean
        Get
            Return (Me.TimerState And TimerStates.Stopping) <> 0
        End Get
    End Property

    ''' <summary> Gets or sets a condition for invoking the call back tick event asynchronously.  Must
    ''' have a <see cref="SynchronizingObject">synchronizing object</see>
    ''' for using asynchronous invocation. </summary>
    ''' <value> The asynchronous invoke. </value>
    Public Property AsynchronousInvoke() As Boolean

    ''' <summary> Start or stops event raising. </summary>
    ''' <value> The enabled. </value>
    Public Shadows Property Enabled() As Boolean
        Get
            Return MyBase.Enabled
        End Get
        Set(ByVal value As Boolean)
            If value Then
                Me.Start()
            Else
                Me.Stop()
            End If
        End Set
    End Property

#End Region

#Region " METHODS "

    ''' <summary> Starts this instance. </summary>
    ''' <returns> <c>True</c> if started (enabled), <c>False</c> otherwise. </returns>
    Public Shadows Function [Start]() As Boolean
        SyncLock onTickLocker
            If Me.IsDisposed Then Return False
            MyBase.Start()
            Me._timerState = TimerStates.Running
            Return MyBase.Enabled
        End SyncLock
    End Function

    ''' <summary> Stops the timer. </summary>
    ''' <returns> <c>True</c> if stopped (not enabled), <c>False</c> otherwise. </returns>
    Public Shadows Function [Stop]() As Boolean
        SyncLock onTickLocker
            If Me._IsDisposed Then
                Return True
            End If
            If Me.IsRunning Then
                Me._timerState = Me._timerState Or TimerStates.Stopping
            End If
            MyBase.Stop()
            Me._timerState = TimerStates.None
            Return Not MyBase.Enabled
        End SyncLock
    End Function

#End Region

#Region " TICK EVENT "

    ''' <summary> Handles tick events </summary>
    Public Event Tick As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveTickEventHandler(ByVal value As EventHandler(Of EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.Tick, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub


    ''' <summary> Raises the <see cref="system.EventHandler">tick</see> event. </summary>
    Private Sub _onTick(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.TickEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Raises the Notify event. </summary>
    ''' <param name="e"> Specifies the event <see cref="System.EventArgs">arguments</see>. </param>
    Protected Sub OnTick(ByVal e As System.EventArgs)

        If Me._AsynchronousInvoke Then
            SynchronizingObject.BeginInvoke(New Action(Of EventArgs)(AddressOf Me._onTick), New Object() {e})
        Else
            If SynchronizingObject IsNot Nothing AndAlso SynchronizingObject.InvokeRequired Then
                SynchronizingObject.Invoke(New Action(Of EventArgs)(AddressOf Me._onTick), New Object() {e})
            Else
                Me._onTick(e)
            End If
        End If

    End Sub

    Private onTickLocker As Object

    ''' <summary> Raises the tick event. </summary>
    ''' <history date="05/07/2009" by="David" revision="1.1.3414.x"> Add a sync lock to
    ''' facilitate the orchestrating of close and dispose. Remove exception handling - this should be
    ''' done within the timer event handler in the parent instance. </history>
    Protected Sub OnElapsed() Handles MyBase.Elapsed
        MyBase.Stop()
        SyncLock onTickLocker
            If Me._IsDisposed Then
                Return
            End If
            Me._timerState = Me.TimerState Or TimerStates.InTick
            Me.OnTick(System.EventArgs.Empty)
            If Me.IsStopping Then
                Me._timerState = TimerStates.None
            Else
                Me._timerState = TimerStates.Running
            End If
        End SyncLock
        If Me.TimerState = TimerStates.Running Then
            MyBase.Start()
        End If
    End Sub

#End Region

End Class

''' <summary> Enumerates the timer states. </summary>
<Flags()> Public Enum TimerStates
  None = 0
  Running = 1
  Stopping = 2
  InTick = 4
End Enum
