﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Namespace ControlCollectionExtensions
    ''' <summary> Includes extensions for <see cref="Control.ControlCollection">Control Collection</see>. </summary>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/19/2010" by="David" revision="1.2.3975.x"> Created. </history>
    Public Module Methods

        ''' <summary> Adds a control to the control collection if not contained. </summary>
        ''' <param name="controls"> The collection of controls. </param>
        ''' <param name="control">  The control. </param>
        <Extension()>
        Public Sub AddIfNotContained(ByVal controls As Control.ControlCollection, ByVal control As Control)
            If controls IsNot Nothing AndAlso Not controls.Contains(control) Then
                controls.Add(control)
            End If
        End Sub

        ''' <summary> Removes a control if it is contained in the control collections. </summary>
        ''' <param name="controls"> The collection of controls. </param>
        ''' <param name="control">  The control to remove. </param>
        <Extension()>
        Public Sub RemoveIfContained(ByVal controls As Control.ControlCollection, ByVal control As Control)
            If controls IsNot Nothing AndAlso controls.Contains(control) Then
                controls.Remove(control)
            End If
        End Sub

        ''' <summary> Retrieve all controls and child controls. </summary>
        ''' <remarks> Make sure to send control back at lowest depth first so that most child controls are
        ''' checked for things before container controls, e.g., a TextBox is checked before a GroupBox
        ''' control. </remarks>
        ''' <param name="controls"> . </param>
        ''' <returns> Returns an array of controls. </returns>
        <Extension()>
        Public Function RetrieveControls(ByVal controls As System.Windows.Forms.Control.ControlCollection) As System.Windows.Forms.Control()

            If controls Is Nothing Then Return Nothing

            Dim controlList As New ArrayList

            ' Dim allControls As ArrayList = New ArrayList
            Dim myQueue As Queue = New Queue
            ' add controls to the queue
            myQueue.Enqueue(controls)

            Dim myControls As System.Windows.Forms.Control.ControlCollection
            'Dim current As Object
            Do While myQueue.Count > 0
                ' remove and return the object at the beginning of the queue
                ' current = myQueue.Dequeue()
                If TypeOf myQueue.Peek Is System.Windows.Forms.Control.ControlCollection Then
                    myControls = CType(myQueue.Dequeue, System.Windows.Forms.Control.ControlCollection)
                    If myControls.Count > 0 Then
                        For i As Integer = 0 To myControls.Count - 1
                            Dim myControl As System.Windows.Forms.Control = myControls.Item(i)
                            ' add this control to the array list
                            controlList.Add(myControl)
                            ' check if this control has a collection
                            If myControl.Controls IsNot Nothing Then
                                ' if so, add to the queue
                                myQueue.Enqueue(myControl.Controls)
                            End If
                        Next
                        'For Each myControl As System.Windows.Forms.Control In myControls
                        ' add this control to the array list
                        'controlList.Add(myControl)
                        ' check if this control has a collection
                        'If Not IsNothing(myControl.Controls) Then
                        ' if so, add to the queue
                        ' myQueue.Enqueue(myControl.Controls)
                        'End If
                        'Next
                    End If
                Else
                    myQueue.Dequeue()
                End If
            Loop
            If controlList.Count > 0 Then
                Dim allControls(controlList.Count - 1) As System.Windows.Forms.Control
                controlList.CopyTo(allControls)
                Return allControls
            Else
                Return New System.Windows.Forms.Control() {}
            End If

        End Function

    End Module
End Namespace
