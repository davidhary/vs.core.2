﻿Imports System.Windows.Forms
Imports System.Runtime.CompilerServices
Imports isr.Core.Controls.ControlCollectionExtensions
Namespace TableLayoutPanelExtensions

    ''' <summary> Includes extensions for <see cref="System.Windows.Forms.TableLayoutPanel">Table Layout Panel</see> control. </summary>
    ''' <license> (c) 2014 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="02/21/2014" by="David" revision="2.1.5165.x"> Created </history>
    Public Module Extensions

        ''' <summary> Adds a control to the layout if not contained. </summary>
        ''' <param name="layout">  The layout. </param>
        ''' <param name="control"> The control. </param>
        ''' <param name="column">  The column. </param>
        ''' <param name="row">     The row. </param>
        <Extension()>
        Public Sub AddIfNotContained(ByVal layout As TableLayoutPanel, ByVal control As Control, ByVal column As Integer, ByVal row As Integer)
            If layout IsNot Nothing AndAlso Not layout.Controls.Contains(control) Then
                layout.Controls.Add(control, column, row)
            End If
        End Sub

        ''' <summary> Removes the control. </summary>
        ''' <param name="layout">  The layout. </param>
        ''' <param name="control"> The control. </param>
        <Extension()>
        Public Sub RemoveIfContained(ByVal layout As TableLayoutPanel, ByVal control As Control)
            If layout IsNot Nothing AndAlso layout.Controls IsNot Nothing Then
                layout.Controls.RemoveIfContained(control)
            End If
        End Sub

    End Module

End Namespace
