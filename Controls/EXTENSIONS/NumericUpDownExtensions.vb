﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Namespace NumericUpDownExtensions
    ''' <summary> Includes extensions for <see cref="NumericUpDown">Numeric Up Down</see> control. </summary>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/19/2010" by="David" revision="1.2.3975.x"> Created. </history>
    Public Module Methods

#Region " CAUSES VALIDATION "

        ''' <summary> Sets the <see cref="Control">text box</see> read-only value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeCausesValidationSetter(ByVal control As UpDownBase, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of UpDownBase, Boolean)(AddressOf NumericUpDownExtensions.SafeCausesValidationSetter), New Object() {control, value})
                Else
                    control.CausesValidation = value
                End If
            End If
            Return value
        End Function

#End Region

#Region " DECIMAL PLACES "

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> 
        ''' <see cref="System.Windows.Forms.NumericUpDown.DecimalPlaces">DecimalPlaces</see>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate DecimalPlaces. </param>
        ''' <returns> The limited DecimalPlaces. </returns>
        <Extension()>
        Public Function SafeDecimalPlacesSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Integer) As Decimal
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of NumericUpDown, Integer)(AddressOf NumericUpDownExtensions.SafeDecimalPlacesSetter), New Object() {control, value})
                Else
                    DecimalPlacesSetter(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> DecimalPlaces to the
        ''' <see cref="System.Windows.Forms.NumericUpDown.DecimalPlaces">DecimalPlaces</see>. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate DecimalPlaces. </param>
        ''' <returns> The limited DecimalPlaces. </returns>
        <Extension()>
        Public Function DecimalPlacesSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Integer) As Decimal
            If control IsNot Nothing Then
                control.DecimalPlaces = value
            End If
            Return value
        End Function

#End Region

#Region " MAXIMUM "

        ''' <summary> Sets the <see cref="NumericUpDown">control</see>
        ''' <see cref="System.Windows.Forms.NumericUpDown.Maximum">Maximum</see>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate Maximum. </param>
        ''' <returns> The limited Maximum. </returns>
        <Extension()>
        Public Function SafeMaximumSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Decimal) As Decimal
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of NumericUpDown, Decimal)(AddressOf NumericUpDownExtensions.SafeMaximumSetter), New Object() {control, value})
                Else
                    MaximumSetter(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see>
        ''' <see cref="System.Windows.Forms.NumericUpDown.Maximum">Maximum</see>. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate Maximum. </param>
        ''' <returns> The limited Maximum. </returns>
        <Extension()>
        Public Function MaximumSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Decimal) As Decimal
            If control IsNot Nothing Then
                control.Maximum = value
            End If
            Return value
        End Function

#End Region

#Region " MINIMUM "

        ''' <summary> Sets the <see cref="NumericUpDown">control</see>
        ''' <see cref="System.Windows.Forms.NumericUpDown.Minimum">Minimum</see>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate Minimum. </param>
        ''' <returns> The limited Minimum. </returns>
        <Extension()>
        Public Function SafeMinimumSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Decimal) As Decimal
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of NumericUpDown, Decimal)(AddressOf NumericUpDownExtensions.SafeMinimumSetter), New Object() {control, value})
                Else
                    MinimumSetter(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> 
        ''' <see cref="System.Windows.Forms.NumericUpDown.Minimum">Minimum</see>. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate Minimum. </param>
        ''' <returns> The limited Minimum. </returns>
        <Extension()>
        Public Function MinimumSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Decimal) As Decimal
            If control IsNot Nothing Then
                control.Minimum = value
            End If
            Return value
        End Function

#End Region

#Region " READ ONLY "

        ''' <summary> Sets the <see cref="Control">control</see> Read-Only value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeReadOnlySetter(ByVal control As UpDownBase, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of UpDownBase, Boolean)(AddressOf NumericUpDownExtensions.SafeReadOnlySetter), New Object() {control, value})
                Else
                    control.ReadOnly = value
                End If
            End If
            Return value
        End Function

#End Region

#Region " VALUE "

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> scaled by the
        ''' <paramref name="scalar">scalar</paramref> and limited by the control range.
        ''' Returns limited value (control value divided by the scalar). The setter is thread safe. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="scalar">  The scalar. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The control value divided by the scalar. </returns>
        <Extension()>
        Public Function SafeValueSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal scalar As Decimal, ByVal value As Decimal) As Decimal
            If control IsNot Nothing Then
                Return SafeValueSetter(control, scalar * value) / scalar
            Else
                Return 0
            End If
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> scaled by the
        ''' <paramref name="scalar">scalar</paramref> and limited by the control range.
        ''' Returns limited value (control value divided by the scalar). This setter is thread safe.
        ''' Returns limited value (control value divided by the scalar). The setter disables the control
        ''' before altering the checked state allowing the control code to use the enabled state for
        ''' preventing the execution of the control checked change actions. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="scalar">  The scalar. </param>
        ''' <returns> The control value divided by the scalar. </returns>
        <Extension()>
        Public Function SafeSilentValueSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal scalar As Decimal, ByVal value As Decimal) As Decimal
            Return SafeSilentValueSetter(control, scalar * value) / scalar
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function SafeValueSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Decimal) As Decimal
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of NumericUpDown, Decimal)(AddressOf NumericUpDownExtensions.SafeValueSetter), New Object() {control, value})
                Else
                    Return ValueSetter(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> value to the. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   limited by the control range. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function SafeValueSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Decimal?) As Decimal?
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of NumericUpDown, Decimal?)(AddressOf NumericUpDownExtensions.SafeValueSetter), New Object() {control, value})
                Else
                    Return ValueSetter(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe. Returns limited value (control value divided by the scaler). The
        ''' setter disables the control before altering the checked state allowing the control code to
        ''' use the enabled state for preventing the execution of the control checked change actions. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function SafeSilentValueSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Decimal) As Decimal
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of NumericUpDown, Decimal)(AddressOf NumericUpDownExtensions.SafeValueSetter), New Object() {control, value})
                Else
                    Return SilentValueSetter(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function SafeValueSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Double) As Double
            Return SafeValueSetter(control, CDec(value))
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' This setter is thread safe. Returns limited value (control value divided by the scalar). The
        ''' setter disables the control before altering the checked state allowing the control code to
        ''' use the enabled state for preventing the execution of the control checked change actions. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function SafeSilentValueSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Double) As Double
            Return SafeSilentValueSetter(control, CDec(value))
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> scaled by the
        ''' <paramref name="scalar">scalar</paramref> and limited by the control range.
        ''' Returns limited value (control value divided by the scalar). </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="scalar">  The scalar. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The control value divided by the scalar. </returns>
        <Extension()>
        Public Function ValueSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal scalar As Decimal, ByVal value As Decimal) As Decimal
            Return ValueSetter(control, scalar * value) / scalar
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> scaled by the
        ''' <paramref name="scalar">scalar</paramref> and limited by the control range.
        ''' Returns limited value (control value divided by the scalar). The setter disables the control
        ''' before altering the checked state allowing the control code to use the enabled state for
        ''' preventing the execution of the control checked change actions. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="scalar">  The scalar. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The control value divided by the scalar. </returns>
        <Extension()>
        Public Function SilentValueSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal scalar As Decimal, ByVal value As Decimal) As Decimal
            Return SilentValueSetter(control, scalar * value) / scalar
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function ValueSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Decimal) As Decimal
            If control IsNot Nothing Then
                control.Value = Math.Max(control.Minimum, Math.Min(control.Maximum, value))
                Return control.Value
            Else
                Return value
            End If
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function ValueSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Decimal?) As Decimal?
            If control IsNot Nothing Then
                If value.HasValue Then
                    control.Value = Math.Max(control.Minimum, Math.Min(control.Maximum, value.Value))
                Else
                    control.Text = ""
                End If

                Return control.Value
            Else
                Return value
            End If
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' The setter disables the control before altering the checked state allowing the control code
        ''' to use the enabled state for preventing the execution of the control checked change actions. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function SilentValueSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Decimal) As Decimal
            If control IsNot Nothing Then
                Dim wasEnabled As Boolean = control.Enabled
                control.Enabled = False
                ValueSetter(control, value)
                control.Enabled = wasEnabled
                Return control.Value
            Else
                Return value
            End If
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function ValueSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Double) As Double
            Return ValueSetter(control, CDec(value))
        End Function

        ''' <summary> Sets the <see cref="NumericUpDown">control</see> value to the
        ''' <paramref name="value">value</paramref> limited by the control range.
        ''' The setter disables the control before altering the checked state allowing the control code
        ''' to use the enabled state for preventing the execution of the control checked change actions. </summary>
        ''' <param name="control"> The numeric up down control. </param>
        ''' <param name="value">   The candidate value. </param>
        ''' <returns> The limited value. </returns>
        <Extension()>
        Public Function SilentValueSetter(ByVal control As System.Windows.Forms.NumericUpDown, ByVal value As Double) As Decimal
            If control IsNot Nothing Then
                Dim wasEnabled As Boolean = control.Enabled
                control.Enabled = False
                ValueSetter(control, CDec(value))
                control.Enabled = wasEnabled
                Return control.Value
            End If
            Return CDec(value)
        End Function

#End Region

    End Module
End Namespace
