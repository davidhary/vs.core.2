﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Namespace TextBoxExtensions
    ''' <summary> Includes extensions for <see cref="TextBox">Text Box</see>. </summary>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/19/2010" by="David" revision="1.2.3975.x"> Created. </history>
    Public Module Methods

        ''' <summary> Sets the <see cref="TextBoxBase">control</see>
        ''' <see cref="TextBoxBase.MaxLength">maximum length</see> value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The <see cref="TextBoxBase">control</see>. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> The value. </returns>
        <Extension()>
        Public Function SafeMaxLengthSetter(ByVal control As System.Windows.Forms.TextBoxBase, ByVal value As Integer) As Integer
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of TextBoxBase, Integer)(AddressOf TextBoxExtensions.SafeMaxLengthSetter), New Object() {control, value})
                Else
                    control.MaxLength = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Control">text box</see> read-only value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeCausesValidationSetter(ByVal control As System.Windows.Forms.TextBoxBase, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of TextBoxBase, Boolean)(AddressOf TextBoxExtensions.SafeCausesValidationSetter), New Object() {control, value})
                Else
                    control.CausesValidation = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="Control">text box</see> read-only value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeReadOnlySetter(ByVal control As System.Windows.Forms.TextBoxBase, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of TextBoxBase, Boolean)(AddressOf TextBoxExtensions.SafeReadOnlySetter), New Object() {control, value})
                Else
                    control.ReadOnly = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Appends <paramref name="value">text</paramref> to the
        ''' <see cref="TextBoxBase">control</see>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The <see cref="TextBoxBase"/> control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function Append(ByVal control As System.Windows.Forms.TextBoxBase, ByVal value As String) As String
            If control IsNot Nothing Then
                If String.IsNullOrWhiteSpace(value) Then
                    value = ""
                Else
                    If control.InvokeRequired Then
                        control.Invoke(New Action(Of TextBoxBase, String)(AddressOf TextBoxExtensions.Append), New Object() {control, value})
                    Else
                        With control
                            .SelectionStart = .Text.Length
                            .SelectionLength = 0
                            If .SelectionStart = 0 Then
                                .SelectedText = value
                            Else
                                .SelectedText = value
                            End If
                            .SelectionStart = .Text.Length
                        End With
                    End If
                End If
            End If
            Return value
        End Function

        ''' <summary> Appends formatted text of the <see cref="TextBoxBase">control</see>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The <see cref="TextBoxBase"/> control. </param>
        ''' <param name="format">  The text format. </param>
        ''' <param name="args">    The format arguments. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function Append(ByVal control As System.Windows.Forms.TextBoxBase, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return Append(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

        ''' <summary> Prepends <paramref name="value">text</paramref> to the
        ''' <see cref="TextBoxBase">control</see>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The <see cref="TextBoxBase"/> control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function Prepend(ByVal control As System.Windows.Forms.TextBoxBase, ByVal value As String) As String
            If control IsNot Nothing Then
                If String.IsNullOrWhiteSpace(value) Then
                    value = ""
                Else
                    If control.InvokeRequired Then
                        control.Invoke(New Action(Of TextBoxBase, String)(AddressOf TextBoxExtensions.Append), New Object() {control, value})
                    Else
                        control.SelectionStart = 0
                        control.SelectionLength = 0
                        control.SelectedText = value
                        control.SelectionLength = 0
                    End If
                End If
            End If
            Return value
        End Function

        ''' <summary> Prepends the formatted text.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The <see cref="TextBoxBase"/> control. </param>
        ''' <param name="format">  The text format. </param>
        ''' <param name="args">    The format arguments. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function Prepend(ByVal control As System.Windows.Forms.TextBoxBase, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return Prepend(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

    End Module
End Namespace
