﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Namespace ComboBoxExtensions
    ''' <summary> Includes extensions for <see cref="ComboBox">Combo Box</see>. </summary>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/19/2010" by="David" revision="1.2.3975.x"> Created. </history>
    Public Module Methods

#Region " READ ONLY SETTER "

        ''' <summary> Sets the <see cref="ComboBox">Combo Box Control</see> Read-Only value to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Combo Box Control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeReadOnlySetter(ByVal control As Core.Controls.ComboBox, ByVal value As Boolean) As Boolean
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, Boolean)(AddressOf ComboBoxExtensions.SafeReadOnlySetter), New Object() {control, value})
                Else
                    control.ReadOnly = value
                End If
            End If
            Return value
        End Function

#End Region

#Region " TEXT SETTER "

        ''' <summary> Sets the <see cref="ComboBox">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The combo box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As System.Windows.Forms.ComboBox, ByVal value As String) As String
            If control IsNot Nothing Then
                If String.IsNullOrWhiteSpace(value) Then
                    value = ""
                End If
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, String)(AddressOf ComboBoxExtensions.SafeTextSetter), New Object() {control, value})
                Else
                    control.Text = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="ComboBox">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. The control is disabled when set so that the handling of the
        ''' changed event can be skipped. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The combo box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentTextSetter(ByVal control As System.Windows.Forms.ComboBox, ByVal value As String) As String
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, String)(AddressOf ComboBoxExtensions.SafeSilentTextSetter), New Object() {control, value})
                Else
                    SilentTextSetter(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="ComboBox">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' The control is disabled when set so that the handling of the changed event can be skipped. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The combo box control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SilentTextSetter(ByVal control As System.Windows.Forms.ComboBox, ByVal value As String) As String
            If control IsNot Nothing Then
                If String.IsNullOrWhiteSpace(value) Then
                    value = ""
                End If
                Dim enabled As Boolean = control.Enabled
                control.Enabled = False
                control.Text = value
                control.Enabled = enabled
            End If
            Return value
        End Function

#End Region

#Region " SEARCH AND SELECT "

        ''' <summary> Searches the combo box and selects a located item. </summary>
        ''' <remarks> Use this method to search and select combo box index. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="combo"> Specifies an instance of a ComboBox. </param>
        ''' <param name="e">     Specifies an instance of the
        ''' <see cref="System.Windows.Forms.KeyPressEventArgs">event arguments</see>,
        ''' which specify the key pressed. </param>
        ''' <returns> The found and select. </returns>
        <Extension()>
        Public Function SearchAndSelect(ByVal combo As System.Windows.Forms.ComboBox,
                                        ByVal e As System.Windows.Forms.KeyPressEventArgs) As Integer

            If combo Is Nothing Then
                Throw New ArgumentNullException("combo")
            End If
            If e IsNot Nothing AndAlso Char.IsControl(e.KeyChar) Then
                Return combo.SelectedIndex
            Else
                Dim cursorPosition As Integer = combo.SelectionStart
                Dim selectionLength As Integer = combo.SelectionLength
                Dim itemNumber As Integer = combo.FindString(combo.Text)
                If itemNumber >= 0 Then
                    ' if we have a match, select the current item and reposition the cursor
                    combo.SelectedIndex = itemNumber
                    combo.SelectionStart = cursorPosition
                    combo.SelectionLength = selectionLength
                    Return itemNumber
                Else
                    Return combo.SelectedIndex
                End If
            End If

        End Function

        ''' <summary> Searches the combo box and selects a located item upon releasing a key. </summary>
        ''' <remarks> Use this method to search and select combo box index. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="combo"> Specifies an instance of a ComboBox. </param>
        ''' <param name="e">     Specifies an instance of the
        ''' <see cref="System.Windows.Forms.KeyEventArgs">event arguments</see>,
        ''' which specify the key released. </param>
        ''' <returns> The found and select. </returns>
        <Extension()>
        Public Function SearchAndSelect(ByVal combo As System.Windows.Forms.ComboBox,
                                        ByVal e As System.Windows.Forms.KeyEventArgs) As Integer

            If combo Is Nothing Then
                Throw New ArgumentNullException("combo")
            End If
            If e Is Nothing Then
                Throw New ArgumentNullException("e")
            End If

            If e.KeyCode.ToString.Length > 1 Then
                ' if a control character, skip
                Return combo.SelectedIndex
            Else
                Dim cursorPosition As Integer = combo.SelectionStart
                Dim selectionLength As Integer = combo.SelectionLength
                Dim itemNumber As Integer = combo.FindString(combo.Text)
                If itemNumber >= 0 Then
                    ' if we have a match, select the current item and reposition the cursor
                    combo.SelectedIndex = itemNumber
                    combo.SelectionStart = cursorPosition
                    combo.SelectionLength = selectionLength
                    Return itemNumber
                Else
                    Return combo.SelectedIndex
                End If
            End If

        End Function

#End Region

#Region " SELECT ITEM "

        ''' <summary> Determines whether the specified control contains display value. </summary>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="control">      The control. </param>
        ''' <param name="displayValue"> The display value. </param>
        ''' <returns> <c>True</c> if the specified control contains display value; otherwise, <c>False</c>. </returns>
        <Extension()>
        Public Function ContainsDisplayValue(ByVal control As System.Windows.Forms.ComboBox, ByVal displayValue As String) As Boolean
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            ElseIf displayValue Is Nothing Then
                Throw New ArgumentNullException("displayValue")
            Else
                Return 0 <= control.FindStringExact(displayValue)
            End If
        End Function

        ''' <summary> Gets the selected or default <paramref name="defaultValue">key value</paramref>. </summary>
        ''' <param name="control">      The control. </param>
        ''' <param name="defaultValue"> The default value. </param>
        ''' <returns> ``0. </returns>
        <Extension()>
        Public Function GetSelectedItemKey(Of T)(ByVal control As System.Windows.Forms.ComboBox, ByVal defaultValue As T) As T
            If control Is Nothing OrElse control.SelectedValue Is Nothing Then
                Return defaultValue
            Else
                Dim v As KeyValuePair(Of T, String) = CType(control.SelectedItem, KeyValuePair(Of T, String))
                Return v.Key
            End If
        End Function

        ''' <summary> Selects an item. </summary>
        ''' <param name="control">      The control. </param>
        ''' <param name="displayValue"> The display value. </param>
        <Extension()>
        Public Sub SelectItem(ByVal control As System.Windows.Forms.ComboBox, ByVal displayValue As String)
            If control IsNot Nothing Then
                If control.SelectedValue Is Nothing OrElse
                    String.IsNullOrWhiteSpace(control.SelectedText) OrElse
                    Not control.Text.Equals(displayValue) Then
                    If String.IsNullOrWhiteSpace(displayValue) Then
                        If control.SelectedIndex <> -1 Then
                            control.SelectedIndex = -1
                        End If
                    Else
                        Dim i As Integer = control.FindStringExact(displayValue)
                        If control.SelectedIndex <> i Then
                            control.SelectedIndex = i
                            If control.SelectedIndex = -1 AndAlso control.DropDownStyle = ComboBoxStyle.DropDown Then
                                control.Text = displayValue
                            End If
                        End If
                    End If
                End If
            End If
        End Sub

        ''' <summary> Selects an item in a thread safe way. </summary>
        ''' <param name="control">      The control. </param>
        ''' <param name="displayValue"> The display value. </param>
        <Extension()>
        Public Sub SafeSelectItem(ByVal control As System.Windows.Forms.ComboBox, ByVal displayValue As String)
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, String)(AddressOf ComboBoxExtensions.SafeSelectItem), New Object() {control, displayValue})
                Else
                    control.SelectItem(displayValue)
                End If
            End If
        End Sub

        ''' <summary> Selects an item in a 'Silent' (control is disabled) and thread safe way. </summary>
        ''' <param name="control">      The control. </param>
        ''' <param name="displayValue"> The display value. </param>
        <Extension()>
        Public Sub SafeSilentSelectItem(ByVal control As System.Windows.Forms.ComboBox, ByVal displayValue As String)
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, String)(AddressOf ComboBoxExtensions.SafeSelectItem), New Object() {control, displayValue})
                Else
                    control.SilentSelectItem(displayValue)
                End If
            End If
        End Sub

        ''' <summary> Selects the <see cref="Control">combo box</see> item by setting the selected item to
        ''' the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Combo box control. </param>
        ''' <param name="value">   The selected item value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSelectItem(ByVal control As System.Windows.Forms.ComboBox, ByVal value As Object) As Object
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, Object)(AddressOf ComboBoxExtensions.SafeSelectItem), New Object() {control, value})
                Else
                    control.SelectedItem = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Selects the <see cref="Control">combo box</see> item by setting the selected item to
        ''' the <see cref="T:System.Collections.Generic.KeyValuePair">key value pair</see>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Combo box control. </param>
        ''' <param name="key">     The selected item key. </param>
        ''' <param name="value">   The selected item value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSelectItem(ByVal control As System.Windows.Forms.ComboBox, ByVal key As System.Enum, ByVal value As String) As Object
            Return ComboBoxExtensions.SafeSelectItem(control, New System.Collections.Generic.KeyValuePair(Of [Enum], String)(key, value))
        End Function

        ''' <summary> Selects the <see cref="Control">combo box</see> item by setting the selected item to
        ''' the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Combo box control. </param>
        ''' <param name="value">   The selected item value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentSelectItem(ByVal control As System.Windows.Forms.ComboBox, ByVal value As Object) As Object
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, Object)(AddressOf ComboBoxExtensions.SafeSilentSelectItem), New Object() {control, value})
                Else
                    ComboBoxExtensions.SilentSelectItem(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary> Selects the <see cref="Control">combo box</see> item by setting the selected item to
        ''' the <see cref="System.Collections.Generic.KeyValuePair">key value pair</see>.
        ''' The setter disables the control before altering the checked state allowing the control code
        ''' to use the enabled state for preventing the execution of the control checked change actions.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Combo box control. </param>
        ''' <param name="key">     The selected item key. </param>
        ''' <param name="value">   The selected item value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentSelectItem(ByVal control As System.Windows.Forms.ComboBox, ByVal key As System.Enum, ByVal value As String) As Object
            Return ComboBoxExtensions.SafeSilentSelectItem(control, New System.Collections.Generic.KeyValuePair(Of [Enum], String)(key, value))
        End Function

        ''' <summary> Selects an item in a 'Silent' (control is disabled) way. </summary>
        ''' <param name="control">      The control. </param>
        ''' <param name="displayValue"> The display value. </param>
        <Extension()>
        Public Sub SilentSelectItem(ByVal control As System.Windows.Forms.ComboBox, ByVal displayValue As String)
            If control IsNot Nothing Then
                If control.SelectedValue Is Nothing OrElse
                    String.IsNullOrWhiteSpace(control.SelectedText) OrElse
                    Not control.Text.Equals(displayValue) Then
                    Dim enabled As Boolean = control.Enabled
                    control.Enabled = False
                    control.SelectItem(displayValue)
                    control.Enabled = enabled
                End If
            End If
        End Sub

        ''' <summary> Selects the <see cref="Control">combo box</see> item by setting the selected item to
        ''' the <see cref="System.Collections.Generic.KeyValuePair">key value pair</see>.
        ''' The setter disables the control before altering the checked state allowing the control code
        ''' to use the enabled state for preventing the execution of the control checked change actions. </summary>
        ''' <param name="control"> Combo box control. </param>
        ''' <param name="value">   The selected item value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SilentSelectItem(ByVal control As System.Windows.Forms.ComboBox, ByVal value As Object) As Object
            If control IsNot Nothing Then
                Dim wasEnabled As Boolean = control.Enabled
                control.Enabled = False
                control.SelectedItem = value
                control.Enabled = wasEnabled
            End If
            Return value
        End Function

        ''' <summary> Selects the <see cref="Control">combo box</see> item by setting the selected item to
        ''' the <see cref="System.Collections.Generic.KeyValuePair">key value pair</see>.
        ''' The setter disables the control before altering the checked state allowing the control code
        ''' to use the enabled state for preventing the execution of the control checked change actions. </summary>
        ''' <param name="control"> Combo box control. </param>
        ''' <param name="key">     The selected item key. </param>
        ''' <param name="value">   The selected item value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SilentSelectItem(ByVal control As System.Windows.Forms.ComboBox, ByVal key As System.Enum, ByVal value As String) As Object
            Return ComboBoxExtensions.SilentSelectItem(control, New System.Collections.Generic.KeyValuePair(Of [Enum], String)(key, value))
        End Function

#End Region

#Region " SELECT Value "

        ''' <summary> Selects the <see cref="Control">combo box</see> Value by setting the selected Value to
        ''' the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Combo box control. </param>
        ''' <param name="value">   The selected Value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSelectValue(ByVal control As System.Windows.Forms.ComboBox, ByVal value As Object) As Object
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, Object)(AddressOf ComboBoxExtensions.SafeSelectValue), New Object() {control, value})
                Else
                    control.SelectedValue = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Selects the <see cref="Control">combo box</see> Value by setting the selected Value to
        ''' the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> Combo box control. </param>
        ''' <param name="value">   The selected Value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeSilentSelectValue(ByVal control As System.Windows.Forms.ComboBox, ByVal value As Object) As Object
            If control IsNot Nothing Then
                If control.InvokeRequired Then
                    control.Invoke(New Action(Of ComboBox, Object)(AddressOf ComboBoxExtensions.SafeSilentSelectValue), New Object() {control, value})
                Else
                    ComboBoxExtensions.SilentSelectValue(control, value)
                End If
            End If
            Return value
        End Function

        ''' <summary> Selects the <see cref="Control">combo box</see> Value by setting the selected Value to
        ''' the <see cref="System.Collections.Generic.KeyValuePair">key value pair</see>.
        ''' The setter disables the control before altering the checked state allowing the control code
        ''' to use the enabled state for preventing the execution of the control checked change actions. </summary>
        ''' <param name="control"> Combo box control. </param>
        ''' <param name="value">   The selected Value. </param>
        ''' <returns> value. </returns>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters",
            Justification:="Does not work with the base class")> <Extension()>
        Public Function SilentSelectValue(ByVal control As System.Windows.Forms.ComboBox, ByVal value As Object) As Object
            If control IsNot Nothing Then
                Dim wasEnabled As Boolean = control.Enabled
                control.Enabled = False
                control.SelectedValue = value
                control.Enabled = wasEnabled
            End If
            Return value
        End Function

#End Region

    End Module
End Namespace
