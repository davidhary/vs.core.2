﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Imports isr.Core.Controls.ControlCollectionExtensions
Namespace ControlContainerExtensions
    ''' <summary> Includes extensions for <see cref="Control"/>  and <see cref="ContainerControl">container control</see>. </summary>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/19/2010" by="David" revision="1.2.3975.x"> Created. </history>
    Public Module Methods

        ''' <summary> Validates the specified control. </summary>
        ''' <param name="value">     Control to be validated. </param>
        ''' <param name="container"> specifies the instance of the container control which controls are to
        ''' be validated. </param>
        ''' <returns> Returns false if failed to validate. Returns true if control validated or not
        ''' visible. </returns>
        <Extension()>
        Public Function Validate(ByVal value As Control, ByVal container As System.Windows.Forms.ContainerControl) As Boolean

            If value Is Nothing OrElse container Is Nothing Then Return True
            ' focus on and validate the control
            If container.Visible AndAlso value.Visible Then
                value.Focus()
                Return container.Validate
            Else
                Return True
            End If

        End Function

        ''' <summary> Validates all controls in the container control. </summary>
        ''' <param name="container"> specifies the instance of the container control which controls are to
        ''' be validated. </param>
        ''' <returns> Returns false if any control failed to validated. </returns>
        <Extension()>
        Public Function ValidateControls(ByVal container As System.Windows.Forms.ContainerControl) As Boolean

            If container Is Nothing Then Return True
            ValidateControls = True
            Dim controls As System.Windows.Forms.Control() = container.Controls.RetrieveControls()
            For i As Integer = 0 To controls.Length - 1
                ' validate the selected control
                If Not controls(i).Validate(container) Then
                    ValidateControls = False
                End If
            Next

        End Function

    End Module
End Namespace
