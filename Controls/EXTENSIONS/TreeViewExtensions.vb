﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Namespace TreeViewExtensions
    ''' <summary> Includes extensions for <see cref="System.Windows.Forms.TreeView">Tree View</see> control. </summary>
    ''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="11/19/2010" by="David" revision="1.2.3975.x"> Created </history>
    Public Module Methods

#Region " TREE NODE "

        ''' <summary> Sets the <see cref="TreeNode">control</see> text to the
        ''' <paramref name="value">value</paramref>.
        ''' This setter is thread safe. </summary>
        ''' <param name="control"> The <see cref="TreeNode"/> control. </param>
        ''' <param name="value">   The value. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As TreeNode, ByVal value As String) As String
            If control IsNot Nothing Then
                If control.TreeView?.InvokeRequired Then
                    control.TreeView.Invoke(New Action(Of TreeNode, String)(AddressOf TreeViewExtensions.SafeTextSetter),
                                            New Object() {control, value})
                Else
                    control.Text = value
                End If
            End If
            Return value
        End Function

        ''' <summary> Sets the <see cref="TreeNode">control</see> text to the formatted value.
        ''' This setter is thread safe. </summary>
        ''' <remarks> The value is set to empty if null or empty. </remarks>
        ''' <param name="control"> The <see cref="TreeNode"/> control. </param>
        ''' <param name="format">  The text format. </param>
        ''' <param name="args">    The format arguments. </param>
        ''' <returns> value. </returns>
        <Extension()>
        Public Function SafeTextSetter(ByVal control As TreeNode, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return SafeTextSetter(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

#End Region

#Region " TREE VIEW "

        ''' <summary> Selects a node. </summary>
        ''' <param name="control"> The <see cref="TreeView"/> control. </param>
        ''' <param name="nodeName"> Name of the node. </param>
        <Runtime.CompilerServices.Extension()>
        Public Sub SelectNode(ByVal control As TreeView, ByVal nodeName As String)
            If control IsNot Nothing Then
                control.SelectedNode = control.Nodes(nodeName)
            End If
        End Sub

        ''' <summary> Selects a node. </summary>
        ''' <param name="control"> The <see cref="TreeView"/> control. </param>
        ''' <param name="nodeHierarchy"> The hierarchy of node names. </param>
        <Runtime.CompilerServices.Extension()>
        Public Sub SelectNode(ByVal control As TreeView, ByVal nodeHierarchy As String())
            If control IsNot Nothing AndAlso nodeHierarchy IsNot Nothing AndAlso nodeHierarchy.Count > 0 Then
                Dim parentTreeNode As TreeNode = Nothing
                Dim childTreeNode As TreeNode = Nothing
                For Each nodeName As String In nodeHierarchy
                    If parentTreeNode Is Nothing Then
                        parentTreeNode = control.Nodes(nodeName)
                        childTreeNode = parentTreeNode
                    ElseIf parentTreeNode.Nodes.Count > 0 Then
                        childTreeNode = parentTreeNode.Nodes(nodeName)
                    Else
                        childTreeNode = Nothing
                    End If
                    If childTreeNode Is Nothing Then
                        Exit For
                    Else
                        parentTreeNode = childTreeNode
                    End If
                Next
                control.SelectedNode = parentTreeNode
            End If
        End Sub

#End Region

    End Module
End Namespace
