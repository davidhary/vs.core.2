﻿Imports System.ComponentModel
Imports System.Windows.Forms
''' <summary> File list box. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="9/27/2013" by="David" revision=""> Created. </history>
<DesignerCategory("code"), System.ComponentModel.Description("File List Box")>
Public Class FileListBox
    Inherits System.Windows.Forms.ListBox

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        '_ReadOnlyBackColor = SystemColors.Control
        '_ReadOnlyForeColor = SystemColors.WindowText
    End Sub

    ''' <summary> Gets the file name pattern for files displayed in the file list box. </summary>
    ''' <value> The pattern. </value>
    <Category("Pattern")>
    <Description("Specifies the file name pattern.")>
    Public Property Pattern As String

    ''' <summary> Updates the display. </summary>
    Public Sub UpdateDisplay()
        If Not String.IsNullOrWhiteSpace(Me.Path) Then
            Me.Items.Clear()
            Dim di As New System.IO.DirectoryInfo(Me._path)
            If di.Exists Then
                For Each fi As System.IO.FileInfo In di.GetFiles(Me.Pattern)
                    Me.Items.Add(fi.Name)
                Next
            End If
        End If
    End Sub

    ''' <summary> Gets the name of the file. </summary>
    ''' <value> The name of the file. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property FileName As String
        Get
            Return Me.Text
        End Get
    End Property

    ''' <summary> Full pathname of the file. </summary>
    Private _path As String

    ''' <summary> Gets or sets the current path. </summary>
    ''' <value> The path. </value>
    <Category("Behavior")>
    <Description("Indicates whether the List Box is read only.")>
    Public Property Path As String
        Get
            Return Me._path
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = ""
            If Not value.Equals(Me.Path) Then
                Me._path = value
                Me.UpdateDisplay()
            End If
        End Set
    End Property

    ''' <summary> The read write context menu. </summary>
    Private readWriteContextMenu As ContextMenu
    ''' <summary> true to read only. </summary>
    Private _ReadOnly As Boolean

    ''' <summary> Gets or sets the read only. </summary>
    ''' <value> The read only. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether the List Box is read only.")>
    Public Property [ReadOnly]() As Boolean
        Get
            Return Me._ReadOnly
        End Get
        Set(ByVal value As Boolean)
            If Me._ReadOnly <> value Then
                Me._ReadOnly = value
                If value Then
                    readWriteContextMenu = MyBase.ContextMenu
                    Me.ContextMenu = New ContextMenu()
                    Dim h As Integer = MyBase.Height
                    Me.Height = h + 3
                    Me.BackColor = Me.ReadOnlyBackColor
                    Me.ForeColor = Me.ReadOnlyForeColor
                Else
                    Me.ContextMenu = readWriteContextMenu
                    Me.BackColor = Me.ReadWriteBackColor
                    Me.ForeColor = Me.ReadWriteForeColor
                End If
            End If
        End Set
    End Property

    Private _ReadOnlyBackColor As Color
    ''' <summary> Gets or sets the color of the read only back. </summary>
    ''' <value> The color of the read only back. </value>
    <DefaultValue(GetType(Drawing.Color), "SystemColors.Control")>
    <Description("Back color when read only"), Category("Appearance")>
    Public Property ReadOnlyBackColor() As Color
        Get
            If Me._ReadOnlyBackColor.IsEmpty Then
                Me._ReadOnlyBackColor = SystemColors.Control
            End If
            Return Me._ReadOnlyBackColor
        End Get
        Set(value As Color)
            Me._ReadOnlyBackColor = value
        End Set
    End Property

    Private _ReadOnlyForeColor As Color
    ''' <summary> Gets or sets the color of the read only foreground. </summary>
    ''' <value> The color of the read only foreground. </value>
    <DefaultValue(GetType(Drawing.Color), "SystemColors.WindowText")>
    <Description("Fore color when read only"), Category("Appearance")>
    Public Property ReadOnlyForeColor() As Color
        Get
            If Me._ReadOnlyForeColor.IsEmpty Then
                Me._ReadOnlyForeColor = SystemColors.WindowText
            End If
            Return Me._ReadOnlyForeColor
        End Get
        Set(value As Color)
            Me._ReadOnlyForeColor = value
        End Set
    End Property

    Private _ReadWriteBackColor As System.Drawing.Color
    ''' <summary> Gets or sets the color of the read write back. </summary>
    ''' <value> The color of the read write back. </value>
    <DefaultValue(GetType(System.Drawing.Color), "SystemColors.Window")>
    <Description("Back color when control is read/write"), Category("Appearance")>
    Public Property ReadWriteBackColor() As System.Drawing.Color
        Get
            If Me._ReadWriteBackColor.IsEmpty Then
                Me._ReadWriteBackColor = SystemColors.Window
            End If
            Return Me._ReadWriteBackColor
        End Get
        Set(value As Color)
            Me._ReadWriteBackColor = value
        End Set
    End Property

    Private _ReadWriteForeColor As System.Drawing.Color
    ''' <summary> Gets or sets the color of the read write foreground. </summary>
    ''' <value> The color of the read write foreground. </value>
    <DefaultValue(GetType(System.Drawing.Color), "System.Drawing.SystemColors.ControlText")>
    <Description("Fore color when control is read/write"), Category("Appearance")>
    Public Property ReadWriteForeColor() As System.Drawing.Color
        Get
            If Me._ReadWriteForeColor.IsEmpty Then
                Me._ReadWriteForeColor = SystemColors.ControlText
            End If
            Return Me._ReadWriteForeColor
        End Get
        Set(value As Color)
            Me._ReadWriteForeColor = value
        End Set
    End Property

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.KeyDown" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.KeyEventArgs" /> that contains the event
    ''' data. </param>
    Protected Overrides Sub OnKeyDown(ByVal e As KeyEventArgs)
        If e Is Nothing Then Return
        If Me.ReadOnly AndAlso (e.KeyCode = Keys.Up OrElse e.KeyCode = Keys.Down OrElse e.KeyCode = Keys.Delete OrElse
                                e.KeyCode = Keys.F4 OrElse e.KeyCode = Keys.PageDown OrElse e.KeyCode = Keys.PageUp) Then
            e.Handled = True
        Else
            MyBase.OnKeyDown(e)
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.KeyPress" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.KeyPressEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnKeyPress(ByVal e As KeyPressEventArgs)
        If e Is Nothing Then Return
        If Me.ReadOnly Then
            e.Handled = True
        Else
            MyBase.OnKeyPress(e)
        End If
    End Sub

End Class
