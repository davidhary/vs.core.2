﻿Imports System.ComponentModel
Imports System.Windows.Forms
''' <summary> Information provider. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="4/4/2015" by="David" revision=""> Created. </history>
<DesignerCategory("code"), System.ComponentModel.Description("Information Provider")>
Public Class InfoProvider
    Inherits System.Windows.Forms.ErrorProvider

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Default constructor. </summary>
    ''' <param name="container"> The container. </param>
    Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyBase.New(container)
    End Sub

    ''' <summary> Default constructor. </summary>
    ''' <param name="parentControl"> The parent control. </param>
    Public Sub New(ByVal parentControl As System.Windows.Forms.ContainerControl)
        MyBase.New(parentControl)
    End Sub

#End Region

#Region " CLEAR "

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <param name="sender"> The event sender. </param>
    Public Overloads Sub Clear(ByVal sender As Object)
        Dim control As Control = TryCast(sender, Control)
        If control IsNot Nothing Then
            Me.Clear(control)
        Else
            Dim toolStripItem As ToolStripItem = TryCast(sender, ToolStripItem)
            If toolStripItem IsNot Nothing Then
                Me.Clear(toolStripItem)
            End If
        End If
    End Sub

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <param name="sender"> The event sender. </param>
    Public Overloads Sub Clear(ByVal sender As Control)
        If sender IsNot Nothing Then
            Me.SetError(sender, "")
        End If
    End Sub

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <param name="sender"> The event sender. </param>
    Public Overloads Sub Clear(ByVal sender As ToolStripItem)
        If sender IsNot Nothing Then
            Me.SetError(sender.Owner, "")
        End If
    End Sub

#End Region

#Region " ANNUNCIATE - OBJECT "

    ''' <summary> Annunciates error. </summary>
    ''' <param name="sender">  The event sender. </param>
    ''' <param name="details"> The details. </param>
    ''' <returns> A String. </returns>
    Public Function Annunciate(ByVal sender As Object, ByVal level As InfoProviderLevel, ByVal details As String) As String
        Dim control As Control = TryCast(sender, Control)
        If control IsNot Nothing Then
            Me.Annunciate(control, level, details)
        Else
            Dim toolStripItem As ToolStripItem = TryCast(sender, ToolStripItem)
            If toolStripItem IsNot Nothing Then
                Me.Annunciate(toolStripItem, level, details)
            End If
        End If
        Return details
    End Function

    ''' <summary> Annunciates error. </summary>
    ''' <param name="sender"> The event sender. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Public Function Annunciate(ByVal sender As Object, ByVal level As InfoProviderLevel, ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Me.Annunciate(sender, level, String.Format(format, args))
    End Function

#End Region

#Region " PAD / ALIGN - CONTROL "

    ''' <summary> Aligns the icon. </summary>
    ''' <param name="sender">        The sender object; must be a control. </param>
    ''' <param name="iconAlignment"> The icon alignment. </param>
    Protected Sub AlignIcon(ByVal sender As Object, ByVal iconAlignment As ErrorIconAlignment)
        If sender IsNot Nothing Then
            Me.SetIconAlignment(TryCast(sender, Control), iconAlignment)
        End If
    End Sub

    ''' <summary> Set icon padding. </summary>
    ''' <param name="sender">  The sender object; must be a control. </param>
    ''' <param name="padding"> The padding. </param>
    Protected Sub PadIcon(ByVal sender As Object, ByVal padding As Integer)
        If sender IsNot Nothing Then
            Me.SetIconPadding(TryCast(sender, Control), padding)
        End If
    End Sub

#End Region

#Region " ANNUNCIATE - CONTROL "

    ''' <summary> Annunciates error. </summary>
    ''' <param name="sender">  The event sender. </param>
    ''' <param name="details"> The details. </param>
    ''' <returns> A String. </returns>
    Public Function Annunciate(ByVal sender As Control, ByVal level As InfoProviderLevel, ByVal details As String) As String
        If sender IsNot Nothing Then
            If TypeOf sender.Container Is ToolStripItem Then
                Me.Annunciate(sender.Container, level, details)
            Else
                Me.SelectIcon(level)
                Me.SetError(sender, details)
            End If
        End If
        Return details
    End Function

    ''' <summary> Annunciates error. </summary>
    ''' <param name="sender"> The event sender. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Public Function Annunciate(ByVal sender As Control, ByVal level As InfoProviderLevel, ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Annunciate(sender, level, String.Format(format, args))
    End Function

#End Region

#Region " ANNUNCIATE -- TOOL STRIP "

    ''' <summary> Searches for the first tool strip item position. </summary>
    ''' <param name="container"> The container. </param>
    ''' <param name="item">      The item. </param>
    ''' <returns> The found tool strip item position. </returns>
    Public Shared Function FindToolStripItemPosition(ByVal container As ToolStrip, ByVal item As ToolStripItem) As Integer
        If container Is Nothing Then Throw New ArgumentNullException("container")
        Dim loc As Integer = 0
        Dim isFound As Boolean = False
        For Each c As ToolStripItem In container.Items
            If c Is item Then
                isFound = True
                Exit For
            ElseIf c.Visible Then
                loc += c.Width + c.Margin.Left + c.Margin.Right
            End If
        Next
        If isFound Then
            If container.GripStyle = ToolStripGripStyle.Visible Then
                loc += container.GripRectangle.Width + container.GripMargin.Left + container.GripMargin.Right
            End If
            Return loc
        Else
            Return 0
        End If
    End Function

    ''' <summary> Annunciates error. </summary>
    ''' <param name="sender">  The sender. </param>
    ''' <param name="details"> The details. </param>
    ''' <returns> A String. </returns>
    Public Function Annunciate(ByVal sender As ToolStripItem, ByVal level As InfoProviderLevel, ByVal details As String) As String
        If sender IsNot Nothing AndAlso sender.Owner IsNot Nothing Then
            Me.SelectIcon(level)
            Me.SetIconAlignment(sender.Owner, ErrorIconAlignment.MiddleLeft)
            Me.SetIconPadding(sender.Owner, -(InfoProvider.FindToolStripItemPosition(sender.Owner, sender) +
                                                             sender.Width + sender.Margin.Left + 10))
            Me.SetError(sender.Owner, details)
        End If
        Return details
    End Function

    ''' <summary> Annunciates error. </summary>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Public Function Annunciate(ByVal sender As ToolStripItem, ByVal level As InfoProviderLevel, ByVal format As String, ByVal ParamArray args() As Object) As String
        Return Me.Annunciate(sender, level, String.Format(format, args))
    End Function

    ''' <summary> Select icon. </summary>
    ''' <param name="level"> The level. </param>
    Private Sub SelectIcon(ByVal level As InfoProviderLevel)
        Select Case level
            Case InfoProviderLevel.Alert
                Me.Icon = My.Resources.exclamation
            Case InfoProviderLevel.Error
                Me.Icon = My.Resources.dialog_error_2
            Case InfoProviderLevel.Info
                Me.Icon = My.Resources.dialog_information_3
            Case Else
                Me.Icon = My.Resources.dialog_information_3
        End Select
    End Sub

#End Region

End Class

''' <summary> Values that represent information provider levels. </summary>
Public Enum InfoProviderLevel
    <Description("Information")> Info
    <Description("Alert")> Alert
    <Description("Error")> [Error]
End Enum
