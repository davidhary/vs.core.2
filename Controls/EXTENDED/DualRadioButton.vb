﻿Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core.Pith.EventHandlerExtensions
''' <summary> A dual radio button control functioning as a check box control displaying both its
''' states. </summary>
''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="12/09/2010" by="David" revision="1.02.3995.x"> created. </history>
<Description("Dual Radio Button"), DefaultEvent("CheckToggled"), DefaultBindingProperty("Checked")>
Public Class DualRadioButton

#Region " CONSTRUCTORS "

    ''' <summary> Clears internal properties. </summary>
    Public Sub New()
        MyBase.New()
        InitializeComponent()
        Me._PrimaryButtonHorizontalLocation = HorizontalLocation.Left
        Me._PrimaryButtonVerticalLocation = VerticalLocation.Center
        Me._SecondaryButtonHorizontalLocation = HorizontalLocation.Right
        Me._SecondaryButtonVerticalLocation = VerticalLocation.Center
        Me.PrimaryRadioButton.ReadOnly = False
        Me.SecondaryRadioButton.ReadOnly = False
        Me._Checked = False
        Me.PrimaryRadioButton.Checked = False
        Me.SecondaryRadioButton.Checked = False
        Me.updateButtonsLocation()
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Pith.PropertyNotifyControlBase and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 12/19/2015. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
                Me.RemoveCheckToggledEventHandler(Me.CheckToggledEvent)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " APPEARANCE "

    ''' <summary> The primary button horizontal location. </summary>
    Private _PrimaryButtonHorizontalLocation As HorizontalLocation

    ''' <summary> Gets or sets the primary button horizontal location. </summary>
    ''' <value> The primary button horizontal location. </value>
    <DefaultValue(GetType(HorizontalLocation), "Left")>
    <Category("Behavior")>
    <Description("Horizontal location of the primary button.")>
    Public Property PrimaryButtonHorizontalLocation() As HorizontalLocation
        Get
            Return Me._PrimaryButtonHorizontalLocation
        End Get
        Set(ByVal value As HorizontalLocation)
            If value <> Me.PrimaryButtonHorizontalLocation Then
                Me._PrimaryButtonHorizontalLocation = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.PrimaryButtonHorizontalLocation))
                updateButtonsLocation()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the text associated with the primary button. </summary>
    ''' <value> The primary button text. </value>
    <Category("Appearance"), Description("The text associated with the primary button"),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue("ON")>
    Public Property PrimaryButtonText() As String
        Get
            Return Me.PrimaryRadioButton.Text
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then
                value = ""
            End If
            If Me.PrimaryButtonText <> value Then
                Me.PrimaryRadioButton.Text = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.PrimaryButtonText))
            End If
        End Set
    End Property

    ''' <summary> The primary button vertical location. </summary>
    Private _PrimaryButtonVerticalLocation As VerticalLocation

    ''' <summary> Gets or sets the primary button vertical location. </summary>
    ''' <value> The primary button vertical location. </value>
    <DefaultValue(GetType(VerticalLocation), "Center")>
    <Category("Behavior")>
    <Description("Vertical location of the primary button.")>
    Public Property PrimaryButtonVerticalLocation() As VerticalLocation
        Get
            Return Me._PrimaryButtonVerticalLocation
        End Get
        Set(ByVal value As VerticalLocation)
            If value <> Me.PrimaryButtonVerticalLocation Then
                Me._PrimaryButtonVerticalLocation = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.PrimaryButtonVerticalLocation))
                updateButtonsLocation()
            End If
        End Set
    End Property

    ''' <summary> The secondary button horizontal location. </summary>
    Private _SecondaryButtonHorizontalLocation As HorizontalLocation

    ''' <summary> Gets or sets the secondary button horizontal location. </summary>
    ''' <value> The secondary button horizontal location. </value>
    <DefaultValue(GetType(HorizontalLocation), "Right")>
    <Category("Behavior")>
    <Description("Horizontal location of the Secondary button.")>
    Public Property SecondaryButtonHorizontalLocation() As HorizontalLocation
        Get
            Return Me._SecondaryButtonHorizontalLocation
        End Get
        Set(ByVal value As HorizontalLocation)
            If value <> Me.SecondaryButtonHorizontalLocation Then
                Me._SecondaryButtonHorizontalLocation = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.SecondaryButtonHorizontalLocation))
                updateButtonsLocation()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the text associated with the secondary button. </summary>
    ''' <value> The secondary button text. </value>
    <Category("Appearance"), Description("The text associated with the Secondary button"),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue("ON")>
    Public Property SecondaryButtonText() As String
        Get
            Return Me.SecondaryRadioButton.Text
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then
                value = ""
            End If
            If Me.SecondaryButtonText <> value Then
                Me.SecondaryRadioButton.Text = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.SecondaryButtonText))
            End If
        End Set
    End Property

    ''' <summary> The secondary button vertical location. </summary>
    Private _SecondaryButtonVerticalLocation As VerticalLocation

    ''' <summary> Gets or sets the secondary button vertical location. </summary>
    ''' <value> The secondary button vertical location. </value>
    <DefaultValue(GetType(VerticalLocation), "Center")>
    <Category("Behavior")>
    <Description("Vertical location of the Secondary button.")>
    Public Property SecondaryButtonVerticalLocation() As VerticalLocation
        Get
            Return Me._SecondaryButtonVerticalLocation
        End Get
        Set(ByVal value As VerticalLocation)
            If value <> Me.SecondaryButtonVerticalLocation Then
                Me._SecondaryButtonVerticalLocation = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.SecondaryButtonVerticalLocation))
                updateButtonsLocation()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Updates the location of the buttons.
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    Private Sub updateButtonsLocation()

        Dim primaryColumn As Integer = 1
        Dim primaryRow As Integer = 1
        Dim secondaryColumn As Integer = 2
        Dim secondaryRow As Integer = 1

        Dim rowPercent As Single = 100.0!
        Dim colPercent As Single = 100.0!

        ' clear the styles
        For Each colStyle As ColumnStyle In Me._Layout.ColumnStyles
            colStyle.SizeType = SizeType.Percent
        Next
        For Each rowStyle As RowStyle In Me._Layout.RowStyles
            rowStyle.SizeType = SizeType.Percent
        Next

        Select Case PrimaryButtonVerticalLocation
            Case isr.Core.Controls.VerticalLocation.Bottom
                primaryRow = 2
            Case isr.Core.Controls.VerticalLocation.Center
                primaryRow = 1
            Case isr.Core.Controls.VerticalLocation.Top
                primaryRow = 0
        End Select
        Me._Layout.RowStyles(primaryRow).SizeType = SizeType.AutoSize

        Select Case PrimaryButtonHorizontalLocation
            Case HorizontalLocation.Center
                primaryColumn = 1
            Case HorizontalLocation.Left
                primaryColumn = 0
            Case HorizontalLocation.Right
                primaryColumn = 2
        End Select
        Me._Layout.ColumnStyles(primaryColumn).SizeType = SizeType.AutoSize

        Select Case SecondaryButtonVerticalLocation
            Case isr.Core.Controls.VerticalLocation.Bottom
                secondaryRow = 2
            Case isr.Core.Controls.VerticalLocation.Center
                secondaryRow = 1
            Case isr.Core.Controls.VerticalLocation.Top
                secondaryRow = 0
        End Select
        Me._Layout.RowStyles(secondaryRow).SizeType = SizeType.AutoSize
        If primaryRow = secondaryRow Then
            rowPercent = 50
        End If
        For Each rowStyle As RowStyle In Me._Layout.RowStyles
            If rowStyle.SizeType = SizeType.Percent Then
                rowStyle.Height = rowPercent
            End If
        Next

        Select Case SecondaryButtonHorizontalLocation
            Case HorizontalLocation.Center
                secondaryColumn = 1
            Case HorizontalLocation.Left
                secondaryColumn = 0
            Case HorizontalLocation.Right
                secondaryColumn = 2
        End Select
        If secondaryColumn = primaryColumn AndAlso secondaryRow = primaryRow Then
            Select Case PrimaryButtonHorizontalLocation
                Case HorizontalLocation.Center
                    secondaryColumn = 2
                Case HorizontalLocation.Left
                    secondaryColumn = 2
                Case HorizontalLocation.Right
                    secondaryColumn = 0
            End Select
        End If
        Me._Layout.ColumnStyles(secondaryColumn).SizeType = SizeType.AutoSize
        If primaryColumn = secondaryColumn Then
            colPercent = 50
        End If
        For Each ColumnStyle As ColumnStyle In Me._Layout.ColumnStyles
            If ColumnStyle.SizeType = SizeType.Percent Then
                ColumnStyle.Width = colPercent
            End If
        Next
        Me._Layout.Controls.Clear()
        Me._Layout.Controls.Add(Me.PrimaryRadioButton, primaryColumn, primaryRow)
        Me._Layout.Controls.Add(Me.SecondaryRadioButton, secondaryColumn, secondaryRow)

    End Sub

#End Region

#Region " BEHAVIOR "

    ''' <summary> Gets or sets the read only property. </summary>
    ''' <value> The read only. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether the dual radio button is read only.")>
    Public Property [ReadOnly]() As Boolean
        Get
            Return Me.PrimaryRadioButton.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            If Me.ReadOnly <> value Then
                Me.PrimaryRadioButton.ReadOnly = value
                Me.SecondaryRadioButton.ReadOnly = value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.ReadOnly))
            End If
        End Set
    End Property

    ''' <summary> <c>True</c> if checked. </summary>
    Private _Checked As Boolean

    ''' <summary> Gets or sets the checked property. </summary>
    ''' <value> The checked. </value>
    <DefaultValue(False)>
    <Category("Behavior")>
    <Description("Indicates whether the radio button is checked or not.")>
    Public Property Checked() As Boolean
        Get
            Return Me._Checked
        End Get
        Set(ByVal value As Boolean)
            If Me.Checked <> value Then
                Me._Checked = value
                PrimaryRadioButton.Checked = value
                SecondaryRadioButton.Checked = Not value
                Me.AsyncNotifyPropertyChanged(NameOf(Me.Checked))
                Me.OnCheckToggled()
            End If
        End Set
    End Property

    ''' <summary> Event handler. Called by PrimaryRadioButton for checked changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PrimaryRadioButton_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PrimaryRadioButton.CheckedChanged
        Me.Checked = PrimaryRadioButton.Checked
    End Sub

#End Region

#Region " EVENTS "

    ''' <summary>Occurs when the checked value changed.
    ''' </summary>
    ''' <param name="e">Specifies the
    ''' <see cref="EventArgs">event arguments</see>.
    ''' </param>
    Public Event CheckToggled As EventHandler(Of EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveCheckToggledEventHandler(ByVal value As EventHandler(Of EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.CheckToggled, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the Check changed event. </summary>
    Protected Overridable Sub OnCheckToggled()
        Dim evt As EventHandler(Of EventArgs) = Me.CheckToggledEvent
        evt?.Invoke(Me, EventArgs.Empty)
    End Sub

#End Region

End Class

''' <summary> Values that represent HorizontalLocation. </summary>
Public Enum HorizontalLocation
    ''' <summary>Button located on the left side of the control (default behavior)</summary>
    Left
    ''' <summary>Button located in the center of the control</summary>
    Center
    ''' <summary>Button located at the right side of the control</summary>
    Right
End Enum

''' <summary> Values that represent VerticalLocation. </summary>
Public Enum VerticalLocation
    ''' <summary>Button located on the top side of the control (default behavior)</summary>
    Top
    ''' <summary>Button located in the center of the control</summary>
    Center
    ''' <summary>Button located at the bottom of the control</summary>
    Bottom
End Enum

