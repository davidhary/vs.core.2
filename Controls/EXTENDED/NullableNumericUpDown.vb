﻿Imports System.ComponentModel
''' <summary> A numeric up down with Nullable value. </summary>
''' <license> (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="08/19/2011" by="David" revision="1.02.4248.x"> created. </history>
<DesignerCategory("code"), System.ComponentModel.Description("Nullable Numeric Up Down")>
Public Class NullableNumericUpDown
    Inherits NumericUpDownBase

    ''' <summary> Initializes a new instance of the <see cref="NullableNumericUpDown" /> class. </summary>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Gets or sets the Nullable value. </summary>
    ''' <value> The Nullable value. </value>
    <Category("Behavior"), Description("The Nullable value."), Browsable(True),
    DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property NullableValue As Decimal?
        Get
            If String.IsNullOrWhiteSpace(Me.TextBox.Text) Then
                Return New Decimal?
            Else
                Return Me.Value
            End If
        End Get
        Set(ByVal value As Decimal?)
            If value.HasValue Then
                Me.Value = value.Value
            Else
                Me.TextBox.Text = ""
            End If
        End Set
    End Property

    ''' <summary> Occurs when the underlying text changes. </summary>
    ''' <param name="source"> The source of the event. </param>
    ''' <param name="e">      An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnChanged(source As Object, ByVal e As System.EventArgs)
        ' this is required so that the Nullable value is updated when spinning after it was set to null.
        Me.OnValueChanged(e)
        MyBase.OnChanged(source, e)
    End Sub

End Class
