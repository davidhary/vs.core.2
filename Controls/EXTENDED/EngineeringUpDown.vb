﻿Imports System.ComponentModel
''' <summary> Engineering up down. </summary>
''' <remarks> Features: <para>
''' Disables up/down events when read only.</para><para>
''' Adds up/down cursor.</para><para>
''' Adds engineering scaling. </para> </remarks>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="4/5/2014" by="David"> Created. </history>
<System.ComponentModel.Description("Engineering Up Down")>
Public Class EngineeringUpDown
    Inherits NumericUpDownBase

#Region " CONSTRUCTORS "

    ''' <summary> object creator. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    Public Sub New()
        MyBase.New()
        Me._Unit = ""
        Me.updateScalingExponent(0)
    End Sub

#End Region

#Region " TEXT BOX "

    ''' <summary> Strips post fix. </summary>
    Private Function StripedText() As String
        If Me.TextBox IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(Me.Postfix) AndAlso
             Me.TextBox.Text.EndsWith(Me.Postfix, StringComparison.OrdinalIgnoreCase) Then
            Return Me.TextBox.Text.Substring(0, Me.TextBox.Text.IndexOf(Me.Postfix, StringComparison.OrdinalIgnoreCase))
        Else
            Return Me.Value.ToString
        End If
    End Function

    ''' <summary> Strips post fix. </summary>
    Private Sub stripPostFix()
        If Me.TextBox IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(Me.Postfix) AndAlso
            Me.TextBox.Text.EndsWith(Me.Postfix, StringComparison.OrdinalIgnoreCase) Then
            Me.TextBox.Text = Me.TextBox.Text.Substring(0, Me.TextBox.Text.IndexOf(Me.Postfix, StringComparison.OrdinalIgnoreCase))
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.LostFocus" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLostFocus(e As System.EventArgs)
        Me.stripPostFix()
        MyBase.OnLostFocus(e)
        Me.UpdateEditText()
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Validating" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnValidating(e As System.ComponentModel.CancelEventArgs)
        Me.stripPostFix()
        MyBase.OnValidating(e)
        Me.UpdateEditText()
    End Sub

    ''' <summary> Validates and updates the text displayed in the spin box (also known as an up-down
    ''' control). </summary>
    Protected Overrides Sub ValidateEditText()
        Me.stripPostFix()
        MyBase.ValidateEditText()
    End Sub

    ''' <summary> Displays the current value of the spin box (also known as an up-down control) in the
    ''' appropriate format. </summary>
    Protected Overrides Sub UpdateEditText()
        MyBase.UpdateEditText()
        If Me.TextBox IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(Me.Postfix) Then
            Me.TextBox.AppendText(Me.Postfix)
        End If
    End Sub

#End Region

#Region " ENGINEERING "

    ''' <summary> Builds the engineering scale to scale exponent hash. </summary>
    ''' <returns> A Dictionary for translating trace events to trace levels. </returns>
    Private Shared Function BuildEngineringUnitHash() As Dictionary(Of Integer, EngineeringScale)
        Dim dix2 As New Dictionary(Of Integer, EngineeringScale)
        Dim dix3 As Dictionary(Of Integer, EngineeringScale) = dix2
        dix3.Add(-18, EngineeringScale.Atto)
        dix3.Add(18, EngineeringScale.Exa)
        dix3.Add(15, EngineeringScale.Femto)
        dix3.Add(-9, EngineeringScale.Giga)
        dix3.Add(-3, EngineeringScale.Kilo)
        dix3.Add(-2, EngineeringScale.Deci)
        dix3.Add(-6, EngineeringScale.Mega)
        dix3.Add(6, EngineeringScale.Micro)
        dix3.Add(2, EngineeringScale.Percent)
        dix3.Add(3, EngineeringScale.Milli)
        dix3.Add(9, EngineeringScale.Nano)
        dix3.Add(-14, EngineeringScale.Peta)
        dix3.Add(12, EngineeringScale.Pico)
        dix3.Add(-12, EngineeringScale.Tera)
        dix3.Add(0, EngineeringScale.Unity)
        dix3 = Nothing
        Return dix2
    End Function

    ''' <summary> Builds the engineering scale to scale exponent hash. </summary>
    ''' <returns> A Dictionary for translating trace events to trace levels. </returns>
    Private Shared Function BuildScaleExponentHash() As Dictionary(Of EngineeringScale, Integer)
        Dim dix2 As New Dictionary(Of EngineeringScale, Integer)
        Dim dix3 As Dictionary(Of EngineeringScale, Integer) = dix2
        dix3.Add(EngineeringScale.Atto, -18)
        dix3.Add(EngineeringScale.Exa, 18)
        dix3.Add(EngineeringScale.Femto, 15)
        dix3.Add(EngineeringScale.Giga, -9)
        dix3.Add(EngineeringScale.Deci, -2)
        dix3.Add(EngineeringScale.Kilo, -3)
        dix3.Add(EngineeringScale.Mega, -6)
        dix3.Add(EngineeringScale.Micro, 6)
        dix3.Add(EngineeringScale.Milli, 3)
        dix3.Add(EngineeringScale.Nano, 9)
        dix3.Add(EngineeringScale.Percent, 2)
        dix3.Add(EngineeringScale.Peta, -15)
        dix3.Add(EngineeringScale.Pico, 12)
        dix3.Add(EngineeringScale.Tera, -12)
        dix3.Add(EngineeringScale.Unity, 0)
        dix3 = Nothing
        Return dix2
    End Function

    Private _EngineeringScale As EngineeringScale

    ''' <summary> Gets or sets the engineering scale. </summary>
    ''' <value> The engineering scale. </value>
    <DefaultValue(0)>
    <Category("Behavior")>
    <Description("The engineering scale.")>
    Public Property EngineeringScale As EngineeringScale
        Get
            Return _EngineeringScale
        End Get
        Set(value As EngineeringScale)
            Me.updateEngineeringScale(value)
        End Set
    End Property

    ''' <summary> Updates the engineering scale described by value. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub updateEngineeringScale(ByVal value As EngineeringScale)
        Static hash As Dictionary(Of EngineeringScale, Integer) = EngineeringUpDown.BuildScaleExponentHash()
        Me.updateScalingExponent(hash(value))
    End Sub

    ''' <summary> Initializes the scale properties. </summary>
    ''' <remarks> Percent units are treated as a special case. </remarks>
    Private Sub updateScalingExponent(ByVal value As Integer)
        Static hash As Dictionary(Of Integer, String) = EngineeringUpDown.BuildScaleValuePrefixHash()
        Static EngineeringScalesHash As Dictionary(Of Integer, EngineeringScale) = EngineeringUpDown.BuildEngineringUnitHash
        ' treat percent units as a special case
        If value = 2 OrElse value = -2 Then
            Me._ScalingExponent = 2
        Else
            ' must be in powers of 3.
            Me._ScalingExponent = 3 * (value \ 3)
        End If
        Me._ScaleFactor = CDec(Math.Pow(10, -Me._ScalingExponent))
        If value = 0 AndAlso String.IsNullOrWhiteSpace(Me.Unit) Then
            Me._Postfix = ""
        Else
            Me._Postfix = String.Format(" {0}{1}", hash(value), Me.Unit)
        End If
        Me._EngineeringScale = EngineeringScalesHash(Me.ScalingExponent)
        ' update the display to show the change in units, if any.
        Me.UpdateEditText()
        Me.OnValueChanged(System.EventArgs.Empty)
    End Sub

    ''' <summary> Builds a scale exponent to unit prefix hash. </summary>
    ''' <returns> A Dictionary for translating exponents to units. </returns>
    Private Shared Function BuildScaleValuePrefixHash() As Dictionary(Of Integer, String)
        Dim dix2 As New Dictionary(Of Integer, String)
        Dim dix3 As Dictionary(Of Integer, String) = dix2
        dix3.Add(-18, "E") ' Exa
        dix3.Add(-15, "P") ' Peta
        dix3.Add(-12, "T") ' Tera
        dix3.Add(-9, "G")  ' Giga
        dix3.Add(-6, "M")  ' Mega
        dix3.Add(-2, "D")  ' Deci
        dix3.Add(-3, "K")  ' Kilo
        dix3.Add(0, "")
        dix3.Add(2, "%")  ' Percent
        dix3.Add(3, "m")  ' Milli
        dix3.Add(6, "u")  ' Micro
        dix3.Add(9, "n")  ' Nano
        dix3.Add(12, "p") ' Pico
        dix3.Add(15, "f") ' Femto
        dix3.Add(18, "a") ' Atto
        dix3 = Nothing
        Return dix2
    End Function

    Private _Unit As String
    <DefaultValue("")>
    <Category("Appearance")>
    <Description("The engineering scale to display, e.g., V, A.")>
    Public Property Unit As String
        Get
            Return Me._Unit
        End Get
        Set(value As String)
            Me._Unit = value
            ' update the post fix.
            Me.updateScalingExponent(Me.ScalingExponent)
        End Set
    End Property

    ''' <summary> Gets or sets the unscaled value. </summary>
    ''' <value> The unscaled value. </value>
    Public Property UnscaledValue As Decimal
        Get
            Dim value As Decimal = 0
            If Not Decimal.TryParse(Me.StripedText, value) Then
                value = Me.Value
            End If
            Return value
        End Get
        Set(value As Decimal)
            Me.Value = value
        End Set
    End Property

    ''' <summary> Gets the scaled sentinel. </summary>
    ''' <value> <c>True</c> if scaled. </value>
    Private ReadOnly Property IsScaled As Boolean
        Get
            Return Me.ScaleFactor <> 1 AndAlso Me.ScaleFactor <> 0
        End Get
    End Property

    ''' <summary> Gets or sets the scaled value. </summary>
    ''' <value> The scaled value. </value>
    <DefaultValue(0)>
    <Category("Behavior")>
    <Description("The scaled value.")>
    Public Property ScaledValue As Decimal
        Get
            If Me.IsScaled Then
                Return Me.ScaleFactor * Me.UnscaledValue
            Else
                Return Me.UnscaledValue
            End If
        End Get
        Set(value As Decimal)
            If Me.IsScaled Then
                value = value / Me.ScaleFactor
            End If
            If Me.TextBox IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(Me.Postfix) Then
                Me.TextBox.Text = value.ToString
            End If
            Me.UnscaledValue = value
            Me.TextBox.AppendText(Me.Postfix)
        End Set
    End Property

    Private _ScalingExponent As Integer
    ''' <summary> Gets or sets the scaling exponent. </summary>
    ''' <value> The scaling exponent. </value>
    Protected Property ScalingExponent As Integer
        Get
            Return Me._ScalingExponent
        End Get
        Set(value As Integer)
            Me.updateScalingExponent(value)
        End Set
    End Property

    ''' <summary> Gets or sets the scale factor. </summary>
    ''' <value> The scale factor. </value>
    Protected Property ScaleFactor As Decimal

    ''' <summary> Gets or sets the text to append to the display value. </summary>
    ''' <value> The post fix. </value>
    Protected Property Postfix As String

#End Region

End Class

''' <summary> Values that represent engineering scale. </summary>
Public Enum EngineeringScale
    <Description("Unity (0)")> Unity = 0
    <Description("Atto (a:-18)")> Atto
    <Description("Femto (f:-15)")> Femto
    <Description("Pico (p:-12)")> Pico
    <Description("Nano (n:-9)")> Nano
    <Description("Micro (u:-6)")> Micro
    <Description("Milli (m:-3)")> Milli
    <Description("Deci (D:2)")> Deci
    <Description("Percent (D:-2)")> Percent
    <Description("Kilo (K:3)")> Kilo
    <Description("Mega (M:6)")> Mega
    <Description("Giga (G:9)")> Giga
    <Description("Tera (T:12)")> Tera
    <Description("Peta (P:15)")> Peta
    <Description("Exa (E:18)")> Exa
End Enum