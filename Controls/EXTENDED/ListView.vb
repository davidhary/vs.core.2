﻿Imports System.Windows.Forms
Imports System.ComponentModel
''' <summary>
''' This class provides a non-flickering ListView.  A heartfelt "thank you" goes to 
''' stormenet on StackOverflow.
''' </summary>
''' <license> (c) 2011 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="08/19/2011" by="David" revision="1.02.4248.x"> created. </history>
<DesignerCategory("code"), System.ComponentModel.Description("Non-Flickering List View")>
Public Class ListView
    Inherits System.Windows.Forms.ListView

    ''' <summary> Default constructor. </summary>
    Public Sub New()

        MyBase.New()

        ' Activate double buffering
        Me.SetStyle(ControlStyles.OptimizedDoubleBuffer Or ControlStyles.AllPaintingInWmPaint, True)

        ' Enable the OnNotifyMessage event so we get a chance to filter out 
        ' Windows messages before they get to the form's WndProc
        Me.SetStyle(ControlStyles.EnableNotifyMessage, True)
    End Sub

    ''' <summary> Notifies the control of Windows messages. </summary>
    ''' <param name="m"> A <see cref="T:System.Windows.Forms.Message" /> that represents the Windows
    ''' message. </param>
    ''' <remarks> Filters out the erase background message. </remarks>
    Protected Overrides Sub OnNotifyMessage(ByVal m As Message)
        ' Filter out the WM_ERASEBKGND message
        If m.Msg <> &H14 Then
            MyBase.OnNotifyMessage(m)
        End If
    End Sub

End Class
