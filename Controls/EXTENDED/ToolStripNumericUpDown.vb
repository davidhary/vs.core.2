﻿Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Windows.Forms.Design
Imports isr.Core.Pith.EventHandlerExtensions
''' <summary> Tool strip numeric up down. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="4/16/2014" by="David" revision=""> Created. </history>
<ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)>
Public Class ToolStripNumericUpDown
    Inherits ToolStripControlHost

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> Call the base constructor passing in a NumericUpDown instance. </remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub New()
        MyBase.New(New NumericUpDown())
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the
    ''' <see cref="T:System.Windows.Forms.ToolStripControlHost" /> and optionally releases the
    ''' managed resources.
    ''' </summary>
    ''' <remarks> David, 12/21/2015. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveValueChangedEventHandler(Me.ValueChangedEvent)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " CONTROL "

    ''' <summary> Gets the numeric up down control. </summary>
    ''' <value> The numeric up down control. </value>
    Public ReadOnly Property NumericUpDownControl() As NumericUpDown
        Get
            Return TryCast(Control, NumericUpDown)
        End Get
    End Property

#End Region

#Region " CONTROL PROPERTIES "

    ''' <summary> Gets or sets the selected text. </summary>
    ''' <value> The selected text. </value>
    <DefaultValue("")>
    <Description("text"), Category("Appearance")>
    Public Overrides Property Text() As String
        Get
            Return Me.NumericUpDownControl.Text
        End Get
        Set(ByVal value As String)
            Me.NumericUpDownControl.Text = value
        End Set
    End Property

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    <DefaultValue("")>
    <Description("Value"), Category("Appearance")>
    Public Property Value() As Decimal
        Get
            Return Me.NumericUpDownControl.Value
        End Get
        Set(ByVal value As Decimal)
            Me.NumericUpDownControl.Value = value
        End Set
    End Property

    ''' <summary> Gets a value indicating whether this object has value. </summary>
    ''' <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property HasValue As Boolean
        Get
            Return Me.NumericUpDownControl.HasValue
        End Get
    End Property

#End Region

#Region " EVENTS "

    ''' <summary> Event queue for all listeners interested in ValueChanged events. </summary>
    Public Event ValueChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveValueChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.ValueChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the value changed event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Private Sub OnValueChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.ValueChangedEvent
        evt?.Invoke(Me, e)
    End Sub

#End Region

#Region " TOOL STRIP MANAGEMENT "

    ''' <summary> Subscribes events from the hosted control. </summary>
    ''' <remarks> Subscribe the control events to expose. </remarks>
    ''' <param name="control"> The control from which to subscribe events. </param>
    Protected Overrides Sub OnSubscribeControlEvents(ByVal control As Control)

        If control IsNot Nothing Then

            ' Call the base so the base events are connected.
            MyBase.OnSubscribeControlEvents(control)

            ' Cast the control to a NumericUpDown control.
            Dim numericControl As NumericUpDown = TryCast(control, NumericUpDown)

            If numericControl IsNot Nothing Then
                ' Add the event.
                AddHandler numericControl.ValueChanged, AddressOf OnValueChanged
            End If

        End If
    End Sub

    ''' <summary> Unsubscribes events from the hosted control. </summary>
    ''' <param name="control"> The control from which to unsubscribe events. </param>
    Protected Overrides Sub OnUnsubscribeControlEvents(ByVal control As Control)

        ' Call the base method so the basic events are unsubscribed.
        MyBase.OnUnsubscribeControlEvents(control)

        ' Cast the control to a NumericUpDown control.
        Dim numericControl As NumericUpDown = TryCast(control, NumericUpDown)

        If numericControl IsNot Nothing Then
            ' Remove the event.
            RemoveHandler numericControl.ValueChanged, AddressOf OnValueChanged
        End If

    End Sub

#End Region

#Region " CONTROL "

#End Region
End Class

