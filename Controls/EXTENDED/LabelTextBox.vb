﻿Imports System.ComponentModel
''' <summary> Text box with read only non-validation. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="9/27/2013" by="David" revision=""> Created. </history>
<DesignerCategory("code"), System.ComponentModel.Description("Label Text Box")>
Public Class LabelTextBox
    Inherits System.Windows.Forms.TextBox

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        MyBase.ReadOnly = True
        Me.CausesValidation = False
    End Sub

    ''' <summary> Gets or sets a value indicating whether text in the text box is read-only. </summary>
    ''' <value> <c>True</c> if [read only]; otherwise, <c>False</c>. </value>
    Public Shadows Property [ReadOnly] As Boolean
        Get
            Return MyBase.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            MyBase.ReadOnly = value
            Me.CausesValidation = Not MyBase.ReadOnly
        End Set
    End Property


End Class
