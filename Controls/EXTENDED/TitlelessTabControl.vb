﻿Imports System.ComponentModel
Imports System.Security.Permissions
''' <summary> A tab control with hidden tab page titles in run time. </summary>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="06/28/2012" by="David" revision="1.02.4562.x"> From Hans PASSANT
''' (http://StackOverflow.com/users/17034/hans-PASSANT)
''' http://StackOverflow.com/questions/1824036/TabControl-how-can-you-remove-the-TabPage-title. </history>
<DesignerCategory("code"), System.ComponentModel.Description("Title-Less Tab Control")>
Public Class TitleLessTabControl
    Inherits System.Windows.Forms.TabControl

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        previousPageCount = 0
    End Sub

#Region " TAB PAGE TITLE HIDING ENGINE "

    ''' <summary> Number of previous pages. </summary>
    Private previousPageCount As Integer

    ''' <summary> Checks if a single tab page. </summary>
    Private Sub checkSingleTabPage()
        If Me.IsHandleCreated Then
            Dim pages As Integer = previousPageCount
            previousPageCount = Me.TabCount
            If (pages = 1 AndAlso previousPageCount > 1) OrElse (pages > 1 AndAlso previousPageCount = 1) Then
                Me.RecreateHandle()
            End If
        End If
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.ControlAdded" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.ControlEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnControlAdded(e As System.Windows.Forms.ControlEventArgs)
        MyBase.OnControlAdded(e)
        checkSingleTabPage()
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.ControlRemoved" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.ControlEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnControlRemoved(e As System.Windows.Forms.ControlEventArgs)
        MyBase.OnControlRemoved(e)
        checkSingleTabPage()
    End Sub

    ''' <summary> Windows Procedure override to hide tab headers. </summary>
    ''' <param name="m"> [in,out] The Windows <see cref="T:System.Windows.Forms.Message" /> to
    ''' process. </param>
    <SecurityPermission(SecurityAction.Demand, Flags:=SecurityPermissionFlag.UnmanagedCode)>
    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        ' Hide tabs by trapping the message
        Const TCM_ADJUSTRECT As Integer = &H1328
        If m.Msg = TCM_ADJUSTRECT AndAlso Not Me.DesignMode AndAlso
            (Me.TabCount = 1 OrElse Not Me.HideSingleTabLabelOnly) Then
            m.Result = New IntPtr(1) ' CType(1, IntPtr)
        Else
            MyBase.WndProc(m)
        End If
    End Sub

#End Region

#Region " VISIBLE PROPERTIES "

    ''' <summary> true to hide, false to show the single tab page title only. </summary>
    Private _hideSingleTabPageTitleOnly As Boolean

    ''' <summary> Gets or sets the hide single tab label only. </summary>
    ''' <value> The hide single tab label only. </value>
    <Category("Behavior"), Description("Toggle hiding all or only one tab page title."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(False)>
    Public Property HideSingleTabLabelOnly As Boolean
        Get
            Return Me._hideSingleTabPageTitleOnly
        End Get
        Set(ByVal value As Boolean)
            Me._hideSingleTabPageTitleOnly = value
            Me.checkSingleTabPage()
        End Set
    End Property

#End Region

End Class
