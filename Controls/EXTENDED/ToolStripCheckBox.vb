﻿Imports System.Windows.Forms
Imports System.Windows.Forms.Design
Imports isr.Core.Pith.EventHandlerExtensions
''' <summary> Tool strip check box. </summary>
''' <license> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="01/23/2015" by="David" revision=""> Created. </history>
<ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)>
Public Class ToolStripCheckBox
    Inherits ToolStripControlHost

    ''' <summary> Default constructor. </summary>
    ''' <remarks> Call the base constructor passing in a CheckBox instance. </remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub New()
        MyBase.New(New CheckBox())
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the
    ''' <see cref="T:System.Windows.Forms.ToolStripControlHost" /> and optionally releases the
    ''' managed resources.
    ''' </summary>
    ''' <remarks> David, 12/21/2015. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveCheckedChangedEventHandler(Me.CheckedChangedEvent)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try

    End Sub

    ''' <summary> Gets the numeric up down control. </summary>
    ''' <value> The numeric up down control. </value>
    Public ReadOnly Property CheckBoxControl() As CheckBox
        Get
            Return TryCast(Control, CheckBox)
        End Get
    End Property

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property Checked() As Boolean
        Get
            Return Me.CheckBoxControl.Checked
        End Get
        Set(ByVal value As Boolean)
            Me.CheckBoxControl.Checked = value
        End Set
    End Property

    ''' <summary> Subscribes events from the hosted control. </summary>
    ''' <remarks> Subscribe the control events to expose. </remarks>
    ''' <param name="control"> The control from which to subscribe events. </param>
    Protected Overrides Sub OnSubscribeControlEvents(ByVal control As Control)

        If control IsNot Nothing Then

            ' Call the base so the base events are connected.
            MyBase.OnSubscribeControlEvents(control)

            ' Cast the control to a CheckBox control.
            Dim containedControl As CheckBox = TryCast(control, CheckBox)

            If containedControl IsNot Nothing Then
                ' Add the event.
                AddHandler containedControl.CheckedChanged, AddressOf OnCheckedChanged
            End If

        End If
    End Sub

    ''' <summary> Unsubscribe events from the hosted control. </summary>
    ''' <param name="control"> The control from which to unsubscribe events. </param>
    Protected Overrides Sub OnUnsubscribeControlEvents(ByVal control As Control)

        ' Call the base method so the basic events are unsubscribed.
        MyBase.OnUnsubscribeControlEvents(control)

        ' Cast the control to a CheckBox control.
        Dim containedControl As CheckBox = TryCast(control, CheckBox)

        If containedControl IsNot Nothing Then
            ' Remove the event.
            RemoveHandler containedControl.CheckedChanged, AddressOf OnCheckedChanged
        End If

    End Sub

    ''' <summary> Event queue for all listeners interested in ValueChanged events. </summary>
    Public Event CheckedChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <remarks> David, 12/17/2015. </remarks>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveCheckedChangedEventHandler(ByVal value As EventHandler(Of EventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.CheckedChanged, CType(d, EventHandler(Of EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the checked changed event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Private Sub OnCheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.CheckedChangedEvent
        evt?.Invoke(Me, e)
    End Sub

End Class

