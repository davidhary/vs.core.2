﻿Imports System.ComponentModel
''' <summary> Drop down text box. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="12/6/2014" by="David" revision=""> Created. </history>
Public Class DropDownTextBox
    Inherits Pith.MyUserControlBase

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.InitialHeight = Me.Height

        If Me._DropDownHeight <= Me.InitialHeight Then
            Me._DropDownHeight = 300
        End If
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Controls.DropDownTextBox and optionally
    ''' releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 12/19/2015. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                ' unable to use null conditional because it is not seen by code analysis
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    ''' <summary> Gets the text box. </summary>
    ''' <value> The text box. </value>
    Public ReadOnly Property TextBox As isr.Core.Controls.TextBox
        Get
            Return Me._TextBox
        End Get
    End Property

    ''' <summary> Toggle multi line. </summary>
    ''' <param name="turnOn"> true to enable, false to disable the turn. </param>
    Private Sub ToggleMultiLine(ByVal turnOn As Boolean)
        If Me._TextBox IsNot Nothing Then
            If turnOn AndAlso Not Me._TextBox.Multiline Then
                If Me.DropDownHeight > Me.InitialHeight Then
                    Me._TextBox.Multiline = True
                    Me.Height = Me.DropDownHeight
                End If
            ElseIf Not turnOn AndAlso Me._TextBox.Multiline Then
                Me._TextBox.Multiline = False
                Me.Height = Me.InitialHeight
            End If
        End If
    End Sub

    ''' <summary> Drop down toggle checked changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _dropDownToggle_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _dropDownToggle.CheckedChanged
        Dim checkBox As System.Windows.Forms.CheckBox = TryCast(sender, System.Windows.Forms.CheckBox)
        If checkBox IsNot Nothing Then
            Me.ToggleMultiLine(checkBox.Checked)
            If Me._TextBox IsNot Nothing Then
                If Me._TextBox.Multiline Then
                    checkBox.Image = My.Resources.go_up_8
                Else
                    checkBox.Image = My.Resources.go_down_8
                End If
            End If
        End If
    End Sub

    ''' <summary> Drop Down Height. </summary>
    ''' <value> The height of the drop down. </value>
    <DefaultValue(300)>
    <Description("Drop Down Height"), Category("Appearance")>
    Public Property DropDownHeight As Integer

    ''' <summary> Gets or sets the height of the initial. </summary>
    ''' <value> The height of the initial. </value>
    Private Property InitialHeight As Integer

End Class
