﻿Imports System.Windows.Forms
'''<summary>
'''This is a test class for MyMessageBoxTest and is intended
'''to contain all MyMessageBoxTest Unit Tests
'''</summary>
<TestClass()>
Public Class MyMessageBoxTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    Private Shared ReadOnly TraceEventId As Integer = 111 ' Diagnosis.ProjectTraceEventId.MyExceptionMessageBoxTest
    '''<summary>
    '''A test for DisplayException
    '''</summary>
    <TestMethod()>
    Public Sub DisplayExceptionTest()
        Try
            Throw New DivideByZeroException()
        Catch ex As Exception
            Dim expected As DialogResult = DialogResult.OK
            Dim actual As DialogResult
            ex.Data.Add("@isr", "Exception test.")
            Dim box As New MyMessageBox(ex)
            actual = box.ShowDialog(Nothing)
            Assert.AreEqual(expected, actual)
        End Try
    End Sub

    ''' <summary> Tests the process exception on a another thread. </summary>
    <TestMethod()>
    Public Sub TestProcessExceptionThread()
        Dim oThread As New Threading.Thread(New Threading.ThreadStart(AddressOf TestShowDialogabortIgnore))
        oThread.Start()
        oThread.Join()
    End Sub

    ''' <summary> Tests showing dialog with abort and ignore buttons. </summary>
    ''' ### <exception cref="DivideByZeroException"> Thrown when an attempt is made to divide a number
    ''' by zero. </exception>
    <TestMethod()>
    Public Sub TestShowDialogabortIgnore()
        Try
            Throw New DivideByZeroException()
        Catch ex As DivideByZeroException

            ex.Data.Add("@isr", "Exception test.")
            Dim box As New MyMessageBox(ex)
            Dim result As DialogResult = MyMessageBox.ShowDialogAbortIgnore(Nothing, ex, MessageBoxIcon.Error)
            Windows.Forms.MessageBox.Show(result.ToString & " Requested")
        End Try
    End Sub

End Class
