﻿Imports isr.Core.Controls
'''<summary>
'''This is a test class for MachineLogOnTest and is intended
'''to contain all MachineLogOnTest Unit Tests
'''</summary>
<TestClass()> _
Public Class MachineLogOnTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '''<summary>
    '''A test for Validate
    '''</summary>
    <TestMethod()> _
    Public Sub ValidateRole()
        Dim target As MachineLogOn = New MachineLogOn()
        Dim userName As String = "admin"
        Dim password As String = "a"
        Dim roles As New List(Of String)
        roles.Add("Administrators")
        Dim allowedUserRoles As New ArrayList(roles)
        Dim expected As Boolean = True
        Dim actual As Boolean
        actual = target.Authenticate(userName, password, allowedUserRoles)
        Assert.AreEqual(expected, actual, target.ValidationMessage)
    End Sub

    '''<summary>
    '''A test for Validate
    '''</summary>
    <TestMethod()> _
    Public Sub ValidateUser()
        Dim target As MachineLogOn = New MachineLogOn()
        Dim userName As String = "david"
        Dim password As String = "a"
        Dim expected As Boolean = True
        Dim actual As Boolean
        actual = target.Authenticate(userName, password)
        Assert.AreEqual(expected, actual, target.ValidationMessage)
    End Sub
End Class
