﻿Imports isr.Core.InputOutput.KnownFolderPaths
'''<summary>
'''This is a test class for KnownFoldersTest and is intended
'''to contain all KnownFoldersTest Unit Tests
'''</summary>
<TestClass()> _
Public Class KnownFoldersTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    ''' <summary>A test for GetPath</summary> 
    Public Sub GetPathTest(ByVal knownFolder As KnownFolder, ByVal expected As String)
        Dim actual As String
        actual = KnownFolders.GetPath(knownFolder)
        Assert.AreEqual(expected, actual, True)
    End Sub

    ''' <summary> A test for GetPath. </summary>
    <TestMethod()> _
    Public Sub GetPathTest()
        Me.GetPathTest(KnownFolderPaths.KnownFolder.Documents, String.Format("C:\Users\{0}\Documents", Environment.UserName))
        Me.GetPathTest(KnownFolderPaths.KnownFolder.UserProfiles, "C:\Users")
        Me.GetPathTest(KnownFolderPaths.KnownFolder.ProgramData, "C:\ProgramData")
    End Sub

    ''' <summary> A test for GetDefaultPath. </summary>
    ''' <param name="knownFolder"> Pathname of the known folder. </param>
    ''' <param name="expected">    The expected. </param>
    Public Sub GetDefaultPathTest(ByVal knownFolder As KnownFolder, ByVal expected As String)
        Dim actual As String
        actual = KnownFolders.GetDefaultPath(knownFolder)
        Assert.AreEqual(expected, actual, True)
    End Sub

    ''' <summary> A test for GetDefaultPath. </summary>
    <TestMethod()> _
    Public Sub GetDefaultPathTest()
        Me.GetDefaultPathTest(KnownFolderPaths.KnownFolder.Documents, String.Format("C:\Users\{0}\Documents", Environment.UserName))
        Me.GetDefaultPathTest(KnownFolderPaths.KnownFolder.UserProfiles, "C:\Users")
        Me.GetDefaultPathTest(KnownFolderPaths.KnownFolder.ProgramData, "C:\ProgramData")
    End Sub
End Class
