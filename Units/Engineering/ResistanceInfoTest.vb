﻿Imports isr.Core.Engineering
'''<summary>
'''This is a test class for ResistanceInfoTest and is intended
'''to contain all ResistanceInfoTest Unit Tests
'''</summary>
<TestClass()> _
Public Class ResistanceInfoTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '''<summary>
    '''A test for TryParse
    '''</summary>
    Public Sub TryParse(ByVal resistanceCode As String, ByVal resistanceExpected As Double, ByVal expected As Boolean)
        Dim resistance As Double = 0.0!
        Dim details As String = ""
        Dim actual As Boolean
        actual = ResistanceInfo.TryParse(resistanceCode, resistance, details)
        Assert.AreEqual(resistanceExpected, resistance, details)
        Assert.AreEqual(expected, actual, details)
    End Sub

    '''<summary>
    '''A test for TryParse
    '''</summary>
    <TestMethod()> _
    Public Sub TryParseTest()
        TryParse("121", 120, True)
        TryParse("230", 23, True)
        TryParse("1000", 100, True)
        TryParse("2R67", 2.67, True)
        TryParse("2.67", 2.67, True)
        TryParse("12K6675", 12667.5, True)
        TryParse("100R5", 100.5, True)
        TryParse("R0015", 0.0015, True)
        TryParse("100R", 100, True)
    End Sub

End Class
