﻿Imports isr.Core.Engineering

''' <summary> A moving window test. </summary>
''' <remarks> David, 1/27/2016. </remarks>
''' <license>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
''' </license>
''' <history date="1/27/2016" by="David" revision="1.0.0.0"> Created. </history>
<TestClass()>
Public Class MovingWindowTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '''<summary>
    '''A test for Moving Window
    '''</summary>
    ''' <remarks>Passed 01/27/2016</remarks>
    <TestMethod()>
    Public Sub MovingWindowTest()
        Dim mw As New MovingWindow
        mw.Length = 5
        mw.Window = 0.1
        mw.UpdateRule = MovingWindowUpdateRule.StopOnWithinWindow
        mw.AddValue(0.9)
        mw.AddValue(1.1)
        mw.AddValue(0.95)
        mw.AddValue(1.05)
        mw.AddValue(1.0)
        Dim actualCount As Integer = mw.Count
        Dim expectedCount As Integer = 5
        Assert.AreEqual(expectedCount, actualCount)
        mw.AddValue(1.01)
        Dim actualOutcome As MovingWindowStatus = mw.Status
        Dim expectedOutcome As MovingWindowStatus = MovingWindowStatus.WithinWindow
        Assert.AreEqual(expectedOutcome, actualOutcome)
        Dim actualMean As Double = mw.Mean
        Dim expectedMean As Double = 1
        Assert.AreEqual(expectedMean, actualMean)
        Dim actualMax As Double = mw.Maximum
        Dim expectedMax As Double = 1.1
        Assert.AreEqual(expectedMax, actualMax)
        Dim actualMin As Double = mw.Minimum
        Dim expectedMin As Double = 0.9
        Assert.AreEqual(expectedMin, actualMin)

        mw.AddValue(1.2)
        actualOutcome = mw.Status
        expectedOutcome = MovingWindowStatus.AboveWindow
        Assert.AreEqual(expectedOutcome, actualOutcome)
        actualMean = mw.Mean
        expectedMean = 1.06
        Assert.AreEqual(expectedMean, actualMean)
        actualMax = mw.Maximum
        expectedMax = 1.2
        Assert.AreEqual(expectedMax, actualMax)
        actualMin = mw.Minimum
        expectedMin = 0.95
        Assert.AreEqual(expectedMin, actualMin)

    End Sub

End Class
