﻿'''<summary>
'''This is a test class for SplashScreenTest and is intended
'''to contain all SplashScreenTest Unit Tests
'''</summary>
<TestClass()> _
Public Class SplashScreenTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    Private Sub wait(ByVal value As TimeSpan)
        Dim endTime As DateTime = DateTime.Now.Add(value)
        Do While endTime < DateTime.Now
            Windows.Forms.Application.DoEvents()
        Loop
    End Sub

    '''<summary>
    '''A test for DisplaySplashMessage
    '''</summary>
    <TestMethod()> _
    Public Sub DisplaySplashMessageTest()
        Using target As isr.Core.WindowsForms.SplashScreen = New isr.Core.WindowsForms.SplashScreen()
            target.Show()
            target.TopmostSetter(True)
            For i As Integer = 1 To 10
                Threading.Thread.Sleep(500)
                Dim value As String = "New splash message @" & Date.Now.ToLongTimeString
                target.DisplayMessage(value)
            Next
        End Using
    End Sub

    ''' <summary> Tests the process exception on a another thread. </summary>
    <TestMethod()> _
    Public Sub DisplaySplashMessageThreadTest()
        Dim oThread As New Threading.Thread(New Threading.ThreadStart(AddressOf DisplaySplashMessageTest))
        oThread.Start()
        oThread.Join()
    End Sub


End Class
