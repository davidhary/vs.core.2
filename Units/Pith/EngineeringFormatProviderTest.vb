﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.Pith
'''<summary>
'''This is a test class for EngineeringFormatProviderTest and is intended
'''to contain all EngineeringFormatProviderTest Unit Tests
'''</summary>
<TestClass()> _
Public Class EngineeringFormatProviderTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    ''' <summary> A test for Formatting. </summary>
    ''' <param name="value">                   The value. </param>
    ''' <param name="format">                  Describes the format to use. </param>
    ''' <param name="usingThousandsSeparator"> true to using thousands separator. </param>
    ''' <param name="expectedValue">           The expected value. </param>
    Public Shared Sub CustomFormatPiTest(ByVal value As Double, ByVal format As String, ByVal usingThousandsSeparator As Boolean, ByVal expectedValue As String)
        Dim target As EngineeringFormatProvider = New EngineeringFormatProvider
        target.UsingThousandsSeparator = usingThousandsSeparator
        Dim actual As String = String.Format(target, format, value)
        Assert.AreEqual(expectedValue, actual)
    End Sub

    ''' <summary> A test for Formatting. </summary>
    ''' <param name="value">         The value. </param>
    ''' <param name="format">        Describes the format to use. </param>
    ''' <param name="expectedValue"> The expected value. </param>
    Public Shared Sub CustomFormatPiTest(ByVal value As Double, ByVal format As String, ByVal expectedValue As String)
        Dim target As IFormatProvider = New EngineeringFormatProvider
        Dim actual As String = String.Format(target, format, value)
        Assert.AreEqual(expectedValue, actual)
    End Sub

    '''<summary>
    '''A test for Formatting PI
    '''</summary>
    <TestMethod()> _
    Public Sub CustomFormatPiTest()
        EngineeringFormatProviderTest.CustomFormatPiTest(Math.PI, "{0}", "3.142")
    End Sub

    '''<summary>
    '''A test for Formatting 0.1 * PI
    '''</summary>
    <TestMethod()> _
    Public Sub CustomFormatDeciPiTest()
        EngineeringFormatProviderTest.CustomFormatPiTest(0.1 * Math.PI, "{0}", "0.3142")
    End Sub

    '''<summary>
    '''A test for Formatting 0.01 * PI
    '''</summary>
    <TestMethod()> _
    Public Sub CustomFormatCentiPiTest()
        EngineeringFormatProviderTest.CustomFormatPiTest(0.01 * Math.PI, "{0}", "31.42e-03")
    End Sub


    '''<summary>
    '''A test for Formatting 0.001 * PI
    '''</summary>
    <TestMethod()> _
    Public Sub CustomFormatMilliPiTest()
        EngineeringFormatProviderTest.CustomFormatPiTest(0.001 * Math.PI, "{0}", "3.142e-03")
    End Sub

    '''<summary>
    '''A test for Formatting 0.000001 * PI
    '''</summary>
    <TestMethod()> _
    Public Sub CustomFormatMicroPiTest()
        EngineeringFormatProviderTest.CustomFormatPiTest(0.000001 * Math.PI, "{0}", "3.142e-06")
    End Sub

    '''<summary>
    '''A test for Formatting 1000 * PI
    '''</summary>
    <TestMethod()> _
    Public Sub CustomFormatKiloPiTest()
        EngineeringFormatProviderTest.CustomFormatPiTest(1000 * Math.PI, "{0}", "3.142e+03")
    End Sub

    '''<summary>
    '''A test for Formatting 1000000 * PI
    '''</summary>
    <TestMethod()> _
    Public Sub CustomFormatMegaPiTest()
        EngineeringFormatProviderTest.CustomFormatPiTest(1000000 * Math.PI, "{0}", "3.142e+06")
    End Sub

    '''<summary>
    '''A test for Formatting 1000 * PI
    '''</summary>
    <TestMethod()> _
    Public Sub CustomFormatKiloWithSeparatorPiTest()
        EngineeringFormatProviderTest.CustomFormatPiTest(1000 * Math.PI, "{0}", True, "3,142")
    End Sub

    '''<summary>
    '''A test for Formatting 10000 * PI
    '''</summary>
    <TestMethod()> _
    Public Sub CustomFormatDekaWithSeparatorPiTest()
        EngineeringFormatProviderTest.CustomFormatPiTest(10000 * Math.PI, "{0}", True, "31,416")
    End Sub

    '''<summary>
    '''A test for Formatting 100000 * PI
    '''</summary>
    <TestMethod()> _
    Public Sub CustomFormatHectoWithSeparatorPiTest()
        EngineeringFormatProviderTest.CustomFormatPiTest(100000 * Math.PI, "{0}", True, "314,159")
    End Sub

    '''<summary>
    '''A test for Formatting 1000000 * PI
    '''</summary>
    <TestMethod()> _
    Public Sub CustomFormatMegaWithSeparatorPiTest()
        EngineeringFormatProviderTest.CustomFormatPiTest(1000000 * Math.PI, "{0}", True, "3.142e+06")
    End Sub

End Class
