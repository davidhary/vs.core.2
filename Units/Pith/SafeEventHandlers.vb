﻿Imports System.ComponentModel
Imports isr.Core.Pith.EventHandlerExtensions
Friend Module SafeEventHandlers

    ''' <summary> Safely triggers an event. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    ''' <param name="e">       The arguments for the event. </param>
    ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
    Public Sub SafeBeginEndInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs),
                                                              ByVal sender As Object, ByVal e As TEventArgs)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, e})
            Else
                Dim target As System.ComponentModel.ISynchronizeInvoke = TryCast(d.Target, System.ComponentModel.ISynchronizeInvoke)
                If target.InvokeRequired Then
                    Dim result As IAsyncResult = target.BeginInvoke(handler, New Object() {sender, e})
                    If result IsNot Nothing Then
                        target.EndInvoke(result)
                    End If
                Else
                    d.DynamicInvoke(New Object() {sender, e})
                End If
            End If
        Next
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChangedEventHandler">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Public Sub SafeInvoke(ByVal handler As PropertyChangedEventHandler, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, e})
                Else
                    target.Invoke(d, New Object() {sender, e})
                End If
            End If
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChangedEventHandler">Event</see> in a property value. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    ''' <param name="e">       The <see cref="PropertyChangedEventArgs" /> instance containing the
    ''' event data. </param>
    Public Sub SafeBeginInvoke(ByVal handler As PropertyChangedEventHandler, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, e})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, e})
                Else
                    target.BeginInvoke(d, New Object() {sender, e})
                End If
            End If
        Next
    End Sub

End Module
