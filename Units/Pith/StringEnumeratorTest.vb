﻿Imports isr.Core.Pith
'''<summary>
'''This is a test class for StringEnumeratorTest and is intended
'''to contain all StringEnumeratorTest Unit Tests
'''</summary>
<TestClass()> _
Public Class StringEnumeratorTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for TryParse
    '''</summary>
    Public Sub TryParseTestHelper(Of T As Structure)(ByVal value As String, ByVal enumValueExpected As T)
        Dim target As StringEnumerator(Of T) = New StringEnumerator(Of T)()
        Dim expected As Boolean = True
        Dim actual As Boolean
        Dim enumValue As T = Nothing
        actual = target.TryParse(value, enumValue)
        Assert.AreEqual(enumValueExpected, CType(enumValue, T))
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()> _
    Public Sub TryParseTest()

        TryParseTestHelper(Of TestValue)("2", TestValue.Error)
    End Sub

    <TestMethod()> _
    Public Sub TryParseTest1()
        Dim colorStrings() As String = {"0", "2", "8", "blue", "Blue", "Yellow", "Red, Green"}
        Dim colorValues() As Colors = {Colors.None, Colors.Green, Colors.Red Or Colors.Green Or Colors.Blue,
                                       Colors.Blue, Colors.Blue, Colors.None, Colors.Red Or Colors.Green}
        For Each colorString As String In colorStrings
            Dim colorValue As Colors
            [Enum].TryParse(colorString, colorValue)
            Dim expected As Boolean = True
            Dim actual As Boolean
            actual = [Enum].TryParse(colorString, colorValue)
            If actual Then
                If [Enum].IsDefined(GetType(Colors), colorValue) Or colorValue.ToString().Contains(",") Then
                    Trace.Write(String.Format("Converted '{0}' to {1}.", colorString, colorValue.ToString()))
                Else
                    Trace.Write(String.Format("{0} is not an underlying value of the Colors enumeration.", colorString))
                End If
            Else
                Trace.Write(String.Format("{0} is not a member of the Colors enumeration.", colorString))
            End If
        Next
    End Sub

    Private Enum TestValue
        None
        Info
        [Error]
    End Enum

    <Flags()> Enum Colors As Integer
        None = 0
        Red = 1
        Green = 2
        Blue = 4
    End Enum


End Class
