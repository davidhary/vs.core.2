﻿Imports System.IO
Imports isr.Core.Pith.FileInfoExtensions
Imports isr.Core.Pith.StackTraceExtensions
'''<summary>
'''This is a test class for ExtensionsTest and is intended
'''to contain all ExtensionsTest Unit Tests
'''</summary>
<TestClass()> _
Public Class ExtensionsTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for Location
    '''</summary>
    <TestMethod()> _
    Public Sub LocationTest()
        Dim expected As String = "C:\My\LIBRARIES\VS\Core\Core\Units\Core.Library.Units\bin\debug"
        Dim actual As String = System.Reflection.Assembly.GetExecutingAssembly.Location
        actual = Windows.Forms.Application.ExecutablePath
        actual = My.Application.Info.DirectoryPath
        actual = My.Application.Info.AssemblyName
        Assert.AreEqual(expected, actual)

    End Sub

    '''<summary>
    '''A test for ParseCallStack for user.
    '''</summary>
    <TestMethod()> _
    Public Sub ParseUserCallStackFrameTest()
        Dim trace As StackTrace = New StackTrace(True)
        Dim frame As StackFrame = New StackFrame(True)
        Dim callStackType As CallStackType = callStackType.UserCallStack
        Dim expected As String = "   at isr.Core.Primitives.Units.ExtensionsTest.ParseUserCallStackFrameTest() in C:\My\LIBRARIES\VS\Core\Core\Units\Core.Primitives.Units\ExtensionsTest.vb:line 78>. 	"
        Dim actual As String
        actual = trace.ParseCallStack(frame, callStackType)
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for ParseCallStack for user.
    '''</summary>
    <TestMethod()> _
    Public Sub ParseFullCallStackFrameTest()
        Dim trace As StackTrace = New StackTrace(True)
        Dim frame As StackFrame = New StackFrame(True)
        Dim callStackType As CallStackType = callStackType.FullCallStack
        Dim expected As String = ""
        Dim actual As String
        actual = trace.ParseCallStack(frame, callStackType)
        Assert.AreNotEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for ParseCallStack
    '''</summary>
    <TestMethod()> _
    Public Sub ParseUserCallStackTest()
        Dim trace As StackTrace = New StackTrace(True)
        Dim skipLines As Integer = 0
        Dim totalLines As Integer = 0
        Dim callStackType As CallStackType = callStackType.UserCallStack
        Dim expected As String = "   at isr.Core.Primitives.Units.ExtensionsTest.ParseUserCallStackFrameTest() in C:\My\LIBRARIES\VS\Core\Core\Units\Core.Primitives.Units\ExtensionsTest.vb:line 78>. 	"
        Dim actual As String
        actual = trace.ParseCallStack(skipLines, totalLines, callStackType)
        Assert.AreEqual(expected, actual)
    End Sub


    '''<summary>
    '''A test for ParseCallStack
    '''</summary>
    <TestMethod()> _
    Public Sub ParseFullCallStackTest()
        Dim trace As StackTrace = New StackTrace(True)
        Dim skipLines As Integer = 0
        Dim totalLines As Integer = 0
        Dim callStackType As CallStackType = callStackType.FullCallStack
        Dim expected As String = ""
        Dim actual As String
        actual = trace.ParseCallStack(skipLines, totalLines, callStackType)
        Assert.AreNotEqual(expected, actual)
    End Sub


    '''<summary>
    '''A test for MoveToFolder
    '''</summary>
    <TestMethod()> _
    Public Sub MoveToFolderTest()
        Dim fileName As String = "1206R010-K14118-039.dat"
        Dim value As FileInfo = New FileInfo(Path.Combine("\\FIG\Pub\Torrents", fileName))
        Dim folder As String = "\\FIG\Pub\Torrents\ImportFailed"
        Dim filePath As String = Path.Combine(folder, fileName)
        value.CopyTo(filePath, True)
        value.MoveToFolder(folder, True)
    End Sub

End Class
