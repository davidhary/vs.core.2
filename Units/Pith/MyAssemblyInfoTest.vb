﻿Imports isr.Core.Pith
'''<summary>
'''This is a test class for MyAssemblyInfoTest and is intended
'''to contain all MyAssemblyInfoTest Unit Tests
'''</summary>
<TestClass()> _
Public Class MyAssemblyInfoTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '''<summary> A test for Location. </summary>
    <TestMethod()> _
    Public Sub LocationTest()
        Dim target As MyAssemblyInfo = New MyAssemblyInfo(My.Application.Info)
        Dim expected As String = "C:\My\LIBRARIES\VS\Core\Core\Units\Core.Primitives.Units\bin\debug\isr.Core.Primitives.Units.dll"
        Dim actual As String
        actual = target.Location
        Assert.AreEqual(expected, actual, True)
    End Sub


    '''<summary> A test for PublicKey. </summary>
    <TestMethod()> _
    Public Sub PublicKeyTest()
        Dim target As MyAssemblyInfo = New MyAssemblyInfo(My.Application.Info)
        Dim expected As String = target.Assembly.GetName.ToString().Split(","c)(3)
        expected = expected.Substring(expected.IndexOf("=") + 1)
        Dim actual As String
        actual = target.PublicKey
        Assert.AreEqual(expected, actual, True)
    End Sub

    '''<summary> A test for Public Key Token. </summary>
    <TestMethod()> _
    Public Sub PublicKeyTokenTest()
        Dim target As MyAssemblyInfo = New MyAssemblyInfo(My.Application.Info)
        Dim expected As String = target.Assembly.GetName.ToString().Split(","c)(3)
        expected = expected.Substring(expected.IndexOf("=") + 1)
        Dim actual As String
        actual = target.PublicKeyToken
        Assert.AreEqual(expected, actual, True)
    End Sub

    ''' <summary> The build number of today. </summary>
    ''' <returns> The build number for today. </returns>
    Public Function BuildNumber() As Integer
        Return CInt(Math.Floor(Date.Now.Subtract(Date.Parse("1/1/2000")).TotalDays))
    End Function

    '''<summary> A test for FileVersionInfo. </summary>
    <TestMethod()> _
    Public Sub FileVersionInfoTest()
        Dim target As MyAssemblyInfo = New MyAssemblyInfo(My.Application.Info)
        Dim expected As String = "2.0." & BuildNumber.ToString
        Dim actual As String = target.FileVersionInfo.FileVersion
        Assert.AreEqual(expected, actual, True)
    End Sub

    '''<summary> A test for ProductVersion. </summary>
    <TestMethod()> _
    Public Sub ProductVersionTest()
        Dim target As MyAssemblyInfo = New MyAssemblyInfo(My.Application.Info)
        Dim expected As String = "2.0." & BuildNumber.ToString
        Dim actual As String
        actual = target.ProductVersion("")
        Assert.AreEqual(expected, actual, True)
    End Sub

    '''<summary> A test for ApplicationFileName. </summary>
    <TestMethod()> _
    Public Sub ApplicationFileNameTest()
        Dim target As MyAssemblyInfo = New MyAssemblyInfo(My.Application.Info)
        Dim expected As String = "isr.Core.Primitives.Units.dll"
        Dim actual As String
        actual = target.ApplicationFileName
        Assert.AreEqual(expected, actual, True)
    End Sub
End Class
