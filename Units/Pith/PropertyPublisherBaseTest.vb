﻿Imports System.Threading
Imports System.ComponentModel
Imports isr.Core.Pith.EventHandlerExtensions
'''<summary>
'''This is a test class for PropertyPublisherBaseTest and is intended
'''to contain all PropertyPublisherBaseTest Unit Tests
'''</summary>
<TestClass()> _
Public Class PropertyPublisherBaseTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    Private Class PropertyChangeWrapper
        Implements System.ComponentModel.INotifyPropertyChanged

        ''' <summary> Initializes a new instance of the <see cref="isr.Core.Pith.PropertyPublisherBase" /> class. </summary>
        Public Sub New()
            MyBase.New()
            Me.SyncContext = SynchronizationContext.Current
        End Sub

        ''' <summary> Propagates a sync context. </summary>
        ''' <value> The synchronization context. </value>
        Public Property SyncContext As SynchronizationContext

        ''' <summary> Event that is raised when a property value changes. </summary>
        Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

        Public Property TestValue As String

        Private Sub InvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            evt?.Invoke(Me, e)
        End Sub

        ''' <summary> Synchronously notifies (Invokes) a change
        ''' <see cref="PropertyChanged">event</see> in Property value. Must be called with the
        ''' <see cref="SynchronizationContext">sync context</see> </summary>
        ''' <param name="obj"> The object. </param>
        Private Sub InvokePropertyChanged(ByVal obj As Object)
            Me.InvokePropertyChanged(CType(obj, System.ComponentModel.PropertyChangedEventArgs))
        End Sub

        ''' <summary> Synchronously notifies (Invokes or Sends) a change
        ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
        ''' <param name="name"> The property name. </param>
        Public Function SendPropertyChanged(ByVal name As String) As TimeSpan
            If Not String.IsNullOrWhiteSpace(name) Then
                If Me.SyncContext Is Nothing Then
                    ' It is possible that the current thread does not have a synchronization context object; or
                    ' a synchronization context has not been set for this thread. Creating an instance of a 
                    ' synchronization context can yield unexpected results if the Current property of the 
                    ' synchronization context is checked from a thread other than the thread on which the UI 
                    ' is running, in which case the context will be null. Rather, we try to check the current
                    ' property when the event is called. 
                    Me.SyncContext = SynchronizationContext.Current
                End If
                Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
                If Me.SyncContext Is Nothing Then
                    Dim sc As New SynchronizationContext
                    sc.Send(New SendOrPostCallback(AddressOf InvokePropertyChanged), New System.ComponentModel.PropertyChangedEventArgs(name))
                Else
                    Me.SyncContext.Send(New SendOrPostCallback(AddressOf InvokePropertyChanged), New System.ComponentModel.PropertyChangedEventArgs(name))
                End If
                Timer.Stop()
                Return Timer.Elapsed
            End If
            Return TimeSpan.Zero
        End Function

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub TryInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            Try
                evt?.Invoke(Me, e)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling property '{0}';. Details: {1}.",
                             e.PropertyName, ex.Message)
            End Try
        End Sub

        ''' <summary> Synchronously notifies (Invokes) a change
        ''' <see cref="PropertyChanged">event</see> in Property value. Must be called with the
        ''' <see cref="SynchronizationContext">sync context</see> </summary>
        ''' <param name="obj"> The object. </param>
        Private Sub TryInvokePropertyChanged(ByVal obj As Object)
            Me.TryInvokePropertyChanged(CType(obj, System.ComponentModel.PropertyChangedEventArgs))
        End Sub

        ''' <summary> Synchronously notifies (Invokes or Sends) a change
        ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
        ''' <param name="name"> The property name. </param>
        Public Function TrySendPropertyChanged(ByVal name As String) As TimeSpan
            If Not String.IsNullOrWhiteSpace(name) Then
                If Me.SyncContext Is Nothing Then
                    ' It is possible that the current thread does not have a synchronization context object; or
                    ' a synchronization context has not been set for this thread. Creating an instance of a 
                    ' synchronization context can yield unexpected results if the Current property of the 
                    ' synchronization context is checked from a thread other than the thread on which the UI 
                    ' is running, in which case the context will be null. Rather, we try to check the current
                    ' property when the event is called. 
                    Me.SyncContext = SynchronizationContext.Current
                End If
                Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
                If Me.SyncContext Is Nothing Then
                    Dim sc As New SynchronizationContext
                    sc.Send(New SendOrPostCallback(AddressOf TryInvokePropertyChanged), New System.ComponentModel.PropertyChangedEventArgs(name))
                Else
                    Me.SyncContext.Send(New SendOrPostCallback(AddressOf TryInvokePropertyChanged), New System.ComponentModel.PropertyChangedEventArgs(name))
                End If
                timer.Stop()
                Return timer.Elapsed
            End If
            Return TimeSpan.Zero
        End Function

        ''' <summary> Synchronously notifies (Invokes or Sends) a change
        ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
        ''' <param name="name"> The property name. </param>
        Public Function DynamicInvokePropertyChanged(ByVal name As String) As TimeSpan
            If Not String.IsNullOrWhiteSpace(name) Then
                If Me.SyncContext Is Nothing Then
                    ' It is possible that the current thread does not have a synchronization context object; or
                    ' a synchronization context has not been set for this thread. Creating an instance of a 
                    ' synchronization context can yield unexpected results if the Current property of the 
                    ' synchronization context is checked from a thread other than the thread on which the UI 
                    ' is running, in which case the context will be null. Rather, we try to check the current
                    ' property when the event is called. 
                    Me.SyncContext = SynchronizationContext.Current
                End If
                Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
                If Me.SyncContext Is Nothing Then
                    Dim evt As PropertyChangedEventHandler = Nothing
                    evt = Me.PropertyChangedEvent
                    For Each d As [Delegate] In evt.SafeInvocationList
                        d.DynamicInvoke(New Object() {Me, New PropertyChangedEventArgs(name)})
                    Next
                Else
                    Me.SyncContext.Send(New SendOrPostCallback(AddressOf InvokePropertyChanged), New System.ComponentModel.PropertyChangedEventArgs(name))
                End If
                timer.Stop()
                Return timer.Elapsed
            End If
            Return TimeSpan.Zero
        End Function

        ''' <summary> Synchronously notifies (Invokes or Sends) a change
        ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
        ''' <param name="name"> The property name. </param>
        Public Function InvokePropertyChanged(ByVal name As String) As TimeSpan
            If Not String.IsNullOrWhiteSpace(name) Then
                If Me.SyncContext Is Nothing Then
                    ' It is possible that the current thread does not have a synchronization context object; or
                    ' a synchronization context has not been set for this thread. Creating an instance of a 
                    ' synchronization context can yield unexpected results if the Current property of the 
                    ' synchronization context is checked from a thread other than the thread on which the UI 
                    ' is running, in which case the context will be null. Rather, we try to check the current
                    ' property when the event is called. 
                    Me.SyncContext = SynchronizationContext.Current
                End If
                Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
                If Me.SyncContext Is Nothing Then
                    Dim evt As PropertyChangedEventHandler = Nothing
                    evt = Me.PropertyChangedEvent
                    ' Debug.Assert(Not Debugger.IsAttached, "This could be slow!")
                    ' begin invoke must not have more than one target. 
                    ' evt.BeginInvoke(Me, e, Nothing, Nothing)
                    evt?.Invoke(Me, New PropertyChangedEventArgs(name))

                Else
                    Me.SyncContext.Send(New SendOrPostCallback(AddressOf InvokePropertyChanged), New System.ComponentModel.PropertyChangedEventArgs(name))
                End If
                timer.Stop()
                Return timer.Elapsed
            End If
            Return TimeSpan.Zero
        End Function

        ''' <summary> Asynchronously notifies (Invokes or Sends) a change
        ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
        ''' <param name="name"> The property name. </param>
        Public Function PostPropertyChanged(ByVal name As String) As TimeSpan
            If Not String.IsNullOrWhiteSpace(name) Then
                If Me.SyncContext Is Nothing Then
                    ' It is possible that the current thread does not have a synchronization context object; or
                    ' a synchronization context has not been set for this thread. Creating an instance of a 
                    ' synchronization context can yield unexpected results if the Current property of the 
                    ' synchronization context is checked from a thread other than the thread on which the UI 
                    ' is running, in which case the context will be null. Rather, we try to check the current
                    ' property when the event is called. 
                    Me.SyncContext = SynchronizationContext.Current
                End If
                Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
                If Me.SyncContext Is Nothing Then
                    Dim sc As New SynchronizationContext
                    sc.Post(New SendOrPostCallback(AddressOf InvokePropertyChanged), New System.ComponentModel.PropertyChangedEventArgs(name))
                Else
                    Me.SyncContext.Post(New SendOrPostCallback(AddressOf InvokePropertyChanged), New System.ComponentModel.PropertyChangedEventArgs(name))
                End If
                timer.Stop()
                Return timer.Elapsed
            End If
            Return TimeSpan.Zero
        End Function

        ''' <summary> Asynchronously notifies (Invokes or Sends) a change
        ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
        ''' <param name="name"> The property name. </param>
        Public Function SaveBeginInvokePropertyChanged(ByVal name As String) As TimeSpan
            If Not String.IsNullOrWhiteSpace(name) Then
                If Me.SyncContext Is Nothing Then
                    ' It is possible that the current thread does not have a synchronization context object; or
                    ' a synchronization context has not been set for this thread. Creating an instance of a 
                    ' synchronization context can yield unexpected results if the Current property of the 
                    ' synchronization context is checked from a thread other than the thread on which the UI 
                    ' is running, in which case the context will be null. Rather, we try to check the current
                    ' property when the event is called. 
                    Me.SyncContext = SynchronizationContext.Current
                End If
                Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
                If Me.SyncContext Is Nothing Then
                    Dim evt As PropertyChangedEventHandler = Nothing
                    evt = Me.PropertyChangedEvent
#If True Then
                    For Each d As [Delegate] In evt.SafeInvocationList
                        If d.Target Is Nothing Then
                            d.DynamicInvoke(New Object() {Me, New PropertyChangedEventArgs(name)})
                        Else
                            Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                            If target Is Nothing Then
                                d.DynamicInvoke(New Object() {Me, New PropertyChangedEventArgs(name)})
                            Else
                                target.BeginInvoke(d, New Object() {Me, New PropertyChangedEventArgs(name)})
                            End If
                        End If
                    Next
#Else
                        evt?.SafeBeginInvoke(Me, New PropertyChangedEventArgs(name))
#End If
                Else
                    Me.SyncContext.Post(New SendOrPostCallback(AddressOf InvokePropertyChanged), New System.ComponentModel.PropertyChangedEventArgs(name))
                End If
                timer.Stop()
                Return timer.Elapsed
            End If
            Return TimeSpan.Zero
        End Function

        ''' <summary> Asynchronously notifies (Invokes or Sends) a change
        ''' <see cref="PropertyChanged">event</see> in Property value. </summary>
        ''' <param name="name"> The property name. </param>
        Public Function SaveInvokePropertyChanged(ByVal name As String) As TimeSpan
            If Not String.IsNullOrWhiteSpace(name) Then
                If Me.SyncContext Is Nothing Then
                    ' It is possible that the current thread does not have a synchronization context object; or
                    ' a synchronization context has not been set for this thread. Creating an instance of a 
                    ' synchronization context can yield unexpected results if the Current property of the 
                    ' synchronization context is checked from a thread other than the thread on which the UI 
                    ' is running, in which case the context will be null. Rather, we try to check the current
                    ' property when the event is called. 
                    Me.SyncContext = SynchronizationContext.Current
                End If
                Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
                If Me.SyncContext Is Nothing Then
                    ' Although the sync context is nothing, one of the target might still require invocation.
                    ' Therefore, save invoke is implemented nonetheless.
                    Dim evt As PropertyChangedEventHandler = Nothing
                    evt = Me.PropertyChangedEvent
#If True Then
                    For Each d As [Delegate] In evt.SafeInvocationList
                        If d.Target Is Nothing Then
                            d.DynamicInvoke(New Object() {Me, New PropertyChangedEventArgs(name)})
                        Else
                            Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                            If target Is Nothing Then
                                d.DynamicInvoke(New Object() {Me, New PropertyChangedEventArgs(name)})
                            Else
                                target.Invoke(d, New Object() {Me, New PropertyChangedEventArgs(name)})
                            End If
                        End If
                    Next
#Else
                        evt?.SafeInvoke(Me, New PropertyChangedEventArgs(name))
#End If
                Else
                    Me.SyncContext.Send(New SendOrPostCallback(AddressOf InvokePropertyChanged), New System.ComponentModel.PropertyChangedEventArgs(name))
                End If
                timer.Stop()
                Return timer.Elapsed
            End If
            Return TimeSpan.Zero
        End Function

        ''' <summary> Asynchronously notifies (Invokes or Sends) a change
        '''           <see cref="PropertyChanged">event</see> in Property value. </summary>
        ''' <param name="name"> The property name. </param>
        Public Function ModuleSaveBeginInvokePropertyChanged(ByVal name As String) As TimeSpan
            If Not String.IsNullOrWhiteSpace(name) Then
                If Me.SyncContext Is Nothing Then
                    ' It is possible that the current thread does not have a synchronization context object; or
                    ' a synchronization context has not been set for this thread. Creating an instance of a 
                    ' synchronization context can yield unexpected results if the Current property of the 
                    ' synchronization context is checked from a thread other than the thread on which the UI 
                    ' is running, in which case the context will be null. Rather, we try to check the current
                    ' property when the event is called. 
                    Me.SyncContext = SynchronizationContext.Current
                End If
                Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
                If Me.SyncContext Is Nothing Then
                    SafeEventHandlers.SafeBeginInvoke(Me.PropertyChangedEvent, Me, New PropertyChangedEventArgs(name))
                Else
                    Me.SyncContext.Post(New SendOrPostCallback(AddressOf InvokePropertyChanged), New System.ComponentModel.PropertyChangedEventArgs(name))
                End If
                timer.Stop()
                Return timer.Elapsed
            End If
            Return TimeSpan.Zero
        End Function

        ''' <summary> Asynchronously notifies (Invokes or Sends) a change
        '''           <see cref="PropertyChanged">event</see> in Property value. </summary>
        ''' <param name="name"> The property name. </param>
        Public Function SubSafeBeginInvokePropertyChanged(ByVal name As String) As TimeSpan
            If Not String.IsNullOrWhiteSpace(name) Then
                If Me.SyncContext Is Nothing Then
                    ' It is possible that the current thread does not have a synchronization context object; or
                    ' a synchronization context has not been set for this thread. Creating an instance of a 
                    ' synchronization context can yield unexpected results if the Current property of the 
                    ' synchronization context is checked from a thread other than the thread on which the UI 
                    ' is running, in which case the context will be null. Rather, we try to check the current
                    ' property when the event is called. 
                    Me.SyncContext = SynchronizationContext.Current
                End If
                Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
                If Me.SyncContext Is Nothing Then
#If False Then
                    ' test 1: Module
                    SafeEventHandlers.SafeBeginInvoke(Me.PropertyChangedEvent, Me, New PropertyChangedEventArgs(name))
#ElseIf True Then
                    ' test 2: Local Sub
                    SafeBeginInvoke(Me.PropertyChangedEvent, Me, New PropertyChangedEventArgs(name))
#ElseIf False Then
                    ' test 3: Local Sub. No Sender casting.
                    Me.SafeBeginInvoke(Me.PropertyChangedEvent, New PropertyChangedEventArgs(name))
#ElseIf True Then
                    ' test 4: Direct code..
                    Dim evt As PropertyChangedEventHandler = Nothing
                    evt = Me.PropertyChangedEvent
#If True Then
                        For Each d As [Delegate] In evt.SafeInvocationList
                            If d.Target Is Nothing Then
                                d.DynamicInvoke(New Object() {Me, New PropertyChangedEventArgs(name)})
                            Else
                                Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                                If target Is Nothing Then
                                    d.DynamicInvoke(New Object() {Me, New PropertyChangedEventArgs(name)})
                                Else
                                    target.Invoke(d, New Object() {Me, New PropertyChangedEventArgs(name)})
                                End If
                            End If
                        Next
#Else
                        evt?.SafeInvoke(Me, New PropertyChangedEventArgs(name))
#End If
                    End If
#End If
                End If
                Me.SyncContext.Post(New SendOrPostCallback(AddressOf InvokePropertyChanged), New System.ComponentModel.PropertyChangedEventArgs(name))
                    timer.Stop()
                    Return timer.Elapsed
            End If
            Return TimeSpan.Zero
        End Function

        ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
        ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
        ''' <see cref="PropertyChangedEventHandler">Event</see> in a property value. </summary>
        ''' <param name="handler"> The event handler. </param>
        ''' <param name="sender">  The sender of the event. </param>
        ''' <param name="e">       The <see cref="PropertyChangedEventArgs" /> instance containing the
        ''' event data. </param>
        Private Shared Sub SafeBeginInvoke(ByVal handler As PropertyChangedEventHandler, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            For Each d As [Delegate] In handler.SafeInvocationList
                If d.Target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, e})
                Else
                    Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If target Is Nothing Then
                        d.DynamicInvoke(New Object() {sender, e})
                    Else
                        target.BeginInvoke(d, New Object() {sender, e})
                    End If
                End If
            Next
        End Sub

        ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
        ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
        ''' <see cref="PropertyChangedEventHandler">Event</see> in a property value. </summary>
        ''' <param name="handler"> The event handler. </param>
        ''' <param name="e">       The <see cref="PropertyChangedEventArgs" /> instance containing the
        ''' event data. </param>
        Private Sub SafeBeginInvoke(ByVal handler As PropertyChangedEventHandler, ByVal e As PropertyChangedEventArgs)
            For Each d As [Delegate] In handler.SafeInvocationList
                If d.Target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, e})
                Else
                    Dim target As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    If target Is Nothing Then
                        d.DynamicInvoke(New Object() {Me, e})
                    Else
                        target.BeginInvoke(d, New Object() {Me, e})
                    End If
                End If
            Next
        End Sub

    End Class

    Private WithEvents testClass As PropertyChangeWrapper
    '''<summary> A test for Send Property Changed. </summary>
    ''' <remarks> 0.152 ms </remarks>
    <TestMethod()> _
    Public Sub SendPropertyChangedTest()
        Dim expectedName As String = "TestValue"
        Dim expectedValue As String = "Sync"
        expectedPropertyName = expectedName
        expectedPropertyValue = expectedValue
        testClass = New PropertyChangeWrapper
        testClass.TestValue = expectedValue
        Dim ts As TimeSpan = testClass.SendPropertyChanged(expectedName)
        Assert.AreEqual(testClass.SyncContext, Nothing)
        Assert.AreEqual(9.1, ts.TotalMilliseconds)
    End Sub

    '''<summary> A test for Try Send Property Changed (handles exceptions). </summary>
    ''' <remarks> 0.25 ms </remarks>
    <TestMethod()> _
    Public Sub TrySendPropertyChangedTest()
        Dim expectedName As String = "TestValue"
        Dim expectedValue As String = "Sync"
        expectedPropertyName = expectedName
        expectedPropertyValue = expectedValue
        testClass = New PropertyChangeWrapper
        testClass.TestValue = expectedValue
        Dim ts As TimeSpan = testClass.TrySendPropertyChanged(expectedName)
        Assert.AreEqual(testClass.SyncContext, Nothing)
        Assert.AreEqual(9.1, ts.TotalMilliseconds)
    End Sub

    '''<summary> A test for Dynamic Invoke. </summary>
    ''' <remarks> 0.045 ms </remarks>
    <TestMethod()> _
    Public Sub DynamicInvokePropertyChangedTest()
        Dim expectedName As String = "TestValue"
        Dim expectedValue As String = "Sync"
        expectedPropertyName = expectedName
        expectedPropertyValue = expectedValue
        testClass = New PropertyChangeWrapper
        testClass.TestValue = expectedValue
        Dim ts As TimeSpan = testClass.DynamicInvokePropertyChanged(expectedName)
        Assert.AreEqual(testClass.SyncContext, Nothing)
        Assert.AreEqual(9.1, ts.TotalMilliseconds)
    End Sub

    '''<summary> A test for Invoke Property Changed. </summary>
    ''' <remarks> 0.037 ms </remarks>
    <TestMethod()> _
    Public Sub InvokePropertyChangedTest()
        Dim expectedName As String = "TestValue"
        Dim expectedValue As String = "Sync"
        expectedPropertyName = expectedName
        expectedPropertyValue = expectedValue
        testClass = New PropertyChangeWrapper
        testClass.TestValue = expectedValue
        Dim ts As TimeSpan = testClass.InvokePropertyChanged(expectedName)
        Assert.AreEqual(testClass.SyncContext, Nothing)
        Assert.AreEqual(9.1, ts.TotalMilliseconds)
    End Sub

    '''<summary> A test for Post Property Changed. </summary>
    ''' <remarks> 0.035 ms </remarks>
    <TestMethod()> _
    Public Sub PostPropertyChangedTest()
        Dim expectedName As String = "TestValue"
        Dim expectedValue As String = "Async"
        expectedPropertyName = expectedName
        expectedPropertyValue = expectedValue
        testClass = New PropertyChangeWrapper
        testClass.TestValue = expectedValue
        Dim ts As TimeSpan = testClass.PostPropertyChanged(expectedName)
        Assert.AreEqual(testClass.SyncContext, Nothing)
        Assert.AreEqual(9.1, ts.TotalMilliseconds)
    End Sub

    '''<summary> A test for Safe Begin Invoke Property Changed. </summary>
    ''' <remarks> 0.044 ms , 0.3 using extensions, o.32 using module. </remarks>
    <TestMethod()> _
    Public Sub SafeBeginInvokePropertyChangedTest()
        Dim expectedName As String = "TestValue"
        Dim expectedValue As String = "Async"
        expectedPropertyName = expectedName
        expectedPropertyValue = expectedValue
        testClass = New PropertyChangeWrapper
        testClass.TestValue = expectedValue
        Dim ts As TimeSpan = testClass.SaveBeginInvokePropertyChanged(expectedName)
        Assert.AreEqual(testClass.SyncContext, Nothing)
        Assert.AreEqual(9.1, ts.TotalMilliseconds)
    End Sub

    '''<summary> A test for Module Safe Begin Invoke Property Changed. </summary>
    ''' <remarks> 1) Module Sub: 0.29-0.33;  
    '''           2) Local Sub:  0.29-0.33; Shared: Same timing.;  
    '''           3) Local no sender:  0.25-0.26;    
    '''           4) internal: 0.046-0.045
    ''' </remarks>
    <TestMethod()> _
    Public Sub SubSafeBeginInvokePropertyChangedTest()
        Dim expectedName As String = "TestValue"
        Dim expectedValue As String = "Async"
        expectedPropertyName = expectedName
        expectedPropertyValue = expectedValue
        testClass = New PropertyChangeWrapper
        testClass.TestValue = expectedValue
        Dim ts As TimeSpan = testClass.SubSafeBeginInvokePropertyChanged(expectedName)
        Assert.AreEqual(testClass.SyncContext, Nothing)
        Assert.AreEqual(9.1, ts.TotalMilliseconds)
    End Sub

    '''<summary> A test for Safe Begin Invoke Property Changed. </summary>
    ''' <remarks> 0.048 ms , 0.38 using extensions! </remarks>
    <TestMethod()> _
    Public Sub SafeInvokePropertyChangedTest()
        Dim expectedName As String = "TestValue"
        Dim expectedValue As String = "Async"
        expectedPropertyName = expectedName
        expectedPropertyValue = expectedValue
        testClass = New PropertyChangeWrapper
        testClass.TestValue = expectedValue
        Dim ts As TimeSpan = testClass.SaveInvokePropertyChanged(expectedName)
        Assert.AreEqual(testClass.SyncContext, Nothing)
        Assert.AreEqual(9.1, ts.TotalMilliseconds)
    End Sub

    Private expectedPropertyName As String
    Private expectedPropertyValue As String
    Private Sub testClass_PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Handles testClass.PropertyChanged
#If False Then
        Try
            If sender IsNot Nothing AndAlso e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
                Assert.AreEqual(expectedPropertyName, e.PropertyName)
                Assert.AreEqual(expectedPropertyValue, Me.testClass.TestValue)
            End If
        Catch ex As Exception
            ' a failed assertion causes an exception.
            Debug.Assert(Debugger.IsAttached, ex.ToString)
        End Try
#End If
    End Sub

End Class
