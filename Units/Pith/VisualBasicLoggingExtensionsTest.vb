﻿Imports isr.Core.Pith
Imports isr.Core.Pith.VisualBasicLoggingExtensions
'''<summary>
'''This is a test class for VisualBasicLoggingExtensionsTest and is intended
'''to contain all VisualBasicLoggingExtensionsTest Unit Tests
'''</summary>
<TestClass()> _
Public Class VisualBasicLoggingExtensionsTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '''<summary>
    '''A test for ReplaceDefaultTraceListener with trace event.
    '''</summary>
    Public Sub TraceEventLogTest(ByVal expected As TraceEventType)

        Dim actual As TraceEventType
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")

        Dim value As Logging.FileLogTraceListener = Nothing
        value = My.Application.Log.ReplaceDefaultTraceListener(UserLevel.AllUsers)
        If Not My.Application.Log.DefaultFileLogWriterFileExists Then
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Critical, 1, "trace event into {0}", My.Application.Log.DefaultFileLogWriterFilePath)
        End If
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Verbose, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Information, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Warning, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Critical, 1, "setting trace level to {0}", expected)
        My.Application.Log.ApplyTraceLevel(TraceEventType.Information)
        actual = My.Application.Log.TraceLevel
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Verbose, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Information, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Warning, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, 1, "trace event level {0}", My.Application.Log.TraceLevel)

    End Sub

    '''<summary>
    '''A test for ReplaceDefaultTraceListener with logging log.
    '''</summary>
    Public Sub LoggingLogTest(ByVal expected As TraceEventType)

        Dim actual As TraceEventType
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")

        Dim value As Logging.FileLogTraceListener = Nothing
        value = My.Application.Log.ReplaceDefaultTraceListener(UserLevel.AllUsers)
        If Not My.Application.Log.DefaultFileLogWriterFileExists Then
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Critical, 1, "trace event into {0}", My.Application.Log.DefaultFileLogWriterFilePath)
        End If
        My.Application.Log.WriteLogEntry(TraceEventType.Critical, "Tracing into {0}", value.FullLogFileName)
        My.Application.Log.WriteLogEntry(TraceEventType.Verbose, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Information, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Warning, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Error, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Critical, "setting trace level to {0}", expected)
        actual = My.Application.Log.TraceLevel
        My.Application.Log.WriteLogEntry(TraceEventType.Verbose, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Information, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Warning, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Error, "trace level {0}", My.Application.Log.TraceLevel)

    End Sub

    '''<summary>
    '''A test for ReplaceDefaultTraceListener with trace event.
    '''</summary>
    <TestMethod()> _
    Public Sub TraceEventLogTestInformation()
        Me.TraceEventLogTest(TraceEventType.Information)
    End Sub

    '''<summary>
    '''A test for ReplaceDefaultTraceListener with logging log.
    '''</summary>
    <TestMethod()> _
    Public Sub LoggingLogTestInformation()
        Me.LoggingLogTest(TraceEventType.Information)
    End Sub

    ''' <summary> Save the trace event identities. </summary>
    Public Sub SaveTraceEventIdentities(ByVal filename As String, ByVal openMode As OpenMode,
                                             ByVal values As IEnumerable(Of KeyValuePair(Of String, Integer)))
        Dim fileNo As Integer = Microsoft.VisualBasic.FreeFile()
        If openMode = OpenMode.Output AndAlso System.IO.File.Exists(filename) Then
            System.IO.File.Delete(filename)
        End If
        Microsoft.VisualBasic.FileOpen(fileNo, filename, openMode)

        For Each value As KeyValuePair(Of String, Integer) In values
            Microsoft.VisualBasic.WriteLine(fileNo, CInt(value.Value).ToString("X"), value.Value.ToString, value.Key)
        Next
        Microsoft.VisualBasic.FileClose(fileNo)
    End Sub

    ''' <summary> (Unit Test Method) enumerates test event identities. </summary>
    <TestMethod()>
    Public Sub EnumerateTestEventIdentities()
        Dim filePath As String = "C:\My\LIBRARIES\VS\Core\Core\Pith\SHARED"
        Dim fileName As String = System.IO.Path.Combine(filePath, "TraceEventIds.txt")
        Dim values As New List(Of KeyValuePair(Of String, Integer))
        For Each value As Global.isr.Core.Pith.My.ProjectTraceEventId In [Enum].GetValues(GetType(Global.isr.Core.Pith.My.ProjectTraceEventId))
            values.Add(New KeyValuePair(Of String, Integer)(isr.Core.Pith.EnumExtensions.Description(value), value))
        Next
        Me.SaveTraceEventIdentities(fileName, OpenMode.Output, values)
    End Sub


End Class
