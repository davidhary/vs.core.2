﻿Imports isr.Core.Pith
'''<summary>
'''This is a test class for TraceMessagesBoxTest and is intended
'''to contain all TraceMessagesBoxTest Unit Tests
'''</summary>
<TestClass()> _
Public Class TraceMessagesBoxTest

    Private testContextInstance As TestContext
    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for AddMessage
    '''</summary>
    <TestMethod()> _
    Public Sub AddMessageTest()
        Dim target As TraceMessagesBox = New TraceMessagesBox() ' TODO: Initialize to an appropriate value
        Dim value As TraceMessage = New TraceMessage(TraceEventType.Information, 1, "Message {0}", DateTime.Now)
        target.AddMessage(value)
    End Sub
End Class
